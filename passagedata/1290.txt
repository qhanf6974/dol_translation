<<effects>>

<<if $submissive gte 1150>>
	"St-stop," you say. "That tickles."
<<elseif $submissive lte 850>>
	"Fuck off or you're getting a broken nose," you say.
<<else>>
	"Cut it out," you say.
<</if>>
<br><br>

<<if $tending gte random(400, 1200)>>
	<<if $steed is "male" or $steed is "female">>
		"Sorry," the <<steed_text>> says. <span class="green">It backs a way from you.</span>
	<<else>>
		The <<steed_text>> responds to something in your voice, <span class="green">and backs away from you.</span>
	<</if>>
	<br><br>
	You keep pushing against the wall until, at last, your hips slide through. You climb to your feet.
	<br><br>

	<<link [[Next|Riding School Lesson Building]]>><</link>>
	<br>

<<else>>
	<span class="red">The <<steed_text>> ignores you.</span> <<steed_He>> continues the assault.
	<br><br>

	<<link [[Next|Riding School Lesson Endure]]>><</link>>
	<br>
<</if>>