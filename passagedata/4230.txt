<<set $outside to 0>><<set $location to "home">><<effects>>

<<npc Whitney>><<generatey2>><<generatey3>><<generatey4>><<generatey5>><<if $halloween_trick_NPC>><<loadNPC 5 trick_or_treat>><<else>><<generatey6>><</if>>

Whitney rings the doorbell. <<person1>><<His>> friends crowd around this time.
<<if $halloween_trick_NPC is "oral">>
The <<person6>><<person>> from earlier answers it. "Back for more?" <<he>> smirks when <<he>> sees you. "And you've brought friends to help."
<br><br>

"More what?" Whitney looks at you. "What's <<he>> talking about?"
<br><br>

"This little slut sells <<pher>> body for sweets," the <<person>> replies. "I got my money's worth."
<br><br>

Whitney stares a moment. Then swings <<person1>><<his>> bat <span class="red">into the <<person6>><<persons>> stomach.</span> <<He>> keels over, and Whitney <span class="red">delivers a second blow to <<his>> back.</span> Whitney's friends join in the pummelling, whacking <<him>> while <<he>> struggles to deflect the blows with <<his>> arms. Others move into the house. You hear a shatter.
<br><br>

Once back out on the street Whitney carries a bag full of stolen booze. "We got a treat for once," <<person1>><<he>> laughs.
<br><br>

<<link [[Next|Whitney Trick 6]]>><<endevent>><<pass 30>><</link>>
<br>

<<elseif $halloween_trick_NPC is "refused">>

The <<person6>><<person>> from earlier answers it. "Changed your mind?" <<he>> smirks when <<he>> sees you. "And you've brought friends to help."
<br><br>

"Changed <<pher>> mind?" Whitney looks at you. "What's <<he>> talking about?"
<br><br>

"This little slut wanted sweets, but refused to pay the price," the <<person>> replies, leering at you. "I don't mind an audience if that's what it takes."
<br><br>

Whitney stares a moment. Then swings <<person1>><<his>> bat <span class="red">into the <<person6>><<persons>> stomach.</span> <<He>> keels over, and Whitney <span class="red">delivers a second blow to <<his>> back.</span> Whitney's friends join in the pummelling, screeching and whacking <<him>> while <<he>> struggles to deflect the blows with <<his>> arms. Others move into the house. You hear a shatter.
<br><br>

Once back out on the street Whitney carries a bag full of stolen booze. "We got a treat for once," <<person1>><<he>> laughs. <<He>> hugs you with one arm. "Imagine trying to sleep with my <<girl>> without my permission."
<br><br>

<<link [[Next|Whitney Trick 6]]>><<endevent>><<pass 30>><</link>>
<br>

<<else>>
The <<person6>><<person>> from earlier answers it. "What you want?"
<br><br>

"Trick or treat!" Whitney and <<person1>><<his>> friends shout, stroking their cricket bats.
<br><br>

The <<person6>><<person>> throws up <<his>> hands. "Alright. I got some sweets here-"
<br><br>

"Fuck your sweets," Whitney interrupts. "We want cash. Or drinks."
<br><br>
	<<if $exposed gte 2 and $uncomfortable.nude is false or $exposed gte 2 and $leftarm is "bound" and $rightarm is "bound">>
	The <<person>> stares at your uncovered <<lewdnessstop>> <<He>> nods. "Sure. I got booze." <<He>> disappears a moment, then returns with a clinking bag in one hand, and <<his>> phone in the other. <<He>> takes a picture of you.
	<<garousal>><<arousal 600>><<fameexhibitionism 10>>
	<br><br>

	"We got a treat for once," Whitney says once back on the road.
	<br><br>

	<<link [[Next|Whitney Trick 6]]>><<endevent>><<pass 30>><</link>>
	<br>

	<<else>>
	The <<person>> thinks a moment, then smirks. "I got booze. But I want a little something in return." <<He>> nods at you. "Show me <<pher>> body."
	<br><br>

	<<link [[Next|Whitney Trick Exhibitionism]]>><</link>>
	<</if>>

<</if>>