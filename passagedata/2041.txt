<<effects>>

<<if $tending gte random(1, 1000)>>

	You sing a gentle hum. The shoots sway to its tune, <span class="green">becoming looser,</span> allowing you to wriggle free.
	<br><br>
	
	<<link [[Next|Garden Flowers]]>><</link>>
	<br>

<<else>>

	You sing a gentle hum. <span class="red">The shoots don't respond,</span> and continue to tighten around you.
	<br><br>
	
	<<link [[Next|Garden Stem Tentacles]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>