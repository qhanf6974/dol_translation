<<effects>>

You lean down and offer the <<person>> your arm. <<He>> takes it, and you help <<him>> to <<his>> feet. "Th-thank you," <<he>> manages, glancing at the cream slipping down your body before looking away with a blush. "I'd, ah, best be on my way."
<<famegood 10>><<cream_damage 15>>
<br><br>

<<endevent>>

<<cream_walk>>