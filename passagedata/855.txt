<<effects>>

<<if $submissive gte 1150>>
	"I-I'm still scared," you admit.
<<elseif $submissive lte 850>>
	"Only a fool wouldn't have been on edge," you say.
<<else>>
	"I was," you say. "Weren't you?"
<</if>>
<br><br>

Alex puffs out <<his>> chest. "I wasn't scared in the slightest. Come on, let's get back to work."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>