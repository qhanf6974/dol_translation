<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>"I wasn't sure about this one," Winter says. "Had to phone an old colleague, but it's genuine. Ancient, and engraved with symbols of the old religion."
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>
<br>