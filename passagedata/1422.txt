<<set $outside to 1>><<set $location to "beach">><<effects>>
The teenagers are happy to have a sixth player, particularly the <<person1>><<personstop>>
<br><br>
<<physique 3>>
<<set $eventcheck to random(1, 10000)>>
<<if $eventcheck gte (9900 - ($allure))>>
	<<if $rng gte 51>><<molested>>
		<<if $worn.lower.type.includes("naked")>>
			You are so focused on the opposing team you do not notice one of your own sneak up on you. A pair of hands grasp the rim of your $worn.under_lower.name.
		<<else>>
			You are so focused on the opposing team you do not notice one of your own sneak up on you. A pair of hands grasp the rim of your $worn.lower.name.
		<</if>>
		<<if $worn.lower.skirt is 0 and $worn.lower.set isnot $worn.upper.set>>
			Before you can react, the <<person1>><<person>> has pulled them all the way to your knees,
		<<elseif $worn.lower.skirt is 0 and $worn.lower.set is $worn.upper.set>>
			Before you can react, the <<person1>><<person>> has pulled it aside,
		<<else>>
			Before you can react, the <<person1>><<person>> has lifted it,
		<</if>>
		<<if $worn.under_lower.name is "naked" or $worn.lower.name is "naked">>
			<<if $uncomfortable.nude is false>>
				exposing your <<genitals>> for all to see. Laughter and lewd gestures erupt from both teams, yet you feel a thrill.
				<br><br>
				You fix your clothes, taking a bit longer than you need to.
				<<garousal>><<arousal 300>>
			<<else>>
				exposing your <<genitals>> for all to see. Laughter and lewd gestures erupt from both teams as your face turns red and you hasten to protect your dignity.
				<<garousal>><<gstress>><<gtrauma>><<arousal 300>><<stress 3>><<trauma 3>>
			<</if>>
		<<else>>
			<<if $uncomfortable.underwear is false>>
				exposing your $worn.under_lower.name. Laughter erupts from both teams, including you. It's just a $worn.under_lower.name, you're not embarrassed.
				<<lcool>><<status +5>><<garousal>><<gstress>>
			<<else>>
				exposing your $worn.under_lower.name. Laughter erupts from both teams and you hasten to protect your dignity.
				<<lcool>><<status -10>><<garousal>><<gstress>><<gtrauma>><<arousal 100>><<stress 1>><<trauma 1>>
			<</if>>
		<</if>>
		<br><br>
	<<else>>
		<!-- if RNG is lower -->
		<<if $topless is 1>>
			Part-way through the match the ball collides with your bare chest, sending you sprawling.
			<<if $breastsize gte 2>>
				<br>
				Your <<breasts>> hurt a little from the bare skin impact.
				<<pain 1>><<gpain>>
				<br>
			<</if>>
			It's embarrassing, but you soon dust yourself off.
			<br><br>
		<<else>>
			Part-way through the match the ball collides with your chest, sending you sprawling.
			<<set $worn.upper.integrity -= 1>><<set $worn.under_upper.integrity -= 1>><<integritycheck>><<exposure>>
			<<if $player.gender_appearance is "m" or !$worn.upper.type.includes("naked") or !$worn.under_upper.type.includes("naked")>>
				It's embarrassing, but you soon dust yourself off.
				<br><br>
			<<else>>
				<<if $uncomfortable.nude is false>>
					Worse, your clothes fall to tatters, baring your chest. Laughter and lewd gestures erupt from both teams, yet you feel a thrill.
					<<garousal>><<arousal 300>><<towelup>>
				<<else>>
					Worse, your clothes fall to tatters, baring your chest. Laughter and lewd gestures erupt from both teams as your face turns red and you hasten to protect your dignity with a towel.
					<<garousal>><<gstress>><<gtrauma>><<arousal 300>><<stress 3>><<trauma 3>><<towelup>>
				<</if>>
				<br><br>
			<</if>>
		<</if>>
		<!-- end of RNG -->
	<</if>>
<<else>>
	You have a good time.
	<br><br>
<</if>>
<<if $daystate is "night">>
	With the sun fully set, continuing to play would be difficult. The <<person1>><<person>> tells you they are going to a beach party and invites you to come along.
	<br><br>
	<<link [[Accept (0:10)|Beach Party Chat]]>><<pass 10>><<endevent>><<lstress>><<status 1>><</link>><<gcool>><<lstress>>
	<br>
	<<link [[Make excuses and say your goodbyes|Beach]]>><<endevent>><</link>>
	<br><br>
<<else>>
	<<link [[Play more (0:30)|Beach Volleyball Play]]>><<pass 30>><<stress -6>><<tiredness 6>><<status 1>><<athletics 3>><</link>><<gcool>><<gathletics>><<lstress>><<gtiredness>>
	<br>
	<<link [[Stop|Beach]]>><<endevent>><</link>>
	<br><br>
<</if>>