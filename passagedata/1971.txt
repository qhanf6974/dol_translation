<<effects>>

Bailey snatches the money from your hand and starts counting. "Good," <<he>> says, satisfied. <<He>> turns to the <<person2>><<personstop>> "Next week I want £300." <<person2>>The <<person>> nods.
<br><br>

"Thank you," <<he>> says once Bailey is out of earshot. "I'm having trouble finding work. Everywhere's full of creeps." <<He>> laughs. "I don't want to know what Bailey had in store for me. I just hoped it wouldn't be too bad."
<br><br>

"I'll make this up to you," <<he>> continues. "I promise."
<br><br>
<<set $orphan_rent to "paid">>
<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>