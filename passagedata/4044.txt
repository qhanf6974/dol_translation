<<set $outside to 0>><<set $location to "town">><<effects>>

"Remember," Robin announces in a hushed tone once outside the orphanage. "Don't let Bailey know. <<endevent>><<npc Bailey>><<person1>><<Hes>> busy today, but be careful anyway."
<br><br>
<<endevent>><<npc Robin>><<person1>>
You walk with Robin to <<his>> room. <<if $halloween_robin_costume is "ghost">><<He>> pulls the sheet off <<his>> head.<<else>><<Hes>> still dressed as a $halloween_robin_costume.<</if>> <<He>> looks tired, but smiles at you. "Thank you for coming," <<he>> says. "It made the orphans easier to manage."
<br><br>

<<link [[Hug (0:05)|Robin Trick Hug]]>><<npcincr Robin love 1>><<pass 5>><</link>><<glove>>
<br>
<<if $robinromance is 1>>
<<link [[Kiss|Robin Trick Kiss]]>><<set $sexstart to 1>><<npcincr Robin love 1>><</link>><<promiscuous1>><<glove>>
<br>
<</if>>
<<link [[Talk (0:05)|Robin Trick Talk]]>><<npcincr Robin love 1>><<pass 5>><</link>><<glove>>
<br>