<<effects>>

<<person1>>You rush to Bailey's office while shouting <<his>> name.

<<if $rng gte 51>>
You arrive at the main hall just as <<he>> enters through the front doors.
<br><br>
<<else>>
You arrive at the main hall to find <<him>> leaving <<his>> office.
<br><br>
<</if>>

"I'm busy," <<he>> says before you say a word.
<br><br>

<<if $submissive gte 1150>>
"I'm sorry," you say. "But someone needs your help."
<<elseif $submissive lte 850>>
"You let an intruder slip by," you say. "They're over there, assaulting a resident."
<<else>>
"But someone broke in," you say. "And they're attacking someone."
<</if>>
You hear another scream.
<br><br>

"Fine," <<he>> says.
<br><br>

<<He>> follows you to the <<person3>><<persons>> room. Without breaking stride <<person1>><<he>> grasps the <<person2>><<person>> by the collar and waist, and hauls <<him>> off <<his>> victim.
<br><br>

The <<person2>><<person>> rolls into the hallway. "Do you know-" <<he>> begins, but Bailey crouches beside <<him>> and grasps <<his>> throat.
<br><br>

Bailey's lips move, but you can't make out the words. The <<person>> turns pale. <<He>> staggers to <<his>> feet as soon as Bailey releases <<himcomma>> and runs.
<br><br>
<<endevent>>

<<link [[Next|Orphanage]]>><</link>>
<br>