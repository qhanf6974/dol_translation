<<set $outside to 0>><<set $location to "temple">><<effects>>
River pierces you with <<his>> blue eyes. "I didn't expect to see you here," <<he>> says, stirring the pot of soup. "Don't cause any trouble."
<br><br>
<<if $submissive gte 1150>>
	"I won't," you say. "I heard you needed help."
<<elseif $submissive lte 850>>
	"Don't talk down to me," you say. "We're not in school. I'm here to help."
<<else>>
	"I heard you needed help," you say.
<</if>>
<br><br>
<<He>> regards you for a moment, then returns <<his>> attention to the soup. "If you want to make yourself useful, start cutting vegetables."
<br><br>
<<endevent>>
<<link [[Next|Soup Kitchen]]>><</link>>
<br>