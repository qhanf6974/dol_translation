<<effects>>

You ride after the <<person>>, who seems to have already forgotten about you. You get close enough to strike.
<br><br>

<<set $skulduggerydifficulty to 300>>
<<link [[Strip|Riding School Lesson Strip]]>><</link>><<skulduggerydifficulty>>
<br>
<<link [[Shove|Riding School Lesson Shove]]>><</link>><<physiquedifficulty 1 12000>>
<br>