<<effects>>

"What's wrong?" you ask. The <<person>> looks at you.
<br><br>

"I want to go home," <<he>> says.
<br><br>

You spend the next hour listening as the <<person>> shares <<his>> fears, offering comforting words when appropriate. <<He>> misses <<his>> parents, is afraid of Bailey, is afraid to go outside, and hates the food. Despite <<his>> distress, <<he>> perks up the longer <<he>> speaks.
<br><br>

"Thank you," <<he>> says. "I feel much better. I'll try to be brave like you." <<His>> cheeks are still wet, but <<hes>> smiling.
<br><br>

<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>