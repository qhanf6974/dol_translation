<<if $molestationstart is 1>>
	<<set $molestationstart to 0>>
	<<controlloss>>
	<<violence 1>>
	<<neutral 1>>
	<<molested>>

	<<set $rescue to 1>>
	<<maninit>>
	<<set $NPCList[0].chest to "none">>
	<<He>> reaches toward you.
<</if>>
<<effects>>
<<effectsman>><<man>>
<<stateman>>
<br><br>

__Combat Tutorial__
<br>

<<if $gamemode is "soft">>
	<i>Your character will be quite passive at first. More actions will become available as you become more promiscuous and comfortable being assertive.
	<br><br>
	<span class="sub">Assertive</span> and <span class="meek">meek</span> actions will arouse your partner, bringing them closer to orgasm. They'll also make them trust you more.
	<br><br>
	<span class="brat">Bratty</span> acts will irritate your partner. Use them too frequently and they will become rougher and less trusting.
	<br><br>
	Some actions have only a chance of working, based on your skill with the bodypart being used and how trusting your partner is.
	<br><br>
	Each part of your body can perform one action per turn. Choose your actions, then click next or press enter to continue.</i>
	<br><br>
<<else>>
	<i>There are three common ways to escape an attacker; fight them off, sexually satisfy them, or be rescued.
	<span class="def">Defiant</span> acts will hurt them. Hurt them enough and you'll escape. However, <span class="def">defiant</span> acts will anger them, making them more violent.
	<br><br>
	They'll be happy to use you as a passive toy, but <span class="sub">submissive</span> acts will make them cum faster. Once spent, they'll usually leave you alone. Some <span class="sub">submissive</span> acts will occupy their genitals, so they can't use them in more dangerous ways.
	<br><br>
	<span class="meek">Meek</span> acts will endear you to them without being directly sexual. <span class="meek">Meek</span> acts will make them trust you more, and often have effects which may help you.
	<br><br>
	<span class="brat">Bratty</span> acts protect you in ways that defies your attacker's will without hurting them. <span class="brat">Bratty</span> acts will reduce trust and increase anger.
	<br><br>
	Finally, you could scream for help. Whether it will help or make things worse depends on who's around to hear. Screaming at night or in the wilderness will only anger your attacker. You won't be able to scream (or speak) if your attacker has you gagged.
	For this encounter, screaming will get you rescued. Each part of your body can perform one action per turn. Choose your actions, then click next or press enter to continue.</i>
	<br><br>
<</if>>
<br><br>
<<actionsman>>
<<if $alarm is 1 and $rescue is 1>>
	<span id="next"><<link [[Next|Tutorial Finish]]>><</link>></span><<nexttext>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<span id="next"><<link [[Next|Tutorial Finish]]>><</link>></span><<nexttext>>
<<elseif $enemyhealth lte 0>>
	<span id="next"><<link [[Next|Tutorial Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link [[Next|Tutorial]]>><</link>></span><<nexttext>>
<</if>>