<<effects>>

You stop brushing fur and approach the old horse. It snorts at you.
<br><br>

<<if $submissive gte 1150>>
	"S-sorry to bother you," you say. "But I think I can help."
<<elseif $submissive lte 850>>
	"Quit whinging," you say. "Do you want to get better or not?"
<<else>>
	"Please don't be frightened," you say. "I can help."
<</if>>

<<if $tending gte random(700, 1000)>>
	You crouch near its wounded leg. <span class="green">It doesn't stop you.</span>
	<br><br>

	You examine the wound. The splinter's in there deep, but it looks like a single, large piece. There's no bleeding. Not right now.
	<br><br>

	You clutch it firm, and pull with a single, sharp movement.
	<br><br>

	<<link [[Next|Livestock Field Horse Help 2]]>><</link>>
	<br>

<<else>>
	You crouch near its wounded leg. <span class="red">It rears up, forelegs kicking at you.</span> Another horse runs in the way, protecting you from a clobbering.
	<<gstress>><<stress 6>>
	<br><br>

	The newcomer snorts at you as well, as if in warning.
	<br><br>

	<<link [[Next|Livestock Field]]>><<endevent>><</link>>
	<br>
<</if>>