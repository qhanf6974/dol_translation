<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
You rush forward, and start skidding down an almost sheer drop. "Stupid <<girlcomma>>" you hear the <<person1>><<monk>> shout above you. Your skid becomes a tumble, down through an unknown darkness.
<br><br>
<<endevent>>
<<link [[Next|Temple Underground Escape]]>><</link>>
<br>