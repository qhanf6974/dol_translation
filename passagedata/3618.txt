<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>

<<if $temple_rank is "prospective">>
	<<if $temple_fire is 1>>
		"I'm afraid you can only attempt a trial of purity once per week," <<he>> says.
	<<else>>
		"I'll prepare the trial when you're ready," <<he>> says.
	<</if>>
	<<if $templetest isnot "failed">>
		"Unless you'd prefer to prove your purity in a different way?"
	<</if>>
	<br><br>
	<<if $temple_fire isnot 1>>
	<<link [[Trial by Fire (1:00)|Temple Join Fire]]>><<set $phase to 0>><<pass 60>><<set $temple_fire to 1>><</link>>
	<br>
	<</if>>
	<<if $templetest isnot "failed">>
		<<link [[Virginity Test|Temple Test]]>><<set $phase to 1>><</link>>
		<br>
	<</if>>
	<<link [[Leave|Temple]]>><<endevent>><</link>>
	<br>
<<else>>
	<<if $templetest isnot "failed">>
		<<if $famegood gte 0>>
			Jordan observes you, unblinking. "Admission to the <<if $player.gender_appearance is "m">>brotherhood<<else>>sisterhood<</if>> is restricted to virgins, and those who have made amends via the trial of purity.
			<br><br>
			<<link [[Virginity Test|Temple Test]]>><<set $phase to 1>><</link>>
			<br>
			<<link [[Ask about Trial|Temple Trial Intro]]>><</link>>
			<br>
			<<link [[Leave|Temple]]>><<endevent>><</link>>
			<br>
		<<else>>
			Jordan observes you, unblinking. "Admission to the <<if $player.gender_appearance is "m">>brotherhood<<else>>sisterhood<</if>> is restricted to virgins, and those who have made amends via the trial of purity."
			<br><br>
			"You also need a modest reputation for kindness. We do important work, and it requires people trust us. If you pray here you'll often see someone going round with a collection plate. Giving to charity would be a good way to prove yourself."
			<br><br>
			"Return when you're better known and we'll discuss the next step. Thank you for your interest." <<He>> turns back to the altar.
			<br><br>
			<<endevent>>
			<<link [[Next|Temple]]>><</link>>
			<br>
		<</if>>
	<<else>>
		"I'm sorry child," Jordan says. "Admission to the <<if $player.gender_appearance is "m">>brotherhood<<else>>sisterhood<</if>> is restricted to virgins, and those who have made amends via the trial of purity."
		<br><br>
			<<if $famegood gte 0>>
				<<link [[Ask about Trial|Temple Trial Intro]]>><</link>>
				<br>
				<<link [[Leave|Temple]]>><<endevent>><</link>>
				<br>
			<<else>>
				"You also need a modest reputation for kindness. We do important work, and it requires people trust us. If you pray here you'll often see someone going round with a collection plate. Giving to charity would be a good way to prove yourself."
			<br><br>
				<br><br>
				"Return when you're better known and we'll discuss the next step. Thank you for your interest." <<He>> turns back to the altar.
				<br><br>
				<<endevent>>
				<<link [[Next|Temple]]>><</link>>
				<br>
			<</if>>
		<br><br>
	<</if>>
<</if>>