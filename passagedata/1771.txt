<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "danube">><<set $lock to 300>>
The door is locked tight.
<br><br>
<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>
	<<link [[Break in (0:05)|Danube House Sneak]]>><<pass 5>><<crimeup 1>><</link>><<crime>>
	<br>
<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span>
	<<skulduggeryrequired>>
	<br><br>
<</if>>
<<link [[Leave|Danube Street]]>><</link>>
<br>