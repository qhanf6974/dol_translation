<<effects>>

You clean the counter near the pair.
<br><br>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
	<<set $smuggler_known to 1>><<set $fedora_unlock to 1>>
<span class="green">They don't react to your presence.</span> The <<person1>><<persons>> voice is clearer.
<br><br>

"Big haul coming in," <<he>> says.
<<if $smuggler_timer gte 2>>
	"<span class="gold">$smuggler_timer days from now.</span> At night, before midnight.
<<elseif $smuggler_timer is 1>>
	"<span class="gold">Tomorrow night.</span> Before midnight.
<<else>>
	"<span class="gold">Tonight.</span> Before midnight.
<</if>>
	<<if $smuggler_location is "forest">>
		They're bringing it through the forest."
	<<elseif $smuggler_location is "sewer">>
		They're bringing it through the old sewers."
	<<elseif $smuggler_location is "beach">>
		They plan to land at that rock near the beach."
	<<elseif $smuggler_location is "bus">>
		They're sneaking it in on a bus."
	<</if>>
<br><br>
	<<if $smuggler_stolen_stat gte 4>>
		"I hear they've had trouble," the <<person2>><<person>> says. "Shipments going missing."
		<br><br>

		"Aye, well," the <<person1>><<person>> responds, casting a furtive look around the club. "They're bringing backup. Don't know who'd be mad enough to mess with those people mind."
	<</if>>
	<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't improve your skulduggery.</span>
	<br><br>
	<</if>>

The rest of the shift passes without event. <<tipreceive>>
<br><br>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>

<<else>>

They glance at you, <span class="red">and fall quiet.</span> They don't speak again until you move away.
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

The rest of the shift passes without event. <<tipreceive>>
<br><br>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>

<</if>>