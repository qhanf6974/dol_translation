<<effects>>
<<generate1>><<person1>>
A security guard watches the entrance and spots you. <<He>> appears to say a few words to someone you can't see, and approaches you.
<br><br>
"Sorry, can't let you past" <<he>> continues, "You still have another <<print $clothingShop.ban + ' day' + ($clothingShop.ban gt 1 ? 's' : '')>>
till your ban is lifted.

<<link [[Next|Shopping Centre Top]]>><<endevent>><</link>>