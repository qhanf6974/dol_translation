<<effects>>
You find a deserted part of the garden and bask in the sun. You feel the warmth seep into your skin.
<br><br>
It might feel even better if you were undressed...
<br><br>
<<link [[Strip|Nude Bask]]>><<set $stress -= 40>><</link>><<exhibitionist1>><<lstress>>
<br>
[[Stop|Garden]]