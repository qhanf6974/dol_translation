<<effects>>
<<if $worn.upper.name isnot "naked">>You remove your $worn.upper.name<<if $worn.lower.name isnot "naked" and $worn.upper.set isnot $worn.lower.set>> and your $worn.lower.name<</if>>.<</if>>
<br><br>
<<upperstrip>><<lowerstrip>>
The warmth feels wonderful on your bare skin.
<br>
<<exhibitionism1>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure) and $eventskip is 0>>
	You open your eyes and notice that your clothes aren't where you left them. You look around with increasing distress, but can't see them anywhere. Someone must have taken them. Maybe whoever did it is still around.
	<br><br>
	<<link [[Call out|Bask Call]]>><</link>>
	<br>
	<<link [[Sneak inside|Bask Sneak]]>><<set $upperoff to 0>><<set $loweroff to 0>><</link>>
	<br>
<<else>>
	<<if $daystate is "day">>
		<<link [[Continue|Nude Bask]]>><<set $stress -= 40>><<pass 20>><<tanned 40 "tanLines">><</link>><<lstress>><<gtanned>>
		<br>
		<<link [[Masturbate|Nude Masturbation]]>><<set $masturbationstart to 1>><</link>>
		<br>
		<<link [[Stop|Garden]]>><<clotheson>><</link>>
	<<else>>
		The sun starts to disappear behind the rooftops.
		<br><br>
		<<link [[Get dressed|Garden]]>><<clotheson>><</link>>
	<</if>>
<</if>>