<<set $outside to 0>><<set $location to "pub">><<effects>><<set $bus to "harvest">>
<<if $pubnpc is 1>>
	<<if $pubdrink gte 8000>>
		"I'm pushing it," <<he>> says. "One more, then I have to hit the road."
		<br><br>
		You leave to buy <<his>> drink, but <<he>> glances at <<his>> watch when you return. "Shit! I need to get home now." <<He>> races from the building. You might have waited too long.
		<br><br>
		<<link [[Next|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 6000>>
		<<He>> seems unsure, "Okay. I can make it a quick one."
		<br><br>
		You leave to buy <<his>> drink. <<He>> talks about <<his>> married life. "Things haven't been so great in, you know, the bedroom. Not since our last kid."
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (£5)|Pub Drink]]>><<set $money -= 500>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 4000>>
		"I shouldn't," <<he>> says. "But I will."
		<br><br>
		You return with the drink. <<He>> continues talking about <<his>> children, and how well they're doing at school.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<set $money -= 500>><<pass 20>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 2000>>
		<<He>> smiles more broadly. "That'd be kind of you. Just don't tell my <<if $pronoun is "m">>wife!"<<else>>husband!"<</if>>
		You return with <<his>> drink, and find <<him>> looking through some photos. "These are my kids," <<he>> says, sharing them with you. <<He>> seems genuinely proud of them.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<set $money -= 500>><<pass 20>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<</if>>
<<elseif $pubnpc is 2>>
	<<if $pubdrink gte 8000>>
		"No," <<he>> says. "Thank you, but I'm at my limit. You're been very kind though, I hope I see you again." You might have waited too long.
		<br><br>
		<<link [[Next|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 6000>>
		"I don't think I've ever drank this much!" <<he>> says.
		<br><br>
		You buy <<him>> a drink, then sit down to chat. "My friends make fun of me for being single for so long. Sometimes I do feel lonely."
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (£5)|Pub Drink]]>><<set $money -= 500>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 4000>>
		"Okay," <<he>> says. "But you have one this time too."
		<br><br>
		You buy <<him>> a drink, then sit down to chat. "I got in trouble a lot. But those actual delinquents were scary."
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<set $money -= 500>><<pass 20>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 2000>>
		<<He>> smiles again. "You're so kind."
		<br><br>
		You buy <<him>> a drink, then sit down to chat. "I graduated last year," <<he>> says. "Are the teachers still the same? I used to get in so much trouble."
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<set $money -= 500>><<pass 20>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<</if>>
<<elseif $pubnpc is 3>>
	<<if $pubdrink gte 8000>>
		<<He>> nods, saying nothing.
		<br><br>
		You leave to buy <<his>> drink, but when you return <<hes>> gone. There's a note on the table that reads "Thank you." You might have waited too long.
		<br><br>
		<<link [[Next|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 6000>>
		<<He>> glances at you as <<he>> nods. You think you see a smile.
		<br><br>
		You give <<him>> <<his>> drink and try to coax <<him>> out <<his>> shell. You joke a bit, and <<he>> laughs.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (£5)|Pub Drink]]>><<set $money -= 500>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 4000>>
		<<He>> glances at you this time as <<he>> nods.
		<br><br>
		You give <<him>> <<his>> drink and try to coax <<him>> out <<his>> shell. You ask <<him>> some simple questions, and manage to get a few nods in response.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<set $money -= 500>><<pass 20>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 2000>>
		<<He>> doesn't look up, but nods.
		<br><br>
		You give <<him>> <<his>> drink and try to coax <<him>> out <<his>> shell. <<He>> still won't speak, but seems happy to listen.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<set $money -= 500>><<pass 20>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<</if>>
<<elseif $pubnpc is 4>>
	<<if $pubdrink gte 8000>>
		You nod and <<he>> stands to get another. By the time <<he>> returns, you're drooped on the table, everything spinning. "Actually," <<he>> says. "I think you've had enough. Let's get you home."
		<br><br>
		<<link [[Next (0:10)|Pub Journey]]>><<pass 10>><</link>>
		<br>
	<<elseif $pubdrink gte 6000>>
		<<He>> buys you yet another glass of liquor.
		<br><br>

		<<He>> starts asking questions about you. Innocent enough, where you go to school. Where you live. "One for the road?" <<he>> asks.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
		<</if>>
		<<link [[Accept (0:20)|Pub Drink]]>><<set $drunk += 200>><<pass 20>><<set $pubdrink += 2000>><</link>>
		<br>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 4000>>
		<<He>> buys you another glass of liquor. This time it goes down a bit easier.
		<br><br>

		<<He>> talks a bit more now, telling you about <<his>> work on a farm. <<if $cow gte 6 and $livestock_intro isnot undefined>>You're not sure how to feel about it<<else>>It sounds difficult<</if>>. <<He>> offers you another drink.
		<br><br>
		<<if $promiscuity gte 15>>
		<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
		<br>
		<</if>>
		<<link [[Accept (0:20)|Pub Drink]]>><<set $drunk += 200>><<pass 20>><<set $pubdrink += 2000>><</link>>
		<br>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 2000>>
		<<He>> buys you a glass of liquor. The bartender didn't ask what <<he>> wanted, seeming to know already. It's strong stuff.
		<br><br>
		<<He>> encourages you to say what's on your mind, and doesn't speak much. "Would you like another?" <<he>> asks once your glass is empty.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
		<</if>>
		<<link [[Accept (0:20)|Pub Drink]]>><<set $drunk += 200>><<pass 20>><<set $pubdrink += 2000>><</link>>
		<br>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<</if>>
<<else>>
	<<if $pubdrink gte 8000>>
		<<He>> nods, saying nothing.
		<br><br>
		You leave to buy <<his>> drink, but when you return <<his>> head lies on the table, snoring. You might have overdone it.
		<br><br>
		<<link [[Next|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 6000>>
		<<He>> nods, "Saves me the pennies."
		<br><br>
		You leave to buy <<his>> drink. <<He>> doesn't look up this time. <<He>> rambles about loneliness and how <<hes>> unappreciated.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (£5)|Pub Drink]]>><<set $money -= 500>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 4000>>
		<<He>> glares at you again, "Go on then."
		<br><br>
		You leave to buy <<his>> drink, feeling <<his>> eyes on you the whole time. <<He>> speaks this time, and complains about problems at work.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<set $money -= 500>><<pass 20>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<<elseif $pubdrink gte 2000>>
		<<He>> glares at you, suspicious. "I'm not going to say no to a free drink. But don't get any ideas."
		<br><br>
		You leave to buy <<his>> drink, feeling <<his>> eyes on you the whole time. <<He>> still refuses to speak, <<his>> gaze buried in <<his>> cup.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
			<br>
			<<if $money gte 500>>
				<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<set $money -= 500>><<pass 20>><<set $pubdrink += 2000>><</link>>
				<br>
			<</if>>
		<</if>>
		<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
		<br>
	<</if>>
<</if>>