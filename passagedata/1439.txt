A whistle pierces the air. You turn and see a lifeguard running towards you.

<<generate1>><<person1>>

<<He>> stops in front of you, but looks away. "I'm sorry, but I need to ask you to cover up. This isn't a nudist beach."
<br><br>

<<link [[Comply|Beach Strip Comply]]>><</link>>
<br>
<<link [[Refuse (1:00)|Beach Strip Refuse]]>><<stress -12>><<trauma -6>><<crimeup 50>><<pass 1 hour>><</link>><<crime>><<ltrauma>><<lstress>>
<br>
<<if $promiscuity gte 15>>
<<link [[Seduce|Beach Strip Seduce]]>><</link>><<promiscuous2>>
<br>
<</if>>