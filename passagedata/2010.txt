<<effects>>

You doubt anyone could see you in the dark, but you keep low between the bushes regardless. You creep up to the fence, and peek over. An alley runs behind the orphanage. It's dark and deserted, but the thought of climbing over makes you quiver regardless.
<br><br>

Sneaking around the orphanage with your <<lewdness>> displayed is one thing, but there's a semblance of safety here. There's nothing of the sort outside.
<br><br>

You look over your shoulder to make sure you aren't being watched, then hoist your self over. You land on the other side. Your heart pounds in your chest.

<<if $ex_fence isnot 1>><<set $ex_fence to 1>><<exhibitionism2>><<else>><br><br><</if>>

<<link [[Next|Residential alleyways]]>><</link>>
<br>