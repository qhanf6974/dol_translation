<<set $outside to 0>><<set $location to "docks">><<dockeffects>><<effects>>

You sneak under a parked vehicle and peek out from beneath.
<br><br>

One of the workers places a crate in front of you, blocking your view. There's a crack in the bottom. A sweet, heady smell leaks out. It relaxes and heats you.
<br><br>

It's the last crate they load into the cargo container. The <<person1>><<person>> locks it, and they climb into the vehicle before driving away.
<br><br>

<<dockoptions>>