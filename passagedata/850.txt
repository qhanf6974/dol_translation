<<effects>>

<<if $submissive gte 1150>>
	"I-I'm tougher than I look," you say.
<<elseif $submissive lte 850>>
	"Nah," you say. "I'm tougher than I look."
<<else>>
	"I knew what I was doing," you say.
<</if>>
<br><br>

Alex helps you to your feet. "Just be more careful," <<he>> says. "Come on, let's get back to work."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>