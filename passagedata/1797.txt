<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>

<<undress "danubeparty">>

<<set $lowertemp to "init">><<lowerwear 4>><<set $worn.lower.colour to either("black", "blue", "brown", "green", "pink", "purple", "red", "tangerine", "teal", "white", "yellow")>>

<<exposure>>

You enter the mansion. You hear voices and the clinking of glasses further in.
<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
	Noticing your bound <<if $leftarm is "bound" or $rightarm is "bound">>arms<</if>><<if ($leftarm is "bound" or $rightarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>, <<he>> gives you a strange smile. "It's not that kind of party. Not this time.<<if $leftarm is "bound" or $rightarm is "bound">> You'll at least need your hands.<</if>>"
	<br>
	<<He>> moves behind you and a moment later <span class="green">your limbs fall free.</span>
	<<unbind>>
	<br><br>
<</if>>
<<He>> passes you the apron and gestures to the door on your left. "You can get changed in the pantry," <<he>> says. "When you're ready, go through the door opposite and you'll find some platters. Take each in turn and offer them to the guests. Now if you'll excuse me, I need to get back to being a good <<if $pronoun is "m">>host<<else>>hostess<</if>>."
<br><br>

You enter the room as instructed. It's small enough that getting changed is difficult, but you <<nervously>> remove your clothing and wrap the apron around your waist. It almost feels like you're wearing nothing at all. You wish you had a mirror to examine yourself properly.
<br><br>

Properly attired and with a tray of champagne in one hand, you enter the main room where the party is being held. A <<person1>><<person>> gasps and drops their glass, the sound of its shattering drawing more attention. Soon everyone in the room is watching you, murmuring amongst themselves. You offer another glass to the <<personcomma>> who doesn't take <<his>> eyes off your <<lewdness>> as <<he>> takes from the tray. <<exhibitionism4>><<fameexhibitionism 30>>
<br><br>

<<link [[Next|Danube Apron2]]>><</link>>
<br>