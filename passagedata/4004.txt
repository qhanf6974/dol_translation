<<set $outside to 0>><<effects>>

"I'm glad we have an understanding," Leighton says, rummaging through the clothes rack. You spend the next hour wearing outfits and striking poses as Leighton meticulously photographs you from every angle. <<He>> lets you change behind a curtain between shoots, but some of the outfits don't hide much.

<<if $player.gender is "f" or $player.gender is "h">>
	<<if $rng gte 81>>
	The most revealing is an undersized micro bikini.
	<<elseif $rng gte 61>>
	The most revealing is a little red dress, cut so low on the bust that a hint of areola remains visible.
	<<elseif $rng gte 41>>
	The most revealing is a leotard that fits so tightly it reveals every little contour of your body.
	<<elseif $rng gte 21>>
	The most revealing is a tiny skirt, so short that you worry the camera can see under it.
	<<else>>
	The most revealing is an oversized shirt, with nothing else. It hangs down to your thighs, but you need to tug the bottom further down make sure your <<genitals_are>> hidden. Unfortunately this stretches the fabric over your <<breastscomma>> making it hug you tightly.
	<</if>>
<<else>>
	<<if $rng gte 76>>
	The most revealing is a pair of undersized trunks, that hug your <<genitals>> so tightly you wonder if it's any better than nudity.
	<<elseif $rng gte 51>>
	The most revealing is a tiny loincloth, so short that that you worry the camera can see under it.
	<<elseif $rng gte 26>>
	The most revealing is a leotard that fits so tightly it reveals every little contour of your body.
	<<else>>
	The most revealing is an oversized shirt, with nothing else. It hangs down to your thighs, but you need to tug the bottom further down make sure your <<genitals_are>> hidden. Unfortunately this stretches the fabric over your <<breastscomma>> making it hug you tightly.
	<</if>>
<</if>>

<br><br>

"At least you're good for something." <<he>> says, handing you some money before leaving you to get dressed. <<tearful>> you count the money. You've earned <span class="gold">£75.</span>
<<set $money += 7500>>
<br><br>

<<clotheson>>
<<endevent>>

<<link [[Next|Brothel]]>><</link>>