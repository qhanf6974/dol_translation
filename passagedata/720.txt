<<effects>>

You rush back to the shed and search for something to cover with. You find a towel, and tie it around your body.
<<towelup>>
<br><br>

You finish feeding the chickens, but are careful to keep them at a distance. The towel wouldn't last against their eagerness.
<br><br>

<<link [[Next|Farm Work]]>><</link>>
<br>