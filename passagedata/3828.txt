<<effects>>

<<if $submissive gte 1150>>
	"Y-you want to see more of me?" you ask, acting coy.
<<elseif $submissive lte 850>>
	"I bet you're loving this," you say.
<<else>>
	"Of course you want to see more," you say. "Everyone does."
<</if>>

<<if $leftarm is "bound" and $rightarm is "bound">>
	You turn to face Avery, letting <<him>> get a clear view of your $worn.under_lower.name.
<<else>>
	You move your hand, letting <<him>> glimpse the outline of your <<genitals>> through your $worn.under_lower.name.
<</if>>
<<exhibitionism4>>

Avery stares for a moment, then kneels in front of you. "Allow me." <<He>> wraps on towel around your waist. <<Hes>> in no hurry.
<br><br>
<<towelup>>

<<link [[Next|Avery Party Dance Return]]>><</link>>
<br>