<<set $outside to 1>><<set $location to "town">><<effects>>
<<set $stress -= 5000>>
<<bind>>
<<facewear 5>>
You wake up, but can't see a thing. You feel something wrapped around your face. You try to lift your hands to investigate, but find them tied behind your back. You can faintly hear the sounds of town, but muffled, as if you're inside.
<br><br>

<<generate1>><<generate2>><<person1>>

"<<pShes>> waking up," you hear a <<personsimple>> say.
<br><br>

"That's fine," someone responds. "<<pShe>> isn't gonna be able to remove that blindfold on <<pher>> own. Let's do it right here."
<br><br>

They grab you from both sides.
<br><br>

<<link [[Next|Street Wake Blindfold Rape]]>><<set $molestationstart to 1>><</link>>
<br>