Are you prepared to add all your
<<if $crateContents is "all">>
	clothes
<<else>>
	outfits
<</if>>
to the crate and send them to be repaired? Might be best to go shopping shortly after.
<br><br>

<<set _value to 0>>
<<for _items range $wardrobe>>
	<<for _i to 0; _i lt _items.length; _i++>>
		<<if _items[_i].outfitSecondary isnot undefined>>
			<<continue>>
		<</if>>
		<<if _items[_i].outfitPrimary is undefined and $crateContents is "outfits">>
			<<continue>>
		<</if>>
		<<set _value += Math.floor(_items[_i].cost * (1 - (_items[_i].integrity / _items[_i].integrity_max)) * 1.25)>>
	<</for>>
<</for>>
It will cost you
<<if _value gt 5000>>
	£<<print ((_value - 5000) / 100).toFixed(2)>>.
<<else>>
	nothing.
<</if>>

<br><br>
<<if $money gte _value>>
	<<link [[Yes|Wardrobe Repair Crate Result]]>><</link>>
<<else>>
	Not enough money.
<</if>>
<br>
<<link [[No|$wardrobeReturnLink]]>>
	<<unset $wardrobeReturnLink>>
	<<unset $crateContents>>
<</link>>