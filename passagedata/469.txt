<<set $outside to 1>><<set $location to "forest">><<effects>>

<<set $seductiondifficulty to 6000>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>>
<br><br>

You sit on the grass and look up at <<himstop>> "I'm just looking for some company," you say, patting the grass next to you.<<promiscuity2>>
<br><br>

<<if $seductionrating gte $seductionrequired>>
<<He>> hesitates. You reach up, take <<his>> arm and gently pull <<him>> on top of you.
<br><br>

<<link [[Next|Forest Cutter Sex]]>><<set $sexstart to 1>><</link>>
<br>

<<else>>
<<He>> hesitates. You reach up and tug <<his>> arm but <<he>> pulls away from you. "No, I really need to get back to work," <<he>> says. "Stay safe."
<br><br>
<<endevent>>
<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
<br>

<</if>>