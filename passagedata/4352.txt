<<effects>>
<<if $swimdistance lte 1>>
	<<tearful>> you haul yourself out of the water, leaving the slime behind you.
	<br><br>
	<<if $timer gte 30>>
		<<set $bus to "residentialdrain">>
	<<elseif $timer gte 20>>
		<<set $bus to "commercialdrain">>
	<<elseif $timer gte 10>>
		<<set $bus to "industrialdrain">>
	<<else>>
		<<set $bus to "drainexit">>
	<</if>>
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<<elseif $timer lte 0>>
	The water carries you toward the end of the drain. You try to cling to the side of the tunnel, but to no avail. You are carried into the open air and plummet into the sea below. <<tearful>> you struggle to the surface.
	<br><br>
	<<if $upperoff isnot 0>>
		<<upperruined>>
	<</if>>
	<<if $loweroff isnot 0>>
		<<lowerruined>>
	<</if>>
	<<if $underloweroff isnot 0>>
		<<underlowerruined>>
	<</if>>
	<<if $underupperoff isnot 0>>
		<<underupperruined>>
	<</if>>
	<<clotheson>>
	<<endcombat>>
	<<seacliffseventend>>
<<else>>
	It's all just too much for you. You pass out.
	<br><br>
	<<if $upperoff isnot 0>>
		<<upperruined>>
	<</if>>
	<<if $loweroff isnot 0>>
		<<lowerruined>>
	<</if>>
	<<if $underloweroff isnot 0>>
		<<underlowerruined>>
	<</if>>
	<<if $underupperoff isnot 0>>
		<<underupperruined>>
	<</if>>
	<<endcombat>>
	<<link [[Next|Drain Water Passout]]>><<pass 1 hour>><</link>>
<</if>>