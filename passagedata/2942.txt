<<effects>><<set $location to "moor">>

Inside is a table covered with cider cans and playing cards. As if a game were interrupted. They push you past it, and onto a bed next to a pile of stacked crates.
<br><br>

"Three at a time," the <<person>> says "There's enough <<girl>> for all of us. First though." <<He>> crouches, and pulls a pair of ankle cuffs out from beneath the bed. They snap shut around your legs. A short chain connects them.<<feetwear 3>>
<br><br>

<<He>> tugs the chain to make sure they're on firm, then climbs on top of you.
<br><br>

<<link [[Next|Maths Abduction Rape]]>><<set $molestationstart to 1>><<set $mathsab to 0>><</link>>
<br>