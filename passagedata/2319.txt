<<effects>>

Winter wraps a heated towel around you, and then a second, for the journey back to the museum. It's only then that <<he>> notices the mask. "Where did you find that?"
<br><br>
<<if $submissive gte 1150>>
	"I-It was at the bottom of the River," you say. The mask's expression is neutral. You thought it was smiling earlier. It was hard to make out underwater. "I hope I didn't do anything wrong by taking it. I didn't think at the time."
<<elseif $submissive lte 850>>
	"It was on the river bed," you say. The mask's expression is neutral. You thought it was smiling earlier. It was hard to make out underwater. "Finders keepers."
<<else>>
	"It was buried in the riverbed," you say. The mask's expression is neutral. You thought it was smiling earlier. It was hard to make out underwater. "I pulled it up with my feet."
<</if>>
<br><br>	
"Is that silver?" Winter asks. "I knew you had a knack for this, but you seem to find valuables wherever you go. It looks valuable. I'd be willing to take it off your hands, but lets get you warmed up first."
<br><br>
Winter looks over <<his>> shoulder. Most of the crowd is following. "I think we've earned some new interest."
<br><br>
<<set $antiquemoney += 3000>><<museumAntiqueStatus "antiquesilvermask" "found">>
You arrive at the museum. <<He>> takes you to a small side room, and leaves you to get dressed.
<br><br>

That was terrifying, <span class="green">yet you feel a strong catharsis.</span><<trauma -24>>
<br><br>
<<endevent>>
<<set $museuminterest += 50>>
<<link [[Next|Museum]]>><<unbind>><<clotheson>><</link>>
<br>