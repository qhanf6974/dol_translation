<<widget "clothingcaption">><<nobr>>

<<if $worn.upper.name is "naked">>
	<<if $worn.lower.name is "naked">>
		<<if $worn.under_lower.name is "naked" and $worn.genitals.name is "naked">>
			<<if $worn.under_upper.name is "naked">>
			<span class="red">You are completely naked!</span>
			<<else>>
			<span class="red">Your bottom half is completely exposed!</span> <span class="pink">Your <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name <<if $worn.under_upper.plural is 1>>give<<else>>gives<</if>> little comfort.</span>
			<</if>>
		<<elseif $worn.under_lower.name is "naked" and $worn.genitals.name isnot "naked">>
			<<if $worn.under_upper.name is "naked">>
				<span class="red">Your <<genitalsintegrity>> $worn.genitals.name <<if $worn.genitals.anal_shield is 1>>with an anal shield<</if>> gives you no comfort.</span>
			<<else>>
				<span class="red">Other than your <<genitalsintegrity>> $worn.genitals.name <<if $worn.genitals.anal_shield is 1>>with an anal shield<</if>>, your bottom half is completely exposed!</span>
			<</if>>
		<</if>>
		<<if $worn.under_lower.name isnot "naked">>
			<<if $worn.under_upper.name is "naked">>
			<span class="pink">You are wearing nothing but a<<if $worn.under_lower.name.last() is "s">> pair of<</if>> <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name.</span>
			<<else>>
				<<if $worn.under_lower.set is $worn.under_upper.set>>
				<span class="pink">You are wearing nothing but <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name.</span>
				<<else>>
				<span class="pink">You are wearing a<<if $worn.under_lower.name.last() is "s">> pair of<</if>> <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name and <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name.</span>
				<</if>>
			<</if>>
			<<if $worn.genitals.name isnot "naked">>
				You wear <<genitalsword>> <<genitalsintegrity>> $worn.genitals.name<<if $worn.genitals.anal_shield is 1>> with an anal shield<</if>>.
			<</if>>
		<</if>>
	<</if>>
	<<if $worn.lower.name isnot "naked">>
		<<if $worn.under_lower.name is "naked">>
			<<if $worn.under_upper.name is "naked">>
			<span class="pink">You are topless and wearing no underwear</span> but your lower half is covered by <<lowerword>> <<lowerintegrity>> <<lowerclothescolour>> $worn.lower.name.
			<<else>>
			<span class="pink">You are wearing <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name</span> and <<lowerword>> <<lowerintegrity>> <<lowerclothescolour>> $worn.lower.name, <span class="purple">with nothing underneath.</span>
			<</if>>
			<<if $worn.genitals.name isnot "naked">>
				You wear <<genitalsword>> <<genitalsintegrity>> $worn.genitals.name<<if $worn.genitals.anal_shield is 1>> with an anal shield<</if>>.
			<</if>>
		<</if>>
		<<if $worn.under_lower.name isnot "naked">>
			<<if $worn.under_upper.name is "naked">>
			<span class="pink">You are topless</span> with <<lowerword>> <<lowerintegrity>> <<lowerclothescolour>> $worn.lower.name and <<underlowerword>> <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name.
			<<else>>
			Your lower half is covered by <<lowerword>> <<lowerintegrity>> <<lowerclothescolour>> $worn.lower.name and <<underlowerword>> <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name, <span class="pink">but only <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name protects your chest.</span>
			<</if>>
			<<if $worn.genitals.name isnot "naked">>
				You wear <<genitalsword>> <<genitalsintegrity>> $worn.genitals.name<<if $worn.genitals.anal_shield is 1>> with an anal shield<</if>>.
			<</if>>
		<</if>>
	<</if>>
<<elseif $worn.upper.name isnot "naked">>
	<<if $worn.lower.name is "naked">>
		<<if $worn.under_lower.name is "naked">>
			<<if $worn.upper.one_piece is "broken" and $worn.lower.set isnot $worn.upper.set>>
				<<if $worn.under_upper.name is "naked">>
				You are wearing just <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name that <<upperhas>> been torn at the waist <span class="red"> leaving your bottom half completely exposed!</span>
				<<else>>
				You are wearing <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name beneath <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name that <<upperhas>> been torn at the waist <span class="red"> leaving your bottom half completely exposed!</span>
				<</if>>
			<<else>>
				<<if $worn.under_upper.name is "naked">>
				<span class="red">Your bottom half is completely exposed!</span> Your top half is covered by <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name, <span class="purple">with nothing beneath.</span>
				<<else>>
				<span class="red">Your bottom half is completely exposed!</span> Your top half is covered by <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name, with <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name beneath.
				<</if>>
			<</if>>
			<<if $worn.genitals.name isnot "naked">>
				<span class="red">Your <<genitalsintegrity>> $worn.genitals.name<<if $worn.genitals.anal_shield is 1>> with an anal shield<</if>> gives you no comfort.</span>
			<</if>>
		<</if>>
		<<if $worn.under_lower.name isnot "naked">>
			<<if $worn.upper.one_piece is "broken" and $worn.lower.set isnot $worn.upper.set>>
				<<if $worn.under_upper.name is "naked">>
				You are wearing <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name that has been torn at the waist <span class="purple"> leaving your <<underlowerword>> <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name exposed.</span>
				<<else>>
				You are wearing <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name beneath <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name that has been torn at the waist <span class="purple"> leaving your <<underlowerword>> <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name exposed.</span>
				<</if>>
			<<else>>
				<<if $worn.under_upper.name is "naked">>
				You are wearing <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name and <<underlowerword>> <span class="purple">exposed <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name.</span>
				<<else>>
					<<if $worn.under_upper.one_piece is 1>>
					You are wearing <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name beneath <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name. <span class="purple">Your $worn.under_upper.name is visible beneath your waist.</span>
					<<else>>
					You are wearing <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name beneath <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name and <<underlowerword>> <span class="purple">exposed <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name.</span>
					<</if>>
				<</if>>
			<</if>>
			<<if $worn.genitals.name isnot "naked">>
				Your <<genitalsintegrity>> $worn.genitals.name<<if $worn.genitals.anal_shield is 1>> with an anal shield<</if>> <<if $worn.under_lower.reveal gte 500>>is clearly visible<<else>>can be made out<</if>> underneath.
			<</if>>
		<</if>>
	<</if>>
	<<if $worn.lower.name isnot "naked">>
		<<if $worn.under_lower.name is "naked">>
			<<if $worn.under_upper.name is "naked">>
			You are wearing <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name
				<<if $worn.lower.one_piece isnot 1>>
				and <<lowerword>> <<lowerintegrity>> <<lowerclothescolour>> $worn.lower.name
				<</if>>
			<span class="purple"><<if $worn.lower.type.includes("swim")>>and<<else>>but<</if>> you are not wearing underwear.</span>
			<<else>>
				You are wearing <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name
				<<if $worn.lower.one_piece isnot 1>>
				and <<lowerword>> <<lowerintegrity>> <<lowerclothescolour>> $worn.lower.name
				<</if>>
				<<if $worn.under_upper.one_piece is "broken">>
				<span class="purple">with just <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name that has been torn at the waist beneath.</span>
				<<else>>
				<span class="purple">with just <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name beneath.</span>
				<</if>>
			<</if>>
			<<if $worn.genitals.name isnot "naked">>
				You wear <<genitalsword>> <<genitalsintegrity>> $worn.genitals.name<<if $worn.genitals.anal_shield is 1>> with an anal shield<</if>>.
			<</if>>
		<</if>>
		<<if $worn.under_lower.name isnot "naked">>
			<<if $worn.under_upper.name is "naked">>
			You are wearing <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name
				<<if $worn.lower.one_piece isnot 1>>
				and <<lowerword>> <<lowerintegrity>> <<lowerclothescolour>> $worn.lower.name
				<</if>>
				<<if $breastsize lt 3>>
					with <<underlowerword>> <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name beneath.
				<<else>>
					<span class="purple">with just <<underlowerword>> <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name beneath.</span>
				<</if>>
			<<else>>
			You are wearing <<upperword>> <<upperintegrity>> <<upperclothescolour>> $worn.upper.name
				<<if $worn.lower.one_piece isnot 1>>
				and <<lowerword>> <<lowerintegrity>> <<lowerclothescolour>> $worn.lower.name
				<</if>>
				<<if $worn.under_lower.one_piece is 1>>
				with <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name beneath.
				<<else>>
				with <<underlowerword>> <<underlowerintegrity>> <<underlowerclothescolour>> $worn.under_lower.name and <<underupperword>> <<underupperintegrity>> <<underupperclothescolour>> $worn.under_upper.name beneath.
				<</if>>
			<</if>>
			<<if $worn.genitals.name isnot "naked">>
				You wear <<genitalsword>> <<genitalsintegrity>> $worn.genitals.name<<if $worn.genitals.anal_shield is 1>> with an anal shield<</if>>.
			<</if>>
		<</if>>
	<</if>>
<</if>>

<<if $worn.face.type.includes("mask")>>
<br>
Your identity is concealed by your $worn.face.name.
<</if>>

<</nobr>><</widget>>

<<widget "stripcaption">><<nobr>>

<<if !$worn.upper.type.includes("naked") and $upperwetstage gte 3 and !$worn.lower.type.includes("naked") and $lowerwetstage gte 3 and !$worn.under_lower.type.includes("naked") and !$worn.genitals.type.includes("chastity") and $underlowerwetstage gte 3 and !$worn.under_upper.type.includes("naked") and $underupperwetstage gte 3>>
	<<if $worn.under_upper.set is $worn.under_lower.set>>
		<<if $worn.upper.set is $worn.lower.set>>
	<br>
	Your $worn.upper.name and $worn.under_upper.name are drenched, <span class="pink">revealing your <<breasts>> and <<genitalsstop>></span>
	<br>
		<<else>>
	<br>
	Your $worn.upper.name, $worn.lower.name and $worn.under_upper.name are drenched, <span class="pink">revealing your <<breasts>> and <<genitalsstop>></span>
	<br>
		<</if>>

	<<else>>
		<<if $worn.upper.set is $worn.lower.set>>
	<br>
	Your $worn.upper.name, $worn.under_lower.name and $worn.under_upper.name are drenched, <span class="pink">revealing your <<breasts>> and <<genitalsstop>></span>
	<br>
		<<else>>
	<br>
	Your $worn.upper.name, $worn.lower.name, $worn.under_lower.name and $worn.under_upper.name are drenched, <span class="pink">revealing your <<breasts>> and <<genitalsstop>></span>
	<br>
		<</if>>
	<</if>>

<<elseif !$worn.upper.type.includes("naked") and $upperwetstage gte 3 and !$worn.lower.type.includes("naked") and $lowerwetstage gte 3 and !$worn.under_lower.type.includes("naked") and !$worn.genitals.type.includes("chastity") and $underlowerwetstage gte 3>>
	<<if $worn.upper.set is $worn.lower.set>>
<br>
Your $worn.upper.name, and $worn.under_lower.name are drenched, <span class="pink">revealing your <<undertop>> and <<genitalsstop>></span>
<br>
	<<else>>
<br>
Your $worn.upper.name, $worn.lower.name and $worn.under_lower.name are drenched, <span class="pink">revealing your <<undertop>> and <<genitalsstop>></span>
<br>
	<</if>>

<<elseif !$worn.upper.type.includes("naked") and $upperwetstage gte 3 and !$worn.under_upper.type.includes("naked") and $underupperwetstage gte 3 and $lowerwetstage gte 3 and !$worn.lower.type.includes("naked")>>
	<<if $worn.upper.set is $worn.lower.set>>
	<br>
	Your $worn.upper.name and $worn.under_upper.name are drenched, <span class="purple">revealing your <<breasts>> and <<undiesstop>></span>
	<br>
	<<else>>
	<br>
	Your $worn.upper.name, $worn.lower.name and $worn.under_upper.name are drenched, <span class="purple">revealing your <<breasts>> and <<undiesstop>></span>
	<br>
	<</if>>

<<elseif !$worn.upper.type.includes("naked") and $upperwetstage gte 3 and $lowerwetstage gte 3 and !$worn.lower.type.includes("naked")>>
	<<if $worn.upper.set is $worn.lower.set>>
<br>
Your $worn.upper.name is drenched, <span class="purple">revealing your <<undertop>> and <<undiesstop>></span>
<br>
	<<else>>
<br>
Your $worn.upper.name and $worn.lower.name are drenched, <span class="purple">revealing your <<undertop>> and <<undiesstop>></span>
<br>
	<</if>>

<<elseif !$worn.upper.type.includes("naked") and $upperwetstage gte 3>>
<br>
Your $worn.upper.name <<upperplural>> drenched, <span class="purple">revealing your <<breastsstop>></span>
<br>

<<elseif !$worn.lower.type.includes("naked") and $lowerwetstage gte 3 and !$worn.under_lower.type.includes("naked") and !$worn.genitals.type.includes("chastity") and $underlowerwetstage gte 3>>
<br>
Your $worn.lower.name and $worn.under_lower.name are drenched, <span class="pink">revealing your <<genitalsstop>></span>
<br>

<<elseif !$worn.lower.type.includes("naked") and $lowerwetstage gte 3>>
<br>
Your $worn.lower.name <<lowerplural>> drenched, <span class="purple">revealing your <<undiesstop>></span>
<br>

<<elseif !$worn.under_lower.type.includes("naked") and !$worn.genitals.type.includes("chastity") and $underlowerwetstage gte 3>>
<br>
Your $worn.under_lower.name <<underlowerplural>> drenched, <span class="pink">revealing your <<genitalsstop>></span>
<br>

<<elseif !$worn.under_upper.type.includes("naked") and $underupperwetstage gte 3>>
<br>
Your $worn.under_upper.name <<underupperplural>> drenched, <span class="pink">revealing your <<breastsstop>></span>
<br>

<<elseif !$worn.upper.type.includes("naked") and $worn.upper.exposed is 2 and !$worn.lower.type.includes("naked") and $worn.lower.exposed is 2 and !$worn.under_lower.type.includes("naked") and !$worn.genitals.type.includes("chastity") and $worn.under_lower.state isnot "waist" and !$worn.under_upper.type.includes("naked") and $worn.under_upper.state isnot $worn.under_upper.state_base>>
	<<if $worn.under_upper.set is $worn.under_lower.set>>
		<<if $worn.upper.set is $worn.lower.set>>
	<br>
	Your $worn.upper.name, skirt and $worn.under_upper.name have been pulled aside, <span class="pink">revealing your <<breasts>> and <<genitalsstop>></span>
	<br>
		<<else>>
	<br>
	Your $worn.upper.name, $worn.lower.name and $worn.under_upper.name have been pulled aside, <span class="pink">revealing your <<breasts>> and <<genitalsstop>></span>
	<br>
		<</if>>
	<<else>>
		<<if $worn.upper.set is $worn.lower.set>>
	<br>
	Your $worn.upper.name, skirt, $worn.under_upper.name and $worn.under_lower.name have been pulled aside, <span class="pink">revealing your <<breasts>> and <<genitalsstop>></span>
	<br>
		<<else>>
	<br>
	Your $worn.upper.name, $worn.lower.name, $worn.under_upper.name and $worn.under_lower.name have been pulled aside, <span class="pink">revealing your <<breasts>> and <<genitalsstop>></span>
	<br>
		<</if>>
	<</if>>

<<elseif !$worn.upper.type.includes("naked") and $worn.upper.exposed is 2 and !$worn.lower.type.includes("naked") and $worn.lower.exposed is 2 and !$worn.under_lower.type.includes("naked") and !$worn.genitals.type.includes("chastity") and $worn.under_lower.state isnot "waist">>
	<<if $worn.upper.set is $worn.lower.set>>
<br>
Your $worn.upper.name, skirt and $worn.under_lower.name have been pulled aside, <span class="pink">revealing your <<undertop>> and <<genitalsstop>></span>
<br>
	<<else>>
<br>
Your $worn.upper.name, $worn.lower.name and $worn.under_lower.name have been pulled aside, <span class="pink">revealing your <<undertop>> and <<genitalsstop>></span>
<br>
	<</if>>

<<elseif !$worn.upper.type.includes("naked") and $worn.upper.exposed is 2 and $worn.lower.exposed is 2 and !$worn.lower.type.includes("naked") and !$worn.under_upper.type.includes("naked") and $worn.under_upper.state isnot $worn.under_upper.state_base>>
	<<if $worn.upper.set is $worn.lower.set>>
<br>
Your $worn.upper.name, skirt and $worn.under_upper.name have been pulled aside, <span class="purple">revealing your <<breasts>> and <<undiesstop>></span>
<br>
	<<else>>
<br>
Your $worn.upper.name, $worn.lower.name and $worn.under_upper.name have been pulled aside, <span class="purple">revealing your <<breasts>> and <<undiesstop>></span>
<br>
	<</if>>

<<elseif !$worn.upper.type.includes("naked") and $worn.upper.exposed is 2 and $worn.lower.exposed is 2 and !$worn.lower.type.includes("naked")>>
	<<if $worn.upper.set is $worn.lower.set>>
<br>
Your $worn.upper.name and skirt have been pulled aside, <span class="purple">revealing your <<undertop>> and <<undiesstop>></span>
<br>
	<<else>>
<br>
Your $worn.upper.name and $worn.lower.name have been pulled aside, <span class="purple">revealing your <<undertop>> and <<undiesstop>></span>
<br>
	<</if>>

<<elseif !$worn.upper.type.includes("naked") and $worn.upper.exposed is 2>>
<br>
Your $worn.upper.name <<upperhas>> been pulled aside, <span class="purple">revealing your <<breastsstop>></span>
<br>

<<elseif !$worn.lower.type.includes("naked") and $worn.lower.exposed is 2 and !$worn.under_lower.type.includes("naked") and !$worn.genitals.type.includes("chastity") and $worn.under_lower.state isnot "waist">>
<br>
Your $worn.lower.name <<lowerhas>> been pulled aside and your $worn.under_lower.name pulled down, <span class="pink">revealing your <<genitalsstop>></span>
<br>

<<elseif !$worn.lower.type.includes("naked") and $worn.lower.exposed is 2>>
<br>
Your $worn.lower.name <<lowerhas>> been pulled aside, <span class="purple">revealing your <<undiesstop>></span>
<br>

<<elseif !$worn.under_lower.type.includes("naked") and !$worn.genitals.type.includes("chastity") and $worn.under_lower.state isnot $worn.under_lower.state_base>>
<br>
Your $worn.under_lower.name <<underlowerhas>> been pulled down to your $worn.under_lower.state, <span class="pink">revealing your <<genitalsstop>></span>
<br>

<<elseif !$worn.under_upper.type.includes("naked") and $worn.under_upper.state isnot $worn.under_upper.state_base>>
<br>
Your $worn.under_upper.name <<underupperhas>> been pulled down to your $worn.under_upper.state, <span class="pink">revealing your <<breastsstop>></span>
<br>

<</if>>

<</nobr>><</widget>>

<<widget "statsCaption">><<nobr>>
	<<if $dev is 1>>
		<<set $money = Math.trunc($money)>>
		£<<print ($money / 100)>>.
	<<else>>
		£<<print Math.trunc($money / 100)>>.<<if $money % 100 lte 9>>0<</if>><<print $money % 100>>
	<</if>>
	&nbsp;
	<<effectstime>>
	<img class="icon_ui" src="img/ui/icon_time.png">
	<<if $timestyle is "ampm">>
		<<if $hour gte 13>>
			<<print ($hour - 12)>>:<<print ($time - $hour * 60).toString().padStart(2, "0")>> pm.
		<<else>>
			<<print ($hour is 0 ? "12" : $hour)>>:<<print ($time - $hour * 60).toString().padStart(2, "0")>> <<if $hour is 12>>pm<<else>>am.<</if>>
		<</if>>
	<<else>>
	<<if $hour lt 10>>0<</if>>$hour:<<print ($time - $hour * 60).toString().padStart(2, "0")>>
	<</if>>
	&nbsp;
	<<if $extendedStats>>
		<br>
	<</if>>
	<img class="icon_ui" src="img/ui/icon_day.png">
	<<switch $weekday>>
	<<case 1>>Sun<<case 2>>Mon<<case 3>>Tue<<case 4>>Wed<<case 5>>Thu<<case 6>>Fri<<case 7>>Sat
	<</switch>>
	<<if $extendedStats>>
		$monthday<<monthday>> <<month true>>
	<</if>>
<</nobr>><</widget>>