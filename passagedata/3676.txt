<<temple_effects>><<effects>>

You struggle against the void, as if trying to wake from a nightmare. The presence is almost upon you when the chamber materialises around you. The torches are lit as if nothing happened.
<br><br>

<<prayerend>>