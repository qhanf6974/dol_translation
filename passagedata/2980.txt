<<set $outside to 0>><<set $location to "town">><<effects>>

<<npc Leighton>><<person1>>
Leighton, River and the two guests confer on stage for a moment.
<<if $mathschance gte 1 and $mathschancestart is 0>>
Leighton seems to be arguing with the others.
	<<if $mathschance gte $rng>>
	River throws up <<if $NPCName[$NPCNameList.indexOf("River")].pronoun is "m">>his<<else>>her<</if>> hands in exasperation as Leighton steps up to the microphone and clears <<his>> throat.
	<<else>>
	<<He>> shakes <<his>> head, then steps up to the microphone and clears <<his>> throat.
	<br><br>
	<</if>>
<</if>>

"We've made a decision," <<he>> says.

<<if $mathschance gte $rng>>
<span class="green">"The winner is the student with the <<haircolourtext>> hair." <<He>> points at you.</span> "Come on up."
<br><br>

You climb the stage and shake Leighton's hand as <<he>> smiles for a waiting photographer. <<He>> hands you the trophy. There's <span class="gold">£2000</span> inside.
<<set $money += 200000>>
The audience cheer and applaud.
<<npcincr River love 5>><<llltrauma>><<trauma -120>><<famebusiness 100>>
<br><br>

<<if $mathsexposed isnot undefined>>
<<endevent>><<npc River>><<person1>>River struggles to look you in the eye. "I expect you to get an A* on every exam from now on," <<he>> says. "You've proven capable." <<He>> and Leighton herd you and the other students from the building. A few people try to grope you on the way out.
<<lldelinquency>><<detention -54>>
<br><br>
<<else>>
<<endevent>><<npc River>><<person1>>River smiles. "I expect you to get an A* on every exam from now on," <<he>> says. "You've proven capable." <<He>> and Leighton herd you and the other students from the building.
<<lldelinquency>><<detention -54>>
<br><br>
<</if>>

<<set $mathsproject to "won">><<set $mathsprojectwon to 1>><<earnFeat "Maths Competition Winner">>
<<mathsprojectfinish>>

<<link [[Next|Cliff Street]]>><<endevent>><<set $eventskip to 1>><</link>>
<br>

<<else>>
<<generate2>>
<span class="red"><<He>> announces that a <<person2>><<person>> is the winner.</span> <<He>> rushes up to accept <<his>> trophy.
<br><br>

<<if $mathsexposed isnot undefined>>
Leighton herds you and the other students from the hall. "I appreciate your effort," River says to you on the way out. <<He>> struggles to look you in the eye.
<<ldelinquency>><<detention -18>>
<br><br>
A few people try to grope you on the way out.
<br><br>
<<else>>
Leighton herds you and the other students from the hall. "I appreciate your effort," River says to you on the way out, before being pulled away by one of the suited guests.
<<ldelinquency>><<detention -18>>
<br><br>
<</if>>

<<set $mathsproject to "done">>
<<mathsprojectfinish>>

<<link [[Next|Cliff Street]]>><<endevent>><<set $eventskip to 1>><</link>>
<br>

<</if>>