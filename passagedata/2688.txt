<<effects>>
<<endcombat>>

<<npc Leighton>><<person1>>Leighton's stern voice resonates through the room, and your assailants back off. Sirris must have summoned the <<if $pronoun is "m">>him<<else>>her<</if>> when <<endevent>><<npc Sirris>><<person1>><<he>> lost control of the class. Leighton doesn't stick around once order is restored, leaving Sirris to cover and console you. <<tearful>> you do your best to put on a brave face and return to your seat.
<br><br>

<<clothesontowel>>

<<link [[Next|Science Lesson]]>><<endevent>><</link>>