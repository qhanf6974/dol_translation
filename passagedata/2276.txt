<<effects>>

You shove the <<person1>><<person>> away from you. <<His>> friends laugh. "Playing hard to get? Fine, but we'll be watching you."
<<glewdity>><<set $stall_rejected += 1>>
<br><br>

You return to your stall as if nothing were amiss.
<br><br>

<<stall_actions>>