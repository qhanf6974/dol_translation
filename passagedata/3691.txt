<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
You shuffle out from the tight space, scroll in hand. The <<person>> awaits you, and clasps <<his>> hands together in joy.
<br><br>
"Thank you so much," <<he>> says. "These are very old, and wouldn't have lasted this long if everyone where as careless as me."
<br><br>
<<endevent>>
<<link [[Next|Temple Quarters]]>><</link>>
<br>