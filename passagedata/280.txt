<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
<<if $gamemode is "hard">>
	<<set $gamemode to "normal">>
	<span class="gold">You survived $days days. Hard mode disabled.</span>
	<<set $hardmodedays to $days>>
	<br><br>
<</if>>
<<if $asylumescaped is 1>>
	<<set $asylumescaped to 0>>
	<<set $suspicion to 80>><<set $asylumstatus to 40>><<set $asylumbound to 3>>
<<else>>
	<<set $suspicion to 20>><<set $asylumstatus to 0>><<set $asylumbound to 1>>
<</if>>
<<set $asylumkeycard to 0>>
<<set $asylumdistracted to 0>>
<<if $asylumintro is 1>>
	<<npc Harper>><<person1>>
	<<bind>><<upperwear 35>>
	You awaken on a bed in a white room. The walls and floor are padded. Doctor Harper stands over you.
	"You're awake," <<he>> says. <<He>> jots something down on <<his>> clipboard. "Welcome back." Your arms are bound. "I'm sorry to admit we failed you last time. That won't happen again."
	<br><br>
	<<He>> looks back at <<his>> notes. "Do you remember the schedule? Sleep at 10 pm. Breakfast at 6 am. You have two treatments scheduled per day, at 10 am and 3 pm. You'll be assessed each evening at 8 pm. Don't worry if you forget, someone will remind you."
	<br><br>
	<<He>> rises to <<his>> feet and walks toward the exit. "You're free to explore the building during the day. Don't enter any doors marked for staff, and make sure to follow instructions. They're for your own good.
	<<if $asylumstate is "sleep">>
		For now, you should stay in your room until morning." The door shuts, and the lock clicks.
	<<else>>
		" <<He>> leaves the door open.
	<</if>>
	<br><br>
	<<endevent>>
	<<link [[Next|Asylum Cell]]>><</link>>
	<br>
<<else>>
	<<set $asylumintro to 1>>
	<<npc Harper>><<person1>>
	<<bind>><<upperwear 35>>
	You awaken on a bed in a white room. The walls and floor are padded. Doctor Harper stands over you.
	<br><br>
	"You're awake," <<he>> says. <<He>> jots something down on <<his>> clipboard. "There's nothing to be nervous about. You're here to get better." <<He>> crouches beside you. "We're in another hospital. In the forest." You can't move your arms. They're bound tight in front of you. "That's for your own safety. If you're good, we'll untie you in a day or two."
	<br><br>
	<<He>> looks back at <<his>> notes. "We follow a schedule here. Sleep at 10 pm. Breakfast at 6 am. You have two treatments scheduled per day, at 10 am and 3 pm. You'll be assessed each evening at 8 pm. Don't worry if you forget; someone will remind you."
	<br><br>
	<<He>> rises to <<his>> feet and walks toward the exit. "You're free to explore the building during the day. Don't enter any doors marked for staff, and make sure to follow instructions. They're for your own good.
	<<if $asylumstate is "sleep">>
		For now, you should stay in your room until morning." The door shuts, and the lock clicks.
	<<else>>
		" <<He>> leaves the door open.
	<</if>>
	<br><br>
	<<endevent>>
	<i>You'll be released should your trauma or awareness become very low, but there are other ways to escape.</i>
	<br><br>
	<<link [[Next|Asylum Cell]]>><</link>>
	<br>
<</if>>