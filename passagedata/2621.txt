<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You turn and bite River's hand. You feel your fangs sink into <<his>> flesh.
<br><br>

<<He>> screams and lurches away from you, clutching <<his>> injury. <<He>> stares at you with wide eyes, seemingly lost for words. Most of the class look amused.

<<if $whitneymaths is "active" or $whitneymaths is "seat">>
	Especially Whitney.
<</if>>

 "You... Det..." the teacher stammers. "Detention." <<He>> walks to the front of the class, taking a detour around you, and rummages in <<his>> desk for a bandage.
<br><br>

<<endevent>>
<<link [[Next|Maths Lesson]]>><</link>>
<br>