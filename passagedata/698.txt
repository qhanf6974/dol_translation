<<set $outside to 1>><<set $location to "wolf_cave">><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>><<wolfpacktrust>>
	<br>
	The wolf yawns and lies down.
	<br><br>
	<<tearful>> you climb to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
<<elseif $enemyhealth lte 0>>
	The wolf limps away from you.
	<br><br>
	<<tearful>> you climb to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
<<elseif $rescue is 1 and $alarm is 1>>
	<<set $rescued += 1>>
	The other wolves growl at the wolf assaulting you. It growls back at first, but then backs down and leaves you alone.
	<<lharmony>>
	<br><br>
	<<tearful>> you climb to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
<<else>>
	The wolf yawns and lies down.
	<br><br>
	<<tearful>> you climb to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
<</if>>
<<link [[Next|Wolf Cave Clearing]]>><</link>>
<br>