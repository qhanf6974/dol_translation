<<effects>>

<<endmasturbation>>
<<endcombat>>
<<clothesontowel>>
<<npc Sam>><<person1>>

<<if $masturbation_fluid gte 30>>

You add your fluid to the mixture, and whip up a new batch of cream.
<br><br>
Sam arrives soon after. You do your best to keep a straight face as <<he>> takes the bun to the rude customer.
<br><br>
You hear shouting from the cafe proper. It sounds more excited than angry. You peek through the door, and see a crowd stood around a table.
<br><br>

<<link [[Next|Chef Help 3]]>><</link>>
<br>

<<elseif $finish is 1>>

You stop masturbating. You whip up a bun without special ingredients.
<br><br>
Sam arrives to collect and deliver it, before arriving back a minute later. "The customer is satisfied," <<he>> says. "Sorry about that."
<br><br>

<<He>> glances at the clock on the wall. "Chef should be back in a moment. Thank you for the help, you saved my life. Here's your pay." <span class="gold"><<He>> hands you £10.</span>
<<set $money += 1000>>
<br><br>

<<endevent>>

<<link [[Next|Ocean Breeze]]>><</link>>
<br>

<<else>>

You hear someone coming. You quickly cover yourself and prepare a bun without special ingredients.
<br><br>
Sam arrives to collect and deliver it, before arriving back a minute later. "The customer is satisfied," <<he>> says. "Sorry about that."
<br><br>

<<He>> glances at the clock on the wall. "Chef should be back in a moment. Thank you for the help, you saved my life. Here's your pay." <span class="gold"><<He>> hands you £10.</span>
<<set $money += 1000>>
<br><br>

<<endevent>>

<<link [[Next|Ocean Breeze]]>><</link>>
<br>

<</if>>