<<set $outside to 1>><<effects>><<set $lock to 200>>
<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>
	<<link [[Pick it (0:10)|Riding School]]>><<pass 10>><<crimeup 20>><</link>><<crime>>
	<br>
<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span><<skulduggeryrequired>>
	<br><br>
<</if>>
<<link [[Leave|Farmland]]>><</link>>
<br>