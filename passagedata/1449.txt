<<effects>>
You drink the milkshake you bought from the cafe. It's deliciously creamy, and each sip helps melt away the stress.
<br><br>
<<link [[Next|Beach]]>><</link>>