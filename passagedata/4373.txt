<<effects>><<set $location to "sewers">><<set $outside to 0>>

<<if $stress gte 10000>>

It's all too much for you. You pass out.
<br><br>
<<sewersend>>
<<sewerspassout>>

<<elseif $hour lte 7>>

Morgan yawns, stretches and falls back on a pile of blankets. <<He>> snores.
<br><br>

Now could be your chance to escape.
<br><br>

<<link [[Sleep|Sewers Sleep]]>><</link>>
<br>
<<link [[Escape|Sewers Escape Night]]>><</link>>
<br>

<<elseif $hour is 18 and $sewersfeeding isnot 1>>

Morgan stares at you and smiles.
<br><br>

<<link [[Next|Sewers Feeding]]>><</link>>
<br>

<<elseif $hour is 9>>
"Now, sweetie," Morgan says. "It's important for people of our status to be educated about the finer things in life. Allow me to begin by telling you again of our family history." <<He>> prattles, gibbers, and mumbles<<his>> way through what <<he>> calls a history lesson.
<br><br>

<<link [[Pay attention (1:00)|Sewers History]]>><<pass 60>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>| <span class="blue">History?</span>
<br>
<<link [[Daydream (1:00)|Sewers History Daydream]]>><<pass 60>><</link>>
<br>
<<link [[Resist (1:00)|Sewers History Resist]]>><<pass 60>><</link>>
<br>

<<elseif $hour is 12>>
"Mathematics," Morgan says. "My favourite subject." <<He>> licks <<his>> lips and hands you an old receipt to write on.
<br><br>

<<link [[Pay attention (1:00)|Sewers Maths]]>><<pass 60>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>| <span class="blue">Maths?</span>
<br>
<<link [[Daydream (1:00)|Sewers Maths Daydream]]>><<pass 60>><</link>>
<br>
<<link [[Resist (1:00)|Sewers Maths Resist]]>><<pass 60>><</link>>
<br>

<<elseif $hour is 15>>
"Now pay attention," Morgan says. "You must be the quintessential socialite."
<br><br>

<<link [[Pay attention (1:00)|Sewers English]]>><<pass 60>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>| <span class="blue">English?</span>
<br>
<<link [[Daydream (1:00)|Sewers English Daydream]]>><<pass 60>><</link>>
<br>
<<link [[Resist (1:00)|Sewers English Resist]]>><<pass 60>><</link>>
<br>

<<elseif $hour is 17>>
"Listen carefully <<charlescomma>>" Morgan says. "This is important." <<He>> instructs you on the nutritional value of food in the old sewers.
<br><br>

<<link [[Pay attention (1:00)|Sewers Science]]>><<pass 60>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>| <span class="blue">Science?</span>
<br>
<<link [[Daydream (1:00)|Sewers Science Daydream]]>><<pass 60>><</link>>
<br>
<<link [[Resist (1:00)|Sewers Science Resist]]>><<pass 60>><</link>>
<br>

<<elseif $hour is 20 and $sewerssex isnot 1>><<set $sewerssex to 1>>
	<<if $exposed gte 2>>
		"I-It's time," Morgan says. "For special learning. Lie down."
		<br><br>
		
		<<link [[Lie down|Sewers Sex Ed]]>><<trauma 6>><<stress 6>><<npcincr Morgan love 1>><</link>><<gtrauma>><<gstress>>
		<br>
	<<else>>
		"I-It's time," Morgan says. "For special learning. Undress and lie down."
		<br><br>
		
		<<link [[Undress and lie down|Sewers Sex Ed]]>><<trauma 6>><<stress 6>><<npcincr Morgan love 1>><</link>><<gtrauma>><<gstress>>
		<br>
	<</if>>

	<<link [[Refuse|Sewers Sex Ed Refuse]]>><<npcincr Morgan love -1>><</link>>
	<br>

<<elseif $rng gte 91 and $worn.neck.collared is 1>>

"<<charlescomma>>" Morgan says. "What's that around your neck? I can get that off for you." <<He>> produces a bobby pin, lubricant, and a crowbar. "Now, be a big <<girl>> and come here."
<br><br>

<<link [[Allow|Sewers Collar]]>><<set $worn.neck.cursed to 0>><<neckruined>><<trauma 6>><<stress 6>><<pain 6>><</link>><<gtrauma>><<gstress>><<gpain>>
<br>
<<link [[Refuse|Sewers Collar Refuse]]>><</link>>
<br>

<<elseif $rng gte 51>>
"I'm going to bake scones," Morgan says, clutching a number of rats by their tails. "You put the kettle on."
<br><br>

<<link [[Help (1:00)|Sewers Scones]]>><<trauma 6>><<stress 6>><<npcincr Morgan love 1>><<pass 60>><</link>><<gtrauma>><<gstress>>
<br>
<<link [[Refuse|Sewers Spank]]>><<set $molestationstart to 1>><<npcincr Morgan love -1>><</link>>
<br>

<<elseif $rng gte 46>>

Morgan offers some advice. "Don't go swimming here without a top on," <<he>> says. "There are nasty parasites living in the water."
<br><br>

<<He>> turns away from you. "Help me search through these antiques," <<he>> says as <<he>> rummages through a pile of rubbish. "Don't be a bad <<girlcomma>> come help."
<br><br>

<<link [[Help (1:00)|Sewers Rummage]]>><<trauma 6>><<stress 6>><<npcincr Morgan love 1>><<pass 60>><</link>><<gtrauma>><<gstress>>
<br>
<<link [[Refuse|Sewers Spank]]>><<set $molestationstart to 1>><<npcincr Morgan love -1>><</link>>
<br>

<<elseif $rng gte 41>>

"<<charlescomma>>" Morgan hesitates. "Since we've been reunited, darling, I've wanted you to have this." <<He>> hands you a pristine <<if $player.gender_appearance is "m">>shirt and shorts<<else>>sundress<</if>>. "I-It's your favourite little outfit. W-would you wear it for me?"
<br><br>

<<link [[Wear|Sewers Outfit]]>><</link>>
<br>
<<link [[Refuse|Sewers Outfit Refuse]]>><</link>>
<br>

<<else>>

"Help me search through these antiques," Morgan says as <<he>> rummages through a pile of rubbish. "Don't be a bad <<girlcomma>> come help."
<br><br>

<<link [[Help (1:00)|Sewers Rummage]]>><<trauma 6>><<stress 6>><<npcincr Morgan love 1>><<pass 60>><</link>><<gtrauma>><<gstress>>
<br>
<<link [[Refuse|Sewers Spank]]>><<set $molestationstart to 1>><<npcincr Morgan love -1>><</link>>
<br>

<</if>>