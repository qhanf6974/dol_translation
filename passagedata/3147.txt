<<set $outside to 0>><<effects>><<set $lock to 0>>

You are in the tattoo parlour. The artists here can turn anything drawn on you into a tattoo. They also have a selection of tattoo designs available. Costs £500.
<br><br>

<<if $daystate is "night">>
	You are alone in the darkness.
<<elseif $exposed gte 1>>
	You hide in a cupboard to protect your dignity.
<<else>>

<</if>>
<br><br>

<<if $stress gte 10000>>
	<<passoutshop>>
<<else>>
	<<if $hour is 21>>
		It's closing time. Security is herding everyone outside.<<if $exposed gte 1>>Panic swells within you as you realise how exposed you'll be should security find you in this state of dress.<</if>>
		<br><br>

		<<if $exposed lte 0>>
			<<link [[Go Outside|High Street]]>><</link>>
			<br>
		<<else>>
			<<link [[Hide until it's over (1:00)|Tattoo Parlour]]>><<pass 1 hour>><</link>>
		<</if>>
	<<else>>

		<<if $daystate is "night" and $hour isnot $closinghour and $tattooparlourtheft isnot 1>>
			<<link [[Examine the cash register|Tattoo Parlour Register]]>><</link>>
			<br>
		<</if>>

		<<if $exposed lte 1 and $daystate isnot "night" and $hour isnot 21>>
			<<if $money lt 50000>>
				You cannot afford a tattoo.
				<br>
			<<else>>
				<<link [[Get a tattoo|Tattoo Choice]]>><</link>>
				<br>
				<<link [[Turn bodywriting into tattoo|Tattoo Bodywriting Choice]]>><</link>>
				<br>
			<</if>>
		<</if>>
		<br>
		<<link [[Leave|Shopping Centre]]>><</link>>
		<br>
	<</if>>
<</if>>