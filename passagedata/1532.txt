<<effects>>
<<if $finish is 1>>
	The <<person1>><<person>> steadies you while the others give you space. <<endevent>><<npc Briar>><<person1>>"Why are you stopping?" Briar storms onto the stage. <<He>> glares at you, then seems to remember where <<he>> is. <<He>> smiles at the audience. "Apologies my friends, but that will be all for today." <<He>> ushers you and the others off the stage.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<npc Briar>><<person1>>"Unprofessional," <<he>> says once you're off the stage and out of sight. "Don't expect payment."
	<<npcincr Briar love -1>>
	<br><br>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	<<if $phase is 0>>
	<<person1>>"Not so stuck-up now, are ya?" the <<person>> says. They leave you lying on the stage. The audience cheer and applaud as you stand and bow.
	<<else>>
	"M-more," you whimper. They leave you lying on the stage. The audience cheer and applaud as you stand and bow.
	<</if>>
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<npc Briar>><<person1>>Briar's waiting for you behind the stage. "Nice work," <<he>> says.
	<<if $phaselast is 0>>
		"Everyone likes to see a stuck-up <<girl>> get ruined like that."
	<<else>>
		"I almost believed your performance myself."
	<</if>>
	<<He>> hands you the £1000 <<he>> promised.
	<<set $money += 100000>><<npcincr Briar love 1>>
	<br><br>
<<elseif $enemyhealth lte 0>>
	You knock the actors away from you and escape from the stage.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<npc Briar>><<person1>>Briar catches you as you pass. "Not so quick," <<he>> says. "Listen." You realise the audience are cheering. "That was unexpected, but they enjoyed it." <<He>> hands you the £1000 <<he>> promised.
	<<set $money += 100000>><<npcincr Briar love 1>>
	<br><br>
<</if>>
<<endevent>>
You return to the dressing room.
<br><br>
<<link [[Next|Brothel Dressing Room]]>><</link>>