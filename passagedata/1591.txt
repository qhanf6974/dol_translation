<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<set $chef_state to 0>>

You shake your head. <<His>> energy deflates. "My offer's open. I hope you change your mind. Everyone should have a chance to taste something so superb."
<br><br>
<<He>> turns to walk away, then remembers <<himselfstop>> "Almost forgot. It's the end of your shift. Here's your pay." <<He>> hands you <span class="gold">£10.</span>
<<set $money += 1000>>
<br><br>

<<endevent>>

<<link [[Next|Ocean Breeze]]>><</link>>
<br>