<<effects>>
You stand on the riverbank, and peer into the churning water. You can't see the bottom. The flow enters and leaves the field through a tunnel. It looks like you could escape this way, but you'd need to be a strong swimmer to avoid being swept downstream.
<br><br>

You could wash here if you were careful.
<br><br>

<<link [[Next|Livestock Field]]>><</link>>
<br>