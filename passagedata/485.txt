<<effects>>
You reach into the rose bushes, searching for seeding flowers. You avoid the thorns as best you can, but a few tear at you regardless.
<br><br>
<span class="gold">You can now grow red roses.</span>
<br><br>
<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
<br>