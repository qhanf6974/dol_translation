<<effects>>

You crouch in a nearby alley, out of sight of onlookers, and try to open the thing.

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>

<span class="green">You manage to open it.</span>
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

Inside are binders full of paperwork, including balance sheets. They belong to a local corporation. Someone in the underworld might be interested in this.
<<set $blackmoney += 100>><<crimeup 100>>
<br><br>

<<destinationeventend>>

<<else>>

Try as you might, <span class="red">you can't figure out a way to open it.</span>
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

	<<link [[Smash it|Street Box Smash]]>><</link>><<physiquedifficulty>>
	<br>
	<<link [[Leave it|Street Box Stop]]>><</link>>
	<br>

<</if>>