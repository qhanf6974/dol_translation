<<set $outside to 0>><<set $location to "arcade">><<effects>>
<<set $kylar_play to 0>>
You are in the amusement arcade on Starfish Street.

<<if $daystate is "day" or $daystate is "dusk">>
The games and gambling machines beep and whirr.
<<else>>
The games and gambling machines lie silent.
<</if>>
<br><br>
<<if $stress gte 10000>>
<<passoutarcade>>
<<else>>
	<<if $hour is 21>>
	It's closing time. Security is herding everyone outside.<<if $exposed gte 1>>Panic swells within you as you realise how exposed you'll be should security find you in this state of dress.<</if>>
	<br><br>

		<<if $exposed lte 0>>
		<<link [[Go Outside|Starfish Street]]>><</link>>
		<br>
		<<else>>
		<<link [[Hide until it's over (1:00)|Arcade]]>><<pass 1 hour>><</link>>
		<</if>>
	<<else>>

		<<if $daystate is "night" or $daystate is "dawn">>
		You are alone in the darkness.
		<br><br>
		<<elseif $exposed gte 1>>
		You hide beneath a pinball machine to protect your dignity.
		<br><br>
		<</if>>
		<<if $money gte 500 and ($daystate is "day" or $daystate is "dusk") and $exposed lte 0>>
			<<link [[Play (£5 0:20)|Arcade Play]]>><<set $money -= 500>><<pass 20>><<stress -2>><</link>><<lstress>>
			<br>
		<</if>>
		<<if $exposed lte 0 and $NPCName[$NPCNameList.indexOf("Kylar")].state is "active" and $weather is "rain" and $daystate is "day">>
			<<if $schoolday isnot 1 or $hour lte 8 or $hour gte 15>>
				<br>
				You see Kylar playing on one of the cabinets.
				<br>
				<<link [[Approach|Kylar Arcade]]>><</link>>
				<br><br>
			<</if>>
		<</if>>

		<<link [[Leave|Starfish Street]]>><</link>>
		<br>
	<</if>>
<</if>>