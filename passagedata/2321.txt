<<effects>>

Winter nods, and stands on <<his>> toes to peer above a hedge. "Everyone's ready," <<he>> says. "We've got a crowd waiting. Come."
<br><br>
You follow Winter around the hedge. There's a large crowd, maybe a hundred people, stood around the ducking stool. They look at Winter, and at you, eager to begin.
<br><br>

"Ladies and gentlemen," Winter begins, gesturing for you to take a seat. "Today we are here to witness the punishment of this rapscallion." <<He>> pulls the straps over you, and binds you down tight. "A con artist, who swindled many good citizens. Now <<pshe>> faces justice."
<br><br>

Winter walks to the other end of the crane, and lifts you into the air. <<He>> continues to talk about the ducking stool as <<he>> pushes you over the river. The water rushes beneath you.
<br><br>

<<link [[Next|Museum Duck Light 2]]>><</link>>
<br>