<<set $outside to 0>><<set $location to "cafe">><<effects>>
<<npc Sam>><<person1>>
You continue to the exit, where Sam is seeing off guests.
<br><br>
"I forgot to mention," <<he>> says. "We're bumping up the price of buns to <span class="gold">£150</span>.
<<set $bun_value to 15000>>
It's expensive, but they're worth it. And we have extra staff to pay."
<br><br>
<<He>> waves you goodbye, and you exit onto Cliff Street.
<br><br>
<<endevent>>
<<link [[Next|Cliff Street]]>><<set $eventskip to 1>><</link>>
<br>