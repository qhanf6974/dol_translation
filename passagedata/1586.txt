<<set $outside to 0>><<set $location to "cafe">><<effects>>

You whip up another bun, stuffed with extra cream. Sam arrives to collect and deliver it, before arriving back a minute later. "<<Hes>> about as satisfied as I've seen <<himstop>> Sorry about that."
<br><br>
<<endevent>>
<<npc Sam>>

<<He>> glances at the clock on the wall. "Chef should be back in a moment. Thank you for the help, you saved my life. Here's your pay." <span class="gold"><<He>> hands you £10.</span>
<<set $money += 1000>>
<br><br>

<<endevent>>

<<link [[Next|Ocean Breeze]]>><</link>>
<br>