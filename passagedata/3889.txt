<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You walk up to the <<person2>><<personstop>> Both <<he>> and the <<person1>>bullied <<if $pronoun is "m">>boy<<else>>girl<</if>> stop and look at you. <<person2>>"What you want?" the <<person>> says.
<br><br>

<<if $submissive gte 1150>>
You avert your eyes. "L-let <<person1>><<him>> go," you say. "Don't be mean."
<<elseif $submissive lte 850>>
"Let <<person1>><<him>> go," you demand. "Or it'll be you up against that locker."
<<else>>
"Stop it," you say. "Bullying is wrong."
<</if>>
<br><br>
The <<person2>><<person>> regards you for a moment, then laughs. Some of the audience join in. <<He>> releases the <<person1>>bullied <<if $pronoun is "m">>boy<<else>>girl<</if>>'s collar. "Sure thing," <<person2>><<he>> manages through wheezes. "I don't want any trouble." <<His>> laughter redoubles. <<He>> staggers away.
<br><br>

The <<person1>>bullied <<if $pronoun is "m">>boy<<else>>girl<</if>> picks up <<his>> bag and starts gathering the contents. "Th-thank you," <<he>> stutters. "I-I'm," <<he>> looks at you, but blushes and looks away. "I'm Kylar." You thought <<he>> was younger than you, but <<he>> seems the same age. <<Hes>> just small. <<He>> turns and runs.
<br><br>
<<endevent>>
<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>