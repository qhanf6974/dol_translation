<<temple_effects>><<effects>>
<<if $submissive gte 1150>>
	"N-no," you say. "The guard let me in."
<<elseif $submissive lte 850>>
	"No," you say. "Though I'm not surprised that old guard forgot."
<<else>>
	"No," you say. "The guard let me through."
<</if>>
<br><br>
Jordan smiles. "Oh?" <<he>> asks. "You didn't use the secret door to get in? The one in the history textbooks at school." <<He>> laughs. "I wonder if the bishop knows."
<br><br>
<<He>> takes your hands in <<his>> own. "Shall we pray together?"
<br><br>
<<link [[Pray (1:00)|Temple Jordan Prayer]]>><<pass 60>><<trauma -12>><<stress -12>><<tiredness -6>><</link>><<lltrauma>><<llstress>><<ltiredness>>
<br>
<<link [[Leave|Temple Jordan Prayer Leave]]>><</link>>
<br>