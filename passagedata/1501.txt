<<set $outside to 0>><<set $location to "brothel">><<effects>>

You are behind the largest stage in the brothel. This is where the more involved performances are prepared.
<br><br>

<<if $brothelshow isnot "none" and $weekday is 6 and $brothelshowdone isnot 1>>
You see Briar talking to someone near the largest stage. You agreed to perform today.
<br><br>

<<link [[Star in show|Brothel Show]]>><</link>>
<br>
<</if>>
<<link [[Leave|Brothel]]>><</link>>
<br>