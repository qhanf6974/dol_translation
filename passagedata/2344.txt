<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>"Candlesticks are common enough," Winter says. "This one was made for a particular noble house that resided here." <<He>> shakes <<his>> head. "A pity what became of them."
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>