<<set $outside to 0>><<schooleffects>><<effects>>
You march into the middle of the room and twirl.
<<if $submissive gte 1150>>
	"I'm naked and exposed in front of you," you say. "I hope I look nice."
<<elseif $submissive lte 850>>
	"You're so easy to enthral," you say.
<<else>>
	"It's rude to stare," you say.
<</if>>
A <<person2>><<person>> reaches to touch, but you slap <<his>> hand away.
<br><br>
You continue teasing them. The girls become more noisy, and daring. Until Mason shouts from the pool, "Is everything okay in there?" The spell breaks, and they back away from you. You saunter from the room.
<br><br>
<<endevent>>
<<link [[Next|School Pool Entrance]]>><<endevent>><</link>>