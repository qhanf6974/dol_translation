<<set $outside to 1>><<set $location to "town">><<effects>>

<<if $pilloryaudience lte 1>>
<<set $pilloryaudience to 1>>
<</if>>

You are stuck in a pillory on Cliff Street, in front of the town hall.

<<if $pillorytime gte 1>>
You have $pillorytime hours left until release.
<<else>>
You should be released any moment from now.
<</if>>

<<if $pilloryaudience + $exposed is 1>>

<span class="teal">A few people stop to look at you but most just glance then continue their business.</span>

<<elseif $pilloryaudience + $exposed is 2>>

<span class="lblue">You've attracted a small crowd of onlookers.</span>

<<elseif $pilloryaudience + $exposed is 3>>

<span class="blue">You've attracted a growing crowd of onlookers.</span>

<<elseif $pilloryaudience + $exposed is 4>>

<span class="purple">A large group has gathered around you, big enough that the crowd itself is attracting more people to see what the fuss is about.</span>

<<elseif $pilloryaudience + $exposed is 5>>

<span class="pink">A large crowd has gathered around you. People throw jeers and insults your way.</span>

<<else>>

<span class="red">A huge crowd has gathered around you. People jeer and insult you. Some throw litter.</span>

<</if>>

<<if $exposed gte 1>>

You can do nothing about your <<lewdness>> displayed for everyone to see, humiliating you further.
<<if $exposed gte 2>> <<trauma +1>><<gtrauma>><</if>>

	<<if $exposed is 1>>
		<<if $pilloryaudience is 1>>
		<<fameexhibitionism 1>>
		<<elseif $pilloryaudience is 2>>
		<<fameexhibitionism 2>>
		<<elseif $pilloryaudience is 3>>
		<<fameexhibitionism 3>>
		<<elseif $pilloryaudience is 4>>
		<<fameexhibitionism 4>>
		<<elseif $pilloryaudience is 5>>
		<<fameexhibitionism 5>>
		<<else>>
		<<fameexhibitionism 10>>
		<</if>>
	<</if>>
	<<if $exposed is 2>>
		<<if $pilloryaudience is 1>>
		<<fameexhibitionism 3>>
		<<elseif $pilloryaudience is 2>>
		<<fameexhibitionism 6>>
		<<elseif $pilloryaudience is 3>>
		<<fameexhibitionism 9>>
		<<elseif $pilloryaudience is 4>>
		<<fameexhibitionism 12>>
		<<elseif $pilloryaudience is 5>>
		<<fameexhibitionism 15>>
		<<else>>
		<<fameexhibitionism 30>>
		<</if>>
	<</if>>
<</if>>

<<if $exposed gte 1 and $pilloryaudience and random(0, 9) gte 7>>
	A person pulls out their <<print ["camera", "phone"].pluck()>> and takes a photo.
	"<<print [
		"Now your <<undies>> can be exposed forever.",
		"I can't wait to post this online.",
		"I can't wait to share this with my friends.",
		"If you didn't want your <<undies>> photographed you shouldn't be such a slut.",
		"If you didn't want your <<undies>> photographed you shouldn't have broken the law.",
		"Aww, is it embarrassing having your <<undies>> photographed?",
		"Is it embarrassing, not being able to cover up?",
		"If you don't want your <<undies>> photographed, don't break the law.",
		"If you don't want people taking photos of your <<undies>> then don't break the law.",
		"I bet <<pshes>> getting off on this.",
		"Is it embarrassing being exposed like this? Too bad.",
		"Is it embarrassing being exposed like this? Too bad, criminal.",
		"I bet <<pshe>> gets off on the humiliation anyway.",
		"Do you like having your <<undies>> photographed?"
	].pluck()>>"
	<<if $exposed is 1>>
		<<trauma +1>><<gtrauma>>
	<<else>>
		<<trauma +2>><<gtrauma>>
	<</if>>
	<br><br>
<</if>>
<<if $exposed gte 2 and $pilloryaudience and $penisexist and random(0, 9) gte 7>><<pillorypeniscomment>><</if>>

<br><br>

<<if $pillorytime lte 0>>

A <<generate1>><<person1>><<person>> in a police uniform arrives. <<He>> removes your collar and unlocks the pillory, freeing you. The crowd boos its disappointment as the officer disperses them.
<br><br>
<<set $worn.neck.cursed to 0>><<neckruined>>
<<set $worn.neck.collaredpolice to 0>>
<<set $pilloryaudience to 0>>
<<unbind>>
<<clotheson>>
<<set $stress -= 1000>>
<<endevent>>
<<cliffeventend>>
<br>
<<else>>

	<<set $rng to random(1, 5)>>
	<<if $pilloryaudience + $exposed + $rng gte 10>>
	<<set $rng to random(1, 100)>>
		<<if $bestialitydisable is "f" and $rng gte 51>>
		You feel something prod your <<bottomstop>> A dog is sniffing you curiously.
		<br><br>

		<<link [[Next|Pillory Dog]]>><<set $molestationstart to 1>><</link>>
		<br>
		<<else>>
		<<generate1>><<person1>>A <<person>> walks right up to the pillory.
		<br><br>

		<<link [[Next|Pillory Molestation]]>><<set $molestationstart to 1>><<set $molestationstart to 1>><</link>>
		<br>
		<</if>>

	<<elseif $pilloryaudience + $exposed + $rng gte 6>>
	<<set $rng to random(1, 100)>>

		<<if $rng gte 65>>
			<<if $worn.lower.skirt_down is 1>>
			A <<generate1>><<person1>><<person>> walks behind you and grabs the hem of your $worn.lower.name.
			<br><br>

			<<link [[Kick them|Pillory Kick]]>><<stress -12>><<trauma -6>><<attackstat>><<set $pilloryaudience += 1>><</link>><<ltrauma>><<lstress>> | <span class="pink">Attracts attention</span>
			<br>
			<<link [[Stay still|Pillory Skirt]]>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>
			<br>
			<<elseif $worn.lower.exposed lte 1 and $worn.lower.set isnot "upperset" and $worn.lower.skirt isnot 1>>
			A <<generate1>><<person1>><<person>> walks behind you can grabs the hem of your $worn.lower.name.
			<br><br>
			<<link [[Kick them|Pillory Kick]]>><<stress -12>><<trauma -6>><<attackstat>><<set $pilloryaudience += 1>><</link>><<ltrauma>><<lstress>> | <span class="pink">Attracts attention</span>
			<br>
			<<link [[Stay still|Pillory Lower]]>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>
			<br>
			<<elseif $worn.lower.exposed gte 2 and $worn.under_lower.exposed is 0>>
			A <<generate1>><<person1>><<person>> walks behind you can grabs the hem of your $worn.under_lower.name.
			<br><br>
			<<link [[Kick them|Pillory Kick]]>><<stress -12>><<trauma -6>><<attackstat>><<set $pilloryaudience += 1>><</link>><<ltrauma>><<lstress>> | <span class="pink">Attracts attention</span>
			<br>
			<<link [[Stay still|Pillory Under]]>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>
			<br>

			<<else>>

			A <<generate1>><<person1>><<person>> walks behind you and gives your exposed <<bottom>> a hard spank. The crowd cheers.
			<<gtrauma>><<gstress>><<trauma 6>><<stress 6>><<set $pain += 20>>
			<br><br>

			<<endevent>>
			<<link [[Next|Pillory]]>><<pass 1 hour>><<set $pillorytime -= 1>><</link>>
			<br>

			<</if>>
		<<elseif $rng gte 30>>
		<<generate1>><<person1>>A <<person>> approaches you and presses <<his>> crotch against your helpless face.
		<br><br>

		<<link [[Next|Pillory Oral]]>><<set $molestationstart to 1>><</link>>
		<br>
		<<else>>
		A number of people disembark from a bus nearby. Some of them gather round to gawk. <span class="pink">The crowd becomes larger and bolder.</span>
		<br><br>

		<<link [[Next|Pillory]]>><<pass 1 hour>><<set $pillorytime -= 1>><</link>>
		<br>
		<</if>>

	<<else>>
	<<set $rng to random(1, 100)>>
		<<if $rng gte 65>>
		<<generatey1>><<generatey2>>A <<person1>><<person>> whispers something to <<his>> friend, a <<person2>><<personcomma>> who laughs and produces a piece of fruit from <<his>> bag.
		<br><br>

		<<link [[Warn them off|Pillory Warn]]>><<set $pilloryaudience += 1>><</link>> | <span class="pink">Attracts attention</span>
		<br>
		<<link [[Brace yourself|Pillory Brace]]>><</link>>
		<br>
		<<elseif $rng gte 30>>
		<<generate1>><<person1>>A <<person>> takes a picture of you with <<his>> phone.
		<br><br>
		<<endevent>>
		<<link [[Next|Pillory]]>><<pass 1 hour>><<set $pillorytime -= 1>><</link>>
		<br>
		<<else>>
		A number of people disembark from a bus nearby. Some of them gather round to gawk. <span class="pink">The crowd becomes larger and bolder.</span>
		<br><br>

		<<link [[Next|Pillory]]>><<pass 1 hour>><<set $pillorytime -= 1>><</link>>
		<br>
		<</if>>
	<</if>>

<</if>>