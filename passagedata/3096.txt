<<set $outside to 0>><<effects>>

<<if $clothingrebuy is 1>>

Disabling clothing rebuy will deactivate the chips in all your clothing. They will no longer attempt to buy replacements when destroyed, and will not charge you anything.
<br><br>

Would you like to disable automatic clothing rebuy?
<br><br>

<<link [[Yes|Clothing Shop]]>><<set $clothingrebuy to 0>><</link>>
<br>

<<else>>

Enabling clothing rebuy will activate chips in all your clothing. When damaged beyond repair, the clothes automatically order a replacement. The cost is the price of the garment, plus an additional 50% service fee. This will not work if you don't have enough money, which will be deducted immediately.
<br><br>

Would you like to enable this?
<br><br>

<<link [[Yes|Clothing Shop]]>><<set $clothingrebuy to 1>><</link>>
<br>

<</if>>
<<link [[No|Clothing Shop]]>><</link>>
<br>