<<widget "effectspain">><<nobr>>

<<if $leftaction is "leftstruggleweak" and $rightaction is "rightstruggleweak">>
	<<set $leftaction to 0>><<set $rightaction to 0>><<set $leftactiondefault to "leftstruggleweak">><<set $rightactiondefault to "rightstruggleweak">>
	You fight through the pain and try to push them away, but have too little strength.
	<<brat 2>><<set _br to true>>
<</if>>

<<if $leftaction is "leftstruggleweak">>
	<<set $leftaction to 0>><<set $leftactiondefault to "leftstruggleweak">>
	You fight through the pain and push them with your left arm, but have too little strength.
	<<brat 1>><<set _br to true>>
<</if>>

<<if $rightaction is "rightstruggleweak">>
	<<set $rightaction to 0>><<set $rightactiondefault to "rightstruggleweak">>
	You fight through the pain and push them with your right arm, but have too little strength.
	<<brat 1>><<set _br to true>>
<</if>>

<<if $leftaction is "leftprotect" and $rightaction is "rightprotect">>
	<<set $leftaction to 0>><<set $rightaction to 0>><<set $leftactiondefault to "leftprotect">><<set $rightactiondefault to "rightprotect">>
	You shield the tender parts of your body, protecting them from further harm. <span class="green"> - Pain</span>
	<<meek 2>><<set $pain -= 2>><<set _br to true>>
<</if>>

<<if $leftaction is "leftprotect">>
	<<set $leftaction to 0>><<set $leftactiondefault to "leftprotect">>
	You clutch a tender spot on your body with your left hand, protecting it from harm. <span class="green"> - Pain</span>
	<<meek 1>><<set $pain -= 1>><<set _br to true>>
<</if>>

<<if $rightaction is "rightprotect">>
	<<set $rightaction to 0>><<set $rightactiondefault to "rightprotect">>
	You clutch a tender spot on your body with your right hand, protecting it from harm. <span class="green"> - Pain</span>
	<<meek 1>><<set $pain -= 1>><<set _br to true>>
<</if>>

<<if _br is true>>
	<br>
<</if>>

<<if $mouthaction is "stifle">>
	<<set $mouthaction to 0>><<set $mouthactiondefault to "stifle">>
	You try to control your breath and stifle your sobs. You're mostly successful. <span class="green"> - Pain</span>
	<<set $pain -= 1>>
<</if>>

<<if $mouthaction is "letout">>
	<<set $mouthaction to 0>><<set $mouthactiondefault to "letout">>
	You don't hold back your tears. Your sobs are punctuated by cries and whimpers. <span class="green"> - Stress</span>
	<<meek 1>><<stress -2>>
<</if>>

<</nobr>><</widget>>