<<set $outside to 0>><<set $location to "museum">><<effects>>

"I know I'm asking a lot," <<he>> says. "I appreciate it, and the audience will too. We'll need a safe word. So if I push things to a point you're uncomfortable with, you can let me know."
<br><br>

What should your safe word be? <<if $wintersafeword is "" or $wintersafeword is undefined>><<set $wintersafeword to "Marshmallow">><</if>>
<<textbox "$wintersafeword" $wintersafeword>>
<br>

<<link [[Next|Museum Horse Extreme]]>><<if $wintersafeword is "" or $wintersafeword is undefined>><<set $wintersafeword to "Marshmallow">><</if>><</link>>
<br>