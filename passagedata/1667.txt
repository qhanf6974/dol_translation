<<set $outside to 0>><<set $location to "cafe">><<effects>>
<<npcincr Sam love -1>>

You turn on the spot and glare at the <<person1>><<personstop>> The grin falls off <<his>> face. "Just having a laugh. No need to pout." Sam looks over at you disapprovingly.
<<llove>>
<br><br>

The rest of the shift passes uneventfully. You earn £5.
<<set $money += 500>><<pass 1 hour>>
<br><br>

<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>