<<set $outside to 0>><<set $location to "home">><<effects>>

<<if $phase is 0>>
You can't be in here when Robin comes back with Bailey. You manage to gather yourself, pick yourself up and dash full-long right out of the building.
<br><br>
<<endevent>>
<<link [[Next|Domus Street]]>><</link>>
<br>

<<elseif $phase is 1>><<set $robinbaileyhelp to 1>>
You curl up into a fetal position and place your hands over your face as you continue to sob. A few minutes later you hear someone come stomping in, and you are pulled up to a sitting position by your hair.
<br><br>
<<endevent>><<npc Bailey>><<person1>>
"What the hell is your problem!?" Bailey growls at you. <<He>> looks you over with a harsh gaze and then slaps you across the face. "Shut up. You got this kid worried enough to bother me. I'm not wasting my time with you."
<br><br>
"Stop bothering me with useless crap." Bailey says to Robin as <<he>> barges from the room.
<br><br>
<<endevent>><<npc Robin>><<person1>>
Robin rushes over to you and holds your arm. <<He>> stares out the door in disbelief as you gather yourself. You are disgusted by the whole scenario and run back to your room.
<br><br>
<<endevent>>
<<link [[Next|Bedroom]]>><</link>>
<br>

<<else>>
You can't be in here when Robin comes back with Bailey. You gather yourself and make it back to your own room. By the time you get there your frantic tears have calmed to light crying.
<br><br>
Around five minutes later you hear the sound of someone knocking on your door. It's Robin. <<He>> asks if you are in there, but does not wait for an answer and opens the door on <<his>> own.
<br><br>
"There you are!" <<he>> says, running in. "Bailey yelled at me when you weren't in my room. What happened?"
<br><br>
<<link [[Complain (0:05)|Robin Hug Break 3]]>><<set $phase to 0>><<pass 5>><<stress -12>><</link>><<lstress>>
<br>
<<link [[Blame (0:05)|Robin Hug Break 3]]>><<set $phase to 1>><<pass 5>><<npcincr Robin love -1>><<npcincr Robin dom -1>><<trauma -6>><<stress -12>><</link>><<ltrauma>><<llstress>><<llove>><<ldom>>
<br>
<<link [[Persecute (0:05)|Robin Hug Break 3]]>><<set $phase to 2>><<pass 5>><<npcincr Robin love -1>><<npcincr Robin dom -1>><<stress -12>><<trauma -12>><</link>><<lltrauma>><<llstress>><<llove>><<ldom>>
<br>
<<link [[Dismiss|Robin Hug Break 3]]>><<set $phase to 3>><</link>>
<br>

<</if>>