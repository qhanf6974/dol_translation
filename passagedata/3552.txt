<<temple_effects>><<effects>>
You duck beneath the stairs, and sit beside the initiates.
<<if $temple_rank is "initiate">>
	They're happy to accept one of their own.
<<else>>
	They're wary at first due to your rank at the temple, but they soon warm up.
<</if>>
<br><br>
You chat for a while. Hiding whenever a <<generate1>><<person1>><<monk>> walks by is fun.
<br><br>
<<endevent>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure)>>
	You leave the space beneath a stairwell. A moment later you hear footsteps behind you. One of the initiates, a <<generatey1>><<person1>><<personcomma>> catches up to you. "You're cute," <<he>> says. <<He>> lightly grasps your forearm. "I know a quiet place we can be alone."
	<br><br>
	<<link [[Accept|Temple Stairs Sex Accept]]>><<grace -1>><</link>><<promiscuous2>><<lgrace>>
	<br>
	<<link [[Refuse|Temple Stairs Sex Refuse]]>><<grace 3>><</link>><<ggrace>>
	<br>
<<else>>
	<<link [[Next|Temple]]>><</link>>
	<br>
<</if>>