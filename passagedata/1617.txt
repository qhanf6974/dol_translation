<<set $outside to 0>><<set $location to "cafe">><<effects>>

You slap Bailey across the cheek. <<He>> barely flinches. The room quiets for a moment, before breaking into laughter and excitement. Bailey leans closer, <<his>> face contorted by that fake smile.
<br><br>

"Best not try that when we're alone," <<he>> whispers. "You're worth more with both hands attached."
<br><br>

<<He>> pulls away as, for the first time since you arrived, attention is diverted elsewhere.
<br><br>

<<endevent>>
<<link [[Look|Chef Opening 7]]>><</link>>
<br>