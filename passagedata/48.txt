<<effects>>
You look through the cupboard containing your clothes. There's an old dressing screen for privacy.
<br><br>
<<set $wardrobe_location to "wardrobe">>
<<wardrobewear>>
<<if $exhibitionism gte 75>>
	<<link [[Done|Eden Cabin]]>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
<<elseif $exhibitionism gte 55>>
	<<if $exposed lte 1>>
	<<link [[Done|Eden Cabin]]>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
	<br><br>
	<<else>>
	You can't remain undressed like this!
	<br><br>
	<</if>>
<<else>>
	<<if $exposed lte 0>>
	<<link [[Done|Eden Cabin]]>><<pass 1>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
	<br><br>
	<<else>>
	You can't remain undressed like this!
	<br><br>
	<</if>>
<</if>>
<<wardrobe>>