<<set $location to "landfill">><<set $outside to 1>><<effects>>

The gate is barred by a heavy-duty padlock.
<br><br>
<<set $lock to 800>>

	<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>

	<<link [[Pick lock (0:05)|Trash Lock Enter]]>><<pass 5>><<crimeup 1>><</link>><<crime>>
	<br>
	<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span><<skulduggeryrequired>>
	<br><br>
	<</if>>

<<link [[Leave|Elk Street]]>><</link>>
<br>