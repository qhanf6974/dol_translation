<<set $outside to 1>><<set $location to "farm">><<effects>>
<<farm_work_update>>
You are on Alex's farm. A rustic farmhouse overlooks a yard and chicken coop. Other buildings nestle between trees and hedgerows.
<br><br>

You'll earn <span class="gold">£<<print $farm.wage / 100>></span> for every hour worked.
<br><br>

<<if $farm_stage lt 6>>
	<<set _clearing to (100 - $farm.clearing)>>
	<<set $percent=Math.floor((_clearing/100)*100)>>
	One of the fields is being cleared, 
	<<if $farm.clearing lte 0>>
		<span class="green">and is ready for planting!</span>
		<div class="meter">
		<<print '<div class="greenbar" style="width:' + $percent + '%"></div>'>>
		</div>
	<<elseif $farm.clearing lte 20>>
		<span class="teal">and is almost ready for planting.</span>
		<div class="meter">
		<<print '<div class="tealbar" style="width:' + $percent + '%"></div>'>>
		</div>
	<<elseif $farm.clearing lte 40>>
		<span class="lblue">and is starting to look civilised.</span>
		<div class="meter">
		<<print '<div class="lbluebar" style="width:' + $percent + '%"></div>'>>
		</div>
	<<elseif $farm.clearing lte 60>>
		<span class="blue">and soil has been exposed in places.</span>
		<div class="meter">
		<<print '<div class="bluebar" style="width:' + $percent + '%"></div>'>>
		</div>
	<<elseif $farm.clearing lte 80>>
		<span class="purple">but the tangle remains defiant.</span>
		<div class="meter">
		<<print '<div class="purplebar" style="width:' + $percent + '%"></div>'>>
		</div>
	<<elseif $farm.clearing lte 99>>
		<span class="pink">but little progress has been made.</span>
		<div class="meter">
		<<print '<div class="pinkbar" style="width:' + $percent + '%"></div>'>>
		</div>
	<<else>>
		<span class="red">but remains a jungle of weeds.</span>
		<div class="meter">
		<<print '<div class="redbar" style="width:' + $percent + '%"></div>'>>
		</div>
	<</if>>
	<br>
<</if>>


<<if $farm_count is undefined>>
	<<set $farm_count to 0>>
<<elseif $farm_count gte 60>>
	<<set $farm_count -= 60>>
	<<set $money += $farm.wage>>
	You've earned <span class="gold">£<<print $farm.wage / 100>></span>.
	<br><br>
<</if>>

<<if $stress gte 10000>>
	<<passoutfarmroad>>
<<elseif $hour lte 5 or $hour gte 22 or ($hour is 21 and $farm_end is 1)>>
	<<link [[Sneak into the barn (0:05)|Farm Barn]]>><<pass 5>><</link>>
	<br><br>
	<<link [[Leave|Farmland]]>><</link>>
	<br>
<<elseif $exposed gte 2 and $farm_naked isnot 1>>
	You hide behind a fence, concealing your <<lewdnessstop>>
	<br><br>
	<<if $exhibitionism gte 75>>
		<<link [[Work anyway|Farm Work Naked]]>><<npcincr Alex lust 3>><<set $farm_naked to 1>><</link>><<exhibitionist5>><<gglust>>
		<br>
	<</if>>
	<<if $farm_work.alex isnot undefined and $farm_work.alex isnot "shower" and $hour gte 6 and $hour lte 20>>
		<<if $worn.upper.exposed gte 2 or $worn.lower.exposed gte 2>>
			<<link [[Ask Alex for clothes (0:05)|Farm Alex Clothes]]>><<farm_yield -1>><<npcincr Alex dom 1>><<npcincr Alex lust 1>><</link>><<exhibitionist1>><<lfarm>><<gdom>><<glust>>
			<br>
		<<else>>
			<<link [[Ask Alex for something to dry with (0:05)|Farm Alex Dry]]>><<farm_yield -1>><<npcincr Alex dom 1>><<npcincr Alex lust 1>><</link>><<exhibitionist1>><<lfarm>><<gdom>><<glust>>
			<br>
		<</if>>
	<</if>>
	<br>
	<<link [[Leave|Farmland]]>><</link>>
	<br>
<<elseif $farm_shift gte 360 and $farm_stage is 2>>
	<<set $farm_stage to 3>>
	<<npc Alex>><<generate2>><<person2>>
	"What do we have here?" says a voice. You turn to face it, and see a <<person>> leaning against the fence at the edge of the farm. "Alex got <<person1>><<himself>> a helper. A cute one too. Why don't you bring that ass over here?"
	<br><br>
	
	"Fuck off," says another voice. It's Alex. <<He>> glares at the <<person2>><<person>>. "I told you what would happen if you trespassed again."
	<br>
	The <<person>> throws up <<his>> hands, but <<his>> face shows no fear. "I'm not trespassing. See? The <<if $pronoun is "f">>girls<<else>>boys<</if>> and me were just wondering if you'd reconsidered our offer."
	<br>
	"I don't need your help," Alex responds, stepping beside you.
	<br>
	"You think this town <<girl>> is enough? This'll be funny." <<He>> turns and walks down the lane.
	<br><br>
	
	<<link [[Next|Farm Visitor]]>><</link>>
	<br>
<<elseif $farm_stage is 3 and $farm.clearing lte 0>>
	<<set $farm_stage to 4>>
	<<set $farm.clearing to 100>>
	<<earnFeat "Farmhand">>
	<<npc Alex>><<person1>>
	"We've done it," says Alex. <<He>> sounds tired, <<his>> clothes are torn, and <<his>> hair is matted with sweat, but a smile spreads across <<his>> face. "We cleared a second field."
	<br><br>
	
	<<He>> leads you to the field in question, stopping at the fence. <<He>> gestures for you to follow as <<he>> sits. "I couldn't have done it without you," <<he>> says, taking a swig from a glass bottle. <<He>> pulls another from <<his>> pocket, and offers it to you.
	<br><br>
	
	<<link [[Drink|Farm Stage 4]]>><<set $phase to 0>><<stress -6>><</link>><<lstress>>
	<br>
	<<link [[Refuse|Farm Stage 4]]>><<set $phase to 1>><</link>>
	<br>
	
	
<<elseif $farm_stage is 4 and $farm.clearing lte 0>>
	<<set $farm_stage to 5>>
	<<set $farm.clearing to 100>>
	<<set $farm.aggro to 0>>
	<<npc Alex>><<person1>>
	"It's done," Alex says, leaning against the wall of the farmhouse. "Another field ready to go." <<He>> reaches into <<his>> pocket, but stops when <<he>> hears the clop of hooves approaching from the lane.
	<br><br>
	<<if $remy_seen is "livestock">>
		<<endevent>><<npc Remy>><<person1>>
		A <<farm_text horse>> rides into the farm. Upon its back sits Remy, <<if $pronoun is "m">><<his>> pale brown hair flicking in the breeze<<else>><<his>> pale brown hair tied in a neat bun<</if>>. You tense as memories of your treatment at <<his>> farm invade your mind. However, there's no domineering malice in <<his>> eyes this time. There's no recognition at all.
		<br><br>
		<<He>> dismounts, and tugs off <<his>> gloves. <<He>> smiles at you, then at Alex. <<His>> green eyes glimmer.
		<br><br>
		"Sorry to arrive unannounced," <<he>> says. "I had trouble getting hold of you. I'm here to discuss business."
		<br><br>
	<<elseif $remy_seen is "riding_school">>
		<<endevent>><<npc Remy>><<person1>>
		A <<farm_text horse>> rides into the farm. Upon its back sits Remy, owner of the nearby riding school. <<if $pronoun is "m">><<His>> pale brown hair flicks in the breeze<<else>><<His>> pale brown hair is tied in a neat bun<</if>>.
		<br><br>
		<<He>> dismounts, and tugs off <<his>> gloves. <<He>> smiles at you, then at Alex. <<His>> green eyes glimmer.
		<br><br>
		"Sorry to arrive unannounced," <<he>> says. "I had trouble getting hold of you. I'm here to discuss business."
		<br><br>
	<<else>>
		<<endevent>><<npc Remy>><<person1>>
		A <<farm_text horse>> rides into the farm. A <<if $pronoun is "m">>man with short, pale brown hair flicking in the breeze<<else>>woman with pale brown hair tied in a neat bun<</if>> sits on <<farm_his horse>> back. Alex's smile vanishes
		<br><br>
		The <<personsimple>> dismounts, and tugs off <<his>> gloves. <<He>> smiles at you. <<His>> green eyes glimmer.
		<br><br>
		"I don't believe I've had the pleasure," <<he>> says, looking at you. I'm Remy. I run the nearby riding school, and a farm of my own." <<He>> turns to Alex. "Sorry to arrive unannounced, but I couldn't get hold of you. I'm here to discuss business."
		<br><br>
		
	<</if>>
	<<endevent>><<npc Alex>><<person1>>
	Alex looks conflicted, but politeness gets the better of <<him>>. "Tea or coffee?"
	<br><br>
	
	<<link [[Tea|Farm Stage 5]]>><<set $phase to 0>><<stress -6>><</link>><<lstress>>
	<br>
	<<link [[Coffee|Farm Stage 5]]>><<set $phase to 1>><<tiredness -6>><</link>><<ltiredness>>
	<br>
	<<set $remy_seen to "farm">>
<<elseif $farm_stage is 5 and $farm.clearing lte 0>>
	<<set $farm_stage to 6>>
	<<set $farm.clearing to 100>>
	<<earnFeat "Farmer">>
	<<set $farm_countdown to random(7, 14)>>
	<<npc Alex>><<person1>>You hear rapid footsteps approach. You turn, and see Alex rushing towards you.
	<<if $weather is "rain" or $weather is "snow">>
		<<Hes>> smiling, despite being covered in mud.
	<<else>>
		<<Hes>> smiling despite, the nettles sticking to <<his>> clothes.
	<</if>>
	<br><br>
	
	"It's done," <<he>> says. "The last field. We've restored the farm."
	<br><br>
	
	<<He>> slumps to the farmhouse porch, and looks at you. "I couldn't have done it alone. If <<if $pronoun is "m">>dad<<else>>mum<</if>> had known how dangerous it was-" <<he>> looks away. "I wanted to prove I could do it without them. Now I have."
	<br><br>
	
	<<He>> climbs to <<his>> feet. "I need to tell them. Right now." <<He>> disappears into the farmhouse.
	<br><br>
	
	<<link [[Next|Farm Stage 6]]>><</link>>
	<br>
	
	
<<elseif $hour is 21 and $farm_end is undefined>>
	<<set $farm_end to 1>>
	<<npc Alex>><<person1>>
	Alex wipes the sweat off <<his>> forehead as <<he>> approaches you. "Done for another day," <<he>> says, smiling.
	<<if $farm_count gte 1>>
		<<set $money += $farm.wage>>
		"Here's a little extra." <<He>> hands you <span class="gold">£<<print $farm.wage / 100>></span>.
	<</if>>
	<br><br>
	You walk down the road leading away from the farm.
	<br><br>
	<<link [[Next|Farmland]]>><<endevent>><</link>>
<<else>>
	<<if $farm_yield_alex gte 1>>
		<<npc Alex>><<person1>>
		<<if $NPCName[$NPCNameList.indexOf("Alex")].dom gte 30>>
			"Hey," shouts Alex, striding towards you. "We've had a better yield than expected lately," <<he>> says, grasping your hand in <<his>> own and pressing something into your palm. <span class="pink">"Keep at it. You'll make a fine farmer someday."</span>
			<br><br>
			
			<<He>> marches away. <span class="gold">You've earned £<<print ($farm_yield_alex * 9)>>.</span><<set $money += ($farm_yield_alex * 900)>>
			<br><br>
		<<elseif $NPCName[$NPCNameList.indexOf("Alex")].dom lte -30>>
			"Hey," shouts Alex, striding towards you. "We've had a better yield than expected lately," <<he>> says, grasping your hand in <<his>> own and pressing something into your palm. <span class="teal">"Dunno what I'd do without you, so this is fair."</span>
			<br><br>
			
			<<He>> marches away. <span class="gold">You've earned £<<print ($farm_yield_alex * 24)>>.</span><<set $money += ($farm_yield_alex * 2400)>>
			<br><br>
		<<else>>
			"Hey," shouts Alex, striding towards you. "We've had a better yield than expected lately," <<he>> says, grasping your hand in <<his>> own and pressing something into your palm. <span class="blue">"Your doing, so this is fair. You've a way with the animals."</span>
			<br><br>
			
			<<He>> marches away. <span class="gold">You've earned £<<print ($farm_yield_alex * 18)>>.</span><<set $money += ($farm_yield_alex * 1800)>>
			<br><br>
		<</if>>
		
		
		
		
		<<link [[Next|Farm Work]]>><<endevent>><</link>>
		<br>
		
		<<unset $farm_yield_alex>>
	<<elseif $farm_work.recent_shift gte $farm_work.event_time and $farm_event isnot 1>>
		<<set $farm_event to 1>>
		<<eventsfarm>>
	<<else>>
		
		
		
		<<farm_work>>
		
		<<link [[Leave|Farmland]]>><</link>>
		<br>
	<</if>>
<</if>>