<<set $outside to 0>><<set $location to "pool">><<schooleffects>><<effects>>

You mock the teacher. "Sorry for interrupting <<sirstop>> I just wanted to see you in
<<if $pronoun is "f">>that tight swimsuit<<else>>those tight shorts<</if>>." A few of the students laugh, others start blushing.
<br><br>

Mason also blushes. "I-I'll pretend I didn't hear that." <<He>> points to the changing rooms, before quickly looking away from you.
<<gdelinquency>>
<br><br>
<<detention 1>><<npcincr Mason love -1>>
<<link [[Boy's changing room|School Boy Changing Room]]>><<endevent>><</link>>
<br>
<<link [[Girl's changing room|School Girl Changing Room]]>><<endevent>><</link>>
<br>