<<set $outside to 1>><<set $location to "cabin">><<effects>>

<<if $edenspring is 0>>

You pick up the net you assume is meant for this purpose and start fishing the debris from the spring. It's so messy you don't know where to start, but you leave the spring a little clearer than you found it.

<<elseif $edenspring is 1>>

You haul out the larger branches that have sunk to the bottom of the spring.

<<elseif $edenspring is 2>>

You use the net to guide the floating branches to the edge, where you can remove them with ease.

<<else>>

You scoop up the twigs with the net. When you're done, the spring is crystal clear.

<</if>>
<<physique 3>>
<br><br>

<<set $edenspring += 1>>
<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
<br>