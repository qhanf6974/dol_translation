<<effects>>

<<if $livestock_muzzle is 1>>
	<<He>> clips a leash to your collar, and <span class="purple">pulls a muzzle against your face.</span><<facewear 17>> <<He>> tugs the leash and pulls you along behind <<himstop>>
<<else>>
	<<He>> clips a leash to your collar, and pulls you along behind <<himstop>>
<</if>>
<br><br>

<<link [[Next|Livestock Job]]>><</link>>
<br>