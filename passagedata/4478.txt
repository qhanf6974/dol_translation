<<if $enemyhealth lte 0>>

The <<person1>><<person>> recoils and pain and tumbles from the stage, and the others back off in instinctive fear. <<tearful>> you jump over <<him>> and run for the stairs. The audience are so bemused they make no move to stop you. You climb the stairs as fast as you can, footsteps thundering beneath you. You emerge in the forest and run in a random direction, aware you are still being pursued.
<br><br>

<<clotheson>>
<<endcombat>>

A chill runs up your spine, a warning from some primal instinct. <span class="red">Something is hunting you.</span>
<<set $foresthunt to 1>>
<br><br>

<<link [[Next|Forest]]>><<set $forest to 40>><</link>>

<<elseif $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

A voice rings out. "We hope you enjoyed our product, but we need to ask you to vacate the stage." The crowd does as instructed.
<br><br>
You are led off the stage and back to your cell.

<<tearful>> you sit on the mattress.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Underground Cell]]>><</link>>

<</if>>