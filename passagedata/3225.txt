<<set $outside to 1>><<effects>>
<<if $phase lte 1>>
	You stay hidden.
	<br><br>
	The <<person3>><<persons>> struggles while <<his>> clothes are torn aside.
	<<if $phase is 0>>
		Just metres from where you hide, the <<person1>><<person>> and <<person2>><<person>> brutally rape the <<person3>><<personcomma>> jeering, laughing and taking pictures all the while.
		Between throating the <<if $pronoun is "m">>boy<<else>>girl<</if>>, beating <<himcomma>> and biting <<his>> <<if $pronoun is "m">>nipples<<else>>breasts<</if>>, it's clear they get off on the brutality.
		<br><br>
		By the time they cum and stagger away, you feel shaken and
		<<if $submissive gte 1150>>
			weirdly aroused. Somewhere in your mind, you can't help imagining it was you, powerless and vulnerable to their animal lust. It fills you with horror and... something else.
			<<gtrauma>><<garousal>><<trauma 3>><<stress 3>><<arousal 300>>
		<<else>>
			upset.
			<<gtrauma>><<gstress>><<trauma 5>><<stress 3>><<pain 4>>
		<</if>>
		<br><br>
	<<elseif $phase is 1>>
		You can't bear to watch, but can't close your ears to the sounds. For what feels like hours, you hide in the bushes, face buried in the ground, as just metres away a helpless <<if $pronoun is "m">>boy<<else>>girl<</if>> <<if $pronoun is $player.gender>>just like you <</if>>is brutally raped.
		<<pain 5>>
		<br><br>
	<</if>>
	The attackers tidy themselves up, sharing glances and satisfied smiles.
	They take some photos of the <<person3>><<personcomma>> broken and used at their feet.
	<br><br>
	"Aww, come on, can we keep <<himcomma>> please!" you hear the <<person2>><<person>> say.
	<br><br>
	The <<person1>><<person>> mutters something inaudible.
	<br><br>
	"One of those police collars," the <<person2>><<person>> replies. "That would do it."
	<br><br>
	They strut away, still chatting.
	<br><br>
	<<if $rng lte 80>>
		The <<person3>><<person>> lays prone on the floor, exposed and unresponsive.
		<br><br>
		<<if $pronoun is "m">>
			<<link [[Help Him (0:10)|StreetEx4 Choice]]>><<set $phase to 0>><</link>>
			<br>
			<<link [[Steal His Clothes|StreetEx4 Choice]]>><<set $phase to 1>><<lowerwear 6>><<underlowerwear 4>><<upperwear 5>><<set $worn.upper.integrity to 100>><<set $worn.lower.integrity to 50>><<set $worn.under_lower.integrity to 15>><</link>>
			<br>
		<<else>>
			<<link [[Help Her (0:10)|StreetEx4 Choice]]>><<set $phase to 0>><</link>>
			<br>
			<<link [[Steal Her Clothes|StreetEx4 Choice]]>><<set $phase to 1>><<lowerwear 7>><<underlowerwear 1>><<upperwear 5>><<set $worn.upper.integrity to 100>><<set $worn.lower.integrity to 50>><<set $worn.under_lower.integrity to 15>><</link>>
			<br>
		<</if>>
	<<else>>
		<<link [[Next|StreetEx4 Play]]>><</link>>
	<</if>>
<<elseif $phase is 2>>
	Best to get away while everyone is distracted. In your current situation you can't help.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<<elseif $phase is 3>>
	<<if $rng lte 30>>
		You launch into action. The <<person1>><<person>> is holding the <<person3>><<persons>> throat with one hand, while struggling to get <<person1>><<his>> underwear past <<his>> knees with the other, leaving <<him>> lined up for a running <<if $NPCList[1].penis isnot "none">>punt to the cock.<<else>>cunt-punt.<</if>> <<He>> curls up with a blood-curdling howl.
		<br><br>
		The <<person2>><<person>> looks up. The surprise in <<his>> eyes turns to fury. As <<he>> starts to rise, the crazed <<person3>><<person>> attacks from below, kicking <<person2>><<him>> in the groin, and clawing <<his>> face. <<He>> struggles free and limps away, leaving the <<person1>><<person>> in the fetal position, gasping.
		<br><br>
		The <<person3>><<person>> thanks you. Something in <<his>> eyes isn't right.
		<br><br>
		Without warning, <<he>> kicks the sobbing <<person1>><<person>> in the face, knocking <<him>> out cold. The <<person3>><<person>> strips the older <<person1>><<if $pronoun is "m">>man<<lowerwear 5>><<underlowerwear 4>><<upperwear 4>><<else>>woman<<underlowerwear 1>><<upperwear 1>><</if>> of <<his>> clothes and hands them to you.
		<br><br>
		"Thanks again."
		<br><br>
		<<endevent>>
		<<destinationeventend>>
	<<elseif $rng lte 80>>
		<<if $rng lte ($physique / 200)>>
			You launch into action. The <<person1>><<person>> is holding the <<person3>><<persons>> throat with one hand, while struggling to get <<person1>><<his>> underwear past <<his>> knees with the other, leaving <<him>> lined up for a running <<if $NPCList[1].penis isnot "none">>punt to the cock.<<else>>cunt-punt.<</if>> <<He>> curls up with a blood-curdling howl.
			<br><br>
			The <<person2>><<person>> looks upon just in time for a follow up kick to the face.
			You grab the <<person3>><<person>> and as many of <<his>> discarded and tattered clothes as possible and escape the attackers. You're not sure if the <<person>> really sees you, but <<he>> is coherent enough to keep up with you. After a little hunting, you find <<his>> phone in a pocket and call an ambulance and police for <<himstop>> When you see the flashing lights approach, you hide yourself and wait until you can move on.
			<br><br>
			Somewhere in the commotion, the <<person1>><<person>> and the <<person2>><<person>> got away.
			<<pass 10>><<famegood 5>><<famescrap 10>>
			<br><br>
			<<endevent>>
			<<destinationeventend>>
		<<else>>
			You launch into action. The <<person1>><<person>> holds the <<person3>><<persons>> throat with one hand, while struggling to get <<person1>><<his>> underwear past <<his>> knees with the other, leaving <<him>> lined up for a running <<if $NPCList[1].penis isnot "none">>punt to the cock.<<else>>cunt-punt.<</if>>
			Unfortunately, you miss and fall over.
			<br><br>
			By the time you are back on your feet, both attackers are closing on you.
			<br><br>
			<<link [[Next|StreetEx4 Rape]]>><<set $phase to 0>><<set $molestationstart to 1>><</link>>
		<</if>>
	<<else>>
		You launch into action. The <<person1>><<person>> holds the <<person3>><<persons>> throat with one hand, while struggling to get <<person1>><<his>> underwear past <<his>> knees with the other, leaving <<him>> lined up for a running <<if $NPCList[1].penis isnot "none">>punt to the cock.<<else>>cunt-punt.<</if>> <<He>> curls up with a blood-curdling howl.
		<br><br>
		The <<person2>><<person>> looks up. The surprise in <<his>> eyes turns to fury. As <<he>> starts to rise, <<he>> trips over the <<person3>><<person>> and falls into a bush, giving you a chance to grab the <<person>> and escape with <<himstop>>
		<br><br>
		Just as you think you've got away, the <<person>> <span class="red">sticks <<his>> leg into yours,</span> tripping you and sending you sprawling face first into a tree.
		<br><br>
		Dazed on the ground, you see <<him>> standing over you. "You're <<if $beauty gte ($beautymax / 7) * 4>>beautiful,<<elseif $beauty lte ($beautymax / 7) * 2>>okay looking,<<else>>pretty,<</if>>" the <<person>> says. "Don't run. My friends'll want to play with you."
		<br>
		...
		<br><br>
		By the time you know what is happening, they have you. One has your arms, while the <<person>> cradles your face, looking into your eyes. "It's bad to poke your nose into other people's business," <<he>> says. "You hurt my friend."
		<br><br>
		<<person1>>"Think you can run around booting <<if $pronoun is "m">>guys in the balls<<else>>girls' pussies<</if>>, huh?" the <<person>> growls in your ear.
		<br><br>
		<<person3>>"Now they're going to hurt you," the <<person>> says. "But don't worry, it'll be painful and degrading, but only as much as sluts like <<if $pronoun is $player.gender>>us<<else>>you<</if>> deserve. And I'll be here with you."
		<br><br>
		<<link [[Next|StreetEx4 Rape]]>><<set $phase to 1>><<set $molestationstart to 1>><</link>>
	<</if>>
<</if>>