<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewersalgae">>
<<set $sewersevent -= 1>>
You are in the old sewers. You can't walk too fast, lest you slip on the algae covering the walls and floor.
<br><br>
<<if $smuggler_location is "sewer" and $smuggler_timer is 0 and $daystate is "night" and $hour gte 18>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		There's a fire burning beside a stool. Looks like someone left in a hurry. <span class="gold">Perhaps they're just hiding from Morgan.</span>
		<br><br>
	<<else>>
		<<smugglerdifficultytext>>
	<</if>>
<</if>>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
		<<link [[Hide in the algae|Sewers Algae Hide]]>><</link>>
		<br>
	<</if>>
	<<if $smuggler_location is "sewer" and $smuggler_timer is 0 and $daystate is "night" and $hour gte 18>>
		<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<else>>
			<<smugglerdifficultyactions>>
		<</if>>
	<</if>>
	<<link [[Muddy Tunnel (0:05)|Sewers Mud]]>><<pass 5>><</link>>
	<br>
	<<link [[Tunnel filled with detritus (0:05)|Sewers Industrial]]>><<pass 5>><</link>>
	<br><br>
<</if>>