<<effects>>

<<if $stall_seduced is 0>><<set $seductiondifficulty to 2000>>
	"Going so soon?" you coo. The <<person>> turns around. You lean on your elbows, looking <<him>> in the eye. "We were just getting to know each other."
	<<promiscuity1>>
<<elseif $stall_seduced is 1>><<set $seductiondifficulty to 4000>>
	"I think you forgot your change," you say. That gets <<his>> attention.
	<<if $worn.upper.type.includes("naked")>>
		<<He>> turns to find your back to <<himcomma>> bent over and wiggling your <<bottom>> in the air. You peek over your shoulder to see if it's having the desired effect.
	<<else>>
		<<He>> turns to find you leaning forward, your top hanging loose. <<He>> can see right down it.
		<<if $player.gender_appearance is "f" and $worn.under_upper.type.includes("naked")>>
			Your lack of a bra makes this even better.
		<</if>>
	<</if>>
	<<exhibitionism1>>
	"My mistake," you coo.
	<br><br>
<<elseif $stall_seduced is 2>><<set $seductiondifficulty to 6000>>
	"Thank you so much for your business," you say, "but you forgot something." <<He>> returns to the table. You clasp <<his>> hands in your own, lean forward, and plant a kiss on <<his>> cheek.
	<<promiscuity2>>
<<else>>
	<<if $worn.under_lower.type.includes("naked")>><<set $seductiondifficulty to 8000>>
		"I love attending this stall," you say. "No one knows I'm naked below the waist."
		<<exhibitionism3>>

	<<else>><<set $seductiondifficulty to 10000>>
		"I love attending this stall," you say. "No one knows I'm only wearing underwear below my waist."
		<<exhibitionism2>>
	<</if>>
<</if>>

<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>>
<span class="gold">You feel more confident in your powers of seduction.</span>
<br><br>
<</if>>
<<seductionskilluse>>

<<if $seductionrating gte $seductionrequired>>
	<<set $stall_seduced += 1>>
	<<He>> blushes and looks away, <span class="green">but no longer seems keen on leaving.</span>
	<<set $enemyarousal += 20>>
	<br><br>

	<<set $stall_plant to $plant_inventory.random()>>
	<<set $stall_amount to random(11, 30)>>
	<<if $stall_amount gte $plants[$stall_plant].amount>>
		<<set $stall_amount to clone($plants[$stall_plant].amount)>>
	<</if>>
	<<set $stall_cost to Math.round(clone(setup.plants[$stall_plant].plant_cost))>>

	<<He>> walks back to your stand and stops in front of the <<print setup.plants[$stall_plant].plural>>. H-how much for $stall_amount of these?
	<br><br>

	<<stall_trust>>
	<br><br>

	<<stall_sell_actions>>

<<else>>

	<<He>> blushes, but turns around <span class="red">and continues on <<his>> way.</span>
	<br><br>

	<<stall_actions>>

<</if>>