<<effects>>

<<if $finish is 1>>

The pig trots behind the stage as Briar storms onto it.<<endevent>><<npc Briar>><<person1>>"Why are you stopping?"<<He>> glares at you, then seems to remember where <<he>> is. <<He>> smiles at the audience. "Apologies my friends, but that will be all for today." <<He>> ushers you off the stage.
<br><br>

<<clotheson>>
<<endcombat>>

<<npc Briar>><<person1>>"Unprofessional," <<he>> says once you're off the stage and out of sight. "Don't expect payment."
<<npcincr Briar love -1>>
<br><br>

<<elseif $enemyarousal gte $enemyarousalmax>>
<<famebestiality 20>>
<<beastejaculation>>

Someone behind the stage tugs on the leash and the pig walks off. <<tearful>> you climb to your feet, bow, and follow.
<br><br>

<<clotheson>>
<<endcombat>>

<<npc Briar>><<generate2>><<person1>>Briar's waiting for you. "Nice work," <<he>> says. A <<person2>><<person>> struggles to stuff the pig back in its cage beside <<person1>><<himstop>> <<He>> hands you the £1500 <<he>> promised.
<<set $money += 150000>><<npcincr Briar love 1>>
<br><br>

<<elseif $enemyhealth lte 0>>

The pig runs away from you in fright. The audience cheers and you follow it off the stage.
<br><br>

<<clotheson>>
<<endcombat>>

<<npc Briar>><<person1>>Briar waits for you beside the pig, which hides in its cage. "They weren't expecting to see a <<girl>> fight a pig," <<he>> says. "They seemed to enjoy the spectacle." <<He>> hands you the £1500 <<he>> promised.
<<set $money += 150000>><<npcincr Briar love 1>>
<br><br>

<</if>>
<<endevent>>
You return to the dressing room.
<br><br>

<<link [[Next|Brothel Dressing Room]]>><</link>>