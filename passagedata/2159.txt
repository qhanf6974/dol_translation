<<set $outside to 1>><<set $location to "hospital">><<effects>>

You are in front of the hospital. It is open at all hours.
<br><br>

<<if $stress gte 10000>>
<<passouthospital>>
<<else>>
<<if $exposed gte 1>>
You are hiding in the bushes just outside the entrance. You should be able to enter without being seen if you time it right.
<br><br>
<</if>>

<<link [[Go inside hospital (0:01)->Hospital Foyer]]>><<pass 1>><</link>>
<br>
<<link [[Nightingale Street (0:01)->Nightingale Street]]>><<pass 1>><</link>>
<br>

<</if>>