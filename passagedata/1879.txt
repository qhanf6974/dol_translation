<<set $outside to 0>><<set $location to "pub">><<dockeffects>><<effects>>

The <<person>> grasps your neck and leans in. "I tried asking nicely," <<he>> whispers. "Now how abouts you come with me, and I won't need to hurt you."
<<gtrauma>><<gstress>><<trauma 1>><<stress 1>>
<br><br>

<<if $submissive lte 500>>

"You're gonna regret touching me," you say. You smash your glass over the <<persons>> head. <<He>> tries to dodge the blow, but loses balance and falls off <<his>> chair. Your colleagues cheer.
<br><br>

"Unprovoked!" the <<person>> says as <<he>> nurses <<his>> scalp.
<br><br>

The pub owner rushes over. <<generate2>><<person2>> "Last thing I need is a brawl," <<he>> says. "You dockers always bring trouble. Get out."
<br><br>

"That was self defence," you argue, but <<he>> won't hear it. Your colleagues down the last of their drink and together you leave the pub. They don't seem to mind.
<<gcool>><<dockstatus 1>>
<br><br>

<<else>>

One of your colleagues notices, and smashes a glass over the <<persons>> head. <<He>> tries to dodge the blow, but loses balance and falls off <<his>> chair.
<br><br>

"Unprovoked!" the <<person>> says as <<he>> nurses <<his>> scalp.
<br><br>

The pub owner rushes over. <<generate2>><<person2>> "Last thing I need is a brawl," <<he>> says. "You dockers always bring trouble. Get out."
<br><br>

Your colleagues argue that it was justified, but <<he>> won't hear it. They down the last of their drink and together you leave the pub.
<br><br>

<</if>>

<<dockpuboptions>>