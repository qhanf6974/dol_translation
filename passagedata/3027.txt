<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
You hiss at the cat. It backs away from you, it's ears turned back. You stare at it and unleash your deepest meow. It growls back, but its face relaxes. It leaps onto the windowsill and disappears behind it.
<br><br>
<<if $bus is "boys">>
	<<link [[Next|School Boy's Toilets]]>><<set $eventskip to 1>><</link>>
	<br>
<<else>>
	<<link [[Next|School Girl's Toilets]]>><<set $eventskip to 1>><</link>>
	<br>
<</if>>