<<effects>>
<<endevent>><<npc Avery>><<person1>>
<<averyscore>>
<<He>> opens the car door beside <<himstop>> "Hop in."
<br><br>
<<He>> opens <<his>> heavy <<wallet>> and thumbing through the cash. <<He>> holds out <span class="gold">£<<print ($NPCName[$NPCNameList.indexOf("Avery")].love * 10 + $endear * 10 + 10)>></span>. "A little something for your time."
<br><br>
You reach for the money, but <<he>> lifts it away from you. "First things first," <<he>> says. <<He>> pulls your body close, moving in for a kiss.
<br><br>
<<link [[Kiss|Avery Date Sex]]>><<set $sexstart to 1>><</link>>
<br>
<<link [[Refuse|Avery Date Refuse 2]]>><<npcincr Avery rage 5>><<npcincr Avery love -1>><</link>><<garage>><<llove>>
<br>