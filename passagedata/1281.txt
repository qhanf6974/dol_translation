<<set $outside to 1>><<set $location to "farm">><<effects>>

You are at Remy's riding school. Stables surround a central courtyard. A small office sits to one side. A dirt track leads into the surrounding countryside. Lessons cost £25.
<br><br>

<<if $daystate isnot "night" and $hour isnot $closinghour>>
	Several people chat in the courtyard, waiting for the next lesson to begin.
	<br><br>
<</if>>

<<if $stress gte 10000>>
	<<passoutfarmroad>>
<<elseif $hour is $closinghour>>
	It's closing time. The horses are being herded into their stables, and the customers from the grounds. <<if $exposed gte 1>>You skulk away before anyone sees your <<lewdness>>.<</if>>
	<br><br>

	<<link [[Next|Farmland]]>><</link>>
	<br>
<<elseif $daystate is "night">>
	<<if $hour isnot $closinghour and $ridingschooltheft isnot 1>>
		<<link [[Examine the office building|Riding School Register]]>><</link>>
		<br>
	<</if>>

	<<link [[Leave|Farmland]]>><</link>>
	<br>
<<else>>
	<<if $ridingschoolintro is undefined>>
		<<set $ridingschoolintro to 0>>
		<<if $remy_seen is "livestock">>
			<<npc Remy>><<person1>>A horse trots up to you. Upon its back sits Remy, <<if $pronoun is "m">><<his>> pale brown hair flicking in the breeze<<else>><<his>> pale brown hair tied in a neat bun<</if>>. You tense as memories of your treatment at <<his>> farm invade your mind. However, there's no domineering malice in <<his>> eyes this time. There's no recognition at all.
			<br><br>

			"Welcome to my riding school," <<he>> says as the horse comes to a stop. "I'm Remy. We offer lessons on both riding and caring for horses. I hope we can be of service."
			<br><br>

			<<He>> spurs <<his>> horse to a trot, and rides away.
			<br><br>

			<<link [[Next|Riding School]]>><<endevent>><</link>>
			<br>
		<<elseif $remy_seen is "farm">>
			<<npc Remy>><<person1>>A horse trots up to you. Upon its back sits Remy, <<if $pronoun is "m">><<his>> short, pale brown hair flicking in the breeze<<else>><<his>> pale brown hair tied in a neat bun<</if>>.
			<br><br>
			"Welcome to my riding school," <<he>> says as the horse comes to a stop. "I'm glad you could make it. We offer lessons on both riding and caring for horses. I hope we can be of service."
			<br><br>

			<<He>> spurs <<his>> horse to a trot, and rides away.
			<br><br>

			<<link [[Next|Riding School]]>><<endevent>><</link>>
			<br>
		<<else>>
			<<npc Remy>><<person1>>A horse trots up to you. Upon its back sits a <<if $pronoun is "m">>man with short, pale brown hair flicking in the breeze<<else>>woman with pale brown hair tied in a neat bun<</if>>.
			<br><br>

			"Welcome to my riding school," <<he>> says as the horse comes to a stop. "I'm Remy. We offer lessons on both riding and caring for horses. I hope we can be of service."
			<br><br>

			<<He>> spurs <<his>> horse to a trot, and rides away.
			<br><br>

			<<link [[Next|Riding School]]>><<endevent>><</link>>
			<br>
		<</if>>
		<<set $remy_seen to "riding_school">>
	<<elseif $remy_seen is "livestock">>
		<<npc Remy>><<person1>>A horse trots up to you. Upon its back sits Remy, <<if $pronoun is "m">><<his>> pale brown hair flicking in the breeze<<else>><<his>> pale brown hair tied in a neat bun<</if>>. You tense as memories of your treatment at <<his>> farm invade your mind. However, there's no domineering malice in <<his>> eyes this time. There's no recognition at all.
		<br><br>

		"It's good to see you again," <<he>> says as the horse comes to a stop. "I hope we can be of service."
		<br><br>

		<<He>> spurs <<his>> horse to a trot, and rides away.
		<br><br>

		<<link [[Next|Riding School]]>><<endevent>><</link>>
		<br>
		<<set $remy_seen to "riding_school">>
	<<else>>

		<<if $money gte 2500>>
			<<link [[Take riding Lesson (1:00)|Riding School Lesson]]>><<set $riding_lesson to 0>><<set $money -= 2500>><<set $thighskill += 9>><</link>><<gtiredness>> | <span class="green">+ Thigh skill</span>
			<br>
		<</if>>

		<<link [[Leave|Farmland]]>><</link>>
		<br>
	<</if>>
<</if>>