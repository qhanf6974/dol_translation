<<set $outside to 0>><<set $location to "town">><<effects>>
The two of you leave the theatre, hand-in-sticky-hand. Robin clings to you tight on the walk home. You can still smell the sugary substance, and you feel an urge to clean <<himstop>> Your cat ears twitch.
<br><br>
Robin breaks the silence as you pass through the park. "Can we rest for a moment?" You nod, following the path until you come across a bench.
<br><br>

Robin pulls <<his>>shirt away from <<his>> skin, before letting it fall back into place. "I'm really sticky. Look." <<he>> holds out <<his>> hand to you.
<br><br>

<<link [[Pull closer|Robin Cat Walk]]>><</link>>
<br>
<<link [[Walk home|Robin Cinema Wet Walk]]>><</link>>
<br>