<<effects>><<set $location to "sewers">><<set $outside to 0>>
<<effects>>
You crouch and try to pick the lock.
<br><br>
<<if $sewerschasedstep gt 0>>
	<<skulduggerycheck>>
<</if>>
<<if $sewerschasedstep lte 0>>
	<<npc Morgan>><<person1>>"Oh, you poor little thing. This simply won't do," Morgan says, stepping out of the shadows. <<He>> twists a pin at the base of the shackle, freeing you. "Now <<charlescomma>> don't make things harder on yourself than they need to be." <<He>> grasps your arm and pulls.
	<br><br>
	<<set $sewerschased to 0>>
	<<link [[Go quietly|Sewers Return]]>><</link>>
	<br>
	<<link [[Fight|Sewers Fight]]>><<set $fightstart to 1>><</link>>
	<br>
<<elseif $skulduggerysuccess is 1>>
	You unlock the shackle and run.
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	You hear Morgan screech, and the clang of metal soon after.
	<br><br>
	<<set $sewerschased to 0>>
	<<destinationsewers>>
<<else>>
	Your fingers fumble under the pressure. <<morganhunt>>
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	<<link [[Struggle|Sewers Shackle Struggle]]>><</link>>
	<<if $phase is 1>>
		<<physiquedifficulty 6000 16000>>
	<<elseif $phase gte 2>>
		<<physiquedifficulty 1 10000>>
	<<else>>
		<<physiquedifficulty 10000 20000>>
	<</if>>
	<br>
	<<set $skulduggerydifficulty to 600>>
	<<link [[Pick the lock|Sewers Shackle Pick]]>><</link>><<skulduggerydifficulty>>
	<br>
<</if>>