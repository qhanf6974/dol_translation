<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
<<generate1>><<person1>>
You sneak to a nurse. "Excuse me," you say. <<He>> turns, and puts a hand to <<his>> mouth to conceal laughter when <<he>> sees you. <<covered>> "Can I have some towels please?"
<br><br>
<<He>> nods, and wraps some around you.
<<towelup>><<fameexhibitionism 1>>
<br><br>
<<link [[Next|Asylum]]>><<endevent>><</link>>
<br>