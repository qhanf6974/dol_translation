<<effects>>

<<if $submissive gte 1150>>
"N-no," you say. "I'd like something else."
<<elseif $submissive lte 850>>
"I didn't ask for that," you say. "Get me a drinks menu."
<<else>>
"No thank you," you say. "I'd like something else to drink."
<</if>>
<br><br>

The <<person2>><<person>> looks to Avery for confirmation. Avery frowns, but nods. <<He>> fetches a drinks menu and you choose a sweet soft drink.
<br><br>
<<person1>>

The <<person2>><<person>> returns once more, this time with the main menu. Just the one. <<He>> hands it to Avery. <<person1>>"I know what you'll like," Avery says. <<if $averyragerevealed is 1>>"Something that preserves your figure."<</if>>
<br><br>

<<link [[Let Avery order for you|Avery Date Order]]>><<set $phase to 0>><<npcincr Avery love 1>><<set $endear += 10>><</link>><<glove>><<gendear>>
<br>
<<link [[Choose something cheap|Avery Date Cheap]]>><<set $phase to 1>><<stress -6>><<set $endear -= 5>><<npcincr Avery rage 5>><</link>><<garage>><<lendear>><<lstress>>
<br>
<<link [[Choose something expensive|Avery Date Expensive]]>><<set $phase to 2>><<set $endear -= 10>><<npcincr Avery rage 5>><<npcincr Avery love -1>><<stress -18>><</link>><<llove>><<garage>><<llendear>><<llstress>>
<br>