<<set $outside to 0>><<set $location to "arcade">><<effects>>

Having a second player does help, and you make it to the end of the game. <<He>> leaves without a word.
<br><br>
<<endevent>>
<<if $money gte 500 and $daystate isnot "night">>
	<<link [[Keep playing (£5 0:20)|Arcade Play]]>><<set $money -= 500>><<pass 20>><<stress -2>><</link>><<lstress>>
	<br>
<</if>>
	<<link [[Stop|Arcade]]>><</link>>
	<br>