<<set $outside to 0>><<set $location to "home">><<effects>>
<<npc Robin>><<person1>>
<<if $halloween_robin_costume is "witch">>
	You approach Robin. <<He>> peeks at you beneath the witch hat.
<<elseif $halloween_robin_costume is "vampire">>
	You approach Robin. <<He>> peeks at you over the vampire collar.
<<else>>
	You approach the person dressed as a ghost. You hear Robin's voice beneath the sheet. Two holes are cut so <<he>> can see. <<He>> turns to you.
<</if>>
<<if $NPCName[$NPCNameList.indexOf("Robin")].pronoun is "m" and $halloween_robin_costume is "witch">>
	<<His>> locks hang around the brim, and the dress hints at feminine contours. If you didn't know <<him>> you wouldn't guess <<he>> was a boy.
<<elseif $NPCName[$NPCNameList.indexOf("Robin")].pronoun is "f" and $halloween_robin_costume is "vampire">>
	<<His>> locks disappear into the tall collar, hiding their length. If you didn't know <<him>> you wouldn't guess <<he>> was a girl.
<</if>>
<br><br>
<<if $worn.upper.name is "vampire jacket">>
	<<if $halloween_robin_costume is "vampire">>
		<<He>> looks at your clothes, then at <<his>> own. <<He>> gasps in realisation. "Our costumes match!" <<he>> shouts, jumping with excitement and startling the orphans. "Sorry everyone." <<He>> turns back to you. "Are you coming trick-or-treating with us? We can be twin vampires."
	<<else>>
		"I love your costume," <<he>> says, turning to the orphans. "I hope <<pshe>> doesn't suck my blood!" The orphans laugh. <<He>> turns back to you. "Are you coming trick-or-treating with us?"
	<</if>>
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $worn.upper.name is "witch dress" or $worn.lower.name is "witch skirt">>
	<<if $halloween_robin_costume is "witch">>
		<<He>> looks at your clothes, then at <<his>> own. <<He>> gasps in realisation. "Our costumes match!" <<he>> shouts, jumping with excitement and startling the orphans. "Sorry everyone." <<He>> turns back to you. "Are you coming trick-or-treating with us? We can be twin witches."
	<<else>>
		"I love your costume," <<he>> says, turning to the orphans. "I hope <<pshe>> doesn't turn me into a newt!" The orphans laugh. <<He>> turns back to you. "Are you coming trick-or-treating with us?"
	<</if>>
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $worn.upper.name is "nun's habit" or $worn.lower.name is "nun's habit skirt">>
	"I love your costume," <<he>> says. "Nuns can be very scary.
	<<if $angel gte 6 or $fallenangel gte 2>>
		The wings and halo are a nice touch.
	<<else>>
	<</if>>
	Are you coming trick-or-treating with us?"
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $worn.upper.name is "maid dress" or $worn.lower.name is "maid skirt">>
	"I love your costume," <<he>> says. "Maids are very cute. Are you coming trick-or-treating with us?"
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $worn.upper.type.includes("costume") or $worn.lower.type.includes("costume") or $worn.upper.type.includes("naked") and $worn.under_upper.type.includes("costume") or $worn.lower.type.includes("naked") and $worn.under_lower.type.includes("costume")>>
	"I love your costume," <<he>> says. "It's very unique. Are you coming trick-or-treating with us?"
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $angel gte 6>>
	"I love your costume," <<he>> says. "It really looks like the halo is hovering. Are you coming trick-or-treating with us?"
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $wolfgirl gte 6>>
	"I love your costume," <<he>> says. "The ears look so authentic. Are you coming trick-or-treating with us?"
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $fallenangel gte 2>>
	"I love your costume," <<he>> says. "It really looks like the different bits of halo are hovering. Are you coming trick-or-treating with us?"
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $demon gte 6>>
	"I love your costume," <<he>> says. "Make sure your tail doesn't fall off. Are you coming trick-or-treating with us?"
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $cat gte 6>>
	"I love your costume," <<he>> says. "It makes me want to pet you. Are you coming trick-or-treating with us?"
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<elseif $cow gte 6>>
	"I love your costume," <<he>> says. "I bet your milk is tasty!" <<He>> seems oblivious to how lewd that statement is. "Are you coming trick-or-treating with us?"
	<br><br>
	<<link [[Go trick-or-treating (3:00)|Robin Trick 1]]>><<pass 30>><</link>>
	<br>
	<<link [[Leave|Orphanage]]>><<endevent>><</link>>
	<br>
<<else>>
	"We're about to go trick-or-treating," <<he>> says. "Would you like to come with us? You'll need a costume too, or it won't be proper."
	<br><br>
	<<link [[Next|Orphanage]]>><<endevent>><</link>>
	<br>
<</if>>