<<set $outside to 1>><<set $location to "forest">><<effects>>

<<if $syndromewolves gte 1>>

You throw your head back and howl into the canopy. The <<if $phase is 1>>wolves join<<else>>wolf joins<</if>> in, and you're answered by others in the distance. After a couple of minutes the howling dies down, and the <<if $phase is 1>>wolves step<<else>>wolf steps<</if>> into view.
<br><br>

	<<if $phase is 1>>
		<<if $deviancy gte 35 and $bestialitydisable is "f">>
		<<link [[Mate|Forest Wolf Sex2]]>><<set $sexstart to 1>><</link>><<deviant3>>
		<br>
		<</if>>
	<<else>>
		<<if $deviancy gte 15 and $bestialitydisable is "f">>
		<<link [[Mate|Forest Wolf Sex]]>><<set $sexstart to 1>><</link>><<deviant2>>
		<br>
		<</if>>
	<</if>>
<<link [[Pat and depart|Forest Pat]]>><<trauma -6>><<stress -12>><</link>><<ltrauma>><<lstress>>
<br>

<<else>>

You throw your head back and howl into the canopy. The <<if $phase is 1>>wolves join<<else>>wolf joins<</if>> in, and you're answered by others in the distance. After a couple of minutes the howling dies down. The <<if $phase is 1>>wolves depart.<<else>>wolf departs.<</if>>
<br><br>

<<link [[Next|Forest]]>><<endevent>><<set $eventskip to 1>><</link>>
<br>

<</if>>