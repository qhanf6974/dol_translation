<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>><<npcincr Kylar lust -20>>

<<He>> moans into your ear, then falls limp. <<He>> hangs from you for a few moments. <<He>> rubs <<his>> nose against yours and stares into your eyes. "We're going to do this every day," <<he>> says. "Forever."

<<if $mouthuse isnot "gagged">><<He>> kisses your mouth.<<else>><<He>> kisses your forehead.<</if>>
<br><br>

<<else>>

You knock <<him>> away from you. "Y-you're v-very kinky," <<he>> stammers. "N-not so rough next time."
<br><br>

<</if>>

<<npc Kylar>><<person1>>
<<He>> walks across the basement without bothering to fix <<his>> clothing. <<He>> hefts a fire axe from the wall. "When you're my <<if $player.gender_appearance is "m">>husband<<else>>wife<</if>>," <<he>> says. "I'll protect you. I'll find everyone who ever looked at you and crack them open." <<He>> laughs. "Then it will be just us."
<br><br>

<<if $mouthuse isnot "gagged">>
<<link [[Plead|Kylar Basement 4]]>><<set $phase to 1>><</link>>
<br>
<<link [[Get angry|Kylar Basement 4]]>><<set $phase to 2>><</link>>
<br>
<</if>>

<<link [[Remain silent|Kylar Basement 4]]>><<set $phase to 0>><</link>>
<br>