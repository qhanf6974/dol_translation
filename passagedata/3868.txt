<<set $outside to 0>><<effects>>

<<endevent>><<npc Robin>><<person1>>
You bow your head and climb into the back of Avery's car. Robin watches you from the pavement, the worry on <<his>> face clear even at a distance.<<endevent>><<npc Avery>><<person1>>
<br><br>

Avery drives past the school, and into a nearby alleyway. <<He>> unbuckles <<his>> seatbelt and climbs into the back with you. <<He>> pushes you down. "Don't be difficult," <<he>> says. "Or I'll be late for work."
<br><br>

<<link [[Next|Avery Walk Rape]]>><<set $molestationstart to 1>><</link>>
<br>