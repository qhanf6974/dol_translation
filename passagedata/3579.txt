<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
You stay still. The <<person1>><<monk>> and <<his>> friend soon catch up. Someone grasps your shoulders and pulls you backwards. "Almost had a tumble there," the <<monk>> says. "Good thing you have us to look after you." You feel <<his>> breath on your neck, sending a shiver down your spine.
<br><br>
<<link [[Next|Temple Garden Rape]]>><<set $molestationstart to 1>><</link>>
<br>