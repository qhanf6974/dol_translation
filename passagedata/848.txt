<<effects>>

<<if $submissive gte 1150>>
	"I-I can't outrun a boar," you say.
<<elseif $submissive lte 850>>
	"You thought we could outrun a boar?" you say. "Maybe if it was crippled. And asleep."
<<else>>
	"We couldn't outrun a boar," you say.
<</if>>
<br><br>

"So your solution was to do nothing?" <<he>> replies, grasping your arm and pulling <<himself>> to <<his>> feet. <<He>> sighs. "Sorry. Let's just get back to work."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>