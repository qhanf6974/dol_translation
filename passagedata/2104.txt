<<effects>>
You glare at the <<person1>><<person>> and <<person2>><<person>> in turn, and don't look away when they look back. The pair laugh.
<br><br>
"I think <<pshes>> got some thoughts of <<pher>> own," the <<person1>><<person>> says as <<he>> reaches forward and rips the gag from your mouth.
<br><br>
<<set $worn.face.type.push("broken")>>
<<faceruined>>
<<link [[Ask to be let go|Sold Ask]]>><</link>>
<br>
<<link [[Say you'll cooperate|Sold Cooperate]]>><<sub 1>><</link>>
<br>
<<link [[Scream|Sold Scream]]>><</link>>
<br>