<<set $outside to 1>><<set $location to "temple">><<temple_effects>><<effects>>
You lie prone and shuffle towards the line, conscious of your <<nuditystop>>
<br><br>
<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
	You crawl next to the pair. The <<persons>> arm reaches behind <<himcomma>> and pads the ground as if searching for something. A trowel lies nearby, so you lift it and place the handle in <<his>> hand. <span class="green"><<He>> returns to gardening,</span> oblivious to your presence.
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	You crawl beneath the habit and tug it off the line. You won't be able to dress like this, so you crawl back to the safety of the hedges.
	<br><br>
	<<upperwear 13>>
	<<endevent>>
	<<link [[Next|Temple Garden]]>><</link>>
	<br>
<<else>>
	You crawl next to the pair. The <<person1>><<persons>> arm reaches behind <<himcomma>> and pads the ground as if searching for something. <span class="red"><<His>> hand finds your <<bottom>> instead.</span> <<He>> leaps to <<his>> feet with a shriek. <<He>> and the <<person2>><<person>> run toward the temple.
	<<llgrace>><<grace -5>><<garousal>><<arousal 600>>
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	You dash to the habit, snatch it off the line, and rush back to the safety of the hedges.
	<br><br>
	<<upperwear 13>>
	<<endevent>>
	<<link [[Next|Temple Garden]]>><</link>>
	<br>
<</if>>