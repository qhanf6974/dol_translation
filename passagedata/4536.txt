<<set $outside to 1>><<set $location to "town">><<effects>>
You <<nervously>> line up beside the mannequins. Hoping to blend in. <<generate1>><<generate2>>A <<person1>><<person>> and <<person2>><<person>> round the corner. The <<person1>><<person>> stares at your <<lewdness>> as <<he>> walks by. "That one looks a little... accurate." You feel self-conscious, but dare not move an inch.
<br><br>
<<endevent>>
<<generate1>><<generate2>>Before the first pair leave view, a <<person1>><<person>> and <<person2>><<person>> pass them by, heading your way. The <<person1>><<person>> stops in front of you. "Look at this one. It's so lifelike. Do you think anyone would notice if we took it?"
<br><br>
<<if $rng gte 81>>
	The <<person2>><<person>> looks up and down the alley. "Alright. But we need to be quick." <<He>> grabs you by the ankles and tries to lift you. "Why is it so heavy? Give us a hand here," the <<person1>><<person>> moves in to help. This isn't what you planned, but you don't know how they'll react if they find out the truth.
	<br><br>
	<<link [[Struggle|Commercial Ex Mannequin Molestation]]>><<set $molestationstart to 1>><</link>>
	<br>
	<<link [[Keep playing the part|Commercial Ex Mannequin 2]]>><</link>>
	<br>
<<else>>
	"Don't be stupid," the <<person2>><<person>> responds without stopping. The <<person1>><<person>> gives you a longing look, then follows.
	<br><br>
	Once they're out of sight, you're free to continue.
	<<gstress>><<garousal>><<stress 2>><<arousal 200>>
	<br><br>
	<<endevent>>
	<<commercialeventend>>
<</if>>