<<effects>>

<<set $seductiondifficulty to (10000 - ($NPCName[$NPCNameList.indexOf("Eden")].lust * 50))>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>>
<br><br>

You climb to your knees.
<<if $submissive gte 1150>>
"I can make it up to you!" you say.
<<elseif $submissive lte 850>>
"I think I know what you need," you say.
<<else>>
"Let me make it up to you," you say.
<</if>>

<<if $pronoun is "f">>

You lift up <<his>> skirt.
<<promiscuity1>>
<br><br>

<<else>>

You open <<his>> fly.
<<promiscuity1>>
<br><br>

<</if>>

<<if $seductionrating gte $seductionrequired>>

"This is a good way to start making it up to me," <<he>> says, pressing a hand against the back of your head and pushing you closer.
<br><br>

<<link [[Next|Eden Recaptured Oral]]>><<set $sexstart to 1>><</link>>
<br>

<<else>>

<<He>> shoves you back to the ground. "Oh no, you're not getting off that easy."
<br><br>

<<link [[Next|Eden Recaptured Spank]]>><<set $molestationstart to 1>><</link>>
<br>

<</if>>