<<set $outside to 0>><<set $location to "docks">><<dockeffects>><<effects>>

<<if $phase is 0>>

You work hard. It's tiring.
<<physique 6>>
<br><br>

<<if $upperwetstage gte 3 or $lowerwetstage gte 3>>
You feel the other dockers ogling your body through your wet clothing.
<<gstress>><<garousal>><<stress 6>><<arousal 600>>
<br><br>
<</if>>

<<elseif $phase is 1>>

You do as little work as you can get away with. Your colleagues grumble about you.
<br><br>

<<if $upperwetstage gte 3 or $lowerwetstage gte 3>>
You feel the other dockers ogling your body through your wet clothing.
<<gstress>><<garousal>><<stress 6>><<arousal 600>>
<br><br>
<</if>>

<<elseif $phase is 2>>

You strip naked behind some cargo containers. <<flaunting>> you take a deep breath and step out onto the dock.
<<if $dockexhibitionism isnot 1>>
	<<set $dockexhibitionism to 1>><<exhibitionism5>>
<<else>>
	<br><br>
<</if>>

<<covered>> Few notice at first, until someone whistles at you and attracts attention. More and more dockers stop what they're doing to cheer and whistle at you. Each new onlooker renews your thrill.
<br><br>

A supervisor tells everyone to get back to work. They do so, though reluctant to take their eyes off you. <<generate1>><<person1>>The supervisor doesn't tell you to get dressed. The longing look <<he>> gives your body suggests why.<<endevent>>
<br><br>

<</if>>

<<dockwork>>