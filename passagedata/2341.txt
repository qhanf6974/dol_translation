<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>"The ivory necklace?" Winter says. "I have to send it away. Terrible shame, but I was told to inform the mayor's office should I acquire anything fitting that description. They've sent someone to pick it up." <<He>> shakes <<his>> head. "Best not interfere with those types."
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>