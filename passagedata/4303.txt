<<effects>>
<<npc Whitney>><<person1>>
<<set $whitney_toilet to 1>>

Whitney shoves you into the <<if $pronoun is "f">>girls'<<else>>boys'<</if>> toilets.
<br><br>
"The rest of you make sure <<pshe>> follows the rule," <<he>> says, turning to <<his>> friends. "Got it?" <<His>> friends nod, casting lecherous looks up and down your body. "Then it's settled." <<He>> turns back to you. "See you later, slut."
<br><br>
Not waiting for your response, <<he>> and <<his>> friends continue down the hallway, laughing and joking with each other.
<br><br>

<<endevent>>
<<link [[Next|Hallways]]>><</link>>
<br>