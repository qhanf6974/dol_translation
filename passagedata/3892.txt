<<set $outside to 0>><<set $location to "school">><<effects>>
<<npc Kylar>><<person1>><<set $leftarm to "bound">><<set $rightarm to "bound">>
<<set $stress -= 2000>><<clothesontowel>><<set $pain += 1>>
You awaken sat on a chair. You can't move. Your legs are chained and your arms are bound behind you. You're in a spacious basement. A tiny window lets in a little light.
<br><br>

You hear a door open above. A person walks down a staircase, carrying something large and hidden by a white sheet. They place it on a table, then step around it into view. It's Kylar. <<His>> hair is even messier than usual, but <<hes>> smiling. <<His>> eyes are crazed and terrible. "You're awake," <<he>> gasps. <<He>> drops to <<his>> knees in front of you. <<He>> places <<his>> hands on your thighs. They're shaking.
<br><br>

"You're so <<if $player.gender_appearance is "m">>handsome<<else>>beautiful<</if>>." <<He>> laughs. "I've done everything. Don't worry."
<br><br>

<<link [[Plead|Kylar Basement 2]]>><<set $phase to 1>><</link>>
<br>
<<link [[Get angry|Kylar Basement 2]]>><<set $phase to 2>><</link>>
<br>
<<link [[Remain silent|Kylar Basement 2]]>><<set $phase to 0>><</link>>
<br>