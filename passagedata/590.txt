<<set $outside to 0>><<set $location to "lake_ruin">><<underwater>><<effects>><<lakeeffects>>
<<if $stress gte 10000>>
	<<passoutlake>>
<<else>>
	<<if $rng gte 81>>
		You reach inside one of the pots, and find a small ivory box. It's sealed by a bronze lock.
		<br><br>
		<<if $historytrait gte 4>>
			The box itself is very old. It must be valuable.
			<br><br>
			<<link [[Just take the box|Lake Ruin Deep]]>><<set $antiquemoney += 20>><<museumAntiqueStatus "antiquebox" "found">><</link>>
			<br>
		<</if>>
		<<set $skulduggerydifficulty to 300>>
		<<link [[Open it|Lake Ruin Box]]>><<wateraction>><</link>><<skulduggerydifficulty>><<loxygen>>
		<br>
		<<if $lakeruinkey is 1>>
			<<link [[Use bronze key|Lake Ruin Door]]>><<wateraction>><</link>><<loxygen>>
			<br>
		<</if>>
		<<link [[Search pots|Lake Pots]]>><<wateraction>><</link>><<loxygen>>
		<br>
		<<if $lakeruinkey is 2>>
			<<link [[Swim inside plinth room|Lake Ruin Plinth]]>><<wateraction>><</link>><<loxygen>>
			<br>
		<</if>>
		<br>
		<<link [[Swim towards exit|Lake Ruin]]>><<wateraction>><</link>><<loxygen>>
		<br>
	<<elseif $rng gte 78 and $swarmdisable is "f">>
		You reach inside one of the pots. Swarms of small fish surge out, surrounding you.
		<br><br>
		<<link [[Next|Lake Swarm]]>><<set $molestationstart to 1>><</link>>
		<br>
	<<elseif $rng gte 51>>
		You reach inside one of the pots. There's purple dust at the bottom. Your probing disturbs it, and it swirls around your arm. You feel heated.
		<<gstress>><<stress 3>><<garousal>><<arousal 600>>
		<br><br>
		<<if $lakeruinkey is 1>>
			<<link [[Use bronze key|Lake Ruin Door]]>><<wateraction>><</link>><<loxygen>>
			<br>
		<</if>>
		<<link [[Search pots|Lake Pots]]>><<wateraction>><</link>><<loxygen>>
		<br>
		<<if $lakeruinkey is 2>>
			<<link [[Swim inside plinth room|Lake Ruin Plinth]]>><<wateraction>><</link>><<loxygen>>
			<br>
		<</if>>
		<br>
		<<link [[Swim towards exit|Lake Ruin]]>><<wateraction>><</link>><<loxygen>>
		<br>
	<<elseif $rng gte 41 and $lakeruinkey is undefined>>
		You reach inside one of the pots. You find a decaying bronze key.
		<br><br>
		<<set $lakeruinkey to 1>>
		<<link [[Open door with bronze key|Lake Ruin Door]]>><<wateraction>><</link>><<loxygen>>
		<br>
		<<link [[Search pots|Lake Pots]]>><<wateraction>><</link>><<loxygen>>
		<br>
		<<if $lakeruinkey is 2>>
			<<link [[Swim inside plinth room|Lake Ruin Plinth]]>><<wateraction>><</link>><<loxygen>>
			<br>
		<</if>>
		<br>
		<<link [[Swim towards exit|Lake Ruin]]>><<wateraction>><</link>><<loxygen>>
		<br>
	<<else>>
		<<set $rng to random(1, 100)>>
		<<if $rng gte 81>>
			You reach inside one of the pots. It's full of broken ceramics.
		<<elseif $rng gte 61>>
			You reach inside one of the pots. Something inside snaps at you. You pull your hand away in time, but it was close.
			<<gstress>><<stress 3>>
		<<elseif $rng gte 41>>
			You reach inside one of the pots. It's full of mud.
		<<elseif $rng gte 21>>
			You reach inside one of the pots. It's full of pebbles.
		<<else>>
			You reach inside one of the pots. It's empty.
		<</if>>
		<br><br>
		<<if $lakeruinkey is 1>>
			<<link [[Use bronze key|Lake Ruin Door]]>><<wateraction>><</link>><<loxygen>>
			<br>
		<</if>>
		<<link [[Search pots|Lake Pots]]>><<wateraction>><</link>><<loxygen>>
		<br>
		<<if $lakeruinkey is 2>>
			<<link [[Swim inside plinth room|Lake Ruin Plinth]]>><<wateraction>><</link>><<loxygen>>
			<br>
		<</if>>
		<br>
		<<link [[Swim towards exit|Lake Ruin]]>><<wateraction>><</link>><<loxygen>>
		<br>
	<</if>>
<</if>>