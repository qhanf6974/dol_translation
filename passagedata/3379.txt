<<set $outside to 0>><<set $location to "farm">><<effects>>

You ride in darkness for what feels like an hour. You must have left the town far behind. At last, the vehicle comes to a stop, and you hear the back doors open. A rough hand grabs your hood and tugs it off your head. Before your eyes adjust, you're pulled from the vehicle onto a straw-strewn floor.
<br><br>

You look around. You're in a barn. A <<person2>><<person>> and <<person3>><<person>> grin at you.
<br><br>

"Gonna make a killing from this one," the <<person3>><<person>> says as the <<person2>><<person>> tugs your leash. You're pulled forward.
<br><br>

<<link [[Next|Livestock Intro]]>><<endevent>><</link>>
<br>