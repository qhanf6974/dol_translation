<<set $outside to 0>><<set $location to "pub">><<dockeffects>><<effects>>

	<<if $submissive gte 1150>>
	"You should talk to <<himcomma>>" you say. "Just be yourself."
	<<elseif $submissive lte 850>>
	"Talk to <<himcomma>>" you say. "<<He>> will appreciate the assertiveness."
	<<else>>
	"You should talk to <<himcomma>>" you say. "You'll regret it if you don't."
	<</if>>
<br><br>
The docker finishes their drink, takes a deep breath and marches over. The <<person>> looks up. You can't hear them over the din of the pub, but <<he>> seems to respond well.

<<if $rng gte 51>>

<<He>> takes the docker's hand and together they walk outside. The docker gives you a grateful smile before disappearing.
<<gcool>><<dockstatus 1>>
<br><br>

<<else>>

Until <<he>> sneers and throws <<his>> drink in the docker's face.
<br><br>

The docker returns, eyes downcast and hair dripping.
<br><br>

<</if>>

You finish your drink and leave with your colleagues.
<br><br>

<<dockpuboptions>>