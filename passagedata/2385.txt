<<effects>>

<<if $submissive gte 1150>>
	"Pl-please stop," you say. "I don't want to go back."
<<elseif $submissive lte 850>>
	"Let go," you say. "Fucking quack."
<<else>>
	"Let me go," you say. "You're supposed to help people."
<</if>>
<br><br>

<<if $english gte random(1, 1000)>>
	<span class="green">Harper's grip softens a moment.</span> It's enough. You struggle free from <<his>> grip, letting out a terrified moo as you tumble to the ground. You stagger to your feet and run.
	<br><br>
	You stop near a tree and look around. Nobody followed.
	<br><br>

	<<link [[Next|Park]]>><<set $eventskip to 1>><<endevent>><</link>>
	<br>
<<else>>
	<span class="red">Harper's grip remains iron.</span>
	<br><br>

	<<link [[Next|Park Run Harper Give]]>><</link>>
	<br>
<</if>>