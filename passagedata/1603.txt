<<effects>>
<<set _temp_timer to $timer>>
<<endmasturbation>>
<<endcombat>>
<<clothesontowel>>
<<if !$chef_event>>
<<set $chef_event to 20>>
<<else>>
<<set $chef_event += 20>>
<</if>>

<<npc Sam>><<person1>>

<<if $masturbation_fluid gte 30>>

	<<if _temp_timer lte 10>>
		Sam barges through the door. You barely cover yourself in time. <<He>> frowns. "Just checking up on you," <<he>> says.
		<br><br>
		You assure <<him>> everything is under control, and <<he>> nods. <<He>> returns to the main room, but wedges the kitchen door open.
		<<gsuspicion>><<set $chef_sus += 5>>
		<br><br>
	<</if>>

<<set _buns_sold to Math.floor(($masturbation_fluid / 30))>>
You add your fluid to the mixture, and whip up a new batch of cream. You have enough to make _buns_sold buns.
<br><br>
Sam arrives as you finish adding the cream, drawn by the smell. <<He>> leans over them, closes <<his>> eyes, and sniffs.

	<<if _buns_sold gte 30>>
	"Marvelous," <<he>> says, staring in awe at the mountain in front of <<himstop>> "This might be enough to satisfy demand. Good job!"
	<<elseif _buns_sold gte 20>>
	"Marvelous," <<he>> says, licking <<his>> lips. "You've been very busy. Our customers will be happy."
	<<elseif _buns_sold gte 10>>
	"Marvelous," <<he>> says, licking <<his>> lips. "Good job. Our customers will be happy."
	<<elseif _buns_sold gte 4>>
	"Marvelous," <<he>> says, licking <<his>> lips. "A Nice batch. Our customers will be happy."
	<<elseif _buns_sold gte 3>>
	"Marvelous," <<he>> says, licking <<his>> lips. "Three customers are going to be happy."
	<<elseif _buns_sold gte 2>>
	"Marvelous," <<he>> says, licking <<his>> lips. "Shame there's only a couple. Our customers might need to draw lots."
	<<else>>
	"Marvelous," <<he>> says. "Shame there's only one. I hope a fight doesn't break out!"
	<</if>>
<br><br>

<<He>> carries the buns into the cafe proper. You hear someone cheer as <<he>> enters.
<br><br>

<<sellbuns>>

<<else>>

	<<if _temp_timer lte 10>>
		Sam barges through the door. You barely cover yourself in time. <<He>> frowns. "Just checking up on you," <<he>> says.
		<br><br>
		You assure <<him>> everything is under control, and <<he>> nods. <<He>> returns to the main room, but wedges the kitchen door open.
		<<gsuspicion>><<set $chef_sus += 5>>
		<br><br>
	<</if>>

You make the buns without special ingredients, using Sam's recipe. A <<if $rng gte 51>>waiter<<else>>waitress<</if>> arrives to collect the fresh batch.
<br><br>
You're cleaning up when Sam enters the kitchen. "Buns aren't selling today," <<he>> says, deflated. "They didn't have the smell of your good ones. I don't want to be pushy, but I know you can do better."
<br><br>

"Still," <<he>> continues. "You've earned this." <<He>> hands you <span class="gold">£10.</span>
<<set $money += 1000>>
<br><br>

<</if>>

<<pass 50>>

<<endevent>>

<<link [[Next|Ocean Breeze]]>><</link>>
<br>