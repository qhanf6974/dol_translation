<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>

You search the public parts of the temple walls for pink lichen, both inside and out. You're about to give up when you spot it through a window. It covers the upper half of one of the towers. There's a window up there that you should be able to reach through.
<br><br>

You walk to the tower's base and find the door locked. Many parts of the temple are closed off to the public, or even initiates.
<br><br>
<<set $skulduggerydifficulty to 400>>
<<link [[Ask for help|Temple Lichen Help]]>><</link>>
<br>
<<link [[Pick the lock|Temple Lichen Lock]]>><</link>><<skulduggerydifficulty>>
<br>
<<link [[Climb|Temple Lichen Climb]]>><<athletics 6>><</link>><<gathletics>>
<br>