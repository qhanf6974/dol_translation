<<set $outside to 1>><<set $location to "town">><<effects>>
<<person1>>
<<if $phase is 0>>
	"Sorry we couldn't find anything," you say.
	<br><br>
	"It's not your fault," Robin says, a smile returning to <<his>> face. "I like going out with you-" <<Hes>> interrupted by a witch hat blowing into <<his>> face.
	<br><br>
<<else>>
	"There must be costumes in stock somewhere," you say.
	<br><br>
	"You're right," Robin says, a smile returning to <<his>> face. "We need to-" <<Hes>> interrupted by a witch hat blowing into <<his>> face.
	<br><br>
<</if>>
<<He>> pulls it off and stares at it with a blank expression. You look in the direction it came from, and see a <<if $NPCName[7].gender is "m">>boy<<else>>girl<</if>> with mousey hair walking in the opposite direction, carrying a large box. The lid is ajar. Inside you see another witch hat flop with each step.
<br><br>
"Wait!" Robin shouts. The mousy <<if $NPCName[7].gender is "m">>boy<<else>>girl<</if>> doesn't hear, and disappears around the corner. Robin runs after <<if $NPCName[7].gender is "m">>him<<else>>her<</if>>. You follow close behind.
<br><br>
<<link [[Next|Robin Forest Shop 3]]>><</link>>
<br>