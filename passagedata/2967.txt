<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $leftarm is "bound" or $rightarm is "bound" or $feetuse is "bound">><<unbind>>
Your bindings come loose. "You can't enter the competition like that," River says from behind, before rushing off to help someone else.
<br><br>
<</if>>

Leighton instructs you and the other entrants to sit in the front row.

<<if $NPCName[$NPCNameList.indexOf("Whitney")].state is "active" or $NPCName[$NPCNameList.indexOf("Whitney")].state is "rescued" and $NPCName[$NPCNameList.indexOf("Whitney")].init is 1>><<set $mathsprojectwhitney to 1>>
<<npc Whitney>><<person1>>Before you can make it however, you feel someone's hand on your <<bottomstop>> You turn right to face the culprit, but a hand snakes around your left shoulder and snatches your solution from your grip. It's Whitney.
<br><br>

You try to take it back, but <<he>> shoves you away. "So this is what you've been up to," <<he>> says, looking at the paper. "What a waste of time. Maybe I should tear it up."
<<gstress>><<stress 6>>
<br><br>

<<link [[Get angry|Maths Competition Angry]]>><<npcincr Whitney dom -1>><</link>><<ldom>>
<br>
<<link [[Beg|Maths Competition Beg]]>><<npcincr Whitney dom 1>><</link>><<gdom>>
<br>
<<link [[Call for help|Maths Competition Help]]>><<npcincr Whitney love -1>><</link>><<llove>>
<br>

<<else>>
You take a seat, and wait as the rest fill.
<br><br>

<<link [[Next|Maths Competition Intro]]>><</link>>
<br>
<</if>>