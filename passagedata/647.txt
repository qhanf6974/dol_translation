<<set $outside to 1>><<set $location to "forest">><<effects>>
You follow the deer tracks. They lift their heads more frequently, as if they can smell something you can't. After some time they break into a sprint.
<br><br>
<<if $athletics gte random(1, 1000)>>
	They don't hold back for you, <span class="green">but you're able to keep up.</span> You soon arrive at a clearing. On the other side stands a lone deer. It stops grazing and breaks into a panicked run at the sight of the pack.
	<br><br>
	<<if 20 + $wolfpackharmony + $wolfpackferocity gte $rng>>
		It bounds through the forest, weaving between the trees and out of sight. The black wolf breaks from the rest of the pack and races after it. You break into another clearing and find the black wolf stood triumphant over its prey.
		<br><br>
		The black wolf keeps the choice parts for itself. You find some berries growing nearby which serve you better.
		<<stress -12>><<lstress>>
		<br><br>
		The pack lazes around, eating at their leisure. A small dispute breaks out and the black wolf leaves its place by the deer to quell it.
		<br><br>
		<<wolfpackhuntoptions>>
	<<else>>
		It bounds through the forest, weaving between the trees. The pack races after it, but running at such speed they tire quickly. The deer escapes.
		<br><br>
		<i>Higher harmony and ferocity increases the chance of a successful hunt.</i>
		<br><br>
		Dejected but not broken, the wolves try to pick up the scent of easier prey. They catch something on the breeze and break into a run.
		<<gathletics>><<athletics 6>><<physique 6>>
		<br><br>
		<<set $bus to "wolfcaveclearing">>
		<<wolfhuntevents>>
	<</if>>
<<else>>
	They don't hold back for you, <span class="red">and you're unable to keep up</span>. You lose them in the forest.
	<br><br>
	<<link [[Next|Forest]]>><<set $forest to 60>><</link>>
	<br>
<</if>>