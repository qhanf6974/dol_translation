<<set $outside to 0>><<set $location to "cafe">><<effects>>

You look around the room. Each face looks back. You take a deep breath, and share how the cream is made.
<br><br>
<<npc Sam>><<person1>>
The room was already quiet. Rendered so by anticipation. Now the silence is defeaning. All blood drains from Sam's face. You're surprised <<he>> doesn't faint.
<br><br>

<<endevent>>

<<generate1>><<person1>>A <<person>> at the back laughs. "We know," <<he>> says. "And it's fucking delicious. Why do you think we came all this way?"
<br><br>
<<generate2>><<person2>>A <<person>> turns to the <<person1>><<personstop>> "Don't encourage the <<girlcomma>>" <<person2>><<he>> says. "That's not an okay joke to make,"
<br><br>
"It's not a joke," the <<person1>><<person>> replies. The <<person2>><<person>> rolls <<his>> eyes.
<br><br>

<<endevent>>
<<link [[Next|Chef Opening Truth 2]]>><</link>>
<br>