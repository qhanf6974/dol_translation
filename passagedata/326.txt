<<set $outside to 0>><<set $location to "cabin">><<effects>>

<<if $phase is 0>>

You make Eden eggs the way you know <<he>> likes them. <<He>> sits at the table and you place the food in front of <<himstop>> <<He>> eats without a word.
<br><br>

<<link [[Ask for a thank you|Eden Breakfast 2]]>><<npcincr Eden dom -1>><<set $submissive -= 1>><<set $phase to 1>><</link>>
<br>
<<link [[Chat|Eden Breakfast 2]]>><<trauma -6>><<stress -12>><<set $phase to 2>><</link>><<ltrauma>><<lstress>>
<br>
<<link [[Sit quietly|Eden Breakfast 2]]>><<set $submissive += 1>><<set $phase to 3>><</link>>
<br>
	<<if $promiscuity gte 55>>
	<<link [[Slip under the table|Eden Table Seduction]]>><</link>><<promiscuous4>>
	<br>
	<</if>>

<<elseif $phase is 1>>

	<<if $rng gte 81>>
	You cook the eggs a little differently to how <<he>> likes them, hoping the variety will spice things up a bit.
	<<elseif $rng gte 61>>
	You find some dried meat, which you think will be nice with some berries for desert.
	<<elseif $rng gte 41>>
	You boil some hearty root vegetables.
	<<elseif $rng gte 21>>
	You find some mushrooms and decide to make an omelette.
	<<else>>
	You think some berries would make a tasty meal.
	<</if>>

	<<He>> sits at the table and you place the food in front of <<himstop>> "I prefer having the same every day," <<he>> says, but <<he>> eats regardless.
	<br><br>

<<link [[Ask for a thank you|Eden Breakfast 2]]>><<npcincr Eden dom -1>><<set $submissive -= 1>><<set $phase to 1>><</link>>
<br>
<<link [[Chat|Eden Breakfast 2]]>><<trauma -6>><<stress -12>><<set $phase to 2>><</link>><<ltrauma>><<lstress>>
<br>
<<link [[Sit quietly|Eden Breakfast 2]]>><<set $submissive += 1>><<set $phase to 3>><</link>>
<br>
	<<if $promiscuity gte 55>>
	<<link [[Slip under the table|Eden Table Seduction]]>><</link>><<promiscuous4>>
	<br>
	<</if>>

<<elseif $phase is 2>>

	<<if $NPCName[$NPCNameList.indexOf("Eden")].love gte 1>>

	"Fine," <<he>> says. "I'll go hungry. It'll be your fault if I starve." <<He>> continues grumbling awhile.
	<br><br>

	<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
	<br>

	<<else>>

	"That wasn't a request," <<he>> says, standing and walking over to you. "I think you need a reminder of your place." <<He>> grabs you and bends you over <<his>> knee.
	<br><br>

	<<link [[Next|Eden Cabin Punishment]]>><<set $molestationstart to 1>><</link>>
	<br>

	<</if>>

<<elseif $phase is 3>>

With the garden mostly clear of weeds, you're able to harvest some delicious-looking vegetables. <<He>> sits at the table and you put the food in front of <<himstop>> <<He>> takes a bite, and <<his>> eyes widen. "This is good!" <<he>> says. "Are these from the garden? I didn't think that weedy patch of land had it in it."
<<npcincr Eden love 1>>
<br><br>

<<link [[Ask for a thank you|Eden Breakfast 2]]>><<npcincr Eden dom -1>><<set $submissive -= 1>><<set $phase to 1>><</link>>
<br>
<<link [[Chat|Eden Breakfast 2]]>><<trauma -6>><<stress -12>><<set $phase to 2>><</link>><<ltrauma>><<lstress>>
<br>
<<link [[Sit quietly|Eden Breakfast 2]]>><<set $submissive += 1>><<set $phase to 3>><</link>>
<br>
	<<if $promiscuity gte 55>>
	<<link [[Slip under the table|Eden Table Seduction]]>><</link>><<promiscuous4>>
	<br>
	<</if>>

<<elseif $phase is 4>>

With so many mushrooms stored, you think it'd be fine to have some for breakfast. You cook them in an omelette. <<He>> sits at the table and you put the food in front of <<himstop>> "I prefer having the same every day," <<he>> says, but <<he>> eats regardless.
<br><br>

After a short while, <span class="lewd"><<his>> face starts to flush.</span> "You need to be careful with the sort of mushrooms you cook with," <<he>> says. "Some of them have interesting effects. Not that you've done wrong, it tastes fine."
<<npcincr Eden lust 30>>
<br><br>

<<link [[Ask for a thank you|Eden Breakfast 2]]>><<npcincr Eden dom -1>><<set $submissive -= 1>><<set $phase to 1>><</link>>
<br>
<<link [[Chat|Eden Breakfast 2]]>><<trauma -6>><<stress -12>><<set $phase to 2>><</link>><<ltrauma>><<lstress>>
<br>
<<link [[Sit quietly|Eden Breakfast 2]]>><<set $submissive += 1>><<set $phase to 3>><</link>>
<br>
	<<if $promiscuity gte 55>>
	<<link [[Slip under the table|Eden Table Seduction]]>><</link>><<promiscuous4>>
	<br>
	<</if>>

<</if>>