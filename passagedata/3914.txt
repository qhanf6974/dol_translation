<<effects>>

<<person1>>
<<if $submissive gte 1150>>
	"<<Hes>> with me," you say. "You should be nicer to people."
<<elseif $submissive lte 850>>
	"<<Hes>> with me," you say. "Now get out of our way."
<<else>>
	"<<Hes>> with me," you say.
<</if>>
<br><br>

"Fine," the <<person2>><<person>> sneers. "But I'm blaming you if someone poisons the food."
<br><br>

You walk past the <<person2>><<person>> and continue to your room. Kylar follows close behind, as if afraid of being obstructed again.
<br><br>
<<endevent>><<npc Kylar>><<person1>>

<<link [[Next|Kylar Orphanage]]>><</link>>
<br>