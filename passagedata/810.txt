<<effects>>

You scream, and the approaching pair stop in their tracks.
<br><br>

"I'm coming!" It's Alex.
<br><br>

"Fuck," the <<person1>><<person>> says. "This wasn't the plan." <<He>> turns and runs. The others cease their attempt at concealment, and run after <<him>>.
<br><br>

<<endevent>><<npc Alex>><<person1>>
Alex arrives in time to see their flight. "You fuckers better stay gone," <<he>> shouts after them. <<He>> turns to you. "Are you alright? Working the fields without me might be dangerous. Be careful."
<br><br>

<<He>> returns to work.
<br><br>

<<link [[Next|Farm Work]]>><</link>>
<br>