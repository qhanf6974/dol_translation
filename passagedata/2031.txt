<<effects>>
<<headwear 14>>
You sit on the grass beside Robin, and rummage among the wild flowers for suitable specimens. Robin watches a moment, then gasps in excitement. <<He>> joins in. "I'm making one for you too."
<br><br>

Soon, you both have flower crowns ready. <<if $tending lt 200>>Yours looks rougher than Robin's, but <<he>> doesn't seem to mind.<</if>> <<He>> reaches atop your head, and places the crown <<he>> made upon it. You place yours atop <<his>> head in turn.

<br><br>
<<if $tending gte random(1, 200)>>
	"I love it," Robin says, <span class="green">smiling beneath the wreathe of flowers.</span>
	<br><br>
<<else>>
	"I love it," Robin says. <<Hes>> barely finished speaking when the <span class="red">the crown on <<his>> head unravels,</span> and it falls to the ground. <<He>> smiles. "It's okay! I still love it. They can be tricky to make, but you'll get it with practice."<<glove>><<npcincr Robin love 1>><<gdom>><<npcincr Robin dom 1>>
	<br><br>
<</if>>

You hear shouting within the orphanage. "Those two are at it again," Robin says, rising to <<his>> feet. "I need to investigate. Thanks for letting me help."
<br><br>

<<link [[Next|Garden Flowers]]>><<endevent>><</link>>
<br>