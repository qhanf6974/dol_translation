<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $phase is 1>>
You settle down at a desk with a science textbook and read through the material. Being sat here in the school library with your <<lewdness>> on display feels naughty, even with no one around. It keeps you alert and focused.
<<gscience>><<scienceskill>><<garousal>><<arousal 300>>
<<elseif $phase is 2>>
You settle down at a desk with a maths textbook and read through the material. Being sat here in the school library with your <<lewdness>> on display feels naughty, even with no one around. It keeps you alert and focused.
<<gmaths>><<mathsskill>><<garousal>><<arousal 300>>
<<elseif $phase is 3>>
You settle down at a desk with an English textbook and read through the material. Being sat here in the school library with your <<lewdness>> on display feels naughty, even with no one around. It keeps you alert and focused.
<<genglish>><<englishskill>><<garousal>><<arousal 300>>
<<elseif $phase is 4>>
You settle down at a desk with a history textbook and read through the material. Being sat here in the school library with your <<lewdness>> on display feels naughty, even with no one around. It keeps you alert and focused.
<<ghistory>><<historyskill>><<garousal>><<arousal 300>>
<</if>>
<br><br>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - ($allure / 2))>>
		<<if $rng gte 51>>
		<<generate1>><<person1>>You fumble the book as you try to place it on its shelf. It thumps against the floor. "That better not be another cat," a voice responds from the corridor outside. You take cover behind the shelf and peek between the books. A <<person>> wearing a security uniform enters. "Who's there?"
		<br><br>

			<<if $exposed gte 2>>
				<<if $exhibitionism gte 75>>
				<<link [[Show yourself|Library Study Show]]>><<set $phase to 0>><</link>><<exhibitionist5>>
				<br>
				<</if>>
			<<elseif $exposed is 1>>
				<<if $exhibitionism gte 15>>
				<<link [[Show yourself|Library Study Show]]>><<set $phase to 1>><</link>><<exhibitionist2>>
				<br>
				<</if>>
			<</if>>
		<<link [[Stay hidden (0:10)|Library Study Hide]]>><<stress 3>><<pass 10>><</link>><<gstress>>
		<br>
		<<else>>
		<<generate1>><<generate2>><<person1>>A <<person>> and <<person2>><<person>> walk along the street outside the window. They will see you if they look through.
		<br><br>

		<<link [[Risk it|Library Study Risk]]>><</link>>
		<br>
		<<link [[Hide (0:20)|Library Study Pair Hide]]>><<pass 20>><</link>>
		<br>

		<</if>>
	<<else>>
	<<link [[Next|School Library]]>><</link>>
	<br>
	<</if>>