<<effects>>
<<He>> recoils in pain, falling backwards out from beneath the stall. By the time <<He>> recovers, you've managed to put some distance between you.
<br><br>
<<clotheson>>
<<endcombat>>
You manage to get there in time and, lying prone, shuffle beneath it. You continue to the other side and slink beneath another stall. From there, it's easy to remain concealed as you make your way across the street. Placing your head against the ground, you flick up the cover of the final stall to make sure the way forward is safe. Heart thumping in your chest, you dart into the alleyway, leaving the bustle behind you.
<br><br>
<<residentialquick>>