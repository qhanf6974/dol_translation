<<effects>>


"If it's only for a few minutes," one girl says. "We might be able to get away with wearing these clothes outside-"
<br>
"Are you kidding? The guys are probably right outside, waiting to take pictures of us!"
<br>
"I don't have time for this. I have a date tonight."
<br><br>
One of the girls pulls on a pair of boys' trousers a few sizes too big for her, along with a mismatched shirt. She waddles over to the door, holding the trousers by the waist. The other girls watch in stunned silence as she strides out the locker room. A few moments later she speaks from the hallway.
<br><br>
"No one's out here. It's empty."
<br><br>
The remaining girls look at each other, waiting to see who will make the next move.
<br><br>
"Screw it. Better than just a towel."
<br>
One by one, they wear the boys' clothing. After a few more moments of silence, they exit the changing room in a group.
<br><br>

<<link [[Next|Hallways]]>><</link>>
<br>