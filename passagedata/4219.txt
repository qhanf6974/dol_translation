<<set $outside to 0>><<set $location to "forest_shop">><<effects>>
"I'm going to buy it," you announce. "You need a proper costume for Halloween."
<br><br>
<<if $NPCName[$NPCNameList.indexOf("Robin")].pronoun is "f">>
	"But I can't dress as a boy," Robin says. "I'm not brave enough."
	<br><br>
	"Yes you are," you reply. "You like the outfit, don't you?"
	<br><br>
	"I do," Robin says, looking at the mirror. "And you're supposed to dress up for Halloween."
	<br><br>
	Robin disappears behind the screen to dress. Despite <<his>> misgivings, you take the outfit to Gwylan and hand over the money.
	<br><br>
	You walk back to the orphanage with a nervous and excited Robin.
	<br><br>
<<else>>
	"That's not fair on you," <<he>> says.
	<br><br>
	"It's a gift," you reply. "You like the outfit, don't you?"
	<br><br>
	"I do," Robin says, looking at the mirror. "It would be nice to have a proper costume."
	<br><br>
	Robin disappears behind the screen to dress. Despite <<his>> misgivings, you take the outfit to Gwylan and hand over the money.
	<br><br>
	You walk back to the orphanage with an excited Robin.
	<br><br>
<</if>>
<<robinoptions>>