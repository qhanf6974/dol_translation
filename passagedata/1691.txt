<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<if $weather is "rain">>
You take a seat and order a fruit salad. It arrives promptly; fresh strawberries, blueberries, kiwifruit, and chunks of pineapple. It's sweet and delicious. You stare out the window at the rain-filled street, and feel comfy.
<br><br>
<<else>>
You take a seat outside and order a fruit salad. It arrives promptly; fresh strawberries, blueberries, kiwifruit, and chunks of pineapple. It's sweet and delicious. You stare out over the sea, and enjoy the breeze.
<br><br>
<</if>>

<<link [[Next|Ocean Breeze]]>><</link>>
<br>