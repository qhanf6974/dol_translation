<<effects>>

<<for _e to 0; _e lt $bodypart_number; _e++>>
	<<activebodypart>>
	<<if $skin[_active_bodypart].pen is "pen" or $skin[_active_bodypart].pen is "marker">>
		<<if $money gte 50000>>
		You have <<if $skin[_active_bodypart].type is "text">><span class="pink">"<<print $skin[_active_bodypart].writing>>"</span> written<<else>>a $skin[_active_bodypart].writing drawn<</if>> on your <<bodypart _active_bodypart>>.
			<<if $skin[_active_bodypart].special is "bestiality" and $deviancy lte 34>>
				<<if (_active_bodypart is "left_bottom" or _active_bodypart is "right_bottom" or _active_bodypart is "pubic" or _active_bodypart is "breasts") and $exhibitionism lte 34>>
					<span class="blue">You can't bare to show your <<bodypart _active_bodypart>> to the tattoo artist, and you're not deviant enough to have such a thing made into a tattoo anyway.</span>
				<<else>>
					<span class="blue">You're not deviant enough to have such a thing made into a tattoo.</span>
				<</if>>
			<<elseif $skin[_active_bodypart].lewd is 1 and $skin[_active_bodypart].special isnot "bestiality" and $promiscuity lte 34>>
				<<if (_active_bodypart is "left_bottom" or _active_bodypart is "right_bottom" or _active_bodypart is "pubic" or _active_bodypart is "breasts") and $exhibitionism lte 34>>
					<span class="blue">You can't bare to show your <<bodypart _active_bodypart>> to the tattoo artist, and you're not lewd enough to have such a thing made into a tattoo anyway.</span>
				<<else>>
					<span class="blue">You're not lewd enough to have such a thing made into a tattoo.</span>
				<</if>>
			<<elseif (_active_bodypart is "left_bottom" or _active_bodypart is "right_bottom" or _active_bodypart is "pubic" or _active_bodypart is "breasts") and $exhibitionism lte 34>>
				<span class="blue">You can't bare to show your <<bodypart _active_bodypart>> to the tattoo artist.</span>
			<<else>>
			<<capture _active_bodypart>> | <<link [[Turn into tattoo (£500 1:00)|Tattoo Bodywriting]]>><<pass 60>><<set $money -= 50000>><<set $tattoo_bodypart to _active_bodypart>><<set $tattoo_parlour to clone($skin[_active_bodypart])>><<if $skin[_active_bodypart].lewd is 1 or $skin[_active_bodypart].special is "bestiality">><<control 50>><<else>><<control 25>><</if>><<set $skin[_active_bodypart].pen to "tattoo">><<pain 6>><</link>><</capture>> <<if $skin[_active_bodypart].lewd is 1 or $skin[_active_bodypart].special is "bestiality">><<gggcontrol>><<else>><<ggcontrol>><</if>>
			<</if>><<gpain>>
		<br>
		<</if>>
	<<elseif $skin[_active_bodypart].pen is "tattoo">>
		Your <<bodypart _active_bodypart>> is already tattooed.
		<br>
	<<elseif $skin[_active_bodypart].pen is "brand">>
		Your <<bodypart _active_bodypart>> is <span class="red">branded</span>.
		<br>
	<<elseif $skin[_active_bodypart].pen is "magic">>
		Your <<bodypart _active_bodypart>> has a magical <span class="pink">seal</span>.
		<br>
	<<elseif !$skin[_active_bodypart].pen>>
		Nothing is written on your <<bodypart _active_bodypart>>.
		<br>
	<</if>>
<</for>>
<br>
<<link [[Back|Tattoo Parlour]]>><</link>>
<br>