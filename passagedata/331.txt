<<set $outside to 0>><<set $location to "cabin">><<effects>>

<<if $phase is 0>>

You remove your clothing as Eden eases <<himself>> into the bath.

<<if $exhibitionism gte 55>>
Feeling comfortable with your nudity, you climb in with <<himstop>>
<<else>>
<<covered>> You climb in with <<himstop>> Unable to hold the sides, you almost slip.
<</if>>
<br><br>

<<He>> reclines opposite you, enjoying the warmth and examining your body.
<<wash>>
<br><br>

<<link [[Examine Eden in return|Eden Bath 2]]>><<set $phase to 0>><</link>>
<br>
<<link [[Wash Eden|Eden Bath 2]]>><<set $phase to 1>><<npcincr Eden love 1>><</link>>
<br>
<<link [[Relax|Eden Bath 2]]>><<set $phase to 2>><<stress -12>><</link>><<lstress>>
<br>
<<if $promiscuity gte 15>>
<<link [[Seduce|Eden Bath Seduction]]>><</link>><<promiscuous2>>
<br>
<</if>>

<<elseif $phase is 1>>

	<<if $NPCName[$NPCNameList.indexOf("Eden")].love gte 1>>

	"Suit yourself," <<he>> says, easing <<himself>> into the water.
	<br><br>

	<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
	<br>

	<<else>>

	"I said get in," <<he>> says, marching over to you. "I think you need another lesson." <<He>> bends you over <<his>> knee.
	<br><br>

	<<link [[Next|Eden Cabin Punishment]]>><<set $molestationstart to 1>><</link>>
	<br>

	<</if>>

<</if>>