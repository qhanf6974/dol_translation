Your lower clothes have been fixed.
<br><br>
<<if $worn.upper.set is $worn.lower.set>>
<<set $worn.lower.integrity = $worn.lower.integrity_max>>
<</if>>

<<if $worn.under_upper.set is $worn.under_lower.set>>
<<set $worn.under_lower.integrity = $worn.under_lower.integrity_max>>
<</if>>

<<link [[Back|Tailor Shop]]>><<endevent>><</link>>