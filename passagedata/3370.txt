<<effects>><<set $outside to 0>>

You try to remain calm, and carefully examine the van interior for any way out, any weakness.
<br><br>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
<<pass 10>>
It takes several minutes, but you notice a thin flow of air in the centre. You feel around the dusty floor, and find a hatch. You throw it open. You wait a few moments, until the van slows for traffic, then drop to the road beneath. You crawl out from under the van and run to safety.
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

<<link [[Next|Harvest Street]]>><<endevent>><<set $eventskip to 1>><</link>>
<br>

<<else>>
<<pass 60>>
Minutes pass, but you can't find anything. You're trapped.
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

<<link [[Next|Street Van Journey]]>><</link>>
<br>

<</if>>