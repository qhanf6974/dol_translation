<<effects>>
You walk up to the mirror and gaze into its depths. It no longer reflects, but glows with a curious light. It's purple, or pink, or maybe red.
<br><br>
The light takes shape and extends into the space in front of you. A single tentacle. It dances closer.
<br><br>
<<link [[Watch|Eerie Mirror 2]]>><</link>><<deviant1>>
<br>
<<link [[Step away|Eerie Mirror Stop]]>><</link>>
<br>