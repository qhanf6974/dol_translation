<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
You walk to the front of the classroom, and Winter gestures you to bend over the bottom part of the pillory. You do so, and <<he>> lifts over the rest of the device, locking you firmly in place. Your hands and head poke through small holes facing the class. The antique is bolted onto the floor, rendering you quite immobile.
<br><br>
<<bind>>
<<if $rng gte 51>>
	<<endevent>>
	<<npc Leighton>><<person1>>Before Winter can speak further, the door opens to reveal Leighton, who doesn't look twice at the <<girl>> in the pillory. <<He>> looks at Winter. "We need to talk." Winter nods, and leaves the room without a word.
	<<endevent>>
	<br><br>
	<<generates1>><<person1>>The class is silent for a few moments, as the sound of footsteps outside the room fade into nothingness. Then a <<person>> stands up, and saunters over to you. "Who thinks the lesson should continue?" <<He>> asks the rest of the class, smiling slyly. Encouraged by the rest of the class, <<he>> walks behind you.
	<br><br>
	<<link [[Next|History Lesson Pillory Molestation]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	Winter lectures about the historical use of the pillory, as well as other archaic punishments. You wonder why <<he>> likes them so much. Before long, <<he>> releases you. "You've been a fine assistant," <<he>> says cheerfully, as you head back to your seat.<<unbind>>
	<br><br>
	<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<br>
<</if>>