<<effects>>

Remy brings the whip down once more. You grit your teeth as the leather assaults your back, pain surges through your body with every blow.
<<if $willpower gte random(300, 700)>>
	<span class="green">You remain obstinate.</span> Remy ceases <<his>> assault. "The-" <<he>> pants. "The barbs." The farmhands glance at each other. "Now!"
	<<gwillpower>><<willpower 2>>
	<br><br>

	One of the farmhands runs from the field as Remy marches closer. <<He>> clutches your chin and raises it. "Listen here, <<if $player.gender_appearance is "m">>bull<<else>>cow<</if>>." <<he>> spits. "I've broken stronger beasts than you. You do as I say, or I might get angry."
	<br><br>

	The farmhand returns, carrying a wooden case in both hands. Remy opens it, and pulls out another whip. <<He>> holds it in front of you. It's similar to the other, except studded with cruel barbs.
	<<gstress>><<stress 6>>
	<br><br>

	Remy returns to <<his>> seat. <<He>> holds the whip above <<his>> head, the length of it falling behind <<himstop>>
	<br><br>

	<<link [[Pull|Livestock Plough]]>><<sub 1>><<pass 60>><<transform cow 1>><<npcincr Remy dom 1>><<tiredness 18>><</link>><<ggtiredness>>
	<br>
	<<link [[Refuse|Livestock Plough Refuse 4]]>><<def 1>><<npcincr Remy dom -1>><<pain 24>><</link>><<willpowerdifficulty 300 700>><<gggpain>>
	<br>
<<else>>
	<span class="red">You can't take it.</span> All pride flees your mind, and your muscles move.
	<<ggwillpower>><<willpower 12>>
	<br><br>
	<<link [[Next|Livestock Plough]]>><<pass 60>><<transform cow 1>><<npcincr Remy dom 1>><<tiredness 18>><</link>><<ggtiredness>>
	<br>
<</if>>