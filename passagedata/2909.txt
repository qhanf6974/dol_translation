<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>

	They lie on the floor as they come to their senses, enduring mockery from the audience.
	<br><br>

	<<tearful>> you bask in your sordid victory.
	<<gcool>><<status 1>>
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<playground>>

<<elseif $enemyhealth lte 0>>

	They lie on the ground, trying to hold back tears and enduring mockery from the audience.
	<<gcool>><<status 1>>
	<br><br>

	<<tearful>> you bask in victory.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<playground>>

<<elseif $rescue is 1 and $alarm is 1>>

	A whistle blows from the school entrance. The audience disperses as Leighton marches closer. The <<person1>><<person>> and <<person2>><<person>> are grasped by the arm and dragged away. They afford you one last evil look.
	<<lcool>><<status -1>><<gdelinquency>><<detention 60>>
	<br><br>

	<<tearful>> you gather yourself.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<playground>>

<<else>>

	<<tearful>> you fall to the ground. You're too hurt to continue fighting.
	<<lcool>><<status -1>>
	<br><br>

	The audience jeer at you for a while, though they soon grow bored.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<playground>>

<</if>>