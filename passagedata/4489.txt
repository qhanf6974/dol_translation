<<set $outside to 0>><<set $location to "underground">><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	You hear the crowd whisper among themselves as the <<person>> grabs your leash and pulls you to your feet. "I hope the other groups had as much fun with their prey as we did."
	<br><br>
	A scream erupts nearby, and the crowd rushes away to investigate, leaving the <<person>> to drag you back into the dungeon and down to your cell.
	<<tearful>> you sit on the mattress.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Underground Cell]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	The <<person>> falls against a tree. <<tearful>> you jump to your feet and run for it. <<He>> tries to stop you, but you dodge <<him>> and escape into the forest.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	A chill runs up your spine, a warning from some primal instinct. <span class="red">Something is hunting you.</span>
	<<set $foresthunt to 1>>
	<br><br>
	<<link [[Next|Forest]]>><<set $forest to 40>><</link>>
<</if>>