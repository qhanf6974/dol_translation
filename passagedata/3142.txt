<<effects>>
	You approach the display, inside it lies a sewing machine. An advertisement poster hangs beside the display, it reads:

	<br><br>
	THE SEWINATOR-9000 <sup>tm</sup>
	<br><br>
	- Dog Chewed Your Undies? <span class="pink">~Tentacles~</span> Tore Your Clothes? No Problem!
	<br><br>
	- Can Fix Any Piece of Clothing in One minute!
	<br><br>
	- Requires <span class="green">No Skill</span>! Even Bailey Could Use It!
	<br><br>
	- Comes With a Lifetime Supply of Cloth For Repairs!
	<br><br>
	- Only <span class ="gold">£1000.00!</span>
	<br><br>

	<<if $money gte 100000>>
		<<link [[Purchase Sewing Machine - Repair your clothes in your room! £1000|Tailor Shop]]>><<set $money -= 100000>><<set $sewingKit to 1>><<set $sewingBought to 1>><<endevent>><</link>>
		<br>
	<</if>>
	<<link [[Go Back|Tailor Shop]]>><<endevent>><</link>>