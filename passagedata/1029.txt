<<effects>>

<<npc Alex>><<person1>>

<<if $rng gte 51>>
	You help Alex follow the farm perimeter, picking up stones from the ground and returning them to the old wall. <<He>> suggests you split up to cover more ground.
	<br><br>
<<else>>
	You take a hammer and nails, and help Alex repair the wooden fence surrounding the fields. Some of the wooden planks have broken free whole, but others need to be replaced. Alex acknowledges your assistance with a wave, but seems focused on patching up the damage in front of <<him>>.
	<br><br>
	
	You step back and admire your handiwork.
	<br><br>
	
<</if>>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>