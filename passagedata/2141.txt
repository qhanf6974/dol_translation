<<if $molestationstart is 1>>
	<<set $molestationstart to 0>>
	<<controlloss>>
	<<violence 1>>
	<<neutral 1>>
	<<molested>>
	<<beastTrainGenerate 1 "wolf">>
	<<beastNEWinit 1 wolf>>
	<<set $water to 1>>
	As you reach the water you hear a sharp buzz and the sound of grinding metal. The water looks foreboding but there's no going back now. You ease yourself into the water and start swimming towards the exit. You spare a glance at the top of the hole. A group of wolves stand at the top, glaring down at you. Two prove more daring than the rest, and jump in after you.
	<br><br>
<</if>>

<<effects>>
<<effectsman>>
<br>
<<beast $enemyno>>
<br><br>

<<stateman>>
<br><br>
<<actionsman>>

<<if $enemyhealth lte 0>>
	<span id="next"><<link [[Next|Abduction Hospital Water Finish]]>><</link>></span><<nexttext>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<span id="next"><<link [[Next|Abduction Hospital Water Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link "Next">><<script>>state.display(state.active.title, null)<</script>><</link>></span><<nexttext>>
<</if>>