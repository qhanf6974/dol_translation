<<set $outside to 0>><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>><<set $prostitutionstat += 1>><<fameprostitution 1>>
	The <<person>> reclines against the wall. "Worth it," <<he>> says, handing you
	<<if $phase is 1>>
		<span class="gold">£20.</span><<set $money += 2000>>
	<<elseif $phase is 2>>
		<span class="gold">£50.</span><<set $money += 5000>>
	<<else>>
		<span class="gold">£100.</span><<set $money += 10000>>
	<</if>>
	<<He>> rises to <<his>> feet and walks away.
	<br><br>
	<<tearful>> you climb over the fence.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><<endevent>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	The <<person>> turns and flees. <<He>> tumbles back over the fence, and throws a fearful glance over <<his>> shoulder as <<he>> escapes.
	<br><br>
	<<tearful>> you dust yourself off.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<elseif $alarm is 1 and $rescue is 1>>
	<<set $rescued += 1>>
	"I'm coming!" It's Alex. The <<person>> backs away from you, and flees the way <<he>> came.
	<br><br>
	<<tearful>> you climb back over the fence.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	
	
	<<npc Alex>><<person1>>
	Alex arrives in time to shout after the intruder. "Don't let me catch you round here again!"
	<br><br>
	
	"No respect," <<he>> mutters. 
	
	<<if $exposed gte 1 and $farm_naked isnot 1>>
		<<covered>> <<He>> blushes and looks away, as if just now noticing your state of dress. "Let me get you something to cover with."<<garousal>><<towelup>>
		<br><br>
		
		<<He>> returns a minute later with some towels, then returns to work.<<pass 1>>
	<<else>>
		"I need to get back to work."
	<</if>>
	<<gdom>><<npcincr Alex dom 1>>
	<<if $exposed gte 1>>
		<<glust>><<npcincr Alex lust 1>>
	<</if>>
	<br><br>

	<<link [[Next|Farm Work]]>><<endevent>><</link>>
	<br>
<<else>>
	"Fine," the <<person>> says as <<he>> rises to <<his>> feet. "But you're not getting paid."
	<br><br>
	<<tearful>> you climb back over the fence.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<</if>>