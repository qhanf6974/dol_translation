<<set $outside to 0>><<set $location to "town">><<effects>>

<<set $mathsexposed to "safe">>

You grasp the hem of your $worn.lower.name with both hands as Whitney and <<his>> friends tug. You manage to prevent them from revealing your <span class="pink"><<undies>></span> to the audience, but the fabric covering you becomes even tighter.
<br><br>

The audience is alive with excitement. Some look away, but most stare in astonishment, or pull phones from their pockets. Leighton sits on the edge of <<endevent>><<npc Leighton>><<person1>><<his>> seat. <<endevent>><<npc River>><<person1>>River remains completely still, hands covering <<his>> eyes.
<br><br>

The photographer now darts around in front of the stage, taking pictures of the struggle from every angle. Perhaps waiting for the moment you're exposed in front of everyone.
<br><br>
<<endevent>><<npc Whitney>><<person1>>
Whitney and friends stop pulling for a moment, but only to reposition in the aisle so they can put their backs into it. The force almost lifts you off the ground.
<br><br>

You feel a slackness as one of the wires snaps, soon followed by the others. Whitney and <<his>> friends fall backwards. You almost fall over yourself. Your skirt floats back where it belongs. You spend a few moments removing the hooks.
<br><br>

<<endevent>><<npc Leighton>><<person1>>"That's enough," Leighton says. "Time to wrap up."
<br><br>

<<if $submissive gte 1150>>
"B-but I haven't finished," you say.
<<elseif $submissive lte 850>>
"I'm not done yet," you say.
<<else>>
"But I still need to-," you begin to protest. Leighton interrupts.
<br><br>
<</if>>

"I said that's enough," <<he>> says. "I have places to be."
<br><br>

You pass the judges on the way down from the stage. You take your seat and wait for their decision.
<br><br>

<<link [[Next|Maths Competition End]]>><</link>>
<br>