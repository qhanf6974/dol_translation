<<effects>>

<<if $physique gte random(2000, 30000)>>
	You grasp a tuft of grass with both hands. It breaks free of the earth, so you bury your fingers into the soils instead. You hold with all your might as the <<farm_text dog>> struggles against you, until <<farm_his dog>> <span class="green">knot comes free with a plop.</span>
	<br><br>
	
	You lie relieved for a moment, as semen runs out and pools beneath you.
	<br><br>
	
	<<clotheson>>
	
	You still hear Alex and the <<personsimple>> discussing, so you keep low.
	<br><br>
	
	<<link [[Next|Farm Work]]>><<endevent>><</link>>
	<br>
	
<<else>>
	You grasp a tuft of grass with both hands. <span class="red">It breaks free of the earth.</span> The <<farm_text dog>> pulls you further along with <<farm_his dog>> knot.
	<br><br>
	
	<<farm_knotted>>
<</if>>