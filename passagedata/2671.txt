<<effects>>

Whitney inserts more pens into the helpless <<person>>, who sobs and bears it. The torment ends when footsteps are heard outside the class. Everyone rushes back to their seats. Everyone but the <<person>>, who seems afraid to move.
<br><br>

At last <<he>> stirs, pulling the pens from <<his>> body and hurrying to cover <<himself>>. <<His>> legs give out and <<he>> collapses to the floor. River returns to find <<him>> crying on <<his>> knees, picking up <<his>> stationery with shaking fingers.
<br><br>
<<endevent>>
<<npc River>><<person1>>
"Who did this?" River asks, <<his>> cold eyes glancing around the room, boring into each student. <<He>> helps the victim pick up <<his>> things, but they won't speak up, nor will any of the witnesses. River's eyes settle on Whitney, who can't contain a smirk.
<br><br>

River sends Whitney out while glaring at you and Whitney's other friends. "You're not off the hook either," <<he>> says, before turning back to the whiteboard.
<br><br>

<<set $whitneymaths to "sent">>
<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
<br>