<<effects>>

<<npc Alex>><<person1>>
You scream as the shoots drag you across the earth.
<br><br>

"I'm coming!" shouts Alex. <<He>> arrives at the edge of the field just before you're dragged under, and rushes over. The shoots fall inert before <<he>> arrives, though they're still wrapped tight around you.
<br><br>

Alex arrives to find you tangled, but there's no sign that the plants were full of malice just a moment before. "How'd you get in a mess like that?" <<he>> laughs. "I thought there was a wild animal."
<br><br>

<<He>> helps you tear free of the plants. <<tearful>> you rise to your feet. "I need to get back to work," <<he>> says. "Try not to get tangled again."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>