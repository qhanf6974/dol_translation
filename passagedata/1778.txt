<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

<<beastejaculation>>

The cat yawns in satisfaction and walks over to the window.

<<tearful>> you rise to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

The cat pacified, you notice its collar glinting. You look closer and see it's encrusted with jewels.
<br><br>

<<link [[Take It|Danube House Collar]]>><<crimeup 100>><</link>><<crime>>
<br>
<<link [[Leave|Danube Street]]>><</link>>
<br>

<<elseif $enemyhealth lte 0>>

The cat cowers and rolls over in submission.

<<tearful>> you rise to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

The cat pacified, you notice its collar glinting. You look closer and see it's encrusted with jewels.
<br><br>

<<link [[Take It|Danube House Collar]]>><<crimeup 100>><</link>><<crime>>
<br>
<<link [[Leave|Danube Street]]>><</link>>
<br>

<<elseif $alarm is 1>>

	<<if $NPCList[0].stance is "top">>

	<<person1>>"What's the matter now?" says a <<personstop>>
	<<He>> laughs when <<he>> sees you. "An intruder?" <<He>> takes a seat. "How exquisite."
	<br><br>

	<<link [[Next|Danube House Cat]]>><<set $phase to 1>><</link>>

	<<else>>

	<<endcombat>>
	<<generate1>><<person1>>"What's the matter now?" says a <<personstop>>

	<<He>> laughs when <<he>> sees you. "That's what you get for trespassing." <<He>> whistles and the cat ceases its attack. Before you can stand, <<he>> presses <<his>> foot against your <<genitalsstop>>
	<<famebestiality 1>>

	<br><br>

	<<link [[Next|Danube House Sneak Molestation]]>><<set $molestationstart to 1>><</link>>
	<br>

	<</if>>

<<else>>

The cat backs away from you, where it stands still and stares. <<tearful>> you glance around. You see nothing of value.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Leave|Danube Street]]>><</link>>
<br>

<</if>>