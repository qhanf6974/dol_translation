<<effects>>

The dizziness becomes too intense. The world spins, and you fall. The vines spring to life as you land on them, wrapping around your limbs and pulling you into the air. You dangle, limp, as they lift you over the maw of the plant, and then inside.<<gtrauma>><<ggstress>><<trauma 6>><<stress 12>>
<br><br>

The fall jolts you to your senses. You land in a thick mucus that sticks to your skin, making it tingle.
<<set $feetgoo += 1>>
<<set $thighgoo += 1>>
<<set $bottomgoo += 1>>
<br><br>

<<link [[Try to escape|Forest Pitcher Struggle Start]]>><</link>><<willpowerdifficulty 1 700>>
<br>
<<link [[Endure|Forest Pitcher Endure]]>><</link>><<gggarousal>>
<br>