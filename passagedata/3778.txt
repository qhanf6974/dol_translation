<<set $outside to 0>><<effects>><<set $lock to 200>>
<<set $tailortheft to 1>>
<<set $tip to random(3000, 9000)>>
<<set $tip to Math.round($tip)>>
You deftly open the cash register and count out the money inside.
There's £<<print ($tip / 100)>>.
<br><br>
<<if $skulduggery lt 300>>
	<<skulduggeryskilluse>>
<<else>>
	<span class="blue">There's nothing more you can learn from locks this simple.</span>
<</if>>
<br><br>
<<if $rng gte 86>>
	<<skulshopevents>>
<<else>>
	<<link [[Take it|Tailor Shop]]>><<set $money += $tip>><<set $crime += ($tip / 100)>><</link>><<crime>>
	<br>
	<<link [[Leave it|Tailor Shop]]>><</link>>
	<br>
<</if>>