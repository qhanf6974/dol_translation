<<effects>>

<<if $submissive lte 850>>
You stare at <<him>> in defiance. "I'm not paying you. I don't care what you do to me."
<<elseif $submissive gte 1150>>
"N-no," you stutter, surprised by your bravery. "I-I won't."
<<else>>
"The money isn't yours," you say, "I'm keeping it."
<</if>>
<br><br>
"That's okay," <<he>> says, smiling. "I've already made arrangements. I don't know what they intend for you, and frankly I don't care." <<He>> produces a hood and length of rope. "Hold still."
<br><br>

<<link [[Submit|Rent Intro]]>><<endevent>><</link>>
<br>
	<<if $bus is "hospital">>
<<link [[Fight|Rent Hospital Fight]]>><<set $fightstart to 1>><</link>>
<br>
	<<else>>
<<link [[Fight|Rent Fight]]>><<set $fightstart to 1>><</link>>
<br>
	<</if>>