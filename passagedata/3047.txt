<<widget "swimminglessoneffects">><<nobr>>

<<pass 5>><<stress 1>><<tiredness 1>>
<<schooleffects>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
<<pass 5>><<stress 1>><<tiredness 1>>
<<schooleffects>>
<<else>>
<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
<<pass 5>><<stress 1>><<tiredness 1>>
<<schooleffects>>
<<else>>
<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
<<pass 5>><<stress 1>><<tiredness 1>>
<<schooleffects>>
<<else>>
<<set $phase to 1>>
<</if>>

<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
<<else>>
<<set $phase to 1>>
<</if>>

<<if $phase is 1>>
	<<set $phase to 0>>
  <<if $exposed gte 1 and $swimall isnot 1>>

		The bell rings, signifying the end of the lesson. You wait as the other students leave. Some students linger, doubtlessly hoping to get a nice view when you climb out, but Mason herds them away, giving you some privacy.
		<br><br>

	<<elseif $exposed lte 0>>

	  The bell rings, signifying the end of the lesson. You climb out of the pool.
	  <br><br>

	<</if>>

  <<if $time is ($hour * 60 + 57)>>
  <<pass 3>>
  <<elseif $time is ($hour * 60 + 58)>>
  <<pass 2>>
  <<elseif $time is ($hour * 60 + 59)>>
  <<pass 1>>
  <</if>>

  <<if $exposed lte 0 or $swimall is 1>>
	  <<link [[Boy's Changing Room|School Boy Changing Room]]>><</link>>
	  <br>

	  <<link [[Girl's Changing Room|School Girl Changing Room]]>><</link>>
	  <br><br>
	<</if>>

  <<if $exposed gte 1>>
  <<link [[Grab some towels and enter the boy's changing room|School Boy Changing Room]]>><<towelupm>><</link>>
  <br>
  <<link [[Grab some towels and enter the girl's changing room|School Girl Changing Room]]>><<towelup>><</link>>
  <br><br>
  <</if>>

<<else>>

  <<if $exposed gte 1>>

  <<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (8900 - $allure)>>
	<<eventsswimming>>
	<<else>>
	<<eventsswimmingsafe>>
	<</if>>

  <<elseif $exposed lte 0>>

  <<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (8900 - ($allure / 5))>>
	<<eventsswimming>>
	<<else>>
	<<eventsswimmingsafe>>
	<</if>>
  <</if>>
<</if>>

<</nobr>><</widget>>

<<widget "school_pool_swap">><<nobr>>
<<if $schoolrep.crossdress gte 1 and $school_pool_swap isnot true and $skulduggery gte 400>>
	<br>
	<<if $minute lte 40>>
		<span class="lblue">An idea strikes you.</span> Perhaps you could distract from your reputation as a crossdresser.
		<br>
		<<link [[Swap the boys' and girls' clothes (0:20)|School Changing Swap]]>><<schoolrep crossdress -1>><<pass 20>><<detention 12>><<set $school_pool_swap to true>><</link>><<ggdelinquency>>
		<br>
	<<else>>
		<span class="lblue">An idea strikes you.</span> Perhaps you could distract from your reputation as a crossdresser.
		<br>
		You peek into the pool. The lesson will end soon. You'd need more time to prepare.
		<br>
	<</if>>
<</if>>
<</nobr>><</widget>>