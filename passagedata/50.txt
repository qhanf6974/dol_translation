<<effects>><<set $outside to 0>><<set $location to "strip_club">>
<<set $wardrobe_location to "wardrobe">>
You are in the strip club's dressing room. <<if $daystate isnot "day" and $daystate isnot "dawn">>There are a few mirrors, currently occupied by staff fixing their hair and makeup.<</if>>
<br><br>
<<wardrobewear>>
<<if $exhibitionism lte 14>>
	<<if $exposed lte 0>>
		<<link [[Back to the club (0:01)|Strip Club]]>><<pass 1>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
		<br><br>
	<<else>>
		You can't go out like this!
		<br><br>
	<</if>>
<<elseif $exhibitionism gte 55>>
	<<link [[Back to the club (0:01)|Strip Club]]>><<pass 1>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
	<br><br>
<<else>>
	<<if $exposed lte 1>>
		<<link [[Back to the club (0:01)|Strip Club]]>><<pass 1>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
		<br><br>
	<<else>>
		You can't go out like this!
		<br><br>
	<</if>>
<</if>>
<<wardrobe>>