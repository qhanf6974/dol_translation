<<effects>>

You hold back your head, and scream.
<br><br>

"Stupid fucking <<bitch>>." the <<person1>><<person>> says, knocking over a table in <<his>> haste to reach you. "I'm going to enjoy fucking you up." <<He>> clasps a hand over your mouth as the <<person2>><<person>> holds you down.
<br><br>

Your cries are by the rough handling. The pair hold you still, and listen.

<<if $rng gte 51>>
	<<generate3>>
	A moment later you hear <span class="green">a knock at the door.</span> "Shit," the <<person1>><<person>> says. <<He>> turns to the <<person2>><<personstop>> "Don't just sit there you lemon. See them off."
	<br><br>
	The <<person2>><<person>> leaves the living room, and you hear the front door open. "Everything okay in there?" says an unfamiliar <<person3>><<if $pronoun is "m">>man's<<else>>woman's<</if>> voice. "Just I thought I heard a scream."
	<br>
	"Everything's fine in here. I just stubbed my toe."
	<br>
	"That so? The scream didn't sound like your voice."
	<br>
	"Maybe you shouldn't be so fucking nosy."
	<br><br>

	The <<person1>><<person>> tenses, <<his>> hand now clutching your face so hard it hurts.
	<br><br>

	<<link [[Make noise|Sold Noise]]>><</link>>
	<br>
	<<link [[Wait (0:02)|Sold Wait]]>><<pass 2>><</link>>
	<br>
<<else>>
	<span class="red">Nothing happens.</span> Confident they haven't been caught, the <<person1>><<person>> sneers at you. "Time to teach you a lesson," <<he>> says. "You're our toy. And no ones coming for you."
	<br><br>

	<<link [[Next|Sold Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

<</if>>