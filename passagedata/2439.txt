<<set $outside to 0>><<set $location to "police_station">><<effects>>
"I'll be right round," The <<person>> says. <<He>> enters the lobby proper moments later, holding a strange-looking key. <<He>> holds it up to the collar, and the lock clicks open, releasing your neck. <<He>> takes the collar. "As I said, government property."
<br><br>
<<set $worn.neck.cursed to 0>><<neckruined>><<set $worn.neck.collaredpolice to 0>>
<<link [[Next|Police Station]]>><</link>>
<br>