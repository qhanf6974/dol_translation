<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $NPCName[$NPCNameList.indexOf("Robin")].lust lte 19>>
You nudge closer to Robin and put an arm around <<himstop>> <<He>>smiles and leans closer to you in turn.
<br><br>
<<else>>
You nudge closer to Robin and put an arm around <<himstop>> <<He>>smiles and leans into you, greatly enjoying every inch of contact. "I could stay like this forever," <<he>> coos.
<br><br>
<</if>>

<<link [[Just watch|Robin Cinema Romance Watch]]>><<pass 15>><</link>>
<br>
<<link [[Rest|Robin Cinema Rest]]>><<tiredness -1>><<pass 15>><<npcincr Robin lust 1>><</link>><<ltiredness>><<glust>>
<br>
<<if $NPCName[$NPCNameList.indexOf("Robin")].lust gte 20>>
<<link [[Get closer still|Robin Cinema Frisky]]>><<arousal 300>><</link>><<garousal>>
<br>
<</if>>