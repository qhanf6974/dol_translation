<<set $outside to 1>><<set $location to "forest">><<effects>>

<<if $molestationstart is 1>>
	<<set $molestationstart to 0>>
	<<set $combat to 1>>
	<<molested>>
	<<controlloss>>

	<<set $vorestage to 1>>
	<<set $vorecreature to "snake">>
	<<set $vorestrength to 1>><<set $position to "doggy">>
	<<set $timer to 15>>
	<<resetLastOptions>>
<</if>>

<<if $timer is 12>>
	<<set $trance to 0>>
	You break free from the trance, and realise the snake is trying to eat you!
<</if>>

<<voreeffects>>
<<vore>>
<<voreactions>>

<<if $timer lte 0 or $vorestomach gte 5>>
	<span id="next"><<link [[Next|Eden Hunt Snake Finish]]>><</link>></span><<nexttext>>
<<elseif $vorestage lte 0>>
	<span id="next"><<link [[Next|Eden Hunt Snake Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link "Next">><<script>>state.display(state.active.title, null)<</script>><</link>></span><<nexttext>>
<</if>>