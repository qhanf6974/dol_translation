<<effects>>

<<npc Mason>><<person1>>

You swim towards Mason. <<He>> doesn't try to outswim you, and starts treading water as you draw close. <<He>> covers <<his>> toned body beneath the water.
<br><br>

"C-can I help you?" <<he>> asks, eager to return to <<his>> laps.
<br><br>

<<link [[Just say Hello|Lake Mason Greet]]>><<stress -6>><</link>><<lstress>>
<br>
<<if $mason_count lte 1>>
	<<link [[Ask to chat|Lake Mason Ask]]>><<npcincr Mason love 1>><</link>><<glove>>
	<br>
<</if>>
<<if $promiscuity gte 15>>
	<<link [[Ask to see|Lake Mason Flirt]]>><<npcincr Mason love -1>><<npcincr Mason lust 1>><<npcincr Mason dom -1>><</link>><<promiscuous2>><<llove>><<glust>>
	<br>
<</if>>