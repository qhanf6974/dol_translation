<<effects>>

You fumble at the lock. It's as simple as they come, but it's dark and you have little time.
<br><br>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>

You <span class="green">successfully</span> open the door and dart through before anyone arrives to retrieve their <<wallet>>. You glimpse an old basement before you pull up the door behind you, plunging you into darkness.
<br><br>

You hear footsteps walk down the stairs, then back up. The group's voices fade soon after, and you breathe a sigh of relief. <<tearful>> you leave the empty basement, and continue your journey.
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

<<endevent>>
<<destinationeventend>>
<br>

<<else>>

	You <span class="red">fail</span> to open it before a <<person1>><<person>> appears at the top of the staircase.
	<br><br>

	"Nice," <<he>> says with surprising calm. "Guys, look what I found." The others stick their heads over the railing, looking down at you. <<covered>><<fameexhibitionism 4>>
	<br><br>

	You walk up the stairs. At least you'll feel less trapped up there. The <<person>> grasps your arm and pulls you in front of <<himcomma>> exposing you to <<his>> friends.
	<br><br>

	"So you're some kind of pervert, exposing yourself for the thrill?" <<he>> whispers. "Don't worry. We'll help get you off."
	<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

	<<link [[Next|Street Ex Gang Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

<</if>>