<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
You drop to your knees and squeeze into the fireplace. You peer through the gloom, lit only by light entering far above. You see the scroll.
<<if $rng gte 81>>
	It's come unravelled. The text is large, and there isn't much. The paper is dominated by a drawing.
	<br><br>
	Curiosity strikes you.
	<br><br>
	<<link [[Read it|Temple Quarters Read]]>><</link>>
	<br>
	<<link [[Just deliver it|Temple Quarters Deliver]]>><</link>>
	<br>
<<else>>
	The seal is still intact.
	<br><br>
	Scroll safely tucked beneath your arm, you shuffle out from the tight space. The <<person>> awaits you, and clasps <<his>> hands together in joy.
	<br><br>
	"Thank you so much," <<he>> says. "These are very old, and wouldn't have lasted this long if everyone where as careless as me."
	<br><br>
	<<endevent>>
	<<link [[Next|Temple Quarters]]>><</link>>
	<br>
<</if>>