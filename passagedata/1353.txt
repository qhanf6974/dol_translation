<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "residential">>
You are in an alleyway in the residential district.
<<if $daystate is "day">>
	There are a lot of people around.
<<elseif $daystate is "dusk">>
	You hear children playing.
<<elseif $daystate is "night">>
	You hear a dog barking.
<<elseif $daystate is "dawn">>
	There are many people around.
<</if>>
Your home is nearby.
<<streeteffects>>
<br><br>
<<if $exposed gte 1>>
	<<exhibitionism "alley">>
<</if>>
<<if $arousal gte 10000>>
	<<orgasmstreet>>
<</if>>
<<if $stress gte 10000>>
	<<passoutalley>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $eventskip is 0>>
		<<eventsstreet>>
	<<else>>
		<<if $map.top is true>>
			<<map "residential">>
			<br>
		<</if>>
		Places of interest
		<br>
		<<if $exposed gte 1>>
			<<link [[Go home (0:02)->Home Fence]]>><<if $fenceclimb isnot 1>><<set $fenceclimb to 1>><<transform cat 1>><</if>><<pass 2>><</link>>
			<br>
		<<else>>
			<<link [[Go home (0:02)->Garden]]>><<if $fenceclimb isnot 1>><<set $fenceclimb to 1>><<transform cat 1>><</if>><<pass 2>><</link>>
			<br>
		<</if>>
		<<if $cat gte 6 and $daystate is "night">>
			<<link [[Meow (0:30)|Residential Meow]]>><<pass 30>><<stress -3>><</link>> | <span class="blue">Cat</span><<lstress>>
			<br>
		<</if>>
		<<if $daystate isnot "night">>
			<<if $exposed gte 2>>
				<<if $exhibitionism gte 75>>
					<<link [[Cross Connudatus Street (0:05)|Road Ex]]>><</link>><<if $ex_road is undefined>><<exhibitionist5>><</if>>
					<br>
				<</if>>
			<<elseif $exposed is 1>>
				<<if $exhibitionism gte 35>>
					<<link [[Cross Connudatus Street (0:05)|Road Ex]]>><</link>><<if $ex_road is undefined>><<exhibitionist3>><</if>>
					<br>
				<</if>>
			<</if>>
		<</if>>
		<br>
		<<if $exposed gte 1 and $daystate isnot "night">>
		<<else>>
			<<add_link "Travel<br>">><<hideDisplay>>
			<<domus>>
			<<barb>>
			<<danube>>
			<<connudatus>>
			<<loiter>>
		<</if>>
		<<add_link "<br>Alternate routes<br>">><<hideDisplay>>
		<<if $exposed gte 1 and $daystate isnot "night">>
		<<else>>
			<<commercial>>
		<</if>>
		<<stormdrain>>
		<<displayLinks>>
		<<if $map.top isnot true>>
			<br>
			<<map "residential">>
		<</if>>
	<</if>>
<</if>>
<<set $eventskip to 0>>