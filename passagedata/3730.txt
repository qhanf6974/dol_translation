<<set $outside to 0>><<set $location to "temple">><<effects>>
You walk around the corner. A <<person1>><<person>> pins a <<person3>><<person>> against the wall with <<his>> forearm, while a <<person2>><<person>> watches on, grinning. They turn to face you.
<br><br>
"What you looking at?" the <<person1>><<person>> spits. "Fuck off, or you're next."
<br><br>
<<link [[Fight to stop them|Soup Kitchen Fight]]>><<set $fightstart to 1>><<set $submissive -= 1>><</link>>
<br>
<<link [[Tell River|Soup Kitchen Tell]]>><</link>>
<br>
<<link [[Ignore|Soup Kitchen Ignore]]>><<set $submissive += 1>><</link>>
<br>