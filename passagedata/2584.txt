<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<set $englishattended to 1>>

<<if $time lte ($hour * 60 + 5)>>
	The English lesson begins, how do you want to conduct yourself?
	<br><br>
<<else>>
	The English lesson continues, how do you want to conduct yourself?
	<br><br>
<</if>>

<<if $kylarenglishstate is "active">>
	<<link [[Focus on the lesson|English Lesson Focus]]>><</link>><<gstress>><<genglish>>
	<br>
	<<link [[Socialise with classmates|English Lesson Socialise]]>><</link>><<ltrauma>><<lstress>><<gharass>>
	<br>
	<<link [[Daydream|English Lesson Daydream]]>><</link>><<lstress>> <<lharass>>
	<br>
	<<if $exhibitionism gte 75>>
		<<link [[Masturbate|English Lesson Masturbate]]>><<set $masturbationstart to 1>><<npcincr Kylar lust 5>><</link>><<exhibitionist5>><<glust>>
		<br>
	<</if>>
<<else>>
	<<link [[Focus on the lesson|English Lesson Focus]]>><</link>><<gstress>><<genglish>>
	<br>
	<<link [[Socialise with classmates|English Lesson Socialise]]>><</link>><<gcool>><<ltrauma>><<lstress>><<gharass>>
	<br>
	<<link [[Daydream|English Lesson Daydream]]>><</link>><<lstress>> <<lharass>>
	<br>
	<<if $exhibitionism gte 55>>
		<<link [[Masturbate|English Lesson Masturbate]]>><<set $masturbationstart to 1>><</link>><<exhibitionist4>>
		<br>
	<</if>>
<</if>>