<<effects>>

<<if $submissive gte 1150>>
	"P-please no," you say. "It's too embarrassing."
<<elseif $submissive lte 850>>
	"Fuck off," you say. "I don't need your help."
<<else>>
	"I can't," you say. "It's too embarrassing."
<</if>>
<br><br>

<<if $NPCName[$NPCNameList.indexOf("Avery")].rage gte random(20, 100)>><<set $averyragerevealed to 1>>
	Avery stares at you a moment, <span class="red">then bunches the towel in <<his>> fist.</span> "I made a fair request," <<he>> says, <<his>> voice cracking with anger. "You can see yourself home." <<He>> leaves the pantry.
	<br><br>

	<<averyscore>>

	You peek out after <<himstop>> Maybe you should call for help.
	<br><br>
	<<endevent>>
	<<link [[Call for help|Avery Party Dance Call]]>><</link>>
	<br>
	<<link [[Search for something to cover with|Avery Party Dance Search]]>><</link>>
	<br>
<<else>>
	Avery stares at you a moment, then throws you the towel. "Fine," <<he>> says, a twinge of irritance in <<his>> voice. You cover yourself.<<towelup>>
	<br><br>

	<<link [[Next|Avery Party Dance Return]]>><</link>>
	<br>
<</if>>