<<set $outside to 0>>

You try to peek behind the shutters but can't make anything out in the dark shop.
<br><br>

<<if $shopFloor is "bottom">>
	[[Leave|Shopping Centre]]
<<elseif $shopFloor is "top">>
	[[Leave|Shopping Centre Top]]
<</if>>
<<unset $shopFloor>>