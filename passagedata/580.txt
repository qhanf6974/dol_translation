<<set $outside to 0>><<set $location to "lake_ruin">><<underwater>><<effects>><<lakeeffects>>

You grip the plinth, and pull your <<bottom>> against it. The instant you do, a red light blazes into life, illuminating the room. Previously unseen statues stare down at you from above.
<br><br>

You jerk back, and convulse. The room shakes and trembles. A crack appears in the floor. It widens, letting in more of the red light. You feel the slime squirm from your ear. It vanishes into the crevice.
<br><br>

<<set $corruption_slime to 1>>

<<if $slime_tf is 1>>
<<set $slime_tf to 0>>
<<set $physicalTransform to 0>>
<</if>>

<<if $parasite.left_ear.name is "slime" or $parasite.right_ear.name is "slime">>
	<<unset $alwaysSleepNaked>>
	<<unset $slimeEvent>>
<</if>>

<<if $parasite.left_ear.name is "slime">>
<<removeparasite left_ear>>
<</if>>
<<if $parasite.right_ear.name is "slime">>
<<removeparasite right_ear>>
<</if>>

<<if $tentacledisable is "f">>

The light fades, and all falls quiet. Yet you feel a presence. Something stayed behind.
<br><br>

Over a dozen tentacles curl up the side of the plinth, forming a prison around you.
<br><br>

<<link [[Next|Lake Ruin Sit Tentacles]]>><<set $molestationstart to 1>><</link>>
<br>

<<else>>

<<link [[Next|Lake Ruin Plinth]]>><</link>>
<br>

<</if>>