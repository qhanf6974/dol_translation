<<effects>>
"Being stubborn?" Remy says. "Fine." <<He>> holds <<his>> hand in the air. Several farmhands rush in, each wielding a cattle prod. They surround you, just out of reach, their weapons pointed at your body.
<br><br>
Remy steps forward, holding a leash. You can't move anywhere without getting shocked.
<<if $livestock_muzzle is 1>>
	Remy clips it to your collar, then pulls a muzzle against your face.<<facewear 17>> <<He>> tries to pull you along behind <<himstop>>
<<else>>
	Remy clips it to your collar. <<He>> tries to pull you along behind <<himstop>>
<</if>>
<br><br><br><br>
<<if $leftarm isnot "bound" or $rightarm isnot "bound">>
	<<link [[Slap|Livestock Job Slap]]>><<npcincr Remy dom -1>><<npcincr Remy love -1>><<livestock_obey -5>><<trauma -6>><<stress -6>><</link>><<llobey>><<ltrauma>><<lstress>>
	<br>
<</if>>
<<link [[Go quietly|Livestock Job]]>><<npcincr Remy dom 1>><</link>>
<br>