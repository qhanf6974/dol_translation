You pretend to occupy yourself with your pancakes as you gently part your legs as naturally as you can.

/* are we stealthy enough to pretend not to be putting on a show? */
<<set $rng to random(0, 50)>>
<<if $skulduggery gte (350 + $rng)>>
	Knowing you're exposing yourself sends a gentle shiver down your spine but you make sure not to give any indication of your building arousal. Noticing <<his>> increased interest you decide to tease a little more. <<seatedflashcrotchunderskirt>>, giving <<him>> a clear view of your bare <<genitalsstop>>
	<br><br>
	The act of exposing your <<genitals>> to a stranger in a cafe gives you a lewd thrill.
	<<exhibitionism5>>

	/* check for photo scene */
	<<set $rng to random(0, 10)>>
	<<if $rng gte 8>>
		/* person attempts to take photo */
		<<CafeExhibitionismLegsPartPantilessPhotoDecision>>
	<<else>>
		<<CafeExhibitionismLegsPartNormalTerminate>>
	<</if>>

/* are we good enough to at least not provoke mockery at our poor attempts at stealth exhibitionism? */
<<elseif $skulduggery gte 100>>
	Knowing you're exposing yourself sends a gentle shiver down your spine but you make sure not to give any indication of your building arousal. Noticing <<his>> increased interest you decide to tease a little. <<seatedflashcrotchunderskirt>> giving <<him>> a clear view of your bare <<genitalsstop>> The <<person>> takes a sip of <<his>> drink and smirks a little to <<himself>>, it seems you weren't quite as subtle as you thought.
	<br><br>
	The act of exposing your <<genitals>> to a stranger in a cafe gives you a lewd thrill, and knowing <<he>> caught you doing it on purpose fills you with a mixture of nervousness and exhilaration.
	<<exhibitionism5>>

	You decide to stop before things get out of hand, finishing up your pancakes before carefully getting out of your seat and leaving. <<His>> eyes and knowing smirk follow you as you make your way to the door, but <<he>> seems happy to leave you alone.
	<br><br>
	<<CafeExhibitionismLegsPartNormalTerminate>>

/* were bad at this and should feel bad */
<<else>>
	Knowing you're exposing yourself sends a gentle shiver down your spine but you try your best not to give any indication of your building arousal. The <<person>> takes a sip of <<his>> drink and smirks a little to <<himself>>, barely suppressing a chuckle. It seems you were nowhere near as subtle as you thought.
	<br><br>
	The act of exposing your <<genitals>> to a stranger in a cafe gives you a lewd thrill, and knowing <<he>> caught you doing it on purpose fills you with a mixture of nervousness and exhilaration.
	<<exhibitionism4>>

	Embarrassed, you finish up your pancakes before getting out of your seat and leaving. <<His>> eyes and knowing smirk follow you as you make your way to the door, but <<he>> seems happy to leave you alone.
	<br><br>
	<<CafeExhibitionismLegsPartNormalTerminate>>
<</if>>