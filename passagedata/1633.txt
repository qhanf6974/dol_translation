<<set $outside to 0>><<set $location to "cafe">><<effects>>

You step away from the podium. The crowd sounds disappointed, though not upset. You hear someone call you cute. The mayor rushes to take over the microphone. <<npc Quinn>><<person1>>"Our chef isn't used to public speaking," <<he>> says. "Please, a round of applause regardless." The audience applaud you as prompted.
<br><br>

The mayor steps away from the podium, and the party begins in earnest. How do you want to conduct yourself?
<br><br>

<<endevent>>

<<chefspeechoptions>>