<<effects>><<set $outside to 0>>

You submit to being bound. <<person2>> The <<person>> ties your arms together behind your back while the <<person1>><<person>> looks on, a sardonic smile on <<his>> face. It doesn't take long, the <<person2>><<person>> soon stands back to admire <<his>> handiwork. You try to move your arms, but they are tied together tightly. You can only wave them impotently behind your back. Feelings of helplessness rise within you.
<<set $leftarm to "bound">><<set $rightarm to "bound">>
<br><br>

"Just one more thing I think," the <<person>> says, producing a collar and leash.
<br><br>

<<link [[Accept|Beach Abduction Collared]]>><<neckwear 1>><<set $phase to 1>><</link>>
<br>
<<link [[Refuse|Beach Abduction Molestation]]>><<set $molestationstart to 1>><</link>>
<br>