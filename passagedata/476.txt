<<set $outside to 1>><<set $location to "forest">><<effects>>

<<if $corruption_slime gte ($willpower / 10)>>
	<<generate1>><<generate2>>
	You turn and run into the forest, making sure you get far away from the pair and whatever the slime wants you to do. You feel a jolt of pain as the slime punishes you for defiance. It tries to force your legs in the other direction, <span class="red">and overpowers your will.</span>
	<<ggwillpower>><<willpower 3>>
	<br><br>

	You stop in the middle of the trail, and wait. A <<person1>><<person>> and <<person2>><<person>> walk into view. They slow down when they see the strange <<girl>> stood still, staring at them.
	<br><br>
	"You lost?" the <<person1>><<person>> asks.
	<br><br>

	"I-I need," you say. "I need you to have me."
	<br><br>

	The pair share a grin. "We'll be happy to oblige," <<he>> says, stepping closer.
	<br><br>

	<<link [[Next|Forest Slime Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

<<else>>

	You turn and run into the forest, making sure you get far away from the pair and whatever the slime wants you to do. You feel a jolt of pain as the slime punishes you for defiance. It tries to force your legs in the other direction, <span class="green">but fails.</span>
	<<gwillpower>><<willpower 1>>
	<br><br>
	<<endevent>>
	<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
	<br>

<</if>>