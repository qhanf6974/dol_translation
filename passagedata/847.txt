<<effects>>

<<if $submissive gte 1150>>
	"S-Sorry!" you say. "I froze up. I didn't want you to get hurt."
<<elseif $submissive lte 850>>
	"I thought you were gonna run," you say. "Sorry."
<<else>>
	"I didn't want this," you say. "I'm sorry."
<</if>>
<br><br>

"It's alright," Alex says, grasping your arm and pulling <<him>> to <<his>> feet. "Let's just get back to work."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>