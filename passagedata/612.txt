<<set $outside to 0>><<set $location to "tentworld">><<effects>>
<<if $tentacleangel is undefined>>
	<<set $tentacleangel to 0>>
	You kneel and reflect upon the events that led you to this place. Completely disgusted by your surroundings, you find the willpower to continue. These horrors will not hold you.
	<<llarousal>><<arousal -12>>
	<br><br>
	<<link [[Next|Tentacle Plains]]>><</link>>
	<br>
<<elseif $tentacleangel is 0>>
	<<set $tentacleangel to 1>>
	As you meditate in the barren Plains, the ground around you almost seems to get brighter. You feel better about your situation.
	<<lstress>><<stress -6>><<llarousal>><<arousal -12>>
	<br><br>
	<<link [[Next|Tentacle Plains]]>><</link>>
	<br>
<<elseif $tentacleangel is 1>>
	<<set $tentacleangel to 2>>
	In the depths of your mind, you hear a distant choir sing a single note. The halo above your head is getting brighter, and the lewd warmth within you melts away. <span class="gold">This place will not hold you.</span>
	<<lllarousal>><<arousal -24>>
	<br><br>
	<<link [[Next|Tentacle Plains]]>><</link>>
	<br>
<<elseif $tentacleangel is 2>>
	<<set $tentacleangel to 3>>
	You are a beacon of light. The tentacles grasping your feet retract away from you. You are above this wasteland, it does not deserve your radiant presence. You keep repeating that to yourself. <span class="gold">This place will not hold you.</span>
	<<lllarousal>><<arousal -24>>
	<br><br>
	<<link [[Next|Tentacle Plains]]>><</link>>
	<br>
<<else>>
	The light from your halo is blinding. You are above this place. You are above this place. You repeat the mantra out loud. The floor falls away from beneath you. You can't see the tentacles. The light from your halo encompasses all. You feel yourself rising higher and higher, the dark sky offering no resistance to your ascent. You rise and rise.
	<br><br>
	<<endevent>><<tentacleworldend>>
	<<set $forest to random(50, 100)>><<set $asylumtentacleescape to 1>><<set $asylumescaped to 1>>
	<<link [[Next|Mirror]]>><<set $location to "home">><</link>>
	<br>
<</if>>