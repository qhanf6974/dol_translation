<<set $outside to 0>><<set $location to "town">><<effects>>

A sinister convoy of police vans arrive, and you are herded into one with the others. The police take you to the station and put you in a holding cell.
<br><br>
"With so many in today, this might take some time," the guard tells you. "They'll get to you. Sit tight."
<br><br>

<<link [[Next|Police Cell]]>><<pass 2 hour>><</link>>