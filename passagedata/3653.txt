<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

The <<person1>><<person>> backs away from you, looking confused and ashamed. "F-forgive me-" <<he>> says, dropping to <<his>> knees and staring at the ceiling. <<He>> slips into prayer. The others follow suit.
<br><br>

<<tearful>> you escape without difficulty.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Temple]]>><</link>>

<<elseif $enemyhealth lte 0>>

You shove the <<monk>> away from you, creating a gap in their circle. <<tearful>> you seize the opportunity. You escape into another room and take refuge in the shadow of a stone pillar.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Temple]]>><</link>>

<<else>>

<<tearful>> you fall to the ground. You're too hurt to continue fighting. The three surround you, looking down with disdain.
<br><br>

<<clotheson>>

<<link [[Next|Temple Arcade Submit]]>><</link>>

<</if>>