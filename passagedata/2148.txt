<<effects>>

<<if $enemyhealth lte 0>>
	<<beastwound>>
	<<if $combatTrain.length gt 0>>
		You keep the <<beasttype>> at bay with your feet, but another is eager for a go.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Abduction Hospital Hole Wolves]]
	<<else>>
		You keep the <<beasttype>> at bay with your feet, it recoils in pain and fear.
		<br><br>

		<<if $enemywounded gte 2 and $enemyejaculated is 0>>
			Feeling that you're more trouble than you're worth, the wolves flee.
		<<elseif $enemywounded is 0 and $enemyejaculated gte 2>>
			The wolves leave you spent and shivering in the wall.
		<<elseif $enemywounded gte 1 and $enemyejaculated gte 1>>
			The wolves leave you spent and shivering in the wall.
		<</if>>
		<br><br>

		<<set $position to 0>>
		<<clotheson>>
		<<endcombat>>

		<<passouthospital>>
	<</if>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	<<if $combatTrain.length gt 0>>
		Satisfied, the <<beasttype>> moves and another takes its turn with your helpless rear.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Abduction Hospital Hole Wolves]]
	<<else>>
		Satisfied, the <<beasttype>> moves away from you.
		<br><br>

		<<if $enemywounded gte 2 and $enemyejaculated is 0>>
			Feeling that you're more trouble than you're worth, the wolves flee.
		<<elseif $enemywounded is 0 and $enemyejaculated gte 2>>
			The wolves leave you spent and shivering in the wall.
		<<elseif $enemywounded gte 1 and $enemyejaculated gte 1>>
			The wolves leave you spent and shivering in the wall.
		<</if>>
		<br><br>

		<<set $position to 0>>
		<<clotheson>>
		<<endcombat>>

		<<passouthospital>>
	<</if>>
<</if>>