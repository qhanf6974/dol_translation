<<set $outside to 0>><<set $location to "strip_club">><<effects>>

You are in the bathroom at the Strip Club. There are several private cubicles for the dancers and staff to use after and during work. They look clean and well maintained.
<br><br>

<<if $stress gte 10000>><<passoutshop>><<else>>
<</if>>

<<link [[Have a shower (0:20)->Strip Club Shower]]>><<strip>><<pass 20>><<stress -1>><<set $hygiene to 0>><</link>><<lstress>>
<br><br>
<<link [[Back to the club (0:01)|Strip Club]]>><<pass 1>><</link>>
<br>