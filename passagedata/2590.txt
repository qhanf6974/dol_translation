<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 90>>
	You pick up your things to move to another seat. Kylar rests a hand on your thigh. <<His>> hand clutches a knife. "Wh-where are you going, my love?" <<he>> says. "We already have our seats."
	<br><br>
	<<link [[Next|English Lesson]]>><<endevent>><</link>>
	<br>
<<else>>
	<<set $kylarenglishstate to "watching">>
	You pick up your things and move to another seat. Kylar doesn't follow, but starts stabbing <<his>> hand with a pen.
	<br><br>
	<<link [[Next|English Lesson]]>><<endevent>><</link>>
	<br>
<</if>>