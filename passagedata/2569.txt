<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $phase is 1>>
	<<He>> sits with you for a few minutes, and helps you fill in some holes in your understanding. You feel like you understand the material better.
	<br><br>
	<<link [[Next|English Lesson]]>><<endevent>><</link>>
	<br>
<<elseif $phase is 0>>
	<<He>> nods and moves on.
	<br><br>
	<<link [[Next|English Lesson]]>><<endevent>><</link>>
	<br>
<</if>>
<<endnpc>>