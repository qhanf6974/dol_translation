<<effects>><<set $location to "sewers">><<set $outside to 0>><<water>>

There's no way out, except a river running along a canal into darkness. One side is blocked by a metal grate. You lower yourself into the water and swim against the gentle current, emerging in a large room. You climb onto the shore.
<br><br>
<<set $sewersevent to random(5, 12)>>
<<link [[Next|Sewers Residential]]>><</link>>
<br>