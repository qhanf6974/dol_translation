<<effects>>

<<if $NPCName[$NPCNameList.indexOf("Eden")].love gte 20>><<set $edenfreedom to 1>><<set $edenshopping to 0>>

<<npc Eden>><<person1>>"Why would you want to leave?" <<he>> asks. "You have everything you need here."
<br><br>

	<<if $submissive gte 1150>>
	"B-but if I'm gone from town too long," you say. "People will come looking for me here."
	<<elseif $submissive lte 850>>
	"Do you think my absence isn't noticed?" you say. "They'll come looking for me here."
	<br><br>
	<<else>>
	"If I keep missing school," you say. "People will start looking for me."
	<</if>>
	<br><br>

<<He>> seems conflicted. "I suppose it was too much to hope you'd be content stuck here. Fine, you can go back to town. Make sure you get your little bottom back here though. <span class="gold">If you're gone longer than a day you'll worry me sick.</span> I'd have to come find you."
<br><br>

<<link [[Hug|Eden Freedom 2]]>><<set $phase to 0>><<npcincr Eden love 1>><</link>>
<br>
<<link [[Nod|Eden Freedom 2]]>><<set $phase to 1>><</link>>
<br>

<<else>>

<<npc Eden>><<person1>>"Why would you want to leave?" <<he>> asks. "You have everything you need here."
<br><br>

	<<if $submissive gte 1150>>
	"B-but if I'm gone from town too long," you say. "People will come looking for me here."
	<<elseif $submissive lte 850>>
	"Do you think my absence isn't noticed?" you say. "They'll come looking for me here."
	<br><br>
	<<else>>
	"If I keep missing school," you say. "People will start looking for me."
	<</if>>
	<br><br>
<<He>> shakes <<his>> head. "They won't find us. I know these woods, and will protect what's mine if necessary."
<br><br>

<i>If <<he>> liked you more <<he>> might be more amenable to your request.</i>
<br><br>

<<if $bus is "edenclearing">>
<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
<br>
<<else>>
<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
<br>
<</if>>

<</if>>