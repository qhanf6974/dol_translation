<<effects>>

<<if $rng gte 51>>
You endure the <<person1>><<persons>> invasive groping. It continues for several minutes, until <<he>> at last grows bored.
<br><br>

<<tearful>> you try to focus on your display.
<br><br>

<<stall_actions>>

<<else>>
	<<generate2>>
	You endure the <<person1>><<persons>> invasive groping. It continues for several minutes, until a <<person2>><<person>> approaches your display. <<Hes>> so taken by it <<he>> doesn't notice what's going on behind the table. Not until <<hes>> close.
	<br><br>

	"Can I interest you in this <<girl>> here?" the <<person1>><<person>> says from behind. "Only £100. Limited time offer."
	<br><br>

	The <<person2>><<person>> strokes <<his>> chin in thought. "£60. And not a penny more."
	<br><br>

	They continue to haggle over you.
	<br><br>

	<<link [[Protest|Stall Protest]]>><</link>>
	<br>
	<<link [[Remain Silent|Stall Wait]]>><</link>>
	<br>

<</if>>