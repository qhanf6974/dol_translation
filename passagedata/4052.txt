<<set $outside to 1>><<set $location to "beach">><<effects>>
<<set $seductiondifficulty to 4000>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>>
<br><br>
<<promiscuity2>>
<br><br>
<<if $seductionrating gte $seductionrequired>>
	You decide to be more assertive, flirting with potential customers and disarming them with charm. When you return to Robin there's a queue. <<He>> smiles at you.
	<br><br>
	"I don't know how you did it, but thank you." <<He>> kisses you on the cheek.
	<<glove>><<npcincr Robin love 1>><<ltrauma>><<lstress>><<trauma -3>><<stress -6>>
	<br><br>
	<<endevent>>
	<<link [[Offer help (0:30)|Robin's Lemonade Help]]>><<npcincr Robin love 1>><<pass 30>><</link>><<glove>>
	<br>
	<<link [[Leave|Beach]]>><</link>>
	<br>
<<else>>
	<<generate2>><<person2>>
	You turn on the charm, but still can't get anyone interested. You return to Robin. <<person1>><<Hes>> servicing a <<person2>><<personstop>>
	<br><br>
	"That'll be one pound please."
	<br><br>
	"Thanks," the <<person>> says. "You interested in making a little extra? I know a quiet place we could go."
	<br><br>
	Robin looks confused. "But I need my stand in a busy place so people can see it. Thanks for trying to help though."
	<br><br>
	"That's not what I-," the <<person>> says. "You know what, forget it."
	<br><br>
	Robin notices you as the <<person>> leaves.<<person1>>"Business has improved a lot." <<He>> says. "Thank you."
	<br><br>
	<<endevent>>
	<<link [[Offer help (0:30)|Robin's Lemonade Help]]>><<npcincr Robin love 1>><<pass 30>><</link>><<glove>>
	<br>
	<<link [[Leave|Beach]]>><</link>>
	<br>
<</if>>