<<effects>>

<<if $phase is 0>>
	<<set $sciencelichentempleready += 1>>
	You measure the pink lichen. You note down its weight, its volume, where you found it and describe what it looks like. You note that it smells of strawberries. You feel warm.
	<<garousal>><<arousal 600>>
	<br><br>
<<elseif $phase is 1>>
	<<set $sciencelichenparkready += 1>>
	You measure the white lichen. You note down its weight, its volume, where you found it and describe what it looks like. You note that it smells of soot. You sneeze.
	<br><br>
<<elseif $phase is 2>>
	<<set $sciencelichendrainready += 1>>
	You measure the violet lichen. You note down its weight, its volume, where you found it and describe what it looks like. You note that it smells of the sea. You feel strange and agitated.
	<<ggarousal>><<arousal 3000>>
	<br><br>
<<else>>
	<<set $sciencelichenlakeready += 1>>
	You measure the purple lichen. You note down its weight, its volume, where you found it and describe what it looks like. You note that it smells sweet. Your hand is shaking by the end.
	<<set $drugged += 180>>
	<br><br>
<</if>>

<<link [[Next|Science Project]]>><<set $phase to 0>><</link>>
<br>