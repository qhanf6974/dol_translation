<<effects>>

<<if $tending gte random(1, 300)>>
	You soothe the chickens. They're slow on the uptake, <span class="green">but calm down once they notice you.</span><<set $farm_work.chickens_panic to 0>><<gfarm>><<farm_yield 1>><<ggtending>><<tending 9>>
	<br><br>

	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	You try to soothe the chickens, <span class="red">but they don't even notice you.</span><<gtending>><<tending 2>>
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<</if>>