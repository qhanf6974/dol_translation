<<set $outside to 0>><<set $location to "home">><<effects>>
<<if $NPCName[$NPCNameList.indexOf("Robin")].init is 1 and $robinmissing isnot 1>>
	<<if $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 80>>
		You enter the orphanage and head to your room. You don't meet anyone. There's a small cake sat on the windowsill with a note attached.
		<br><br>
		"Welcome home!
		<br>
		- Robin"
		<br><br>
	<<elseif $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 20>>
		You enter the orphanage and head to your room. You find <<npc Robin>><<person1>>Robin waiting. <<He>> jumps to <<his>> feet and hugs you. "Bailey said the hospital phoned," <<he>> says. "They told us you were coming back." <<He>> pulls away and smiles. "They wouldn't let me visit. I got you something." <<He>> drops to <<his>> knees and pulls something out from under your bed. It's a small cake.
		<br><br>
		"I had to hide it." <<He>> hugs you again. "Welcome home. I missed you."
		<br><br>
		You chat with <<him>> a while. <<He>> asks about where you stayed. What your room was like, if the food was good, if the people were nice. You're not sure how much to tell <<himstop>> "I need to go," <<he>> says after a while. "Bailey gave me chores." <<He>> waves goodbye as <<he>> leaves.
		<<ltrauma>><<lstress>><<trauma -12>><<stress -12>>
		<br><br>
	<<else>>
		A face appears in a window as you approach the orphanage. It's Robin. <<npc Robin>><<person1>><<He>> runs at you once you're inside, and almost knocks you over with <<his>> hug. "Bailey said the hospital phoned," <<he>> says. "They told us you were coming back." <<He>> pulls away and smiles. "They wouldn't let me visit. I got you something. Come with me." <<He>> takes your hand and tugs you after <<himstop>>
		<br><br>
		<<He>> drops to <<his>> knees once in your room, and pulls something out from under your bed. It's a small cake. "I had to hide it." <<He>> hugs you again. "Welcome home. I missed you."
		<br><br>
		You chat with <<him>> a while. <<He>> asks about where you stayed. What your room was like, if the food was good, if the people were nice. You're not sure how much to tell <<himstop>> "I need to go," <<he>> says after a while. "Bailey gave me chores." <<He>> waves goodbye as <<he>> leaves.
		<<ltrauma>><<lstress>><<trauma -12>><<stress -12>>
		<br><br>
	<</if>>
<<else>>
	You enter the orphanage and head to your room. You don't meet anyone.
	<br><br>
<</if>>

<<endevent>>
<<link [[Next|Bedroom]]>><</link>>
<br>