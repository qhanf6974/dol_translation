<<if $enemyhealth lte 0>>

The <<person>> staggers away from you.

	<<if $undergroundwater gte 1>>
	"You'll give in." <<he>> says. "And if you don't, you'll become too weak to fight back. Why make it harder on yourself?" <<He>> slams the door shut.
	<<else>>
	"So that's how it's gonna be?" <<he>> says, looking disappointed. "Fine. We'll see how stubborn you are without water." <<He>> slams the door shut.
	<<set $undergroundwater to 1>>
	<</if>>

<<clotheson>>
<<endcombat>>

<<tearful>> you sit on the mattress.
<br><br>
<<endevent>>

<<link [[Next|Underground Cell]]>><</link>>

<<elseif $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

<<He>> pants. "Maybe... maybe another time. I need a lie down." The door slams shut behind <<himcomma>> plunging you into darkness.
<br><br>

<<tearful>> you sit on the mattress.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Underground Cell]]>><</link>>

<<elseif $pain gte 1>>

You're too badly beaten to put up a fight. The <<person>> drags your helpless body from the cell.
<br><br>

<<if $phase is 1>>
<<link [[Next|Underground Dance Intro]]>><</link>>
<br><br>
<<elseif $phase is 2>>
<<link [[Next|Underground Presentation]]>><</link>>
<br>
<<elseif $phase is 3>>
<<link [[Next|Underground Stage Intro]]>><</link>>
<br>
<<elseif $phase is 4>>
<<link [[Next|Underground Film Intro]]>><</link>>
<br>
<<elseif $phase is 5>>
<<link [[Next|Underground Hunt Intro]]>><</link>>
<br>
<</if>>

<<endcombat>>

<</if>>