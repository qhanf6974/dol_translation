<<effects>><<set $location to "sewers">><<set $outside to 0>><<water>>
There's no way out, except a river running along a canal into darkness. One side is blocked by a metal grate.
<br><br>
Morgan lurches after you, slowed by legs tangled in their decaying outfit. You drop into the water and swim.
<br><br>
You swim against the gentle current and emerge in a large room. You climb onto the shore. "I'm coming <<charlescomma>>" Morgan's voice shouts behind you. You hear a splash of water. <span class="red">You are being hunted.</span>
<br><br>
<<link [[Next|Sewers Residential]]>><</link>>
<br>