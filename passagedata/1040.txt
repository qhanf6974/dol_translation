<<effects>>
<<if $cow gte 6>>
	You crawl on your hands and knees, searching for tasty tufts of grass. You find a particularly juicy one, and enjoy chewing it.
	<<if $uncomfortable.nude is true or $uncomfortable.underwear is true>>
		<br><br>
		<span class="gold">Your exposure doesn't concern you as it once did. It feels natural.</span>
		<<set $uncomfortable.underwear to false>>
		<<set $uncomfortable.nude to false>>
	<</if>>
<<elseif $cow gte 5>>
	You kneel in the grass and lean forward, pulling up tufts of grass with your teeth. You find a particularly tasty patch.
<<elseif $cow gte 4>>
	You kneel in the grass and lean forward, pulling up tufts of grass with your teeth. It tastes better this way.
<<elseif $cow gte 3>>
	You kneel in the grass, and pull up a tuft. It doesn't taste that bad.
<<elseif $cow gte 2>>
	You kneel in the grass, and pull up a tuft. It takes a long time to chew, but goes down easily enough.
<<elseif $cow gte 1>>
	You pull up some grass, and nibble the blades. It doesn't taste bad, but it's not great either.
<<else>>
	You pull up some grass, and nibble the blades. They taste awful. You're not even sure you can digest them.
<</if>>
<br><br>

<<if $rng gte 81>>
	<<if $monsterchance gte 1 and ($hallucinations gte 1 or $monsterhallucinations is "f")>>
		<<if $malechance lt random(1, 100)>>
			A cowgirl sniffs you, and moos. You pet its head.
			<<lstress>><<stress -6>>
		<<else>>
			A bullboy sniffs you, and moos. You pet its head.
			<<lstress>><<stress -6>>
		<</if>>
	<<else>>
		A cow sniffs you, and moos. You pet its head.
		<<lstress>><<stress -6>>
	<</if>>
	<br><br>
<<elseif $rng gte 51>>
	<<if $uncomfortable.nude is true>>
		You look up, and see a <<generate1>><<person1>><<person>> leering at you. <<covered>><<gstress>><<stress 6>>
	<<else>>
		You look up for just a moment. You spot a <<generate1>><<person1>><<person>> leering at you,
		but you pay <<him>> no mind as you continue your search for tasty tufts of grass.
	<</if>>
	<br><br>
<<else>>
	<<livestock_overhear>>
<</if>>

<<link [[Next|Livestock Field]]>><<endevent>><</link>>
<br>