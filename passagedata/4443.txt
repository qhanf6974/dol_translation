<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewersrubble">>
<<set $sewersevent -= 1>>
You are in the old sewers. Rubble is strewn throughout this tunnel. The stone is different to that which makes up the walls and floor.
<br><br>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
		<<link [[Hide behind rubble|Sewers Rubble Hide]]>><</link>>
		<br>
	<</if>>
	<<link [[Ruined tunnel (0:05)|Sewers Ruins]]>><<pass 5>><</link>>
	<br>
	<<link [[Tunnel filled with detritus (0:05)|Sewers Industrial]]>><<pass 5>><</link>>
	<br><br>
<</if>>