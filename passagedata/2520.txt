<<set $outside to 0>><<schooleffects>><<effects>>
You lean against the lockers and bend over. The <<person>> walks beside you, and leans on your back. "Make sure you get this," <<he>> says. There's silence for a moment, then a loud smack as something hits your <<bottomstop>> Then another. <<Hes>> spanking you with a float used by the younger students. It's made of foam, but <<he>> whips it against your skin with such speed that the pain makes you jolt.
<<gpain>><<set $pain += 40>>
<br><br>
<<fameexhibitionism 50>>
"Someone else have a go," <<he>> says after a while. Someone does. They aren't as effective, but the <<person>> leans against the lockers and pulls your hair back. <<He>> leans close. "This is what happens to <<if $exposed gte 2 and $player.gender is "h">>freaks<<elseif $player.gender_appearance is "f">>girls<<else>>'girls'<</if>> who come in here," <<he>> says. Another jolt of pain. "Consider yourself lucky I'm so nice. Some wanted much worse." <<He>> glances at your body, and regret darkens <<his>> face. <<He>> stands up. "That's enough. <<pShes>> learnt <<pher>> lesson." <<He>> throws your clothes at you. <<tearful>> you take your clothing and leave the room.
<br><br>
<<clothesontowel>>
<<endcombat>>
<<link [[Next|School Pool Entrance]]>><</link>>
<br>