<<effects>>
<<npc Eden>><<person1>>
<<if $phase is 0>>

"What's yours is mine anyway," Eden says. "Thanks though."
<br><br>

	<<if $bus is "edenclearing">>
	<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
	<br>
	<<else>>
	<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
	<br>
	<</if>>

<<elseif $phase is 1>>

"Sounds about right," <<he>> reaches into <<his>> pouch. "Here you go."
<br><br>

You've gained £50.
<<set $money += 5000>>
<br><br>

	<<if $bus is "edenclearing">>
	<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
	<br>
	<<else>>
	<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
	<br>
	<</if>>

<<else>>

<<skulduggerycheck>>
	<<if $skulduggerysuccess is 1>>

	"Two hundred?" Eden looks annoyed. "You got ripped off." <<He>> reaches into <<his>> pouch. "Still, worth me not doing it. Here you go."
	<br><br>

You've gained £200.
<<set $money += 20000>>
<br><br>

		<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
		<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
		<</if>>

		<<if $bus is "edenclearing">>
		<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
		<br>
		<<else>>
		<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
		<br>
		<</if>>

	<<else>>

	"I'm not gonna fall for that," Eden says. "You can pay out of your own pocket."
	<br><br>

		<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
		<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
		<</if>>

		<<if $bus is "edenclearing">>
		<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
		<br>
		<<else>>
		<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
		<br>
		<</if>>

	<</if>>

<</if>>