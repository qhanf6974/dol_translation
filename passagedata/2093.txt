<<set $outside to 0>><<set $location to "town">><<effects>>
"And any jewellery."
<br><br>
You take off your clothes. <<if $exhibitionism gte 55>>Disappointingly,<<elseif $exhibitionism lt 15>>Thankfully,<</if>> the kid pays you zero attention as you strip,
focusing heavily on your clothes. Once you are fully nude, all your clothes are shoved into a metal basket. The lid clips down.
<br><br>
"Faraday cage," the eyes fix on your face. "Stops all transmissions. All listening devices. All radio. All bugs," said with a meaningful glance. "Nothing in or out."
<br>
"What?"
<br>
The kid studies your face for a long moment. Seems to weigh something.
<<if $hairlength gte 200>><<pass 5>>
	<br>
	"Okay. Look, if- Wait! Wait right there... Nice try!"
	<br>
	The kid glares at your hair, walks up to you and runs fingers through your hair, checking it. This goes on for a few minutes. You ears are also checked for earrings
	and headsets.
<</if>>
<br><br>
"Good-good. Okay. Look if you know about me, and Landry knows about me, then it's no longer safe. Word's out and spreading. Just a matter of time before Bailey hears something.
That would be bad. I need to disappear. I'll help you later. But I need you now. Need you to do three things."
<br>
You nod. A small USB device is shoved into your hand.
<br><br>
"One. There's a computer in Bailey's office. Ancient. Full of holes. It's where all the orphan records are. Easy pickings. Plug this device into it.
Computer has to be on and logged in - probably need to catch Bailey using it. This will let me in so I can delete all records of myself.
<br>
"Two. Get into Leighton's office. At the school. There's a safebox under the desk. SD memory cards. Lots of them. Pictures of things. Things Leighton does to naive people who don't know better.
People like I was. Leighton can not keep those. I don't allow it. So steal them. You'll have to pick the lock. Don't get caught.
<br>
"Three. Put in a good word with Landry. I can make Landry money. Insane money. And I need a place to lay low. So it's win-win: Landry hides me - I make Landry very rich.
Bailey never finds me. So talk to Landry. BUT. Do the other stuff first. That's more urgent."
<br><br>
"Okay," you say.
<br>
"Good-good. Do this for me, I'll look out for you. I mean... you'll probably never see me again. But I will. Promise. Anyway get to it."
<br>
"Uh..."
<br>
"What? Get to it!"
<br>
You wave a hand to indicate your nudity.
<br>
"Oh, wow," the eyes widen. "Sorry."
<br>
The kid grabs the clothes basket, passes it over.
<br>
"Sorry. Forgot. You... uh, look good."
<br><br>
You grab your clothes and quickly dress.
<br><br>
<<link [[Next|Orphanage]]>><<storeon "mickey">><<set $hacker_tasks to []>><</link>>