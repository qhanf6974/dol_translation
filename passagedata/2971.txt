<<set $outside to 0>><<set $location to "town">><<effects>>

More students arrive, some carrying papers, others here to watch. Leighton instructs the other entrants to sit in the front row.
<br><br>
<<npc Leighton>><<person1>>
The <<if $pronoun is "m">>headmaster<<else>>headmistress<</if>> climbs the stage and taps the microphone. River stands beside <<himstop>> "Thank you, boys and girls, ladies and gentlemen, for coming," <<he>> says. "This is a fuller audience than we expected. I'll be brief. Myself, River and two guests from outside town will judge. Each contestant will present their solution on stage, and use the projector to display their work."

<<if $anxiety gte 2>>
You feel a little sick.
<<ggstress>><<stress 18>>
<<elseif $anxiety gte 1>>
You feel anxious.
<<gstress>><<stress 6>>
<</if>>
<br><br>
A nervous murmur erupts from the front row. River frowns. "Did I forget to mention that part?"
<br><br>
<<generatey2>><<person2>>
The first student to be called up, a <<personcomma>> tries to flee the room. <<Hes>> stopped by <<his>> friends and convinced to go through with it. <<He>> trips over <<his>> words and drops <<his>> papers twice, but <<hes>> smiling at the end of it.
<br><br>

The next student is more confident. Leighton calls the entrants up one by one, working down the row. You'll be the last. Not a single student's solution is anything like another's.
<br><br>
<<endevent>>

Leighton points at you. <<if $anxiety gte 1>>You swallow your nerves and climb the stage.<<else>>You climb the stage.<</if>>
<br><br>

<<link [[Next|Maths Competition Stage]]>><</link>>
<br>