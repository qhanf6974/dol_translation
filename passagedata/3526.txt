<<effects>>

You break free from the <<person1>><<persons>> grip, and run.

<<if $athletics gte random(1, 400)>>
	<<He>> tries to grab you again, <span class="green">but you're too quick.</span> You run out onto the street.
	<br><br>

	<<link [[Next|Connudatus Street]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>
<<else>>
	<<He>> tries to grab you again, <span class="red">and manages to clasp <<his>> hand over your shoulder.</span> "I'm serious," <<he>> says, steering you back into the club. "You'll get hurt carrying on like that."
	<br><br>

	<<link [[Next|Strip Club]]>><<endevent>><</link>>
	<br>
<</if>>