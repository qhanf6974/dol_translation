<<set $outside to 0>><<set $location to "town">><<effects>>
<<generate1>><<generate2>>You decide against going through the strange hatch. The police arrive, panting. One of them threatens you with a taser. "We've got you," <<he>> says. "You're coming with us."
<br><br>
<<link [[Next|Hospital Arrest Journey]]>><</link>>
<br>