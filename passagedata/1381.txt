<<set $outside to 0>><<set $location to "arcade">><<effects>>

<<if !$worn.upper.type.includes("naked")>>

The claw rests above your chest, then plunges down toward your $worn.upper.name. It hooks the fabric, and begins to rise. The <<person>> and <<his>> friends watch intently.

<br><br>
	<<if $rng gte 51>>
		The claw loosens its grip, dropping your $worn.upper.name back against your body. The crowd sigh in frustration.
		<br><br>

		<<if $phase is 0>>
			"Idiot," a <<person2>><<person>> says, shoving <<his>> friend aside. "I'll show you how it's done."
			<br><br>

			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>

		<<elseif $phase is 1>>
			"My turn," a <<person3>><<person>> says, taking over the lever. "This can't be hard."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<elseif $phase is 2>>
			"You guys must be new to this," a <<person4>><<person>> says. "Watch."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<elseif $phase is 3>>
			"This is taking too long," a <<person5>><<person>> says. "Someone will catch us soon. Give me a go."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<else>>
			<<endevent>><<generate1>><<person1>>
			"What you kids up to over there?" a <<person>> shouts from somewhere out of view. The crowd turn and flee.
			<br><br>
			The <<person>> walks into view. <<He>> gapes at the sight of you. Snapping to attention, <<he>> crouches beside the machine. A hatch opens after a few moments of <<his>> fiddling. <<He>> reaches in, and unties the cables that bind you.
			<br><br>
			You crawl through the hole, escaping into the arcade proper.
				<<if $exposed gte 1>>
				Blushing, <<he>> wraps some towels around you. "Not much, but better than nothing aye?"
				<<towelup>>
				<</if>>
			<br><br>
			"I can't believe how cruel some kids can be," <<he>> says, shaking <<his>> head. "You go careful now."
			<br><br>

			<<endevent>>
			<<link [[Next|Arcade]]>><</link>>
			<br>

		<</if>>
	<<else>>
		<<if $worn.upper.set is $worn.lower.set>>
		<<set $worn.upper.integrity to 0>><<set $worn.lower.integrity to 0>>
		The claw tugs at your fabric, pulling it off your body. With a tear, your $worn.upper.name is torn from you.<<integritycheck>><<exposure>>
		<br>

		The <<person>> and <<his>> friends gape in awe and excitement, leering at your <<lewdnessstop>>
		<br><br>

		<<link [[Next|Arcade Crane 2]]>><</link>>
		<<else>>
		<<set $worn.upper.integrity to 0>>
		The claw tugs at your fabric, pulling it off your body. With a tear, your $worn.upper.name is torn from you.<<integritycheck>><<exposure>>
		<br>

		The <<person>> and <<his>> friends gape in awe and excitement, leering at your <<lewdnessstop>>
		<br><br>

		<<link [[Next|Arcade Crane 2]]>><</link>>
		<</if>>
	<</if>>

<<elseif !$worn.lower.type.includes("naked")>>
The claw rests above your thighs, then plunges down toward your $worn.lower.name. It hooks the fabric, and begins to rise. The <<person>> and <<his>> friends watch intently.

<br><br>

	<<if $rng gte 51>>
		The claw loosens its grip, dropping your $worn.lower.name back against your body. The crowd sigh in frustration.
		<br><br>

		<<if $phase is 0>>
			"Idiot," a <<person2>><<person>> says, shoving <<his>> friend aside. "I'll show you how it's done."
			<br><br>

			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>

		<<elseif $phase is 1>>
			"My turn," a <<person3>><<person>> says, taking over the lever. "This can't be hard."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<elseif $phase is 2>>
			"You guys must be new to this," a <<person4>><<person>> says. "Watch."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<elseif $phase is 3>>
			"This is taking too long," a <<person5>><<person>> says. "Someone will catch us soon. Give me a go."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<else>>
			<<endevent>><<generate1>><<person1>>
			"What you kids up to over there?" a <<person>> shouts from somewhere out of view. The crowd turn and flee.
			<br><br>
			The <<person>> walks into view. <<He>> gapes at the sight of you. Snapping to attention, <<he>> crouches beside the machine. A hatch opens after a few moments of <<his>> fiddling. <<He>> reaches in, and unties the cables that bind you.
			<br><br>
			You crawl through the hole, escaping into the arcade proper.
				<<if $exposed gte 1>>
				Blushing, <<he>> wraps some towels around you. "Not much, but better than nothing aye?"
				<<towelup>>
				<</if>>
			<br><br>
			"I can't believe how cruel some kids can be," <<he>> says, shaking <<his>> head. "You go careful now."
			<br><br>

			<<endevent>>
			<<link [[Next|Arcade]]>><</link>>
			<br>

		<</if>>
	<<else>>
		<<set $worn.upper.integrity to 0>><<set $worn.lower.integrity to 0>>
		The claw tugs at your fabric, pulling it off your body. With a tear, your $worn.upper.name is torn from you.
		<<integritycheck>><<exposure>>
		<br>

		The <<person>> and <<his>> friends gape in awe and excitement, leering at your <<lewdnessstop>>

		<<link [[Next|Arcade Crane 2]]>><</link>>

	<</if>>

<<elseif !$worn.under_upper.type.includes("naked")>>
The claw rests above your thighs, then plunges down toward your $worn.under_upper.name. It hooks the fabric, and begins to rise. The <<person>> and <<his>> friends watch intently.
	<br><br>

	<<if $rng gte 51>>
		The claw loosens its grip, dropping your $worn.under_lower.name back against your body. The crowd sigh in frustration.
		<br><br>

		<<if $phase is 0>>
			"Idiot," a <<person2>><<person>> says, shoving <<his>> friend aside. "I'll show you how it's done."
			<br><br>

			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>

		<<elseif $phase is 1>>
			"My turn," a <<person3>><<person>> says, taking over the lever. "This can't be hard."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<elseif $phase is 2>>
			"You guys must be new to this," a <<person4>><<person>> says. "Watch."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<elseif $phase is 3>>
			"This is taking too long," a <<person5>><<person>> says. "Someone will catch us soon. Give me a go."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<else>>
			<<endevent>><<generate1>><<person1>>
			"What you kids up to over there?" a <<person>> shouts from somewhere out of view. The crowd turn and flee.
			<br><br>
			The <<person>> walks into view. <<He>> gapes at the sight of you. Snapping to attention, <<he>> crouches beside the machine. A hatch opens after a few moments of <<his>> fiddling. <<He>> reaches in, and unties the cables that bind you.
			<br><br>
			You crawl through the hole, escaping into the arcade proper.
				<<if $exposed gte 1>>
				Blushing, <<he>> wraps some towels around you. "Not much, but better than nothing aye?"
				<<towelup>>
				<</if>>
			<br><br>
			"I can't believe how cruel some kids can be," <<he>> says, shaking <<his>> head. "You go careful now."
			<br><br>

			<<endevent>>
			<<link [[Next|Arcade]]>><</link>>
			<br>

		<</if>>
	<<else>>
		<<if $worn.under_upper.set is $worn.under_lower.set>>
		<<set $worn.under_upper.integrity to 0>><<set $worn.under_lower.integrity to 0>>
		The claw tugs at your fabric, pulling it off your body. With a tear, your $worn.upper.name is torn from you.<<integritycheck>><<exposure>>
		<br>

		The <<person>> and <<his>> friends gape in awe and excitement, leering at your <<lewdnessstop>>
		<br><br>

		<<link [[Next|Arcade Crane 2]]>><</link>>
		<<else>>
		<<set $worn.under_upper.integrity to 0>>
		The claw tugs at your fabric, pulling it off your body. With a tear, your $worn.under_upper.name is torn from you.<<integritycheck>><<exposure>>
		<br>

		The <<person>> and <<his>> friends gape in awe and excitement, leering at your <<lewdnessstop>>
		<br><br>

		<<link [[Next|Arcade Crane 2]]>><</link>>
		<</if>>
	<</if>>

<<elseif !$worn.under_lower.type.includes("naked")>>
The claw rests above your thighs, then plunges down toward your $worn.under_lower.name. It hooks the fabric, and begins to rise. The <<person>> and <<his>> friends watch intently.

	<br><br>

	<<if $rng gte 51>>
		The claw loosens its grip, dropping your $worn.lower.name back against your body. The crowd sigh in frustration.
		<br><br>

		<<if $phase is 0>>
			"Idiot," a <<person2>><<person>> says, shoving <<his>> friend aside. "I'll show you how it's done."
			<br><br>

			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>

		<<elseif $phase is 1>>
			"My turn," a <<person3>><<person>> says, taking over the lever. "This can't be hard."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<elseif $phase is 2>>
			"You guys must be new to this," a <<person4>><<person>> says. "Watch."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<elseif $phase is 3>>
			"This is taking too long," a <<person5>><<person>> says. "Someone will catch us soon. Give me a go."
			<br><br>
			<<link [[Next|Arcade Crane 2]]>><<set $phase += 1>><</link>>
		<<else>>
			<<endevent>><<generate1>><<person1>>
			"What you kids up to over there?" a <<person>> shouts from somewhere out of view. The crowd turn and flee.
			<br><br>
			The <<person>> walks into view. <<He>> gapes at the sight of you. Snapping to attention, <<he>> crouches beside the machine. A hatch opens after a few moments of <<his>> fiddling. <<He>> reaches in, and unties the cables that bind you.
			<br><br>
			You crawl through the hole, escaping into the arcade proper.
				<<if $exposed gte 1>>
				Blushing, <<he>> wraps some towels around you. "Not much, but better than nothing aye?"
				<<towelup>>
				<</if>>
			<br><br>
			"I can't believe how cruel some kids can be," <<he>> says, shaking <<his>> head. "You go careful now."
			<br><br>

			<<endevent>>
			<<link [[Next|Arcade]]>><</link>>
			<br>

		<</if>>
	<<else>>
		<<set $worn.under_lower.integrity to 0>>
		The claw tugs at your fabric, pulling it off your body. With a tear, your $worn.under_lower.name is torn from you.<<integritycheck>><<exposure>>
		<br>

		The <<person>> and <<his>> friends gape in awe and excitement, leering at your <<lewdnessstop>>
		<br><br>
		<<link [[Next|Arcade Crane 2]]>><</link>>

	<</if>>

<<else>>
	You lie helpless as the <<person>> and <<his>> friends pull phones from pockets and take pictures. You continue to test your bindings, but it's no use.
	<<fameexhibitionism 20>>
	<br><br>
	<<endevent>><<generate1>><<person1>>
	"What you kids up to over there?" a <<person>> shouts from somewhere out of view. The crowd turn and flee.
	<br><br>
	The <<person>> walks into view. <<He>> gapes at the sight of you. Snapping to attention, <<he>> crouches beside the machine. A hatch opens after a few moments of <<his>> fiddling. <<He>> reaches in, and unties the cables that bind you.
	<br><br>
	You crawl through the hole, escaping into the arcade proper. Blushing, <<he>> wraps some towels around you. "Not much, but better than nothing aye?"
	<<towelup>>
	<br><br>

	"I can't believe how cruel some kids can be," <<he>> says, shaking <<his>> head. "You go careful now."
	<br><br>

	<<endevent>>
	<<link [[Next|Arcade]]>><</link>>
	<br>

<</if>>