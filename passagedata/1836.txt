<<set $outside to 0>><<set $location to "docks">><<dockeffects>><<effects>>
<<set $dockwage to 1000>><<set $dockstatus to 10>><<set $dockhours to 0>>
"Remember," the manager says. "Work starts at <span class="gold"><<if $timestyle is "ampm">>7:00 am<<else>>7:00<</if>></span>, and you can't work on school days. Welcome to the team."
<br><br>

<<link [[Next|Docks]]>><</link>>
<br>