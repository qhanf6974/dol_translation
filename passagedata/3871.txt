<<set $outside to 0>><<effects>>

You try to pull away from Avery's grip, but <<he>> drags you closer to <<his>> car. You feel a tug on your other arm. It's Robin.<<endevent>><<npc Robin>><<person1>> "L-Let <<phim>> go," <<he>> says. "St-stop!"
<br><br>

<<endevent>>
<<npc Avery>><<person1>>

Together, you and Robin are able to keep Avery from pulling you any further. The struggle attracts attention from passers by. Avery notices, and lets go. "I swear," <<he>> snarls. "You'll learn to respect your betters." <<He>> throws open <<his>> car door, climbs in, and drives away.
<<llove>><<garage>><<gstress>><<stress 6>><<gtrauma>><<trauma 6>><<npcincr Avery love -1>><<npcincr Avery rage 5>>
<br><br>

<<endevent>>
<<npc Robin>><<person1>>

You almost fall when Avery releases your arm, but Robin holds you upright. <<Hes>> trembling, but clings to you until Avery's car disappears around a bend, as if the wind could blow you away. "That was horrible," <<he>> says into your shoulder.
<br><br>

The rest of the journey is uneventful, but Robin insists <<he>> walk closer to the road.
<<gdom>><<glove>><<npcincr Robin dom 1>><<npcincr Robin love 1>>
<br><br>

<<link [[Next|School Front Playground]]>><<endevent>><</link>>
<br>