<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "danube">>

You walk up to one of the mansions and knock on the door.
<br><br>

	<<if $rng gte 91 and $breastfeedingdisable is "f" and $malechance lt 100>>

	<<generatef1>><<person1>>A <<person>> answers the door. <<Hes>> wearing nothing but a shirt and panties. "Can I help you?" <<he>> asks.
	<br><br>

	<<link [[Ask for work|Danube House Work]]>><<set $phase to 7>><</link>>
	<br>
	<<link [[Say you've got the wrong address and leave|Danube Street]]>><<endevent>><</link>>
	<br>

	<<elseif $rng gte 61>>

	There's no response.
	<br><br>

	<<link [[Break in|Danube House Lock]]>><</link>>
	<br>
	<<link [[Go to the Next House (0:02)|Danube House Knock]]>><<pass 2>><</link>>
	<br>
	<<link [[Stop|Danube Street]]>><</link>>
	<br>

	<<elseif $rng gte 51>>

	<<generatey1>><<generate2>><<person2>>A <<person>> answers the door. "Can I help you?" <<He>> asks.
	<br><br>

	<<link [[Ask for work|Danube House Work]]>><<set $phase to 6>><</link>>
	<br>
	<<link [[Say you've got the wrong address and leave|Danube Street]]>><<endevent>><</link>>
	<br>

	<<elseif $rng gte 41>>

	<<generate1>><<generate2>><<person2>>A <<person>> answers the door. "You aren't who we were expecting." <<He>> says.
	<br><br>

	<<link [[Ask for work|Danube House Work]]>><<set $phase to 1>><</link>>
	<br>
	<<link [[Say you've got the wrong address and leave|Danube Street]]>><<endevent>><</link>>
	<br>

	<<elseif $rng gte 31>>

	<<generate1>><<person1>>A <<person>> answers the door. "I do not like being disturbed. Speak." <<he>> says.
	<br><br>

	<<link [[Ask for work|Danube House Work]]>><<set $phase to 2>><</link>>
	<br>
	<<link [[Say you've got the wrong address and leave|Danube Street]]>><<endevent>><</link>>
	<br>

	<<elseif $rng gte 21>>

	<<generate1>><<person1>>A <<person>> answers the door. <<He>> examines you head to feet before speaking. "Such a fine young thing," <<he>> says. "Is there anything I can do for you?"
	<br><br>

	<<link [[Ask for work|Danube House Work]]>><<set $phase to 3>><</link>>
	<br>
	<<link [[Say you've got the wrong address and leave|Danube Street]]>><<endevent>><</link>>
	<br>

	<<elseif $rng gte 11>>

	<<generate1>><<generate2>><<person1>>A <<person>> answers the door. "My <<person2>><<if $pronoun is "m">>master<<else>>mistress<</if>><<person1>> does not like being disturbed," <<he>> says. "Out with it."
	<br><br>

	<<link [[Ask for work|Danube House Work]]>><<set $phase to 4>><</link>>
	<br>
	<<link [[Say you've got the wrong address and leave|Danube Street]]>><<endevent>><</link>>
	<br>

	<<else>>

	<<generatey1>><<person1>>A <<person>> answers the door. <<He>> opens it slightly and peeks through the gap. <<He>> doesn't say anything.
	<br><br>

	<<link [[Ask for work|Danube House Work]]>><<set $phase to 5>><</link>>
	<br>
	<<link [[Say you've got the wrong address and leave|Danube Street]]>><<endevent>><</link>>
	<br>

	<</if>>