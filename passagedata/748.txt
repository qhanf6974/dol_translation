<<effects>>

"I'm coming!" It's Alex.
<<if $farm_work.horse.monster is true>>
	The centaur backs up at the sound. "Fuck," <<farm_he horse>> says, running for the field and pointing at the other. "It was <<farm_him horse>>!"
<<else>>
	The horse backs up at the sound. Both run for the field.
<</if>>
<<lrespect>><<gdom>><<farm_horses -1>><<npcincr Alex dom 1>>
<br><br>

<<clotheson>>
<<endcombat>>

<<npc Alex>><<person1>>
Alex arrives at the stable, and runs along the lane. "You okay?" <<he>> asks. "You don't want to lose control around the horses." 
<<if $exposed gte 1 and $farm_naked isnot 1>>
	<<He>> helps you steady yourself, and offers some towels to cover with.
<<else>>
	<<He>> helps you steady yourself.
<</if>>
<<if $exposed gte 1>>
	<<glust>><<npcincr Alex lust 1>>
<</if>>
<br><br>

<<tearful>> you close the gate. "Leave the horses to me if you like," Alex adds. "For all their stubbornness, the pigs can be easier to manage."
<br><br>

<<He>> returns to <<his>> work.
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>