<<widget "industrialex1">><<nobr>>

<<generate1>><<person1>>You continue on, your heart pounding in your chest. You peek round a corner to make sure the way forward is safe. Just before you move however, a force impacts your left leg, closely followed by a sharp pain. You see the culprit as you fall to the ground, a <<personstop>> <<He>> drops the metal pipe and is on top of you before you can recover.
<br><br>
<<set $pain += 50>><<set $molestationstart to 1>>

<span id="next"><<link [[Next->Molestation Industrial]]>><</link>></span><<nexttext>>

<</nobr>><</widget>>

<<widget "industrialex2">><<nobr>>

You come to a dead end, your path blocked by a brick wall. You hear voices coming from the way you arrived. If you go back that way, you fear your <<lewdness>> will be seen. You notice a small hole at the base of the wall, maybe large enough for you to squeeze through. You could also wait until the threat has passed.
<br><br>

<<link [[Squeeze through the hole|Industrial Ex Hole]]>><</link>>
<br>
<<link [[Hide and wait for them to pass (0:30)|Industrial Ex Hide]]>><<pass 30>><<stress 3>><</link>><<gstress>>
<br>

<</nobr>><</widget>>

<<widget "industrialex3">><<nobr>>
You're passing empty barrels lined up against a wall when the front of a van peeks around the corner up ahead.
<br><br>

<<link [[Hide in a barrel|Industrial Ex Barrel]]>><</link>>
<br>
<<link [[Run back the way you came|Industrial Ex Run]]>><</link>><<athleticsdifficulty 1 1000>>
<br>

<</nobr>><</widget>>