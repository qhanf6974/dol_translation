<<widget "eventsscience">><<nobr>>
<<if $rng gte 91 and $player.gender_appearance is "m" and $worn.legs.name is "girl's gym socks">>
<<generates1>><<person1>>A <<person>> looks at your feet. "Why are you wearing girl's socks?" <<he>> asks loudly. "Do they make you feel pretty?" <<His>> friends start laughing.
<br><br>

<<link [[Kick|Science Kick]]>><<stress -12>><<trauma -6>><<detention 3>><</link>><<gdelinquency>><<ltrauma>><<lstress>>
<br>
<<link [[Ignore|Science Lesson]]>><<stress 6>><<trauma 6>><<endevent>><</link>><<gtrauma>><<gstress>>
<br>

<<elseif $rng gte 91 and $player.gender_appearance is "f" and $worn.legs.name is "boy's gym socks">>
<<generates1>><<person1>>A <<person>> looks at your feet. "Why are you wearing boy's socks?" <<he>> asks loudly. "Do you think they'll make people forget you're a slut?" <<His>> friends start laughing.
<br><br>

<<link [[Kick|Science Kick]]>><<stress -12>><<trauma -6>><<detention 3>><</link>><<gdelinquency>><<ltrauma>><<lstress>>
<br>
<<link [[Ignore|Science Lesson]]>><<stress 6>><<trauma 6>><<endevent>><</link>><<gtrauma>><<gstress>>
<br>

<<elseif $rng gte 81>>
<<npc Sirris>><<generates2>><<person2>>The class conducts an experiment with chemicals.
<br><br>

A <<person>> splashes your clothes with acid! "Sorry, I slipped," <<he>> says, smirking.

<<set $worn.upper.integrity -= 20>>
<<set $worn.lower.integrity -= 20>>
<<set $worn.under_lower.integrity -= 20>>

<<integritycheck>>
<<exposure>>
<br><br>
  <<if !$worn.upper.type.includes("naked") and !$worn.lower.type.includes("naked")>>
  It's not a very potent acid, but it does leave your clothing worse for wear.
  <br><br>

  <<link [[Next|Science Lesson]]>><<endevent>><</link>>
  <br>
  <<else>>
  The acid eats through your clothes, and you soon find yourself exposed in front of the entire class! You do your best to conceal your <<lewdness>> as Sirris rushes over with some towels.
	  <<if $exhibitionismaction is "excited">>
		You let the class have a good look at your <<nuditystop>>
	  <<else>>
		You cover up and try to hide your exposed parts, but not before the rest of the class get a good look.
	  <</if>>

  <br><br>
  <<towelup>>

  <<link [[Next|Science Lesson]]>><<endevent>><</link>>
  <br>
  <</if>>
<<elseif $rng gte 61>>
<<npc Sirris>><<person1>>Sirris asks you to come to the front of the class.
<br><br>

<<link [[Next|Science Event3]]>><</link>>
<br>

<<elseif $rng gte 41>>
The class arrange themselves around the terrariums lining the edge of the room. You observe the plants contained within and try to identify the species.
<br><br>

<<link [[Next|Science Terrarium]]>><</link>>
<br>

<<elseif $rng gte 21>>
A group of delinquents decide to pick on you.
<br><br>

<<link [[Ask the teacher for help|Science Pick]]>><<status -10>><</link>><<lcool>>
<br>
<<link [[Endure it|Science Pick]]>><<set $phase to 1>><<stress 2>><<trauma 2>><</link>><<gtrauma>><<gstress>>
<br>

<<elseif $rng gte 1>>
Sirris asks everyone to group up into pairs.
<br><br>
<<set $rng to random(1, 100)>>

  <<if $cool gte 80>>
  You find a partner and together dissect a <<if $rng gte 75>>sheep's eye<<elseif $rng gte 50>>insect<<elseif $rng gte 25>>worm<<else>>frog<</if>>.
  <br><br>

  <<link [[Just focus on the dissection|Science Dissection]]>><<scienceskill>><</link>><<gscience>>
  <br>
  <<link [[Joke around with your partner|Science Dissection]]>><<trauma -1>><<stress -2>><<status 1>><<set $phase to 1>><</link>><<ltrauma>><<lstress>><<gcool>>
  <br>
  <<else>>
  <<npc Sirris>><<person1>>The other students avoid you, and you find yourself unable to find a partner. Sirris carries in a plastic box, but stops to speak to you when <<he>> sees that you're alone.
  <br><br>

  "I have a special job for you. There's another box like this one in the shed just next to the school entrance. Could you bring it here?"
  <br><br>

  You walk to the shed, which is unlocked, and find the box just inside. It's really heavy! You can barely lift it, but manage to carry it back into the school.
  <<physique>><<endevent>>
  <br><br>
	<<if $rng gte 21>>
	You arrive back at the classroom, where the rest of the class is busy dissecting frogs. Lacking a partner, you get one all to yourself.
	<<gscience>><<scienceskill>>
	<br><br>

	<<link [[Next|Science Lesson]]>><<endevent>><</link>>
	<br>
	<<else>>
	<<generatey1>><<generatey2>><<generatey3>>
	You walk past three delinquents smoking in a corridor. You didn't see them coming due to the box blocking your view. They start hounding you down the hall.

	<<person1>>The <<person>> grabs the hem of your $worn.lower.name and tries to tug them down!

	<<if $worn.under_lower.type.includes("naked")>>
	<span class="lewd">You shiver as you realise you aren't wearing any underwear!</span>
	<</if>>
	<br><br>

	"Let's see what <<pshes>> got on under this," the <<person2>><<person>> and <<person3>><<person>> move closer, intending to join in.
	<br><br>

	You're unable to protect yourself while your arms are occupied by the box. You'll have to bend over to put the box down without breaking anything inside, but you feel that doing so would make you even more vulnerable.
	<br><br>

	<<link [[Drop the box and protect yourself|Science Dissection]]>><<set $phase to 2>><<detention 2>><</link>><<gdelinquency>>
	<br>
	<<link [[Just endure it|Science Dissection]]>><<set $phase to 3>><<stress 2>><<trauma 2>><</link>><<gtrauma>><<gstress>>
	<br>
	<<link [[Bend over and put the box down gently|Science Dissection]]>><<set $phase to 4>><</link>>
	<</if>>
  <</if>>
<</if>>
<</nobr>><</widget>>

<<widget "eventssciencesafe">><<nobr>>
<<if $scienceproject is "none" and $yeardays gte 2 and $mathsprojectdays isnot (21 + (7 - $weekday))>><<set $scienceproject to "ongoing">><<set $scienceprojectdays to (21 + (7 - $weekday))>>
<<npc Sirris>><<person1>><<scienceprojectstart>>
"The science fair will be held in a few weeks," Sirris announces. "Taking part is optional, but it's a good opportunity to learn in a more practical way. The theme is local wildlife. I have a project idea for each of you, but you can come up with your own if you like."
<br><br>

Sirris asks the class to turn to page $rng of their textbook, then walks around room handing out small cards. Yours reads: "Lichen grows in many places around town. What impact does the local environment have on the species that thrive?" It continues on to some variables you could test, and ends with: "Remember to ask permission before entering private property!"
<<set $sciencelichenknown to 1>>
<br><br>

Sirris arrives back at the front. "The fair will be held on a Saturday, at the town hall. I hope you'll be there even if you don't bring a project."
<br><br>

<span class="gold">Science project added to journal.</span>
<br><br>

<<if $sciencephallusknown isnot 1 and $promiscuity gte 35>>
<<set $sciencephallusknown to 1>>
A lewd thought comes to you as the other students discuss ideas. <span class="gold">You've conceived the "local phalli" science project.</span>
<br><br>
<</if>>

<<endevent>>
<<link [[Next|Science Lesson]]>><</link>>
<br>

<<elseif $rng gte 81>>
Sirris asks the class to turn to page $rng of their textbook. The classroom has a laid-back atmosphere, most students spend more time chatting than reading.
<br><br>

<<link [[Next|Science Lesson]]>><</link>>

<<elseif $rng gte 71>>
Sirris puts on a short video about <<print either("nature.","space.","magnetic fields.","electrons.","chemical bonds.","Einstein.","the pioneers of the scientific method.","Isaac Newton.","nuclear fusion.","the role of enzymes.", "DNA.")>> The classroom has a laid-back atmosphere. Most students watch, but many carry on chatting.
<br><br>

<<link [[Next|Science Lesson]]>><</link>>

<<elseif $rng gte 61>>
The class conducts an experiment with chemicals. You're careful not to spill any.
<br><br>

<<link [[Next|Science Lesson]]>><</link>>

<<elseif $rng gte 51>>
<<generates1>><<generates2>><<person1>>
The class conducts an experiment with chemicals.
<<if $rng % 2>>
  A <<person>> accidentally spills chemicals down <<his>> clothes.
<<else>>
  A <<person1>><<person>> 'accidentally' spills chemicals all over a <<person2>><<persons>> clothes.
<</if>>
You see the panicking <<if $pronoun is "m">>boy<<else>>girl<</if>> fleeing from the class as <<his>> clothes start to dissolve.
<br><br>

<<link [[Next|Science Lesson]]>><</link>>

<<elseif $rng gte 41>>
Some delinquents pick on one of the nerdy students. Sirris has the offenders move to the other side of the classroom.
<br><br>

<<link [[Next|Science Lesson]]>><</link>>

<<elseif $rng gte 31>>
<<npc Sirris>><<person1>>
Sirris puts on a video about human reproduction.
<<if $days gte 25 and $rng % 3 is 0>>/*1 in 3 chance, and won't happen too early*/
  <br><br>
  It doesn't seem like an educational film at all - it's graphic. Very graphic. Primarily just a 14 minute home-sex-tape. The only 'educational' input comes from Sirris periodically freezing the video to quiz students on which body parts are being licked, stroked, sucked, bitten or penetrated.
  While the video never shows faces, you start to suspect that one of the participants is Sirris.
  <br>
  The entire class watches with rapt attention right up to the couple's panting orgasm.<<garousal>><<arousal 200>>
  <br><br>

  "I know that was quite explicit," Sirris says. "But good scientists aren't squeamish. Humans are animals. And this is simply observing animal behaviour. And as well as knowing the facts and terminology, it's important that you understand what real sex is and what it looks like."<<awarenessup 200 1>>
<<else>>
  It has a strong educational focus, talking extensively about sexual health, STIs, pregnancy, contraception and consent.
  It goes into the basic mechanics of sex with some simple diagrams labelling the body parts and cell types involved.<<awarenessup 100 1>>
  <br><br>
  The classroom has a laid-back atmosphere. Some students watch, but few with any real interest and many carry on chatting throughout.
<</if>>
<br><br>
<<link [[Next|Science Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 21>>
<<npc Sirris>><<person1>>
Sirris asks the class a question. You think you know the answer.
<br><br>

<<link [[Put your hand up|Science Event1]]>><<set $phase to 1>><<stress 1>><<scienceskill 1>><</link>><<gstress>><<gscience>>
<br>
<<link [[Keep your head down|Science Event1]]>><</link>>
<br>

<<elseif $rng gte 11>>
  Sirris looks around the room, glancing at you for a moment before asking a
  <<if $player.gender_appearance is "m">><<generatesf1>>girl<<else>><<generatesm1>>boy<</if>><<person1>> to come to the front of the class.
  <br><br>
  Sirris smiles at the <<personstop>>

  <<if $scienceprogression gte 2>>
	<<He>> looks nervous. "This part of the lesson is on human anatomy. I need you to remove your clothing, all of it. Don't be embarrassed, this is important for the lesson."
	<br>
	<<if $rng % 2 == 0>>
	  The <<if $pronoun is "m">>boy<<else>>girl<</if>> runs straight out of the room and does not return.
	  <br><br>
	  "I suppose we'll have to make do with the old anatomy charts then," Sirris says. "Sorry class."
	<<else>>
	  <<if $pronoun is "m">>
	  <<print either("The boy excitedly agrees.", "The boy cheerfully agrees.", "The boy casually agrees.", "The boy reluctantly agrees.", "The boy looks like he wants to cry, but nods meekly.", "The boy tearfully agrees.")>>
	  <<else>>
	  <<print either("The girl excitedly agrees.", "The girl cheerfully agrees.", "The girl casually agrees.", "The girl reluctantly agrees.", "The girl looks like she wants to cry, but nods meekly.", "The girl tearfully agrees.")>>
	  <</if>>
	  <<He>> strips, which immediately gets the entire class's full attention. The <<person>> stands naked at the front as the teacher points out numerous parts of <<his>> anatomy.
	  <<if $scienceprogression gte 3>>
		Sirris then instructs <<him>> to climb on the desk and spread <<his>> legs. Sirris <<if $pronoun is "m">>holds up his penis<<else>>uses a speculum<</if>> to give the excited class a clearer view.
	  <</if>>
	  <<if $scienceprogression gte 4>>
		Things finally get a little out of control. The <<person>> squeals as some students get a bit 'handsy.' Finally regaining control,
	  <</if>>
	  Sirris brings the show to an end, inviting the <<person>> to dress and sit back down.
	  <br><br>

	  While you feel bad for the <<personcomma>> it definitely made the session more interesting and you think you learned something.<<gscience>><<scienceskill>>
	<</if>>
  <<elseif $scienceprogression gte 1>>
	"I need you to undress for this part. Don't worry, you can keep your underwear on."
	<br>
	<<if $rng % 3 == 0>>
	  The <<if $pronoun is "m">>boy<<else>>girl<</if>> runs straight out of the room and does not return.
	  <br><br>
	  "I suppose we'll have to make do with the old anatomy charts then," Sirris says. "Sorry class."
	<<elseif $rng % 4 == 0>>
	  The <<if $pronoun is "m">>boy<<else>>girl<</if>> whispers something to Sirris.
	  <br><br>
	  Sirris turns bright red and quickly asks the <<person>> to sit back down. "On second thoughts, class, ah... We'll use the anatomy charts," Sirris says. "They're... clearer."
	<<else>>
	  <<if $pronoun is "m">>
	  <<print either("The boy excitedly agrees.", "The boy cheerfully agrees.", "The boy casually agrees.", "The boy reluctantly agrees.", "The boy looks like he wants to cry, but nods meekly.", "The boy tearfully agrees.")>>
	  <<else>>
	  <<print either("The girl excitedly agrees.", "The girl cheerfully agrees.", "The girl casually agrees.", "The girl reluctantly agrees.", "The girl looks like she wants to cry, but nods meekly.", "The girl tearfully agrees.")>>
	  <</if>>
	  <<He>> strips to <<his>> underwear, which immediately grabs the class's attention.
	  Sirris continues with the lesson, pointing to parts of the <<persons>> body and explaining various functions.
	  Sirris soon finishes the demonstration, inviting the <<person>> to dress, sit down and rejoin the class.
	  <br><br>

	  While you feel a little bad for the <<personcomma>> it definitely made the session more interesting and you think you learned something.
	  <<gscience>><<scienceskill>>
	<</if>>
  <<else>>
	"Don't worry, we're not going to do anything invasive."
	<br>
	Over the next few minutes, Sirris talks about various bones and organs, pointing to parts of the <<persons>> body to demonstrate location. It's interesting.
	<<gscience>><<scienceskill>>
  <</if>>
  <br><br>

  <<link [[Next|Science Lesson]]>><</link>>
  <br>

<<elseif $rng gte 1>>
  <<if $dissociation gte 1>>
<<npc Sirris>><<person1>>Sirris approaches you. "Is everything okay?" <<He>> asks. "You don't look... well."
<br><br>

<<link [[I'm fine|Science Event2]]>><</link>>
<br>
<<link [[Break down|Science Event2]]>><<set $phase to 1>><</link>>
<br>
  <<else>>
Sirris approaches you. "Is there anything I can help clarify for you?"
<br><br>

<<link [[Yes|Science Event2]]>><<set $phase to 2>><<scienceskill 1>><</link>><<gscience>>
<br>
<<link [[No|Science Event2]]>><<set $phase to 3>><</link>>
<br>
  <</if>>
<</if>>
<</nobr>><</widget>>