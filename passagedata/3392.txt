<<effects>>

<<clotheson>>
<<set $player.gender_appearance to "m">>
The <<person2>><<person>> looks concerned. "Are you injured?" <<he>> asks.
<br><br>

<<if $pain gte 60>>
	<<He 2>> sees the red marks on you, and starts rifling through <<his>> spilled shopping. "I just picked up some cream that might help. Here." <<He 2>> spends a few minutes applying an ointment. You're not sure if it's the ointment itself, or just the benevolent attention, but you do feel better.
	<<lpain>><<ltrauma>><<lstress>><<pain -6>><<trauma -6>><<stress -6>><<pass 3>>
<<else>>
	You shake your head. You've suffered worse. <<He 2>> doesn't seem convinced, but nods anyway.
<</if>>
<br><br>
<<if $exposed gte 1>>
	"Your clothes are ruined," <<he>> says.
	<<if $player.gender_appearance is "f">>
		"I got this for my daughter, but you look the same size."<<upperwear 38>> <<He 2>> hands you a cute dress, and turns while you put it on. "Suits you perfectly."
	<<else>>
		"I got these for my son, but you look the same size." <<upperwear 33>><<lowerwear 5>> <<He 2>> hands you a shirt and shorts, and turns while you put it on. "Suits you perfectly."
	<</if>>
<<else>>
<</if>>
<br><br>
<<He> 2>> walks you back to the street. <<He 2>> waves goodbye once sure you're not in immediate danger.
<br><br>

<<endcombat>>
<<destinationeventend>>