<<set $outside to 0>><<set $location to "arcade">><<effects>>

You shoulder barge <<him>> aside and grab <<his>> gun. <<He>> takes the hint.
<br><br>
The rest of the game is easier with the extra weapon, but you do need to manage your lives more carefully.
<br><br>

<<endevent>>
<<if $money gte 500 and $daystate isnot "night">>
	<<link [[Keep playing (£5 0:20)|Arcade Play]]>><<set $money -= 500>><<pass 20>><<stress -2>><</link>><<lstress>>
	<br>
<</if>>
	<<link [[Stop|Arcade]]>><</link>>
	<br>