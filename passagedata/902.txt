<<effects>>


<<if $phase is 0>>
	Alex looks uncomfortable. "I'm sorry," <<he>> says. "I imagine you have school and stuff? You can work whenever you like between dawn and dusk. I'll pay you <span class="gold">£15</span> an hour."
<<elseif $phase is 1>>
	"I thought so," Alex says. "I imagine you have school and stuff? You can work whenever you like between dawn and dusk. I'll pay you <span class="gold">£15</span> an hour."
<<else>>
	"That's okay," Alex smiles. "I can tell you're from town though. I imagine you have school and stuff. You can work whenever you like between dawn and dusk. I'll pay you <span class="gold">£15</span> an hour."
<</if>>
<br><br>

"I'd pay more," <<he>> continues. "But the farm is just breaking even. I'm hoping that'll change with your help. I'm not gonna lie though. It's hard work."
<br><br>

<i>Alex has all the farm options you do. Covering chores will free <<him>> to perform different tasks. Getting the other fields running will improve your wage. Alex may also give you a cut of profits should there be an exceptional yield.
<br><br>
Some options are only available at certain times of day.
</i>
<br><br>

<<link [[Next|Farm Intro 10]]>><</link>>
<br>