<<set $outside to 0>><<set $location to "dance_studio">><<effects>>

<<if $dancestudiointro isnot 1>>
	<<set $dancestudiointro to 1>>
	<<npc Charlie>><<person1>>You pay the receptionist who ushers you through the door to her left. You enter into a large room with wood panel floors and mirrors lining the walls. A dozen people are already inside. It's easy to pick out the instructor. Hands on hips, <<he>> stands addressing the class, explaining the importance of proper posture when walking.
	<br><br>
	The door closes behind you, alerting <<him>> to your presence. <<He>> turns <<his>> emerald eyes to you and smiles. "We have a new student! Welcome." Short ginger hair crowns <<his>> freckled face. <<His>> skintight outfit shows off <<his>> toned and athletic body. <<if $pronoun is "m">>You can't help but notice the bulge of <<his>> penis pressing against <<his>> bottoms. <<elseif $pronoun is "f">><<His>> generous breasts are held close to <<his>> torso, giving <<him>> a slim outline.<</if>>
	<br><br>

	If <<he>> noticed your lewd gaze, <<he>> gives no sign. You should introduce yourself, but how?
	<br><br>
	<<link [[Timidly|Heel Lesson]]>><<npcincr Charlie love 20>><<set $phase to 1>><</link>>
	<br>
	<<link [[Confidently|Heel Lesson]]>><<set $phase to 2>><</link>>
	<br>
	<<link [[Flirtatiously|Heel Lesson]]>><<npcincr Charlie love -10>><<stress -12>><<trauma -6>><<set $phase to 3>><</link>><<promiscuous1>><<lstress>><<ltrauma>>
	<br>

<<elseif $phase is 1>>
	<<set $phase to 0>>
	"H-hello..." you mumble, eyes transfixed on your feet.
	<br><br>
	Without saying anything, the instructor walks over to you and puts a protective arm around your shoulders. <<He>> whispers, "Don't worry, I don't bite. My name's Charlie. I'll help you out of your shell." <<He>> gently escorts you over to the rest of the class before commencing the lesson.
	<br><br>
	<<link [[Next|Heel Lesson]]>><<endevent>><</link>>
	<br>
<<elseif $phase is 2>>
	<<set $phase to 0>>
	"Pleased to meet you! I can't wait to get started!"
	<br><br>
	The instructor responds, "Woah there, eagerness is all well and good, but too much will get your muscles strained." Despite <<his>> caution, <<he>> seems happy. "Come on over. I'm Charlie by the way. I'll be your teacher. Right then, where was I?" You head over as the lesson commences.
	<br><br>
	<<link [[Next|Heel Lesson]]>><<endevent>><</link>>
	<br>
<<elseif $phase is 3>>
	<<set $phase to 0>>
	<<promiscuity1>>"Nice outfit. Really shows off your assets." You let your eyes probe <<his>> body, letting <<him>> know exactly which assets you mean.
	<br><br>
	<<He>> shifts uncomfortably on the spot, as if suddenly self-conscious. <<He>> looks annoyed, "Would you like to take your place over here? My name's Charlie." <<He>> turns back to the rest of the class as you head over. The lesson soon commences.
	<br><br>
	<<link [[Next|Heel Lesson]]>><<endevent>><</link>>
	<br>

<<else>>
	<<set $rng to random(1, 5)>>
	<<if $rng is 1>>
	Charlie hands you and the other class members a stack of books. Instead of telling you to read them, <<npc Charlie>><<person1>><<he>> instructs you to balance them on your head and do laps around the room. It takes all of your concentration to keep them from falling.
	<<elseif $rng is 2>>
	You enter the dance room to find an assortment of car tyres resting on the floor. To Charlie's direction, you and your classmates take turns stepping into and out of each tyre. Charlie shouts as you walk, "Faster! Faster! Faster! Faster!"
	<<elseif $rng is 3>>
	The dance room is cluttered with discarded leotards, dance equipment, and floor mats. Charlie greets the class and begins, "For this lesson, I want you to clean the room up without using your hands. Use your heels." You do as <<npc Charlie>><<person1>><<he>> says.
	<<else>>
	A line of tape is placed on the dance room floor. You and your classmates take turns doing catwalks down the line, moving elegantly to imitate fashion models. As you walk, Charlie grabs you by the waist and guides each step. "That's it! Keep moving!"
	<</if>>
	<br><br>
	<<tiredness 20>>
	<<set $feetskill += 10>>
	<span class="gold">You feel more confident in your ability to give pleasure with your feet.</span>
	<br><br>
	<<link [[Finish up|Heel Lesson Video]]>><</link>>
<</if>>