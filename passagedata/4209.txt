<<set $outside to 0>><<set $location to "forest_shop">><<effects>>
<<if !$robin_forest_visited>><<set $robin_forest_visited to 1>>
	<<if $forest_shop_intro isnot 1>>
		<<set $forest_shop_intro to 1>>
		You catch occasional glimpses of the <<if $NPCName[7].gender is "m">>boy<<else>>girl<</if>> through the trees, until at last you come to a strange building. You hear shuffling inside. A sign on the door says "Open."
		<br><br>
		Robin hesitates again, but steels <<himself>> and enters.
		<br><br>
		<<endevent>><<npc Gwylan>><<person1>>
		The interior is dark and crowded by tall shelves holding all manner of items. The quiet is broken by a scraping, then a thud. A <<if $pronoun is "m">>boy<<else>>girl<</if>> holding a stepladder appears at the end of the closest aisle.
		<br><br>
		"Customers!" <<he>> says, dropping the ladder and walking over. <<He>> wipes <<his>> hands on <<his>> apron. "Welcome to my shop."
		<br><br>
		<<He>> looks about your age, with mousy hair and a hint of freckles. "I'm older than I look," <<he>> says, as if reading your mind. "I'm Gwylan," <<he>> reaches forward and shakes both your and Robin's hand. "Pleased to meet you. I sell-," <<he>> pauses. "My grand<<if $pronoun is "m">>father<<else>>mother<</if>> sells-" another pause. "I sell knick-knacks here. Things you won't find in normal shops. Even I don't know what I have in stock!" <<He>> sounds proud.
		<br><br>
		<<link [[Next|Robin Forest Shop 5]]>><</link>>
		<br><br><br>
	<<else>>
		You catch occasional glimpses of the <<if $NPCName[7].gender is "m">>boy<<else>>girl<</if>> through the trees, until at last you come to a strange building. Gwylan's shop.
		<br><br>
		"It's okay," you say to a nervous Robin. "I've been here before. It's a shop." All apprehension leaves the orphan, and <<he>> enters.
		<br><br>
		<<endevent>><<npc Gwylan>><<person1>>
		You follow Robin into the shop. Your eyes adjust to the gloom just as Gwylan appears at the end of an aisle. "You brought a friend!" <<he>> says, rushing over and grasping Robin's hand in both <<his>> own. "I'm Gwylan. Welcome to my shop."
		<br><br>
		<<link [[Next|Robin Forest Shop 5]]>><</link>>
		<br>
	<</if>>
<<else>>
	<<endevent>><<npc Gwylan>><<person1>>Gwylan greets you as you enter. "I'm glad to see you again," <<he>> says, shaking both your and Robin's hand. "Let me know if you need anything."
	<br><br>
	<<endevent>><<npc Robin>><<person1>>Robin beelines towards the dark screen. You catch up and find <<him>> admiring the outfits.
	<br><br>
	<<link [[Ask Robin to try on the girl's witch costume|Robin Forest Witch]]>><</link>>
	<br>
	<<link [[Ask Robin to try on the boy's vampire costume|Robin Forest Vampire]]>><</link>>
	<br>
	<<link [[Walk home|Robin Forest Shop Home]]>><</link>>
	<br>
<</if>>