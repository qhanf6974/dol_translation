<<effects>>

<<set $tattoo_parlour to {}>>
<<if $skin.forehead.special is "whitney">>
	<<set $skin.forehead.pen to "tattoo">><<set $tattoo_parlour to clone($skin.forehead)>><<set $tattoo_bodypart to "forehead">>
<<elseif $skin.left_cheek.special is "whitney">>
	<<set $skin.left_cheek.pen to "tattoo">><<set $tattoo_parlour to clone($skin.left_cheek)>><<set $tattoo_bodypart to "left_cheek">>
<<elseif $skin.right_cheek.special is "whitney">>
	<<set $skin.right_cheek.pen to "tattoo">><<set $tattoo_parlour to clone($skin.right_cheek)>><<set $tattoo_bodypart to "right_cheek">>
<<elseif $skin.left_shoulder.special is "whitney">>
	<<set $skin.left_shoulder.pen to "tattoo">><<set $tattoo_parlour to clone($skin.left_shoulder)>><<set $tattoo_bodypart to "left_shoulder">>
<<else>>
	<<set $skin.right_shoulder.pen to "tattoo">><<set $tattoo_parlour to clone($skin.right_shoulder)>><<set $tattoo_bodypart to "right_shoulder">>
<</if>>

Blushing, you tell the artist what you want. <<Hes>> unfazed by the request. "Young lovers, eh?" <<he>> says. "Bit of a lewd one, but I won't judge. Please have a seat."
<br><br>
You sit as instructed. "This will sting a bit." <<Hes>> not wrong. Your <<bodypart $tattoo_bodypart>> stings the whole way through, but you get used to it, becoming bored instead.

"There we go," <<he>> says at least, reaching for a mirror. You examine your new tattoo,

<span class="lewd"><<if $tattoo_parlour.type is "text">>"<<print $tattoo_parlour.writing>>"<<else>>a <<print $tattoo_parlour.writing>><</if>></span>

now permanently emblazoned on your <<bodypart $tattoo_bodypart>>.
<br><br>
The artist explains how to care for your skin while the tattoo is still raw. You get the gist. "Thanks for your custom," <<he>> says at the end.
<br><br>

<<unset $tattoo_parlour>>
<<unset $tattoo_bodypart>>
<<link [[Next|Bully Tattoo Accept 2]]>><</link>>
<br>