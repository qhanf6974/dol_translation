<<set $outside to 0>><<set $location to "lake_ruin">><<underwater>><<effects>><<lakeeffects>>
Whatever it is, it sounds like it's behind you. No matter what direction you face. You sense a thirst.
<br><br>
<<if $deviancy gte 55>>
	<<link [[Offer yourself|Lake Ruin Deep Offer 2]]>><<loxygen>><</link>><<deviant4>>
	<br>
<</if>>
<<link [[Leave|Lake Ruin Deep]]>><</link>>
<br>