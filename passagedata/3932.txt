<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $submissive gte 1150>>
"You're scaring me," you say to Kylar. "Stop it."
<<elseif $submissive lte 850>>
"You fucking creep," you say. You tear down the portrait.
<<else>>
"You're creepy," you say. "Stop it."
<</if>>

The crowd cheers. Kylar struggles free and runs through the other students, one hand held to <<his>> eyes.
<br><br>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><<endevent>><</link>>
<br>