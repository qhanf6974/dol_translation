<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>
	<<if $pain gte 100 and $willpowerpain is 0>>
		"It's not over yet, slut," the <<person1>><<person>> says, reaching into <<his>> coat. <span class="red"><<He 1>> pulls out a gag and blindfold.</span> You're too hurt to resist as <<he>> forces the former into your mouth, and ties them both around your head. You can't see a thing.
		<<facewear 6>>
		<br><br>

		"Have fun," <<he>> laughs, shoving you into the street. You hear <<him>> walk away.
		<br><br>

		<<tearful>> you try to get your bearings.
		<br><br>

		<<blindfoldintro>>

		<<clotheson>>
		<<endcombat>>
		<<clearNPC bartend>>
		<<set $eventskip to 1>>
		<<destinationbondage>>

	<<else>>
		"It's not over yet, slut," the <<person>> says, reaching into <<his>> coat. <span class="red"><<He 1>> pulls out a gag and blindfold.</span> <<He>> tries to jam the former into your mouth, <span class="green">but you manage to duck out of the way.</span>
		<br><br>

		<<tearful>> you run to safety.
		<br><br>

		<<clotheson>>
		<<endcombat>>
		<<clearNPC bartend>>
		<<destinationeventend>>
	<</if>>
<<elseif $enemyhealth lte 0>>

The <<person1>><<person>> loses <<his>> footing and tumbles into the street, just as a car approaches. It swerves to avoid the <<himcomma>> and collides with a street light.
<br><br>

The driver emerges, furious, and shouts at the <<personstop>> <<tearful>> you seize the opportunity and run to safety.
<br><br>

<<clotheson>>
<<endcombat>>
<<clearNPC bartend>>
<<destinationeventend>>

<<else>>
<<set $rescued += 1>>

A car pulls up, and <<generate4>><<person4>><<person>> climbs out. "Leave the <<girl>> alone," <<he>> says. "Or you're gonna get beat."
<br><br>
The <<person1>><<person>> glares at the <<person4>><<personcomma>> incensed. "Who the fuck do you think you are?" <<he>> says, releasing you and marching over.
<br><br>

<<tearful>> you exploit the resulting shouting match and run to safety.
<br><br>

<<clotheson>>
<<endcombat>>
<<clearNPC bartend>>
<<destinationeventend>>

<</if>>