<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>"You can't take twenty steps in the forest without finding an old arrowhead," Winter says. "Not that I'm ungrateful. Most people wouldn't spot them."
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>