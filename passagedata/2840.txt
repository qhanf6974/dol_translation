<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>><<set $bus to "schoollibrary">>
You are in the school library.
<<if $schoolday is 1>>
/* indev - double <br> replaced with single */
	<<if $schoolstate is "early">>
	The library is devoid of people, it being so early in the day.
	<br>
	<<elseif $schoolstate is "late">>
	The library is devoid of people, students and teachers having gone home for the day.
	<br>
	<<elseif $schoolstate is "lunch">>
	The room is packed with students using their lunch break to catch up with homework.
	<br>
	<<else>>
	Several students are present, working at the desks and searching among the bookshelves.
	<br>
	<</if>>
<<elseif $schoolday isnot 1>>
The library is devoid of people.
<br>
<</if>>
/*indev - Extra library description*/
There are several shelves with typical academic literature. You see books on engineering, biology, social sciences and language learning. Everything that is useful in catching up.
<br>
Next to it is a small basket with returned and not yet sorted books.
<<if $english lt 200>>
	You wonder if there may be something more interesting here if you were one of those bookworms.
<</if>>
<br><br>
<<if $exposed gte 1>>
	<<if $schoolday is 1 and $schoolstate isnot "early" and $schoolstate isnot "late">>
	You hide between the bookshelves, doing your best to conceal your <<nuditystop>>
	<br>
	<<else>>
	<<exhibitionclassroom>>
	<</if>>
<</if>>

<<if $stress gte 10000>>
<<link [[Everything fades to black...|School Passout]]>><</link>>
<<else>>
	<<if $exposed gte 1 and $schoolday is 1 and $schoolstate isnot "early" and $schoolstate isnot "late">>
	<<else>>
<<projectoptions>>
		<<if $exposed lte 0 and $schoolday is 1 and ($schoolstate is "morning" or $schoolstate is "afternoon" or $schoolstate is "lunch") and $mathsproject is "ongoing" and $mathslibrarystudent isnot 1 and $mathslibraryknown isnot 1>>

			You see a student working on a solution for the maths competition.
			<br>

			<<link [[Approach|Maths Library Student]]>><</link>>
			<br><br>

		<</if>>
		<<if $exposed lte 0 and $NPCName[$NPCNameList.indexOf("Kylar")].state is "active" and $weather is "rain" and $schoolstate is "lunch" and $luncheaten is 1>>
		Kylar sits at a desk, scribbling something.
		<br>
		<<link [[Approach|Kylar Library]]>><</link>>
		<br><br>
		<</if>>
		<<if $exposed gte 1>>
			<<link [[Study science (0:20)|Library Study Exposed]]>><<pass 20>><<set $phase to 1>><</link>><<gscience>>
			<br>
			<<link [[Study maths (0:20)|Library Study Exposed]]>><<pass 20>><<set $phase to 2>><</link>><<gmaths>>
			<br>
			<<link [[Study English (0:20)|Library Study Exposed]]>><<pass 20>><<set $phase to 3>><</link>><<genglish>>
			<br>
			<<link [[Study history (0:20)|Library Study Exposed]]>><<pass 20>><<set $phase to 4>><</link>><<ghistory>>
			<br>
		<<else>>
			<<link [[Study science (0:20)|Library Study]]>><<pass 20>><<set $phase to 1>><</link>><<gscience>>
			<br>
			<<link [[Study maths (0:20)|Library Study]]>><<pass 20>><<set $phase to 2>><</link>><<gmaths>>
			<br>
			<<link [[Study English (0:20)|Library Study]]>><<pass 20>><<set $phase to 3>><</link>><<genglish>>
			<br>
			<<link [[Study history (0:20)|Library Study]]>><<pass 20>><<set $phase to 4>><</link>><<ghistory>>
			<br>
		<</if>>
		/*indev - scarlet book possible to be taken. English 200+ required*/
		<<if $english gte 300>>
			<<link [[Take "Raul and Janet" from the basket (0:01)|ScarletBook1]]>><<pass 1>><</link>>
			<br>
		<<elseif $english gte 200>>
			<<link [[Take the medium-sized scarlet book from the basket (0:01)|ScarletBook1]]>><<pass 1>><</link>>
			<br>
		<</if>>
	<br>
	<</if>>
<<link [[Leave the library (0:01)|Hallways]]>><<pass 1>><</link>>
<br>
<</if>>