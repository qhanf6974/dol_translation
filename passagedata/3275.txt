<<set $outside to 0>><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>><<set $prostitutionstat += 1>><<fameprostitution 1>>
	<<if $crime gte 2000 and $rng % 2>>
		<<if $enemyanger lt 1 and $crime lte 4000 and $beauty gte ($beautymax / 7) * 5>>
			The <<person1>><<person>> looks conflicted. Finally <<he>> speaks.
			<br>
			"Listen," <<he>> says. "I'm with the police. This was a sting operation. Targeting you. We know you've been committing crimes and prostitution is an easy way to get an arrest. So... so this."
			<br><br>
			"Look I..." <<he>> says. "You're a sweet, gorgeous <<if $player.gender_appearance is "m">>guy<<else>>girl<</if>>, and that was pretty good just now. I liked that. A lot. And no one else knows I caught you yet, so... We can just pretend I didn't, right."
			<br><br>
			<<if $stealstate is 3>>
				"You already helped yourself to the money in my wallet, so we'll agree you've been paid."
			<<else>>
				"Here's your pay."
				<<tipreceive>>
			<</if>>
			<br><br>
			"Be careful. The people after you may be closer than you think."
			<br>
			<<clotheson>>
			<<endcombat>>
			<<destinationeventend>>
		<<else>>
			The <<person1>><<person>> looks at you lovingly.
			<br>
			"What do I owe you for such amazing sex? Just name your price."
			<br><br>
			<<if $skulduggery gte 700>>
				<span class="gold">Thanks to your knowledge of trickery and deceit, you smell a rat. The car. The person. Everything feels wrong. This feels like a setup.</span>
				<br><br>
			<</if>>
			<<link [[Nothing|Street Car Sex Entrap]]>><<set $phase to 0>><</link>>
			<br>
			<<link [[£50|Street Car Sex Entrap]]>><<set $phase to 2>><</link>>
			<br>
			<<link [[£100|Street Car Sex Entrap]]>><<set $phase to 2>><</link>>
			<br>
			<<link [[£150|Street Car Sex Entrap]]>><<set $phase to 2>><</link>>
			<br>
			<<link [[£200|Street Car Sex Entrap]]>><<set $phase to 2>><</link>>
			<br>
			<<link [[£500|Street Car Sex Entrap]]>><<set $phase to 2>><</link>>
			<br>
		<</if>>
	<<else>>
		"That was great."
		<br>
		<<He>> smiles, strokes your cheek and then shows you a police badge.
		<br>
		"There's no cost involved, right? That'd be prostitution. A <<girl>> like you isn't a whore? I'd have to take you in for that."
		<br><br>
		<<if $submissive lte 500>>
			<<link [[Demand Payment|Street Car Sex Entrap]]>><<set $phase to 3>><<crimeup 50>><</link>>
			<br>
		<</if>>
		<<link [[No Cost|Street Car Sex Entrap]]>><<set $phase to 0>><</link>>
		<br>
	<</if>>
<<elseif $finish is 1>>
	"Fine. I hope you don't expect anything." <<He>> shoves you out the car and drives away. <<tearful>> you struggle to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<<else>> /*only triggers if event goes nonconsens and fight*/
	<<if $rng % 2>>
		You manage to knock the <<person>> backwards, giving you time to escape. <<tearful>> you escape the car and flee down the alley.
		<br><br>
		<<clotheson>>
		<<endcombat>>
		<<destinationeventend>>
	<<else>>
		You manage to knock the <<person>> backwards, and try to escape, <span class="red">but the car doors won't open.</span>
		<br>
		The <<person>> grabs you and quickly puts you in an arm-lock.
		<br><br>
		"You just assaulted a police officer," <<he>> puts something on your arms, binding them behind you.
		<br><br>
		<<set $rightarm to "bound">><<set $leftarm to "bound">>
		<<set $rng to random(50, 100)>>
		<<if $crime gt ($rng + ($enemyangermax - $enemyanger)) * 20>> /*more angry = more likely to arrest you for smaller crime value*/
			"With your record, no one will ask too many awkward questions about how I caught you. I'll probably even get a bonus."
			<br><br>
			<<link [[Next|Street Car Sex Entrap]]>><<set $phase to 1>><</link>>
			<br>
			<<else>>
			"It's your lucky day. I'm on thin ice already with the boss - if I haul you in there'll be too many questions."
			<br><br>
			"I should hurt you," <<He>> smiles viciously. "You're lucky I'm a police officer. I'll let you go. But I'll be keeping your clothes as 'evidence'."
			<br><br>
			<<He>> shoves you out of the car naked. With your arms still bound you're unable to stop yourself bashing into a wall.
			<br><br>
			"Have a nice walk home!" <<His>> car screeches away.<<upperruined>><<lowerruined>><<underruined>>
			<<endcombat>>
			<<destinationeventend>>
		<</if>>
	<</if>>
<</if>>