<<effects>>

You shake your head.

<<if $submissive gte 1150>>
"Bailey is too grouchy," you say.
<br><br>

The <<person>> looks at <<his>> feet and nods.
<br><br>
<<elseif $submissive lte 850>>
"No," you say. "Bailey is an arse."
<br><br>

The <<person>> claps <<his>> hands to <<his>> face, while a <<generateyv2>><<person2>><<person>> about your age puts <<his>> hands over the younger orphan's ears. "Language!" <<he>> says, glaring at you.
<br><br>
<<else>>
"You can get a dog when you leave the orphanage," you say.
<br><br>

The <<person>> looks at <<his>> feet and nods.
<br><br>
<</if>>

<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>