<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You pick up the bullied <<person1>><<if $pronoun is "m">>boy<<else>>girl<</if>>'s bag and gather <<his>> books and pens. The <<person2>><<person>> soon finishes <<his>> game, and drops <<his>> victim to the ground. <<He>> and the audience lose interest.
<br><br>

You're the only one still paying <<person1>><<him>> attention when you walk up to the bullied <<if $pronoun is "m">>boy<<else>>girl<</if>> and hold out <<his>> things. <<He>> steps back as if threatened.
<br><br>

<<if $submissive gte 1150>>
"I know what it's like," you say. "But it's okay. Things will be better."
<<elseif $submissive lte 850>>
"Don't let <<person2>><<him>> get to you," you say. "<<Hes>> not worth it."
<<else>>
"I hope you're okay," you say.
<</if>>
<br><br>

<<person1>>"Th-thank you," <<he>> stutters. "I-I'm," <<he>> looks at you, but blushes and looks away. "I'm Kylar." You thought <<he>> was younger than you, but <<he>> seems the same age. <<Hes>> just small. <<He>> turns and runs.
<br><br>
<<endevent>>
<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>