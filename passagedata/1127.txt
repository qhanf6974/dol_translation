<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	The farmhands walk together toward the gate, leaving you lying on the grass.
	<br><br>
	<<tearful>> you stagger to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Livestock Field]]>><</link>>
	<br>

<<elseif $enemyhealth lte 0>>
	The farmhands back away from you, surprised by your ferocity. They stand still, facing you in a line.
	<br><br>
	
	<<link [[Charge|Livestock Field Gang Charge]]>><<def 1>><</link>>
	<br>
	<<link [[Stare|Livestock Field Gang Stare]]>><</link>>
	<br>


<<else>>
	<<set $rescued += 1>>
	<<earnFeat "Equine Rescue">>
	You hear hooves. The <<person1>><<person>> looks up. "Shit!" <<he>> shouts, backing away from you. The others follow suit, leaving you alone on the grass.
	<br><br>
	<<if $monsterchance gte 1 and ($hallucinations gte 1 or $monsterhallucinations is "f")>>
		<<if $malechance lt random(1, 100)>>
			The hooves grow louder, and a moment later you're surrounded by centaur. They form a protective circle around you while a couple chase the farmhands off the field.
			<br><br>
			The <<person4>><<person>> is last through the gate, and shuts it in a hurry. A firm-breasted centaur close to you throws back her head, and laughs. "That didn't take much."
			<br><br>
			Satisfied that you're safe, the centaur return to their territory.
			<br><br>
		<<else>>
			The hooves grow louder, and a moment later you're surrounded by centaur. They form a protective circle around you while a couple chase the farmhands off the field.
			<br><br>
			The <<person4>><<person>> is last through the gate, and shuts it in a hurry. A barrel-chested centaur close to you throws back his head, and laughs. "That didn't take much."
			<br><br>
			Satisfied that you're safe, the centaur return to their territory.
			<br><br>
		<</if>>
	<<else>>
		The hooves grow louder, and a moment later you're surrounded by horses. Most stand around you, as if protecting you, while a couple chase the farmhands off the field.
		<br><br>
		The <<person4>><<person>> is last through the gate, and shuts it in a hurry.
		<br><br>
		Satisfied that you're safe, the horses return to their territory.
		<br><br>
	<</if>>
	<br><br>
	<<tearful>> you rise to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Livestock Field]]>><</link>>
	<br>

<</if>>