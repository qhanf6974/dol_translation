<<set $outside to 1>><<set $location to "forest">><<effects>>

"There was a big snake and it coiled around me and it tried to swallow me and-" You stumble through your explanation with a trembling voice, before being cut off by a tight hug.
<br><br>

<<He>> lets you cry into <<his>> shoulder. "I'm so sorry," <<he>> says. "I shouldn't have let that happen to you." <<He>> squeezes you close to <<his>> chest, shivering. "I don't want to think about losing you. Let's go home."
<br><br>

<<if $exposed gte 1>>
Robin gives you the rug to cover with and together you return to the orphanage.
<br><br>

<<link [[Next|Bedroom]]>><<endevent>><</link>>
<br>
<<else>>
Together you return to the orphanage.
<br><br>

<<robinoptions>>
<</if>>