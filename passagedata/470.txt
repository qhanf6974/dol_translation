<<set $outside to 1>><<set $location to "forest">><<effects>>

You give <<him>> a slow twirl while walking towards <<himcomma>> making sure <<he>> gets to see you from other angles. <<He>> looks away, <<his>> face turning red. "Don't be shy," you purr. "You can look all you want." You're close to <<him>> now, so close <<he>> has to look up to avoid seeing you. <<He>> turns around.

"Y-you should take these to cover with," <<he>> says, pulling some towels from a bag sat on a tree stump. "You might attract some bad types dressed like that." <<He>> looks through the trees, still facing away from you, "Please leave me to work. If we're seen people will get the wrong idea."
<br><br>

<<endevent>>
<<link [[Take the towels|Forest]]>><<set $eventskip to 1>><<towelup>><</link>>
<br>
<<link [[Just leave|Forest]]>><<set $eventskip to 1>><</link>>
<br>