<<effects>><<set $location to "sewers">><<set $outside to 0>><<water>>
<<set $swimmingdifficulty to random(1, 200)>>
You lower yourself into the water and try to swim against the current.
<<if $swimmingskill gte $swimmingdifficulty>>
	<span class="green">You succeed,</span> and climb onto the bank some way up.
	<br><br>
	<<link [[Next|Sewers Commercial]]>><</link>>
	<br>
<<else>>
	<span class="red">The water proves too violent,</span> and you are forced back with the flow until you manage to find purchase on the shore and haul yourself up.
	<br><br>
	<<link [[Next|Sewers Residential]]>><</link>>
	<br>
<</if>>