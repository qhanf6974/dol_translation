<<set $outside to 1>><<set $location to "forest">><<effects>>
You talk with Robin.
<<if $rng gte 81>>
	"It's really peaceful here," <<he>> says.
<<elseif $rng gte 61>>
	"I hear bad things happen to <<girls>> who wander in too deep," <<he>> says.
<<elseif $rng gte 41>>
	"It's such a nice day," <<he>> says.
<<elseif $rng gte 21>>
	"I wonder if it'll rain tomorrow," <<he>> says.
<<else>>
	"I hope the picnic is okay," <<he>> says.
<</if>>
<br><br>
<<Hes>> content at first, but soon grows restless. "That was fun," <<he>> says. "But let's go home." You help <<him>> pack up the picnic and together you make your way back to the orphanage.
<br><br>
<<robinoptions>>