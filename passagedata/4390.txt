<<effects>><<set $location to "sewers">><<set $outside to 0>>
Morgan is delighted with your interest in scones.
<br><br>
<<if $rng gte 75>>
	<<His>> take on things seems strange.
	<<lscience>><<set $science -= 1>><<set $school -= 1>>
	<br><br>
<<elseif $rng gte 50>>
	<<His>> take on things seems strange.
	<<ggscience>><<scienceskill 2>>
	<br><br>
<<else>>
	<<He>> rambles a lot, but has moments of greater lucidity.
	<<gscience>><<scienceskill>>
	<br><br>
<</if>>
<<link [[Next|Sewers Morgan]]>><</link>>
<br>