<<set $outside to 0>><<set $location to $container.lastLocation>><<effects>>
<<set _pregnancy to $sexStats.anus.pregnancy>>

<<switch $pregChoice>>
<<case "ReplaceSelected">>
	<<for _i to 0; _i lt $pregResult.length;_i++>>
		<<if $checkboxResult[$pregResult[_i]] is true>>
			<<if _i lt $parasiteReplaceArray.length>>
				<<moveCreature $pregResult[_i] "replace" $parasiteReplaceArray[_i]>>
			<<else>>
				<<moveCreature $pregResult[_i] "container">>
			<</if>>
		<<elseif $checkboxResult[$pregResult[_i]] is false>>
			<<moveCreature $pregResult[_i] "portable">>
		<</if>>
	<</for>>
	You decide to keep <<print $parasiteSelectionCount>> of <<if _pregnancy.namesChildren is true>>your children<<else>>the parasites<</if>> and so place them in your <<print $container[$location].name>> in place of <<print $parasiteReplaceArray.length>> others, while preparing the rest for Doctor Harper.
	<br><br>
<<case "KeepSelected">>
	<<for _i to 0; _i lt $pregResult.length;_i++>>
		<<if $checkboxResult[$pregResult[_i]] is true>>
			<<moveCreature $pregResult[_i] "container">>
		<<elseif $checkboxResult[$pregResult[_i]] is false>>
			<<moveCreature $pregResult[_i] "portable">>
		<</if>>
	<</for>>
	You decide to keep <<print $parasiteSelectionCount>> of <<if _pregnancy.namesChildren is true>>your children<<else>>the parasites<</if>> and so place them in your <<print $container[$location].name>> while preparing the rest for Doctor Harper.
	<br><br>
<<case "keepAll">>
	<<for _i to 0; _i lt $pregResult.length;_i++>>
		<<moveCreature $pregResult[_i] "container">>
	<</for>>
	You decide to keep all of <<if _pregnancy.namesChildren is true>>your children<<else>>the parasites<</if>> and so place them in your <<print $container[$location].name>>.
	<br><br>
<<case "sellAll">>
	<<for _i to 0; _i lt $pregResult.length;_i++>>
		<<moveCreature $pregResult[_i] "portable">>
	<</for>>
	You decide to sell all <<if _pregnancy.namesChildren is true>>your children<<else>>the parasites<</if>> and so prepare to take them to Doctor Harper.
	<br><br>
<</switch>>

<<link [[Next|Containers]]>>
	<<unset $pregChoice>><<unset $pregResult>>
	<<unset $checkboxResult>><<unset $checkboxReplace>>
	<<unset $parasiteSelectionCount>><<unset $parasiteReplaceArray>>
<</link>>