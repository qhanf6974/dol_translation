<<effects>>
<<if $enemyhealth lte 0>>
	<<beastwound>>
	<<if $combatTrain.length gt 0>>
		The <<beasttype>> recoils in pain and fear, but another is eager for a go.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Beast Gang]]
	<<else>>
		The <<beasttype>> recoils in pain and fear.
		<br><br>
		<<link [[Next|Beast Gang End]]>><<set $finish to 1>><</link>>
	<</if>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	<<if $combatTrain.length gt 0>>
		Satisfied, the <<beasttype>> moves and another takes its turn.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Beast Gang]]
	<<else>>
		Satisfied, the <<beasttype>> moves away from you.
		<br><br>
		<<link [[Next|Beast Gang End]]>><<set $finish to 1>><</link>>
	<</if>>
<<elseif $finish is 1>>
	<<if $enemywounded is 1 and $enemyejaculated is 0>>
		The <<beasttype>> whimpers and flees.
	<<elseif $enemywounded is 0 and $enemyejaculated is 1>>
		Satisfied, the <<beasttype>> leaves.
	<<elseif $enemywounded gte 2 and $enemyejaculated is 0>>
		Feeling that you're more trouble than you're worth, the beasts flee.
	<<elseif $enemywounded is 0 and $enemyejaculated gte 2>>
		The beasts leave you spent and shivering.
	<<elseif $enemywounded gte 1 and $enemyejaculated gte 1>>
		The beasts leave you spent and shivering.
	<</if>>
	<br><br>
	<<tearful>> you gather yourself.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<<elseif $alarm is 1 and $rescue is 1>>
	<<set $rescued += 1>>

	You are rescued.
	<br><br>
	<<tearful>> you gather yourself.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<</if>>