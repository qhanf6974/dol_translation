<<effects>>

You chase the <<person1>><<person>> through the market.
<<if $athletics gte random(1, 1200)>>
	<<if $rng gte 81>>
		<span class="green">You gain on your quarry,</span> and chase <<him>> into an alley. It's a dead end.
		<br><br>

		<<He>> turns to face you, then charges.
		<br><br>

		<<link [[Next|Stall Fight]]>><<set $fightstart to 1>><</link>>
		<br>
	<<elseif $rng gte 41>>
		<span class="green">You gain on your quarry,</span> and chase <<him>> into an alley. It's a dead end.
		<br><br>

		<<He>> turns to face you, eyes darting this way and that, looking for a way out. <<He>> throws the <<print $stall_plant.replace(/_/g," ")>> into the air, then turns and climbs the wall while you catch it.
		<br><br>

		You return to your stall.
		<br><br>

		<<stall_actions>>
	<<else>>
		<span class="green">You gain on your quarry,</span> and chase <<him>> into an alley. It's a dead end.
		<br><br>

		"S-sorry," <<he>> says, holding out the <<print $stall_plant.replace(/_/g," ")>> and avoiding your gaze. "I wanted it for my <<if $pronoun is "m">>girlfriend<<else>>boyfriend<</if>>, but I couldn't afford it. It was wrong."
		<br><br>

		<<link [[Let them keep it|Stall Keep]]>><</link>>
		<br>
		<<link [[Scold|Stall Scold]]>><</link>>
		<br>
		<<link [[Remain silent|Stall Silent]]>><</link>>
		<br>
		<<if $skulduggery gte 300>>
			<<link [[Give pointers|Stall Pointers]]>><<trauma -6>><</link>><<ltrauma>>
			<br>
		<</if>>
	<</if>>
<<else>>
	<span class="red"><<Hes>> too fast,</span> you're unable to catch up.
	<br><br>

	You return to your stall empty-handed.
	<br><br>

	<<stall_actions>>
<</if>>