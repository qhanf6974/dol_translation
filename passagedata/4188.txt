<<set $outside to 1>><<set $location to "forest">><<effects>>

You offer to go shopping for a new one with Robin. <<His>> eyes light up. "Thank you. I've been needing a new basket anyway."
<br><br>

Together you walk to the market and pick out a new picnic basket to buy. Afterwards, you make your way back to the orphanage. You hold Robin's hand with your own, and the new basket in the other.
<br><br>
<<robinoptions>>