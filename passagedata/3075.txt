<<effects>>

You dance for the three <<if $NPCList[0].pronoun is "f" and $NPCList[1].pronoun is "f" and $NPCList[2].pronoun is "f">>fisherwomen<<else>>fishermen<</if>>.
<<if $danceskill gte 800>>
	You're confident in your abilities, and you think you know what they want to see.
<<elseif $danceskill gte 400>>
	You think you're okay at this, but you hope their standards aren't too high.
<<elseif $danceskill gte 100>>
	This isn't your first dance by any means, but it's a strange situation and you worry they won't like it.
<<else>>
	You don't really know what you're doing, but do your best to be graceful. You hope it's enough.
<</if>>

<<if $phase is 0>>
	<<set _dance_difficulty to 600>>
<<elseif $phase is 1>>
	<<set _dance_difficulty to 600>>
	They hoot as you expose your body for them, but you're not sure you could manage this and maintain dignity at the same time.
<<else>>
	<<set _dance_difficulty to 1000>>
	Concealing your body at the same time proves difficult. You hope the coyness is worth something.
<</if>>
<<danceskilluse>>
<br><br>

<<if $danceskill gte random(1, _dance_difficulty)>>
	<<pass 5>>
	<span class="green">You hold their attention.</span> They reach to grope more than once, but you gracefully weave out of the way. They're cheering by the time the music stops, and demand an encore.
	<br><br>

	<<covered>>
	<<if $submissive gte 1150>>
		"I-I danced for you," you say. "Please bring me to shore."
	<<elseif $submissive lte 850>>
		"That's my end of the agreement," you say. "You better uphold yours."
	<<else>>
		"I danced for you," you say. "As we agreed."
	<</if>>
	<br><br>

	"Fine," the captain responds, finishing <<his>> drink.
	<br><br>

	They let you stay indoors and give you some towels to dry with. You soon pull into the dock.
	<br><br>
	<<tearful>> you climb from the ship.
	<br><br>
	<<dry>><<exposure>><<towelup>><<pass 30>>

	<<endevent>>
	<<link [[Next|Docks]]>><</link>>
	<br>

<<else>>
	Unfortunately you <span class="red">slip</span> on the wet floor. "Don't worry <<girl>>," the <<person1>><<person>> says as <<his>> crew keel over laughing. "We'll help you up."
	<br><br>

	<<link [[Next|Sea Boat Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>