<<set $outside to 1>><<set $location to "home">><<home_effects>><<effects>>
You are in the garden behind the orphanage.
<br><br>
<<if $mason_pond is 3>>
	A portion of the garden has been cordoned off.
	<<if $daystate is "night">>
		<<if $mason_pond_timer gte 7>>
			The hole they plan to dig beyond might be dangerous in the dark.
		<<elseif $mason_pond_timer gte 4>>
			The hole they've dug beyond might be dangerous in the dark.
		<<elseif $mason_pond_timer gte 1>>
			The hole they've dug beyond might be dangerous in the dark.
		<<else>>
			It's hard to tell in the dark, but the spring is finished.
		<</if>>
	<<else>>
		<<if $weather is "rain">>
			<<if $mason_pond_timer gte 7>>
				The rain prevents work from commencing.
			<<elseif $mason_pond_timer gte 4>>
				The hole in the ground is abandoned for the time being, waiting for the rain to pass.
			<<elseif $mason_pond_timer gte 1>>
				The pond is taking shape. The workers must be getting close to breaching the spring. For now, it fills with rainwater.
			<</if>>
		<<else>>
			<<if $mason_pond_timer gte 6>>
				Several workers dig.
			<<elseif $mason_pond_timer gte 3>>
				The pond looks pretty deep, but still the workers dig.
			<<elseif $mason_pond_timer gte 1>>
				The pond is taking shape. The workers must be getting close to breaching the spring.
			<</if>>
		<</if>>
	<</if>>
	<br><br>
<<elseif $mason_pond is 4 and $daystate isnot "night">>
	<<set $mason_pond to 5>>
	You hear laughter amidst a splashing of water. The workers have finished the spring, and some orphans are taking advantage. They sit along its edge, bathing their feet and splashing each other.
	<br><br>
	They cheer when they spot you.
	<<lltrauma>><<trauma -18>><<llstress>><<stress -18>><<ggghope>><<hope 9>>
	<br><br>
	<<add_plot garden water 1 small>>
	Some of the water laps over neighbouring soil, near the flower beds. <span class="gold">You could grow crops there. Some plants like lots of water.</span>
	<br><br>
<</if>>
<<if $stress gte 10000>>
	<<passouthome>>
<<elseif $exposed gte 1 and $hour lt 22 and $hour gt 6>>
	<<exhibitionismgarden>>
	You can't bear the thought of someone catching you like this. You hasten to your bedroom.
	<br><br>
	[[Next|Bedroom]]
<<else>>

<<if $hour gte 7 and $hour lt 20 and $weather is "rain">>
Some orphans are playing.
<br><br>
<</if>>

<<if $hour gte 7 and $hour lt 20 and $dev is 1>>
<br>
<<link [[Join in (0:20)|Garden Play]]>><<trauma -2>><<stress -4>><<pass 20>><</link>><<ltrauma>><<lstress>>
<br>
<</if>>

<<link [[Flower Bed|Garden Flowers]]>><</link>>
<br>

<<if $weather is "clear" and $daystate is "day">>
<<link [[Bask in the sun (0:10)|Bask]]>><<pass 10>><<tanned 10>><<set $stress -= 20>><</link>><<lstress>><<gtanned>>
<br>
<</if>>

<<if $exposed gte 2>>
	<<if $daystate is "night">>
		<<if $exhibitionism gte 55>>
			<<link [[Climb over the fence (0:05)|Garden Fence Naked Night]]>><<pass 5>><</link>><<if $ex_fence isnot 1>><<exhibitionist4>><</if>>
			<br>
		<</if>>
	<<else>>
		<<if $exhibitionism gte 75>>
			<<link [[Climb over the fence (0:05)|Garden Fence Naked Day]]>><<pass 5>><</link>><<if $ex_fence isnot 1>><<exhibitionist5>><</if>>
			<br>
		<</if>>
	<</if>>
<<elseif $exposed gte 1>>
	<<if $daystate is "night">>
		<<if $exhibitionism gte 15>>
			<<link [[Climb over the fence (0:05)|Garden Fence Undies Night]]>><<pass 5>><</link>><<if $ex_fence isnot 1>><<exhibitionist2>><</if>>
			<br>
		<</if>>
	<<else>>
		<<if $exhibitionism gte 35>>
			<<link [[Climb over the fence (0:05)|Garden Fence Undies Day]]>><<pass 5>><</link>><<if $ex_fence isnot 1>><<exhibitionist3>><</if>>
			<br>
		<</if>>
	<</if>>
<<else>>
	<<link [[Go outside (0:01)|Domus Street]]>><<pass 1>><</link>>
	<br>
<</if>>

<<link [[Main Hall (0:01)->Orphanage]]>><<pass 1>><</link>>
<br>

<</if>>