<<effects>>
You leap from the bus. You land rolling on a patch of grass. <<tearful>> you struggle to your feet.
<<set $pain += 60>>
<<clotheson>>
<<endcombat>>
<<set $rng to random(1, 13)>>
<<switch $rng>>
<<case 1>>
	<<set $bus to "nightingale">>
<<case 2>>
	<<set $bus to "domus">>
<<case 3>>
	<<set $bus to "elk">>
<<case 4>>
	<<set $bus to "high">>
<<case 5>>
	<<set $bus to "starfish">>
<<case 6>>
	<<set $bus to "barb">>
<<case 7>>
	<<set $bus to "connudatus">>
<<case 8>>
	<<set $bus to "wolf">>
<<case 9>>
	<<set $bus to "harvest">>
<<case 10>>
	<<set $bus to "oxford">>
<<case 11>>
	<<set $bus to "danube">>
<<case 12>>
	<<set $bus to "mer">>
<<case 13>>
	<<set $bus to "cliff">>
<</switch>>
<<destination>>