<<set $outside to 0>><<set $location to "home">><<effects>>

Whitney shrugs, and nods to <<person1>><<his>> friends.

<<if $exposed gte 2>>

Hands reach from all around, grabbing your arms and <span class="lewd">pulling them aside.</span> The <<person6>><<person>> looks about to salivate. "The booze," Whitney says. The <<person>> nods and disappears for a moment, returning with a clinking bag.
<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
<br><br>

<<else>>

Hands reach from all around, grabbing your clothes and <span class="lewd">pulling them aside.</span> The <<person6>><<person>> leers at your exposed body. <<He>> looks about to salivate. "The booze," Whitney says. The <<person>> nods and disappears for a moment, returning with a clinking bag.
<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
<br><br>

<</if>>

Whitney takes the bag, then jabs the <<person6>><<person>> in the stomach with <<his>> bat. <<person1>>"That's for asking for something in return," <<he>> says as the <<person6>><<person>> splutters.
<br><br>

"We got a treat for once," Whitney says once back on the road.
	<br><br>

<<link [[Next|Whitney Trick 6]]>><<endevent>><<pass 30>><</link>>
<br>