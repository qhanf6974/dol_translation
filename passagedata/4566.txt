<<set $outside to 1>><<set $location to "town">><<effects>>
You drop to the ground and shuffle beneath a parked car. The vehicle is soon surrounded by people, their feet stepping just inches from you.
<br><br>
<<if $rng gte 81>>
	A few minutes have passed when the engine rumbles into life above you, and the car beings to move. Afraid of being caught surrounded in public with your <<lewdness>> on display, you cling to the underside, allowing it to carry you along. The journey is over soon, and once the driver has vacated you shuffle out into the daylight. You're in the clear for now.
	<br><br>
	<<set $rng to random(1, 13)>>
	<<switch $rng>>
	<<case 1>>
		<<set $bus to "nightingale">>
	<<case 2>>
		<<set $bus to "domus">>
	<<case 3>>
		<<set $bus to "elk">>
	<<case 4>>
		<<set $bus to "high">>
	<<case 5>>
		<<set $bus to "starfish">>
	<<case 6>>
		<<set $bus to "barb">>
	<<case 7>>
		<<set $bus to "connudatus">>
	<<case 8>>
		<<set $bus to "wolf">>
	<<case 9>>
		<<set $bus to "harvest">>
	<<case 10>>
		<<set $bus to "oxford">>
	<<case 11>>
		<<set $bus to "danube">>
	<<case 12>>
		<<set $bus to "mer">>
	<<case 13>>
		<<set $bus to "cliff">>
	<</switch>>
<<else>>
	The crowd disperses after a short time. You shuffle back out from beneath the car. You're in the clear for now.
	<br><br>
<</if>>
<<endevent>>
<<destinationeventend>>