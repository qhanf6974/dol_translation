<<effects>>

<<if $farm.beasts.horses lte -20>>
	<<set _tending_difficulty to 1200>>
<<else>>
	<<set _tending_difficulty to 800>>
<</if>>
<<if $tending gte random(1, _tending_difficulty)>>
	"Bad horses," you say, <span class="green">your voice firm and sure.</span> "Get in the field with the others. I won't give you a second chance."<<grespect>><<farm_horses 1>>
	<br><br>
	
	<<if $farm_work.horse.monster is true>>
		The centaur glances over your shoulder, looking at its colleague. It sneers and turns. Both run for the field. You shut the gate behind them.
		<br><br>
	<<else>>
		The horse shakes its head, then steps away from you. Both run for the field. You shut the gate behind them.
		<br><br>
	<</if>>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	"B-bad horses," you say, <span class="red">but your voice trembles.</span> "G-get in the field with the others. Shoo."<<lrespect>><<farm_horses -1>><<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
	<br><br>
	
	<<link [[Next|Farm Horses Endure]]>><</link>>
	<br>
	
<</if>>