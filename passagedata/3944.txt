<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $NPCName[$NPCNameList.indexOf("Kylar")].rage gte 95>>

Kylar is quiet, only speaking to point out something behind you. It's gone by the time you look.
<br><br>

You finish the food. "I'm going to wait in the classroom," Robin says when you're done. "I don't like being late." <<He>> hugs you.
<br><br>
<<endevent>><<npc Kylar>><<person1>>
<span class="red">You stagger as you rise to your feet.</span> You feel light-headed. "Are you okay?" you hear Kylar say through thickening fog. "You need the nurse." <<He>> puts an arm around your shoulder and steers you towards the door. You black out.
<br><br>
<<endevent>>
<<link [[Next|Kylar Basement]]>><</link>>
<br>

<<else>>

Robin tries to engage Kylar in conversation, but Kylar interrupts <<endevent>><<npc Robin>><<person1>><<him>> whenever <<he>> tries to speak.
<br><br>

	<<if $rng gte 51>>

	Kylar tries to kiss you.
	<br><br>
	<<endevent>>

	<<link [[Kiss Kylar|Kylar Canteen Kiss]]>><<npcincr Kylar love 5>><<npcincr Kylar rage -5>><<status -10>><</link>><<lcool>>
	<br>
	<<link [[Kiss Robin instead|Kylar Canteen Robin]]>><<npcincr Robin love 1>><<npcincr Kylar love -5>><<npcincr Kylar rage 5>><</link>>
	<br>
	<<link [[Refuse|Kylar Canteen Refuse]]>><<npcincr Kylar love -1>><<npcincr Kylar rage 1>><</link>>
	<br>

	<<else>>

	You finish the food. "I'm going to wait in the classroom," Robin says when you're done. "I don't like being late." <<He>> hugs you. Kylar glares at <<himcomma>> but <<he>> gives no sign of noticing.
	<br><br>

<<endevent>>
<<link [[Next|Canteen]]>><</link>>
<br>
	<</if>>
<</if>>