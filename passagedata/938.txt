<<effects>>

You peer through the dark, and then you see it. A flash of white. You jog after it, careful to avoid the rocks strewn across the floor.
<br><br>

You see the end of the tunnel up ahead, lit by an unseen source. The tunnel turns, opening into a larger cavern. Another pool of water laps at a shore, but this one is different. Darker, and looking straight at it fills you with dread. As if something were looking back.<<gstress>>
<br><br>

<<link [[Next|Meadow Cave 2]]>><</link>>
<br>