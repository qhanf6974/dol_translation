<<effects>>

Heart racing and face reddening you flee the room, finding refuge in a pantry. Avery arrives soon after, carrying a towel.
<br><br>

"Believe me," <<he>> says as you reach for it. "I'm going to cause a fuss over this. It was uncalled for." Despite <<his>> words, <<he>> doesn't look upset. As your fingers brush the fabric, <<he>> pulls it away from you.
<br><br>

"Let's have a little peek first," <<he>> says. "Or you can walk home like that." You can't tell if <<hes>> joking.
<br><br>

<<if !$worn.under_lower.type.includes("naked")>>
	<<if $exhibitionism gte 15>>
		<<link [[Flaunt|Avery Party Dance Undies Flaunt]]>><<set $endear += 20>><</link>><<exhibitionist2>><<ggendear>>
		<br>
	<</if>>
	<<link [[Show|Avery Party Dance Show]]>><<sub 1>><<trauma 6>><<stress 6>><<set $endear += 20>><</link>><<gtrauma>><<gstress>><<ggendear>>
	<br>
	<<link [[Refuse|Avery Party Dance Naked Refuse]]>><<def 1>><<set $endear -= 10>><<npcincr Avery rage 5>><</link>><<garage>><<llendear>>
	<br>
<<else>>
	<<if $exhibitionism gte 55>>
		<<link [[Flaunt|Avery Party Dance Naked Flaunt]]>><<set $endear += 30>><</link>><<exhibitionist4>><<gggendear>>
		<br>
	<</if>>
	<<link [[Show|Avery Party Dance Show]]>><<sub 1>><<trauma 6>><<stress 6>><<set $endear += 30>><</link>><<gtrauma>><<gstress>><<gggendear>>
	<br>
	<<link [[Refuse|Avery Party Dance Naked Refuse]]>><<def 1>><<set $endear -= 5>><<npcincr Avery rage 5>><</link>><<garage>><<lendear>>
	<br>
<</if>>
<br><br>