<<effects>>
<<if $enemyhealth lte 0>>
	<<beastwound>><<famescrap 10>>
	<<if $combatTrain.length gt 0>>
		The dog recoils in pain and fear, but another is eager for a go.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Cream Ex Dog Rape]]
	<<else>>
		The dog recoils in pain and fear.
		<br><br>
		<<link [[Next|Cream Ex Dog Rape Finish]]>><<set $finish to 1>><</link>>
	<</if>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>><<famebestiality 10>>
	<<if $combatTrain.length gt 0>>
		Satisfied, the dog moves and another takes its turn.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Cream Ex Dog Rape]]
	<<else>>
		Satisfied, the dog moves away from you.
		<br><br>
		<<link [[Next|Cream Ex Dog Rape Finish]]>><<set $finish to 1>><</link>>
	<</if>>
<<else>>
	<<if $alarm is 1 and $rescue is 1>>
		Your screams stir some of the crowd, and they rush forward to help. There's a brief tussle as those enjoying it try to stop them, but soon the dogs are grabbed by their collars and hauled off you.
		<br><br>
	<<else>>
		<<if $enemywounded gte 2 and $enemyejaculated is 0>>
			The dogs whimper and flee.
		<<elseif $enemywounded is 0 and $enemyejaculated gte 2>>
			The dogs leave you spent and shivering as the audience applauds.
		<<elseif $enemywounded gte 1 and $enemyejaculated gte 1>>
			The dogs leave you spent and shivering as the audience applauds.
		<</if>>
	<</if>>
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<tearful>> you struggle to your feet. You walk to the edge of the crowd. At first you're not sure if they'll let you leave, but then they step aside, giving you an avenue through. The crowd follows you the rest of the way. Your legs move quicker as you reach the park. Some follow you in, but you soon lose them in the maze of tall bushes and trees.
	<br><br>

	<<cream_end>>

	<<link [[Next|Park]]>><<endevent>><</link>>
	<br>
<</if>>