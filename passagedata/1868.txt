<<set $outside to 0>><<set $location to "pub">><<dockeffects>><<effects>>
<<set $dockpubcount += 1>>
<<if $hour gte 3 and $hour lte 18>>

Your colleagues pat your back, congratulating your endurance. They separate and stagger home.
<<gcool>><<dockstatus 5>>
<br><br>

<<destinationeventend>>

<<elseif $rng gte 81>>
<<generate1>><<generate2>><<generate3>><<generate4>><<generate5>><<generate6>><<person1>>
Accompanying your colleagues, you <<if $drunk gte 360>>stagger<<else>>walk<</if>> into a pub on Mer Street. It's busy. Sailors from the moored ships are unwinding here.
<br><br>

"Pub quiz in ten minutes," the jovial owner shouts. "Winning team gets a round on the house." Your colleagues sign up at once. The sailors scoff. "You got no chance," one of them, a <<personcomma>> says. Your colleagues throw colourful insults in response.
<br><br>

"You lot talk big," the <<person>> says after much back and forth. <<He>> pulls £60 from <<his>> pocket. <<His>> fellow sailors do likewise. "That's £360 says we win the quiz." Despite their previous bluster, the dockers seem unsure.
<br><br>

<<if $promiscuity gte 35>>
<<link [[Bet your body|Docks Pub Crawl Body]]>><<dockstatus 1>><</link>><<promiscuous3>><<gcool>>
<br>
<</if>>
<<if $money gte 6000>>
<<link [[Match their bet (£60)|Docks Pub Crawl Bet]]>><<set $money -= 6000>><</link>>
<br>
<</if>>
<<link [[Don't bet anything|Docks Pub Crawl Quiz Refuse]]>><<set $phase to 0>><</link>>
<br>
<<link [[Don't take part|Docks Pub Crawl Quiz Refuse]]>><<set $phase to 1>><</link>>
<br>

<<elseif $rng gte 61>>

Accompanying your colleagues, you <<if $drunk gte 360>>stagger<<else>>walk<</if>> into a pub on Harvest Street. Some rough looking patrons eye you up as you enter.
<br><br>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
	<<generate1>><<person1>>A <<person>> sits beside you and places their hand on your thigh. "Fancy disappearing together for a few minutes?" <<he>> says.
	<br><br>

		<<link [[Accept|Docks Pub Crawl Sex]]>><<set $sexstart to 1>><</link>><<promiscuous1>>
		<br>
		<<link [[Refuse|Docks Pub Crawl Sex Refuse]]>><</link>>
		<br>
	<<else>>
	You drink a couple of rounds together, ignoring evil looks from the regular patrons. You leave with the other dockers.
	<br><br>

	<<dockpuboptions>>
	<</if>>

<<elseif $rng gte 41>>

Accompanying your colleagues, you <<if $drunk gte 360>>stagger<<else>>walk<</if>> into a pub on Barb Street.
<br><br>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
	You overhear one of the other dockers talking about you. <<generate1>><<person1>>"You sure the <<girl>> can handle it," <<he>> says. "The drinking, I mean. I don't want to get in trouble with <<pher>> parents or nothing."
	<br><br>

	<<link [[Challenge to drinking contest|Docks Pub Crawl Contest]]>><<dockstatus 1>><<set $phase to 0>><</link>><<gcool>>
	<br>
	<<link [[Remain silent|Docks Pub Crawl Silent]]>><<dockstatus -1>><</link>><<lcool>>
	<br>

	<<else>>
	People pay you and the other dockers no mind. You drink a few rounds and leave together.
	<br><br>

	<<dockpuboptions>>
	<</if>>

<<elseif $rng gte 21>>

Accompanying your colleagues, you <<if $drunk gte 360>>stagger<<else>>walk<</if>> into a pub on Connudatus Street.
<br><br>

<<generate1>><<generate2>><<person2>>You notice a docker eyeing up a <<person>> on the other side of the room.
<br><br>

<<link [[Encourage|Docks Pub Crawl Eyeing Encourage]]>><</link>>
<br>
<<link [[Ignore|Docks Pub Crawl Eyeing Ignore]]>><</link>>
<br>

<<else>>

Accompanying your colleagues, you <<if $drunk gte 360>>stagger<<else>>walk<</if>> into a pub on the High Street.
<br><br>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
	You attract attention. Other patrons keep trying to flirt with you.
	<br><br>

	<<link [[Flirt|Docks Pub Crawl Flirt]]>><</link>><<promiscuous1>>
	<br>
		<<if $worn.lower.skirt is 1>>
		<<link [[Tease|Docks Pub Crawl Tease]]>><</link>><<exhibitionist1>>
		<br>
		<</if>>
	<<link [[Ignore|Docks Pub Crawl Flirt Ignore]]>><</link>>
	<br>

	<<else>>

	No one pays you or the other dockers much mind. You drink a few rounds and leave together.
	<br><br>

	<<dockpuboptions>>

	<</if>>

<</if>>