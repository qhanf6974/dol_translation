<<set $outside to 0>><<set $location to "arcade">><<effects>>

You drop your gun and walk away. The <<person>> picks it up. <<Hes>> distracted managing both, so you reach for the money sticking out of <<his>> pocket.
<br><br>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>

<span class="green"><<He>> doesn't notice.</span> <span class="gold">You've gained £30.</span>
<<set $money += 3000>>
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>
<<endevent>>
<<link [[Next|Arcade]]>><</link>>
<br>

<<else>>

<span class="red"><<He>> turns and spots you.</span> "Oi," <<he>> says. "Keep your mitts to yourself." <<He>> tucks the money further into <<his>> pocket, well beyond easy reach.
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>
<<endevent>>
<<link [[Next|Arcade]]>><</link>>
<br>

<</if>>