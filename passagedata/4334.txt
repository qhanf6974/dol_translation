<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "commercialdrain">>
You are within the storm drain system beneath the commercial district. Water rushes down the centre, flanked by a thin walkway on each side.
<br><br>
You can access several parts of town from here.
<br><br>
<<if $stress gte 10000>>
	<<passoutdrain>>
<<else>>
	<<set $rng to random(1, 100)>>
	<<if $weather is "rain" and $rng gte 75 and $eventskip is 0>>
		<<eventsdrain>>
	<<elseif $rng gte 92 and $eventskip is 0>>
		<<eventsdrain>>
	<<else>>
		<<if $sewersintro is 1>>
			<<link [[Climb down|Sewers Commercial]]>><<sewersstart>><<set $eventskip to 1>><</link>>
			<br><br>
		<<elseif $historytrait gte 1>>
			<<link [[Examine a ladder leading down|Drain Ladder]]>><</link>>
			<br><br>
		<</if>>
		<<residentialdrain>>
		<br>
		<<industrialdrain>>
		<br><br>
		<<if $exposed gte 1 and $daystate isnot "night">>
		<<else>>
			<<high>>
			<<connudatus>>
			<<cliff>>
			<<nightingale>>
			<<starfish>>
			<<oxford>>
		<</if>>
		<br>
		<<commercial>>
		<<park>>
		<br>
		<<if $historytrait gte 2 and $schooldrainintro gte 1>>
			<<link [[Secret tunnel to school (0:02)|School Toilets]]>><<pass 2>><</link>>
			<br>
		<</if>>
	<</if>>
	<<displayLinks>>
<</if>>
<<set $eventskip to 0>>