<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<if $NPCName[$NPCNameList.indexOf("Quinn")].init is 1>>

<<npc Quinn>><<person1>>Mayor Quinn enters the cafe. <<He>> passes <<his>> <<if $pronoun is "f">>fur <</if>> coat to a <<if $rng gte 51>>waiter<<else>>waitress<</if>>, and poses for a photographer.
<br><br>

<<He>> looks around the room, <<his>> gaze settling on you. Pushing through the gathered crowd, <<he>> makes <<his>> way over. "It's good to see you again," <<he>> says, grasping your hand and shaking it.
<br><br>

<<else>>

<<npc Quinn>><<person1>>A middle-aged <<if $pronoun is "m">>man<<else>>woman<</if>> enters the cafe. <<He>> passes <<his>> <<if $pronoun is "f">>fur <</if>> coat to a <<if $rng gte 51>>waiter<<else>>waitress<</if>>, and poses for a photographer.
<br><br>

<<He>> looks around the room. <<His>> gaze settles on you. Pushing through the gathered crowd, <<he>> makes <<his>> way over. "You must be the star of the show," <<he>> says, grasping your hand and shaking it. "I'm Quinn." You recognise the name. <span class="red"><<Hes>> the mayor.</span>
<br><br>

<</if>>

Bailey leans in and whispers something to Quinn. The mayor laughs. "Nonsense," <<he>> says. "The speech will go ahead." Quinn grasps your arm again, and pulls you beside <<himstop>> Niki appears from the crowd, camera in hand.
<br><br>

<<link [[Smile for the camera|Chef Opening Smile]]>><<set $NPCName[$NPCNameList.indexOf("Quinn")].love += 1>><<famebusiness 50>><</link>>
<br>
<<link [[Pull a funny face|Chef Opening Funny]]>><<set $NPCName[$NPCNameList.indexOf("Quinn")].love += 1>><</link>>
<br>
<<link [[Refuse to be photographed|Chef Opening Refuse Photograph]]>><<set $NPCName[$NPCNameList.indexOf("Quinn")].love -= 1>><</link>>
<br>