<<effects>>

You chase after your headwear, off the road and down the ridge of a hill. It settles on a tree branch.

<<if $physiquesize gte 16000>>
	You jump in the air, and just about reach it. <<large_text>>
<<else>>
	It's too high to reach, so you pick up a nearby stick to help. You manage to hook it on the end.
<</if>>
<br><br>
<<clotheson>>

<<if $rng gte 61>>
	You hear a buzz. A swarm of bees emerge from a hive, agitated by the moving branch. You run back to the road in a panic.<<gtrauma>><<gstress>><<gstress>><<stress 6>>
	<br><br>

	<<destinationfarmroad>>
<<else>>
	You return to the road, your $worn.head.name back where it belongs.
	<br><br>

	<<destinationfarmroad>>
<</if>>