<<effects>>

<<if $submissive gte 1150>>
	"I-I'm not afraid of you," you say, surprised by your confidence.
<<elseif $submissive lte 850>>
	"Your toys won't help," you say. "Either get me back to shore, or I'm taking your ship and sailing there myself."
<<else>>
	"Give up while you still can," you say. "I only want to get back to shore."
<</if>>
<br><br>
<<dry>>

<<dry>>The <<person1>><<person>> sneers at you, but winces and clutches <<his>> hips. "Fine. But don't you dare breathe a word of this to anyone."
<br><br>

You spend the rest of the voyage indoors. You find towels to dry yourself with, and soon pull into the dock.
<br><br>

<<tearful>> you climb ashore.
<<pass 30>>
<br><br>

<<clothesontowel>>
<<endcombat>>
<<pass 30>>
<<link [[Next|Docks]]>><</link>>
<br>