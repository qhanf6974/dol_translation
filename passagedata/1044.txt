<<effects>>

You wash your body in the shallows. The water is clean, at least. You're careful not to step out too far, lest you be swept away.
<br><br>

<<link [[Next|Livestock Field]]>><</link>>
<br>