<<effects>>
<<if $molestationstart is 1>>
	<<set $molestationstart to 0>>
	<<set $combat to 1>>
	<<set $enemytype to "tentacles">>
	<<molested>>
	<<controlloss>>
	<<tentaclestart 7 15>>
<</if>>
<<statetentacles>>
You count $tentacles.active tentacles surrounding you.
<<effects>>
<<effectstentacles>>
<<tentacles>>
<<actionstentacles>>
<<if $tentacles.active lte ($tentacles.max / 2)>>
	<span id="next"><<link [[Next|Temple Garden Tentacles Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link [[Next|Temple Garden Tentacles]]>><</link>></span><<nexttext>>
<</if>>