<<set $outside to 1>><<set $location to "temple">><<temple_effects>><<effects>>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $rng gte 81>>
You ask a priest what work needs doing around the garden. You're asked to trim a row of hedges. You take a pair of shears and get to work.
<br><br>
	<<if $danger gte (9900 - $allure) and $tentacledisable is "f" and $hallucinations gte 2>>
		<<tending_text>> It's hard going, but you neaten things up a great deal. <<physique 6>>
		<br><br>
		You're reaching in to cut a rogue branch when your wrist gets stuck. You reach your other hand in to help free yourself, and that gets stuck too. You feel something wrap around both ankles. A force pulls you off your feet and into the hedge.
		<br><br>
		You find yourself suspended within its branches, held aloft by tendrils wrapped around your body. More tendrils emerge from every direction.
		<br><br>

		<<link [[Struggle|Temple Garden Tentacles]]>><<set $molestationstart to 1>><</link>>
		<br>
		<<link [[Soothe|Temple Garden Soothe]]>><</link>><<tendingdifficulty 1 1200>>
		<br>
	<<else>>
		<<tending_text>> It's hard going, but you neaten things up a great deal. <<physique 6>>
		<br><br>
		<<link [[Next|Temple Garden]]>><</link>>
		<br>
	<</if>>
<<elseif $rng gte 61>>
You ask a priest what work needs doing around the garden. You're asked to deweed the flower beds. You drop to your knees and get to work.
<br><br>
	<<if $danger gte (9900 - $allure) and $bestialitydisable is "f">>
		<<tending_text>> Your hands get dirty, and beads of sweat slip down your cheek, but the flowers have lots more space when you're done. <<physique 6>>
		<br><br>
			<<if $syndromewolves is 1>>
				You hear footsteps behind you. You turn just as a wolf jumps on top of you. It licks your face. You can't help but laugh.
				<<ltrauma>><<lstress>><<trauma -6>><<stress -6>>
				<br><br>
				You watch as it returns to the forest.
				<br><br>

				<<link [[Next|Temple Garden]]>><</link>>
				<br>
			<<else>>
				You hear footsteps behind you. Before you can turn, a wolf leaps atop you.
				<br><br>
				<<link [[Next|Temple Garden Work Wolf]]>><<set $molestationstart to 1>><</link>>
				<br>
			<</if>>
	<<else>>
		<<tending_text>> Your hands get dirty, and beads of sweat slip down your cheek, but the flowers have lots more space when you're done. <<physique 6>>
		<br><br>

		<<link [[Next|Temple Garden]]>><</link>>
		<br>
	<</if>>
<<elseif $rng gte 41>>
You ask a priest what work needs doing around the garden. You're asked to gather fruit from the trees growing near the temple proper.
<br><br>
	<<if $danger gte (9900 - $allure)>>
		Basket in hand, you climb a stepladder and start picking <<print either("lemons", "limes", "apples", "oranges", "pears")>>. <<tending_text>> It's pleasant, though you need to stretch pretty far to reach some of them. <<physique 6>><<lstress>><<stress -6>>
		<br><br>

		<<generate1>><<person1>>
		"Need a hand up there?" a <<monk>> says behind you. Without waiting for a response, <<he>> climbs the step ladder. There's not much room at the top. <<He>> grasps your hips to steady <<himselfstop>> <<if $NPCList[0].penis isnot "none">><span class="lewd">You feel something hard beneath <<his>> habit, rubbing against your <<bottomstop>></span><</if>>
		<br><br>

		<<link [[Shove|Temple Garden Shove]]>><<trauma -6>><<stress -6>><<grace -5 monk>><</link>><<llgrace monk>><<ltrauma>><<lstress>>
		<br>
		<<link [[Put up with it|Temple Garden Put]]>><<grace 1 monk>><</link>><<ggrace monk>>
		<br>
	<<else>>
		Basket in hand, you climb a stepladder and start picking <<print either("lemons", "limes", "apple", "oranges", "pears")>>. It's pleasant,though you need to stretch pretty far to reach some of them. <<physique 6>><<lstress>><<stress -6>>
		<br><br>

		<<link [[Next|Temple Garden]]>><</link>>
		<br>
	<</if>>
<<elseif $rng gte 21>>
You ask a priest what work needs doing around the garden. You're asked to tend the fire in the hedge maze.
<br><br>

	<<set $rng to random(1, 100)>>
	<<if $rng gte 81>>
		<<npc Jordan>><<generate2>><<generatey3>><<person2>>
		You shovel coal onto the fire. A <<monk>> keeps an eye on you as a <<he>> trims the hedges around the clearing. <<physique 6>>
		<br><br>

		<<tending_text>> It doesn't take long. You're just finishing up when Jordan arrives from the temple, a <<person3>><<person>> in tow.
		<br><br>

		Jordan whispers to the <<person2>><<monkcomma>> who smiles and looks at you. "Keep shovelling," <<he>> says. "This young'un is attempting the trial." You return to work, shovelling more coal onto the fire while the <<monk>> encourages the flame with a pair of bellows.
		<br><br>

		The <<monk>> grabs a shovel and starts spreading the coals into a thin bed. You follow suit. You soon have the bed ready. The <<person3>><<person>> looks on, apprehensive.
		<br><br>

		Jordan gives <<him>> a brief explanation, and <<he>> steps onto the coals.

		<<set $rng to random(1, 100)>>
		<<if $rng gte 91>>
			<<He>> walks across at a brisk pace. <<His>> determined expression strains as <<he>> moves, but <<he>> makes it to the other side before stumbling and checking <<his>> feet. The <<person2>><<person>> cheers. You find yourself cheering too.
			<<ltrauma>><<trauma -6>>
			<br><br>
			Jordan steers the triumphant <<person3>><<person>> back to the temple while explaining the duties of an initiate.
		<<elseif $rng gte 71>>
			<<He>> almost makes it to the other end before pain gets the better of <<himstop>> Jordan reassures <<him>> as <<person1>><<he>> steers <<person3>><<him>> back to the temple.
		<<elseif $rng gte 51>>
			<<He>> makes it to <<his>> second step before leaping away in pain. Jordan reassures <<him>> as <<person1>><<he>> steers <<person3>><<him>> back to the temple.
		<<else>>
			<<He>> recoils in pain as soon as <<his>> toe comes in contact with the heat. The <<person2>><<person>> chuckles. Jordan reassures the <<person3>><<person>> as <<person1>><<he>> steers <<person3>><<him>> back to the temple.
		<</if>>
		<br><br>

		<<endevent>>

		<<link [[Next|Temple Garden]]>><</link>>
		<br>
	<<else>>
		<<generate1>><<person1>>
		You shovel coal onto the fire. A <<monk>> keeps an eye on you as a <<he>> trims the hedges around the clearing. <<physique 6>>
		<br><br>
		<<tending_text>> It doesn't take long. The <<monk>> asks you to help clean up the coals left over from a previous trial, which takes a bit longer.
		<br><br>

		<<endevent>>

		<<link [[Next|Temple Garden]]>><</link>>
		<br>
	<</if>>
<<else>>
<<generate1>><<person1>>
You ask a priest what work needs doing around the garden. You're asked to assist the <<monk>> tending to the crops at the forest's edge.
<br><br>
You find <<him>> tending to a row of trellises, each bearing a vibrant bine of hops.
	<<set $rng to random(1, 100)>>
	<<if $rng gte 81>>
		<<He>> has you search the plants for mites. <<tending_text>>
	<<elseif $rng gte 61>>
		<<He>> has you search the plants for mildew. <<tending_text>>
	<<elseif $rng gte 41>>
		<<He>> has you remove weeds from the soil. <<tending_text>>
	<<elseif $rng gte 21>>
		<<He>> has you removes weeds from the soil. <<tending_text>>
	<<else>>
		<<He>> has you harvest the riper plants. <<tending_text>>
	<</if>>
	<<physique 6>>
<br><br>
	"Nice work," <<he>> says once you're finished. <<He>> sits down, glances around, then pulls a bottle and two mugs out from under <<his>> chair. <<He>> pours a brown liquid into them.
	<br><br>
	"How about a little treat? We brew these into beer," <<he>> continues, gesturing at the bines. "Makes the temple a nice little side income. We're not supposed to drink really, but a little tipple for those working the fields is overlooked." <<He>> hands you a mug. "Go on. I insist."
	<br><br>

	<<if $danger gte (9900 - $allure) and $grace lt 0 or $danger gte (9900 - $allure) and random(1, 10) is 10>>
		<<link [[Drink|Temple Drink Drugged]]>><<set $drunk += 180>><<set $drugged += 180>><</link>>
		<br>
	<<else>>
		<<link [[Drink|Temple Drink]]>><<set $drunk += 180>><</link>>
		<br>
	<</if>>
	<<link [[Refuse|Temple Drink Refuse]]>><<grace -1 monk>><</link>><<lgrace monk>>
	<br>
<</if>>