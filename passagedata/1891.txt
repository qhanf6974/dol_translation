<<set $outside to 0>><<set $location to "pub">><<dockeffects>><<effects>>

You let your colleagues have their share. You've gained £60.
<<set $money += 6000>>
<br><br>

You and your colleagues enjoy your free drinks and leave in an elevated mood.
<<set $drunk += 60>><<lstress>><<stress -6>>
<br><br>

<<dockpuboptions>>