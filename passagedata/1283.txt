<<effects>>

<<if $tending gte random(1, 700)>>
	You tell the <<steed_text>> to stop. <span class="green">It hears the urgency in your voice,</span> and halts in its tracks. You fumble with the branch until you manage to free your $worn.lower.name, then spur on your steed once more.
	<br><br>

	<<link [[Next|Riding School Lesson]]>><</link>>
	<br>
<<else>>
	You tell the <<steed_text>> to stop. <span class="red"><<steed_He>> ignores you.</span> You cling on tight as the branch continues to tug.
	<br><br>

	<<link [[Next|Riding School Lesson Hold]]>><</link>>
	<br>
<</if>>