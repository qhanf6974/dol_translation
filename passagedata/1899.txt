<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "domus">>
<<if $phase is 1>>
	<<if $rng gte 20>>
		<<if $player.gender is "m">>
			<<set $clothingselector to "m">>
			"Very well then," <<he>> says. "I'll fetch some clothes."
			<br><br>
			<<link [[Get changed and leave (0:01)|Domus Street]]>><<spareclothesdomus>><<endevent>><<pass 1>><</link>>
			<br>
		<<else>>
			"Very well then," <<he>> says. "I'll fetch some clothes."
			<br><br>
			<<link [[Get changed and leave (0:01)|Domus Street]]>><<spareclothesdomus>><<endevent>><<pass 1>><</link>>
			<br>
		<</if>>
	<<else>>
		"I'm sorry," <<he>> says. "I have nothing to spare you. Sorry, <<girlstop>>"
		<br><br>
		<<link [[Try another house|Domus House Nude]]>><<endevent>><</link>>
		<br>
		<<link [[Stop|Domus Street]]>><<endevent>><</link>>
		<br>
	<</if>>
<<elseif $phase is 2>>
	"I'm not getting scammed like that again," <<he>> says. "Go be naked somewhere else."
	<br><br>
	<<link [[Try another house|Domus House Nude]]>><<endevent>><</link>>
	<br>
	<<link [[Stop|Domus Street]]>><<endevent>><</link>>
	<br>
<<elseif $phase is 3>>
	<<if $rng gte 20>>
		The <<person>> thinks for a moment. "If you clean my living room for me, I'll find you something to wear," <<he>> says. <<if $leftarm is "bound" and $rightarm is "bound">>"You'll have to hold the duster in your mouth."<<else>>"It should only take ten minutes."<</if>>
		<br><br>
		<<link [[Accept|Domus Nude Exhibit]]>><</link>>
		<br>
		<<link [[Refuse|Domus Street]]>><<endevent>><</link>>
		<br>
	<<else>>
		<<He>> bites his lip and considers. "I don't have any," <<he>> says, with a sly smile. "Turn that cute butt around and ask somebody else."
		<br><br>
		<<link [[Try another house|Domus House Nude]]>><<endevent>><</link>>
		<br>
		<<link [[Stop|Domus Street]]>><<endevent>><</link>>
		<br>
	<</if>>
<<elseif $phase is 4>>
	<<if $rng gte 20>>
		<<He>> smiles. "I've got some spares you can have if you fuck me," <<he>> says.
		<br><br>
		<<link [[Accept|Domus Nude Sex]]>><<set $sexstart to 1>><</link>><<promiscuous1>>
		<br>
		<<link [[Refuse|Domus Street]]>><<endevent>><</link>>
		<br>
	<<else>>
		"I have nothing to get you," <<he>> shrugs <<his>> shoulders. "See you at school."
		<br><br>
		<<link [[Try another house|Domus House Nude]]>><<endevent>><</link>>
		<br>
		<<link [[Stop|Domus Street]]>><<endevent>><</link>>
		<br>
	<</if>>
<<else>>
	<<if $rng gte 20>>
		"Sure," <<he>> says. "Just press the buzzer on that house over there and run back quickly."
		<br><br>
		<<He>> points at a building across the road.
		<br><br>
		<<link [[Accept|Domus Streak]]>><</link>>
		<br>
		<<link [[Refuse|Domus Street]]>><<endevent>><</link>>
		<br>
	<<else>>
		"And ruin such a nice view?" <<he>> laughs. "Not likely."
		<br><br>
		<<He>> leers at you.
		<br><br>
		<<link [[Try another house|Domus House Nude]]>><<endevent>><</link>>
		<br>
		<<link [[Stop|Domus Street]]>><<endevent>><</link>>
		<br>
	<</if>>
<</if>>