<<effects>>
<<pass 60>>
<<if $submissive gte 1150>>
	"You big meanies," you say. "Is money more important than protecting people?"
<<elseif $submissive lte 850>>
	"Corrupt shits like you will face a reckoning," you say. "Whatever you're being paid isn't worth it."
<<else>>
	"How much are you being paid?" you ask. "I bet they got you cheap."
<</if>>
They both laugh. "You're a smart <<girl>>," the <<person>> says. "Almost a waste."
<br><br>
After what feels like an hour, you turn down a thin lane and pull to a stop outside a barn.
<br><br>
<<link [[Next|Chef Blackmail Livestock 2]]>><</link>>
<br>