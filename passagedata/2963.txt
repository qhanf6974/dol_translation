<<effects>>

You read the <<persons>> work, and spot some useful calculations. You finish copying them for yourself just as a librarian rounds the corner. <<generate2>><<person2>><<He>> rushes to the <<person1>><<persons>> side.
<br><br>

<<person2>><<He>> eyes you with suspicion as you leave, but doesn't say anything.
<br><br>
<<endevent>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>