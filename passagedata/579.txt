<<set $outside to 0>><<set $location to "lake_ruin">><<underwater>><<effects>><<lakeeffects>>
<<set $lakeruinkey to 2>>
You hear the ancient mechanism creak and the door groans open. Beyond is a plinth, stood alone in a rubble-strewn room. Atop it sits an ivory necklace. It's studded with blue jewels.
<br><br>
<<link [[Take it|Lake Ruin Deep]]>><<set $antiquemoney += 2000>><<museumAntiqueStatus "antiqueivorynecklace" "found">><</link>>
<br>
<<link [[Leave|Lake Ruin Deep]]>><</link>>
<br>