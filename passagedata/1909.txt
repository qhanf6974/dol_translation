<<set $outside to 0>><<set $location to "town">><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	The beast barks happily and walks over to the window.
	<<tearful>> you rise to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	The dog pacified, you notice its collar glinting. You look closer and see it's encrusted with jewels.
	<br><br>
	<<link [[Take It|Domus House Collar]]>><<crimeup 50>><</link>><<crime>>
	<br>
	<<link [[Leave|Domus Street]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	The beast cowers and rolls over in submission.
	<<tearful>> you rise to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	The dog pacified, you notice its collar glinting. You look closer and see it's encrusted with jewels.
	<br><br>
	<<link [[Take It|Domus House Collar]]>><<crimeup 50>><</link>><<crime>>
	<br>
	<<link [[Leave|Domus Street]]>><</link>>
	<br>
<<elseif $alarm is 1>>
	<<if $NPCList[0].stance is "top">>
		<<person1>>"What you got there boy?" says a <<personstop>>
		<<He>> laughs when <<he>> sees you. "Serves you right, sneaking around my property like that." <<He>> takes a seat. "Don't mind me."
		<br><br>
		<<link [[Next|Domus House Dog]]>><<set $phase to 1>><</link>>
	<<else>>
		<<endcombat>>
		<<generate1>><<person1>>"What you got there boy?" says a <<personstop>>
		<<He>> laughs when <<he>> sees you. "Serves you right, sneaking around my property like that. But I have a better idea." <<He>> grabs the dog by the collar and hauls it off you.
		<<famebestiality 1>>
		<br><br>
		<<link [[Next|Domus House Sneak Molestation]]>><<set $molestationstart to 1>><</link>>
		<br>
	<</if>>
<<else>>
	The beast backs away from you. It growls at you again, but doesn't move. <<tearful>> you glance around. You see nothing of value.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Leave|Domus Street]]>><</link>>
	<br>
<</if>>