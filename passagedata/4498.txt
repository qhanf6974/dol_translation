<<set $outside to 0>><<set $location to "underground">><<effects>>
You are dragged through a door and down a flight of steps. You hear the sound of rushing water. At the bottom the <<person>> turns on a torch, revealing an old stone corridor leading away into darkness.
<<pass 15>>
<br><br>
After walking awhile you come to a metal door. The <<person2>><<person>> knocks, and is answered by a thin opening sliding apart near the top. It closes promptly, and the door opens.
<br><br>
<<endcombat>>
You are forced through the door and to your knees before a <<generate1>><<person1>><<personcomma>> who crouches and grabs your neck. <<He>> turns your head to examine one side of your face, then the other, then forces your mouth open and looks inside. "<<pShe>> will do," <<he>> says, standing up.
<br><br>
<<endevent>>
<<if $worn.neck.collared isnot 1>>
	<<neckwear 1>>
	Your neck is freed only briefly. Someone closes something hard and cold around it before you are yanked to your feet and pulled away.
<<else>>
	Someone else yanks you to your feet and pulls you away.
<</if>>
<<generate1>>A <<person1>><<person>> holds the other end of the leash attached to your collar, and leads you towards a door at the far end of the room.
<br><br>
<<stealclothes>>
<<link [[Next|Underground Intro2]]>><</link>>
<br>