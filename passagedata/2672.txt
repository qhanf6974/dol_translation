<<effects>>

You struggle to keep yourself covered. Whitney doesn't push you too far, but seems disappointed. "Fine," <<he>> says, reclining with <<his>> arms behind <<his>> head.
<br><br>

<<endevent>><<npc River>><<person1>>
River almost swoons when <<he>> sees Whitney without a shirt, and needs to steady <<himself>> on <<his>> desk. "O-out," <<he>> manages. "And put your shirt on."
<br><br>
<<endevent>>
<<npc Whitney>><<person1>>
Whitney rolls <<his>> eyes, but rises to <<his>> feet. <<He>> doesn't wear the shirt, instead slinging it over <<his>> shoulder as <<he>> swaggers from the room.
<br><br>

<<set $whitneymaths to "sent">>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
<br>