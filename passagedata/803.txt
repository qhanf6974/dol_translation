<<effects>>

<<if $submissive gte 1150>>
	"Y-you think I'm cute?" you say.
<<elseif $submissive lte 850>>
	"Even <<girls>> as hot as me need to work," you say.
<<else>>
	"You don't look so bad yourself," you say.
<</if>>
<<promiscuity1>>

The <<person>> glances over <<his>> shoulder. "Why don't you take a break?" <<he>> says. "I'll keep you company."
<br><br>

<<link [[Refuse|Farm Tending Ignore]]>><<set $phase to 1>><</link>>
<br>
<<if $promiscuity gte 15>>
	<<link [[Accept|Farm Tending Accept]]>><</link>><<promiscuous2>>
	<br>
<</if>>

<<if $promiscuity gte 35>>
	<<link [[Demand £20 first|Farm Tending Demand]]>><<set $phase to 1>><<def 1>><</link>><<promiscuous3>>
	<br>
	<<link [[Demand £50 first|Farm Tending Demand]]>><<set $phase to 2>><<def 1>><</link>><<promiscuous3>>
	<br>
	<<link [[Demand £100 first|Farm Tending Demand]]>><<set $phase to 3>><<def 1>><</link>><<promiscuous3>>
	<br>
<<elseif $uncomfortable.prostituting is false>>
	<<link [[Demand £20 first|Farm Tending Demand]]>><<set $phase to 1>><<def 1>><<set $desperateaction to 1>><</link>><<promiscuous3>>
	<br>
	<<link [[Demand £50 first|Farm Tending Demand]]>><<set $phase to 2>><<def 1>><<set $desperateaction to 1>><</link>><<promiscuous3>>
	<br>
	<<link [[Demand £100 first|Farm Tending Demand]]>><<set $phase to 3>><<def 1>><<set $desperateaction to 1>><</link>><<promiscuous3>>
	<br>
<</if>>