<<effects>>

<<if $worn.lower.skirt is 1>>
	You lift your skirt to your waistline, and try to hoist up your $worn.under_lower.name, hoping to give the impression that you're wearing nothing beneath.
<<else>>
	You pull your $worn.lower.name and $worn.under_lower.name down a little, hoping to give the impression that you're wearing nothing beneath.
<</if>>
<br><br>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>

	<span class="green">"Nice,"</span> Whitney says, fooled. River turns and glares at <<himstop>>
	<br><br>

		<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
		<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
		<</if>>

	<<link [[Next|Maths Lesson]]>><</link>>
	<br>
	<<unset $whitneypantiesmaths>>
<<else>>

<span class="red">Whitney glares at you.</span> <<He>> saw through the ruse. <<He>> doesn't say anything, but this won't be the end of it.
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

	<<link [[Next|Maths Lesson]]>><</link>>
	<br>

	<<set $whitneypantiesmaths to "seen">>
<</if>>