<<set $outside to 1>><<set $location to "forest">><<effects>>
You come to a decaying stump at the base of the cliff. There's a collection of mushrooms growing at its base, some red some pink.
<<if $science gte 400>>
	You recognise the red ones as wolfshrooms, thought to grant strength and prized by warriors in ancient times. You recognise the pink ones too. They're an aphrodisiac, and poisonous to many animals. Including wolves.
<<elseif $science gte 200>>
	You recognise the red ones as wolfshrooms, thought to grant strength and prized by warriors in ancient times. You don't recognise the pink ones.
<<else>>
	You don't recognise either of them.
<</if>>
<br><br>
<<link [[Take red mushroom|Wolf Cliff2]]>><<set $wolfpackshroom to "red">><</link>>
<br>
<<link [[Take pink mushroom|Wolf Cliff2]]>><<set $wolfpackshroom to "pink">><<arousal 600>><</link>><<garousal>>
<br>
<<link [[Leave them alone|Wolf Cliff2]]>><</link>>
<br>