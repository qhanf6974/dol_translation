<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

The pair leave you to slump against a wall. You think you're in an alley, but you can't be sure.
<br><br>

<<tearful>> try to get your bearings.
<br><br>

<<clotheson>>
<<endcombat>>

<<set $eventskip to 1>>
<<destinationbondage>>

<<elseif $enemyhealth lte 0>>

You shove the pair away from you and stumble in a random direction.
<br><br>

<<tearful>> you try to get your bearings.
<br><br>

<<clotheson>>
<<endcombat>>

<<set $eventskip to 1>>
<<destinationbondage>>

<<else>>
	<<set $rescued += 1>>

		<<endcombat>>
		<<generate1>><<person1>>You hear a police whistle. The pair back away from you, leaving you to slump against a wall. Another whistle, and you hear rapid footsteps as the pair flee.
		<br><br>

		<<clotheson>>

		<<tearful>> you try to get your bearings. You hear more footsteps approaching.
		<br><br>

		<<if $exposed gte 1>>
			"You're under arrest for public indecency," says an angry voice. "And for inciting indecency in others!" <<He>> grasps your arm and tries to drag you away.
			<<crime>><<crime 10>>
			<br><br>
			<<if $worn.face.type.includes("gag")>>
				Gagged as you are, you can't voice your protests.
				<br><bR>
			<</if>>

			<<link [[Go willingly|Bondage Police Willing]]>><</link>>
			<br>
			<<if !$worn.face.type.includes("gag")>>
				<<link [[Try to explain|Bondage Police Explain]]>><</link>> | <span class="blue">English</span>
				<br>
			<</if>>
			<<link [[Fight to escape|Bondage Police Fight]]>><<set $fightstart to 1>><<crimeup 50>><</link>><<crime>>
			<br>

		<<else>>

			"It's okay <<if $player.gender_appearance is "f">>miss<<else>>sir<</if>>," An authoritative voice says. "I'm a police officer. Do you require assistance?"
			<br><br>
			<<if $worn.face.type.includes("gag")>>
				<<He>> must notice your gag, as <<he>> assumes you do need help.
			<<else>>
				You explain the situation.
			<</if>>
			<<blindfoldremove>>

			"Who did this to you?" <<he>> asks. "Tell me as much as you can about when and where it happened. I'll file a report. Though with how busy we've been lately I can't guarantee it will be seen to in a timely manner."
			<br><br>

			<<link [[Give report (0:20)|Bondage Police Report]]>><<pass 20>><<stress 6>><<trauma -6>><</link>><<gstress>><<ltrauma>>
			<br>
			<<link [[Don't give report|Bondage Police No Report]]>><</link>>
			<br>
		<</if>>
<</if>>