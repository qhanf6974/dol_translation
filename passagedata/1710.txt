<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<if $submissive gte 1150>>
You look at your feet. "I liked the old cafe more," you admit. "Sorry."
<<elseif $submissive lte 850>>
You scoff. "It's a big much for some cream buns," you say "Don't you think?"
<<else>>
"It's a little gaudy," you admit.
<</if>>
<br><br>

Sam frowns, but it only lasts a moment. "You're underestimating your talent," <<he>> says. "I've always wanted a place like this."
<br><br>

<<He>> looks around the room. "I've begun preparations for the grand opening," <<he>> says. "I'd like you to make an appearance. Nothing big! Just a little speech. People from all over the world are coming, so the town's movers and shakers will want to make an appearance."
<br><br>

"There's no need to be shy," <<he>> adds. "You can say whatever you like, really. People just like to know who's making the food."
<br><br>

<<link [[Next|Ocean Breeze Rework 2]]>><</link>>
<br>