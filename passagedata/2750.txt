<<set $outside to 0>><<set $location to "pool">><<schooleffects>><<water>><<effects>>

<<if $trauma gte (($traumamax / 10) * 7)>>
	You keep to yourself, afraid that one wrong move will result in the whole class molesting you.
<<elseif $trauma gte (($traumamax / 10) * 2)>>
	You focus on your own thoughts, trying to keep the creeping anxiety at bay.
<<else>>
	<<if $exposed gte 1>>
		Vulnerable as you are you don't really pay attention to the lesson, instead idly paddling and keeping your <<lewdness>> covered.
	<<elseif $exposed lte 1>>
		You don't really pay attention to the lesson, instead idly paddling and thinking about what you'll do after school.
	<</if>>
<</if>>
<br><br>

<<pass 5>><<status 1>><<trauma -1>><<stress -1>>
<<schooleffects>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress -1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<trauma -1>><<stress -1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress -1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
<<else>>
	<<set $phase to 1>>
<</if>>

<<if $phase is 1>>
	<<set $phase to 0>>
	<<if $exposed gte 1>>
		The bell rings, signifying the end of the lesson. You wait as the other students leave. Some students linger,
		doubtlessly hoping to get a nice view when you climb out, but Mason herds them away, giving you some privacy.
		<br><br>
	<<elseif $exposed lte 0>>
		The bell rings, signifying the end of the lesson. You climb out of the pool.
		<br><br>
	<</if>>
	<<if $time is ($hour * 60 + 57)>>
		<<pass 3>>
	<<elseif $time is ($hour * 60 + 58)>>
		<<pass 2>>
	<<elseif $time is ($hour * 60 + 59)>>
		<<pass 1>>
	<</if>>
	<<if $exposed lte 0>>
		<<link [[Boy's Changing Room|School Boy Changing Room]]>><</link>>
		<br>
		<<link [[Girl's Changing Room|School Girl Changing Room]]>><</link>>
		<br><br>
	<<elseif $exposed gte 1>>
		<<link [[Grab some towels and enter the boy's changing room|School Boy Changing Room]]>><<towelupm>><</link>>
		<br>
		<<link [[Grab some towels and enter the girl's changing room|School Girl Changing Room]]>><<towelup>><</link>>
		<br><br>
	<</if>>
<<else>>
	<<if $exposed gte 1>>
		<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
		<<if $danger gte (8900 - ($allure / 2))>>
			<<eventsswimming>>
		<<else>>
			<<eventsswimmingsafe>>
		<</if>>
	<<elseif $exposed lte 0>>
		<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
		<<if $danger gte (8900 - ($allure / 10))>>
			<<eventsswimming>>
		<<else>>
			<<eventsswimmingsafe>>
		<</if>>
	<</if>>
<</if>>