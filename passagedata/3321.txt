<<set $outside to 1>><<set $location to "town">><<effects>>
You reach for the tentacle, and carefully touch it with your fingers. For a moment it stiffens, but it relaxes as you rub it and starts pulsing against your hand. <<deviancy3>>
<br>
You wrap your fingers around it and start stroking. The tentacle throbs and pulses in rythm with your stokes. After a dozen strokes its pulses grow stronger, and shivers run through it. It ejaculates slime all over your hands.
<<arousal 3>>
<<set $leftarmgoo += 1>><<set $rightarmgoo += 1>>
<br><br>
Worn out, the slimy tentacle retracts as several more emerge from the manhole, wrapping around your limbs and waist and pulling you underground. You prove too heavy even for their combined strength, and plummet several feet through darkness.
<br><br>
Your fall is broken by something slimy and soft. It flees before your eyes adjust to the dark, taking the tentacles with it. You're in the drains, surrounded by stolen clothing and handbags.
<br><br>
<<set $eventskip to 1>>
<<link [[Search for valuables (0:10)|Street Tentacle Search]]>><<pass 10>><</link>>
<br>
<<destinationdrain>>