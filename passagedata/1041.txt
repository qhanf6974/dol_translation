<<effects>>

You run around the field, and work up a sweat. There's plenty of space. It would almost be idyllic, were you not imprisoned and treated as cattle.
<<physique 6>>
<br><br>

<<if $rng gte 51>>
	<<livestock_overhear>>
<</if>>

<<link [[Next|Livestock Field]]>><</link>>
<br>