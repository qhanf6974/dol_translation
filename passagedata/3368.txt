<<effects>>

You don't waste time talking. Bullies only understand one language. You throw yourself at the <<person2>><<personcomma>> shoving <<him>> to the ground before <<he>> realises what's happening. The <<person3>><<person>> falls next as you shunt <<him>> against the wall and wrestle the <<person1>><<persons>> bag from <<himstop>>
<br><br>

The <<person4>><<person>> backs away as <<his>> friends struggle to their feet. You take a step forward, and the trio turn and run.
<br><br>

The <<person1>><<person>> gawks at you, dumbstruck. You offer <<him>> <<his>> bag.

<<if $rng gte 71>>
	"I could have taken them," <<he>> says as <<he>> takes it, now more confident. "Thanks though." <<He>> walks away.
<<elseif $rng gte 41>>
	<<He>> takes it, then turns and runs without a word.
<<else>>
	<<He>> takes it, then breaks free of <<his>> stupor. "Th-thank you," <<he>> says. "You saved me." <<He>> gives you an awkward hug, then turns and walks away.
	<<ltrauma>><<trauma -6>>
<</if>>
<br><br>

<<endevent>>
<<destinationeventend>>