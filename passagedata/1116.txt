<<effects>>

<<if $submissive gte 1150>>
	"Pl-please don't be angry," you say. "I just want us to be friends."
<<elseif $submissive lte 850>>
	"The way I see it," you say. "We're sharing this field together, and should get used to each other."
<<else>>
	"I mean no harm," you say. "I'm just curious."
<</if>>
<br><br>

The centaur laughs. "You're braver than the rest. Fine, but watch yourself."
<br><br>

<<link [[Next|Livestock Field Horse]]>><</link>>
<br>