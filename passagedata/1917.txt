<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>
The <<person>> can't see you from where <<he>> sits, so you slowly start to creep out.
<br><br>
<<if $rng gte 81>>
	As you tiptoe past the back of <<his>> chair, <<he>> reaches back and grabs you. "Thought I couldn't hear you?" <<he>> says, hauling you onto <<his>> lap. "I think you need to be taught a lesson."
	<br><br>
	<<link [[Next|Domus House Sneak Molestation]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	<<He>> remains glued to <<his>> television, letting you slip out with no difficulty.
	<br><br>
	<<link [[Leave|Domus Street]]>><<endevent>><</link>>
	<br>
<</if>>