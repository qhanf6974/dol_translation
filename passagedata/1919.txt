<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>
<<set _risk to 0>>
<<if $phase is 0>>
	<<set _risk to 10>>
	You head straight for the exit.
<<elseif $phase is 1>>
	<<set _risk to 25>>
	Grabbing as much of the equipment as you can conceal, you head for the exit.
<</if>>
<br><br>
<<set $rng to random(1, 100)>>
You open the trapdoor and peek out.
<<if $rng gt _risk>>
	The room remains empty and silent. You climb from the trapdoor, close it and silently escape the property.
	<br><br>
	<<link [[Leave|Domus Street]]>><<unset $resc>><</link>>
<<else>>
	The room looks empty. Climbing out, <span class="red">hands seize you.</span>
	<br><br>
	<<link [[Next|Domus House Dungeon Rape]]>><<set $molestationstart to 1>><</link>>
<</if>>
<br>