<<set $outside to 1>><<set $location to "town">><<effects>>
<<flaunting>> you sneak through the dark orphanage. You hear the occasional snore or movement from one of the rooms, and stop, moving again when the only sound is the thudding of your heart.
<br><br>

You open the back door, and peek out. You take one step, then another, then close the door behind you. You're outside. <<covered>>
<br><br>

<<link [[Next|Garden Fence Naked Night]]>><<pass 5>><</link>>
<br>