<<effects>>

You search every nook and cranny in the yard, but to no avail. You're checking the fence for overlooked holes when you hear a cluck above you. The chicken perches on the roof of the farmhouse.
<br><br>

<<link [[Get a ladder (0:10)|Farm Chickens Ladder]]>><<pass 10>><<farm_count 10>><</link>>
<br>
<<link [[Get Alex|Farm Chickens Alex]]>><<npcincr Alex dom 1>><</link>><<npc Alex>><<person1>><<gdom>><<endevent>>
<br>