<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

The <<person1>><<person>> and <<person2>><<person>> high five each other, then leave you lying in the water.
<br><br>

<<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<<elseif $enemyhealth lte 0>>

The trio recoil in pain and back away from you. <<tearful>> you seize the chance to escape.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<<elseif $alarm is 1 and $rescue is 1>>
<<set $rescued += 1>>

	<<if $delinquency gte 200>>
	Your cry attracts Mason's attention. "I knew it would have something to do with you. Stop harassing the other students." The trio go along with it, struggling to suppress their smiles.
	<<gdelinquency>><<detention 1>>
	<<else>>
	Your cry attracts Mason's attention. The teacher glares at the trio. "You're in serious trouble, get changed and go to the head's office." The group leave the pool, chatting and laughing.
	<</if>>
	<br><br>
<<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<<else>>
You hear Mason's voice. "What's happening over there?" They play innocent. "Nothing!" The <<person3>><<person>> says. Mason looks at them with suspicion, but doesn't say anything else. Your attackers move away from you, not wanting to get in trouble.
<br><br>

<<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<</if>>