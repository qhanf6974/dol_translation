<<set $outside to 0>><<set $location to "dance_studio">><<effects>>
You are in the dance studio. Here you can pay for lessons to improve your dancing skill. They also offer lessons on how to walk in high heels. The instructor is said to be well-connected in the professional scene. Lessons cost £12.
<br><br>
<<if $stress gte 10000>>
	<<passoutshop>>
<<else>>
	<<if $hour is $closinghour>>
		It's closing time. The receptionist is herding everyone outside.<<if $exposed gte 1>>Panic swells within you as you realise how exposed you'll be should you be found in this state of dress.<</if>>
		<br>
		<<storeon "dance_studio" "check">>
			<<if _store_check is 1>>
				<br>
				You remember you left your clothes in the changing room.
				<br>
				<<link [[Dress before leaving|Barb Street]]>><<storeon "dance_studio">><</link>>
				<br>
			<</if>>
	<<elseif $openinghours is 0>>
		You are alone in the darkness.
		<br><br>
	<<elseif $exposed gte 1>>

		<<if $uncomfortable.nude is false>>
		You are hiding in a cupboard, you wouldn't mind others seeing you exposed, but this time you stay put.
		<<else>>
		You are hiding in a cupboard to protect your dignity.
		<</if>>
		<br><br>
	<</if>>
	<<if $openinghours is 1 and $exposed lt 1>>
		<<if $money gte 1200 and $exposed lt 1>>
			<<link [[Take a dance lesson (1:00)|Dancing Lesson]]>><<set $phase to 0>><<set $money -= 1200>><<set $dancestage to 0>><</link>> Costs £12 <<gtiredness>> | <span class="green">+ Dancing Skill</span>
			<br>
		<</if>>
		<<if ($weekday is 3 or $weekday is 5) and $worn.feet.type.includes("heels")>>
			<<if $money gte 1200 and $exposed lt 1>>
				<<link [[Take high heel lesson (0:30)|Heel Lesson]]>><<set $phase to 0>><<set $money -= 1200>><<pass 30>><</link>> Costs £12 <<gtiredness>> | <span class="green">+ Feet Skill</span>
				<br>
			<</if>>
		<<else>>
			<<link [[Inquire about high heel lessons|Heel Lesson Inquiry]]>><</link>>
			<br>
		<</if>>
	<</if>>
	<<if $hour isnot $closinghour>>
		<<link [[Changing Room|Dance Studio Changing Room]]>><</link>>
		<br>
	<</if>>
	<<if $openinghours is 0 and $hour isnot $closinghour and $dancestudiotheft isnot 1>>
		<<link [[Examine the cash register|Dance Studio Register]]>><</link>>
		<br>
	<</if>>
	<br>
	<<storeon "dance_studio" "check">>
	<<if _store_check is 1 and ($worn.upper.name is "naked" or $worn.lower.name is "naked") and $exposed is 1>>
		You left clothes in the changing room. Your <span class="lewd"><<lewdness>></span> is fine in here, but would be considered lewd outside.
		<br><br>
		<<if $exhibitionism gte 15>>
			<<link [[Go outside anyway|Dance Studio Outside Exhibitionism]]>><</link>><<if $ex_studio isnot 1>><<exhibitionist2>><</if>>
			<br>
		<</if>>
	<<else>>
		[[Leave|Barb Street]]
		<br><br>
	<</if>>
<</if>>