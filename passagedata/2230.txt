<<set $location to "landfill">><<set $outside to 1>><<effects>>

You are in the landfill on Elk Street.
	<<if $trash gte 100>>
	You're in the depths of the complex. A colossal compactor sits in a clearing, fed by several conveyor belts.
	<<elseif $trash gte 50>>
	Mountains of rubbish pile all around.
	<<elseif $trash gte 30>>
	Some of the piles of rubbish tower over you.
	<<elseif $trash gte 10>>
	The piles of rubbish get taller the further you move from the street.
	<<else>>
	You're at the front gate. You can see Elk Street on the other side. A sign reads: "These premises and all objects within are private property."
	<</if>>

<br><br>
<<set $danger to random(1, 10000)>>
<<if $stress gte 10000>>
<<passouttrash>>
<<elseif $arousal gte 10000>>
The warmth growing within you passes the point of no return. Your knees buckle. <<orgasmpassage>>

	<<if $rng gte 51 and $parasitedisable is "f" and ($parasite.left_ear.name is undefined or $parasite.right_ear.name is undefined) and $slimedisable is "f" and $hallucinations gte 1>>
		Attracted by your orgasm, something stirs within the detritus. It oozes out from deep within one of the mountains, and squirms across the concrete. You see it just before it reaches you, a living slime. It leaps on your back, and squirms up your neck. You try to reach for it, but it's too fast.
		<br><br>

		<<if $parasite.left_ear.name is undefined>>
			<span class="lewd">It squirms into your left ear,</span> leaving no trace.
			<<parasite left_ear slime>>
		<<elseif $parasite.right_ear.name is undefined>>
			<span class="lewd">It squirms into your right ear,</span> leaving no trace.
			<<parasite right_ear slime>>
		<</if>>
		<br><br>
		<<tearful>> you rise to your feet. You feel unnaturally aroused, despite your orgasm. It's a small feeling at first. Then it grows, until <span class="lewd">an overwhelming desire for lewdity overtakes you.</span>
		<br><br>
		<i>A corruption bar has appeared in the Characteristics tab.</i>
		<br><br>

		<<link [[Next|Trash Slime]]>><</link>>
		<br>
	<<else>>
	To have cum in such a place!<<llcontrol>><<gtrauma>><<trauma 6>><<control -25>>
	<br><br>

	<<link [[Next|Trash]]>><<set $eventskip to 1>><</link>>
	<br>
	<</if>>
<<elseif $danger gte (9900 - $allure) and $eventskip is 0>>
<<eventstrash>>
<<else>>

	<<link [[Search for valuables (1:00)|Trash Search]]>><<pass 60>><</link>><<crime>>
	<br>

	<<if $trash lte 0>>
		<<if $trash_unlocked is 1>>
			<<link [[Elk Street (0:10)|Elk Street]]>><<pass 10>><</link>>
			<br>
		<<else>>
			<<if $trash_key is 1>>
			<<link [[Use the key (0:10)|Trash Leave]]>><<pass 10>><<set $trash_unlocked to 1>><</link>>
			<br>
			<<else>>
			<<link [[Try the gate (0:05)|Trash Gate Leave]]>><<pass 5>><</link>>
			<br>
			<</if>>
		<</if>>
	<</if>>
	<<if $trash gte 1>>
	<<link [[Go towards the sound of traffic (0:10)|Trash]]>><<pass 10>><<set $trash -= 10>><</link>>
	<br>
	<</if>>
	<<if $trash lte 99>>
	<<link [[Go towards the sound of heavy machinery (0:10)|Trash]]>><<pass 10>><<set $trash += 10>><</link>>
	<br>
	<</if>>

<</if>>
<<set $eventskip to 0>>