<<set $outside to 1>><<set $location to "sea">><<effects>>
<<if $molestationstart is 1>>
	<<set $molestationstart to 0>>
	<<molested>>
	<<controlloss>>
	<<set $combat to 1>>

	<<swarminit "eels" "swarms" "moving towards you" "encircle you" "fend off" 1 9>>

	<<set $swimdistance to 15>>
	<<set $water to 1>>
<</if>>
<<if $swimdistance gte 20>>
	You need to start swimming toward the buoy!
<<elseif $swimdistance gte 10>>
	The buoy is still a long way off.
<<elseif $swimdistance gte 5>>
	You've passed halfway to the buoy.
<<elseif $swimdistance gte 1>>
	You're almost at the buoy.
<<else>>
	The buoy is within arm's reach!
<</if>>
<br><br>
<<swarmeffects>>
<<swarm>>
<<swarmactions>>
<<if $stress gte 10000>>
	<span id="next"><<link [[Next|Sea Eels Finish]]>><</link>></span><<nexttext>>
<<elseif $swimdistance lte 0>>
	<span id="next"><<link [[Next|Sea Eels Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link "Next">><<script>>state.display(state.active.title, null)<</script>><</link>></span><<nexttext>>
<</if>>