<<if $tryOn.autoReset isnot false>><<tryOnReset>><</if>><<unset $tempDisable>>
<<set $outside to 0>><<set $location to "shopping_centre">><<effects>>

You are in the clothing shop. It has a large selection of ordinary clothes, but you'll need to go elsewhere if you're looking for something exotic.
<br><br>

<<if $stress gte 10000>>
<<passoutshop>>
<<else>>
	<<if $hour is 21>>
		It's closing time. Security is herding everyone outside.<<if $exposed gte 1>>Panic swells within you as you realise how exposed you'll be should security find you in this state of dress.<</if>>
		<br><br>

		<<if $exposed lte 0>>
			<<link [[Go Outside|High Street]]>><</link>>
			<br>
		<<else>>
			<<link [[Hide until it's over (1:00)|Clothing Shop]]>><<pass 1 hour>><</link>>
		<</if>>
	<<else>>
		<<tryOnStats>>

		<<if $daystate is "night">>
			You are alone in the darkness.
			<br><br>
		<<elseif $exposed gte 1>>
			You are hiding within a clothing stand to protect your dignity.
			<br><br>
		<</if>>

		<<if $debug is 1>>
			[[View over outfits|Over Outfit Shop]]
			<br><br>
		<</if>>
		[[View outfits|Outfit Shop]]
		<br>
		[[View tops|Top Shop]]
		<br>
		[[View bottoms|Bottom Shop]]
		<br><br>
		[[View under outfits|Under Outfit Shop]]
		<br>
		[[View under tops|Under Top Shop]]
		<br>
		[[View under bottoms|Under Bottom Shop]]
		<br><br>
		[[View Head Accessories|Head Shop]]
		<br>
		[[View Face Accessories|Face Shop]]
		<br>
		[[View Neck Accessories|Neck Shop]]
		<br>
		[[View Legwear|Legs Shop]]
		<br>
		[[View Shoes|Shoe Shop]]
		<br><br>
		<<if $clothingrebuy is 1>>
			[[Disable automatic clothing Rebuy|Rebuy Shop]]
			<br>
		<<else>>
			[[Enable automatic clothing rebuy|Rebuy Shop]]
			<br>
		<</if>>

		<<if $daystate is "night" and $hour isnot $closinghour and $clothingshoptheft isnot 1>>
			<<link [[Examine the cash register|Clothing Shop Register]]>><</link>>
			<br>
		<</if>>

		<br>

		<<if $daystate is "night" or $tryOn.value is 0>>
			<<if $daystate is "night" and $clothingShop.spotted is true and $clothingShop.stolenClothes gt 0>>
				<<link[[Leave|Clothing Shop Night Guard Found]]>><<ShowUnderEquip "normal">><<ShowUnderEquip "over">><<set $tryOn.autoReset to true>><</link>>
			<<else>>
				<<link[[Leave|Shopping Centre Top]]>><<ShowUnderEquip "normal">><<ShowUnderEquip "over">><<set $tryOn.autoReset to true>><</link>>
			<</if>>
			<br><br>
		<<elseif $tryOn.value gt 0>>
			<<link[[Return clothes|$passage]]>><<clothingReset>><</link>>
			<br>
			<<if $tryOn.value lt $money>>
				<<link[[Buy clothes|$passage]]>><<buyTryOnClothes "wear">><</link>>
				<br>
				<<link[[Buy clothes and send to wardrobe|$passage]]>><<buyTryOnClothes "wardrobe">><</link>>
				<br>
			<</if>>
			<<link[[Leave with stolen clothes|Stolen Clothing Shop Result]]>><</link>>
			<br>
		<</if>>
	<</if>>
<</if>>