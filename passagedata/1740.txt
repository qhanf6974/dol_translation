<<set $outside to 0>><<set $location to "compound">><<effects>>
<<if $compoundmock is 1>>
	"Sure, I'll tell you everything," you say. "I'm here to spank naughty lab assistants. Unbind me so I can get to work."
	<br><br>
	"You won't be laughing for long," <<he>> says, spinning a dial on the machine. Pain sears through you, but leaves your mind feeling clearer.
	<br><br>
<<elseif $compoundmock is 2>>
	"You think you can hurt me with your little toy?" you say through gritted teeth. "It barely tickles."
	<br><br>
	"Oh, then you won't mind this." <<He>> spins the dial again.
	<br><br>
<<elseif $compoundmock is 3>>
	"I bet you tie yourself to this thing for fun," you say. "It's okay. We all have our fetishes."
	<br><br>
	"I'm starting to think I've found yours." <<He>> spins the dial once more.
	<br><br>
<<elseif $compoundmock is 4>>
	"I could get used to this," you say. "If the science thing doesn't work out, you could have a shining career as a kinky whore."
	<br><br>
	"Is this thing even working?" <<he>> muses as <<he>> spins the dial again.
	<br><br>
<<else>>
	"You like hurting <<girls>> then?" you say. "Better keep your porno stash hidden, if the police fi-"
	<br><br>
	"Shut up," <<he>> interrupts, spinning the dial.
	<br><br>
<</if>>
<<compoundoptions>>