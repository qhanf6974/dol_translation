<<effects>>
<<endevent>><<npc Avery>><<person1>>
<<averyscore>>

Avery drives you home. Once back at the orphanage <<he>> opens <<his>> heavy <<wallet>> and thumbs through the cash. <<He>> holds out <span class="gold">£<<print ($NPCName[$NPCNameList.indexOf("Avery")].love * 10 + $endear * 10 + 10)>></span>. "You should buy something nice. My treat."
<br><br>

<<if $rng gte 51>>

You reach for the money, but <<he>> lifts it away from you. "First," <<he>> says. "I want to see more of you. Take off your clothes."
<br><br>

<<link [[Strip|Avery Date Strip]]>><</link>>
<br>
<<link [[Refuse|Avery Date Refuse]]>><</link>>
<br>

<<else>>

You reach for the money, but <<he>> lifts it away from you. "First," <<he>> says. <<He>> strokes your cheek, then leans forward for a kiss.
<br><br>

<<link [[Kiss|Avery Date Sex]]>><<set $sexstart to 1>><</link>><<promiscuous1>>
<br>
<<link [[Refuse|Avery Date Refuse]]>><<npcincr Avery rage 5>><<npcincr Avery love -1>><</link>><<garage>><<llove>>
<br>

<</if>>