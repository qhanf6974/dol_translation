<<set $outside to 1>><<set $location to "town">><<effects>>
You reach towards its head. It doesn't move as you touch it, but you hear a low rumble from its throat. It licks your hand, turns, and bounds into the night.
<br><br>
<<link [[Next|Residential alleyways]]>><</link>>
<br>