<<set $outside to 0>><<set $location to "town">><<effects>>

You peek outside to make sure there's nobody around, and then help Robin out of the bush. You brush stray leaves from <<him>> before planting a kiss on <<his>> forehead. <<He>> clings to you for the entire walk home.
<br><br>

<<link [[Next|Robin Options]]>><</link>>
<br>