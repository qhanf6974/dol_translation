<<widget "cream_init">><<nobr>>
<<if $worn.over_upper.type.includes("naked") and $worn.upper.exposed gte 1>>
	<<overupperwear 2>>
<</if>>
<<if $worn.over_lower.type.includes("naked") and $worn.lower.exposed gte 1>>
	<<overlowerwear 2>>
<</if>>
<<set $walk_timer to 100>>
<</nobr>><</widget>>

<<widget "cream_end">><<nobr>>
<<if $worn.over_upper.name is "cream">>
	<<overupperruined>>
<</if>>
<<if $worn.over_lower.name is "cream">>
	<<overlowerruined>>
<</if>>
<<unset $walk_timer>>
<<unset $cream_warning>>
<</nobr>><</widget>>

<<widget "cream_description">><<nobr>>
<<if ($worn.over_upper.integrity gte 80 and $worn.over_upper.name is "cream") or ($worn.over_lower.integrity gte 80 and $worn.over_lower.name is "cream")>>
	<span class="lblue">Your body is covered with cream.</span>
<<elseif ($worn.over_upper.integrity gte 60 and $worn.over_upper.name is "cream") or ($worn.over_lower.integrity gte 60 and $worn.over_lower.name is "cream")>>
	<span class="blue">Gaps have appeared in the cream, showing the skin beneath.</span>
<<elseif ($worn.over_upper.integrity gte 40 and $worn.over_upper.name is "cream") or ($worn.over_lower.integrity gte 40 and $worn.over_lower.name is "cream")>>
	<span class="purple">More and more cream drips from your body, revealing more of your skin.</span>
<<elseif ($worn.over_upper.integrity gte 10 and $worn.over_upper.name is "cream") or ($worn.over_lower.integrity gte 10 and $worn.over_lower.name is "cream")>>
	<span class="pink">There's little cream left. You worry people can tell how exposed you are beneath.</span>
<<else>>
	<span class="red">The cream barely covers your essentials!</span>
<</if>>
<<if $walk_timer gte 80>>
	<span class="pink">The park is still a long way off.</span>
<<elseif $walk_timer gte 60>>
	<span class="purple">You're making progress, but still a long way from the park.</span>
<<elseif $walk_timer gte 40>>
	<span class="blue">You're about halfway to the park.</span>
<<elseif $walk_timer gte 20>>
	<span class="lblue">You're close to the park, but you've still a ways to go.</span>
<<elseif $walk_timer gte 1>>
	<span class="teal">You're almost at the park.</span>
<<else>>
	<span class="green">You've made it to the park!</span>
<</if>>

<</nobr>><</widget>>

<<widget "cream_walk">><<nobr>>

<<link [[Walk quickly|Cream Ex Naked Walk Quick]]>><<endevent>><<set $walk_timer -= 20>><<cream_damage 15>><</link>> | <span class="pink"> + Harassment</span>
<br>
<<link [[Walk carefully|Cream Ex Naked Walk Careful]]>><<endevent>><<set $walk_timer -= 12>><<cream_damage 15>><</link>>
<br>
<</nobr>><</widget>>

<<widget "cream_damage">><<nobr>>
<<if $args[0]>>
	<<if $worn.over_upper.name is "cream">>
		<<set $worn.over_upper.integrity -= $args[0]>>
	<</if>>
	<<if $worn.over_lower.name is "cream">>
		<<set $worn.over_lower.integrity -= $args[0]>>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "cream_events">><<nobr>>
<<set $rng to random(1, 8)>>
<<if $rng is 8>>
	<<generatey1>><<person1>>You spot a <<person>> you recognise from school. <<He>> grins as recognition dawns on <<his>> face. <<He>> darts into a nearby shop.
	<br><br>

	Your attention is taken by the other onlookers, but a moment later water sprays over your shoulder. You turn, and see the <<person>> wielding a water pistol. <<He>> takes aim, the grin still on <<his>> face.
	<br><br>

	<<link [[Jump|Cream Ex Naked Walk Jump]]>><</link>><<athleticsdifficulty 1 1000>>
	<br>
	<<link [[Dodge|Cream Ex Naked Walk Dodge]]>><</link>><<dancedifficulty 1 1000>>
	<br>
<<elseif $rng is 7>>
	You hear a rapid patter of footsteps. A dog runs through the crowd, locked onto the cream covering your body. It's owner runs behind it, straining to pull it back.
	<br><br>

	<<if $bestialitydisable is "f">>

		<<link [[Soothe|Cream Ex Naked Walk Soothe]]>><</link>><<tendingdifficulty 1 1000>>
		<br>
		<<link [[Run|Cream Ex Naked Walk Run]]>><</link>>
		<br>
	<<else>>
		You're forced into a run to avoid getting licked. The owner manages to get the dog under control, but the activity loosened the cream.
		<<cream_damage 15>>
		<br><br>

		<<cream_walk>>
	<</if>>
<<elseif $rng is 6>>
	<<generate1>><<generate2>><<generate3>>
	You pass by a popular shop. A queue of people trail down the road, and each turns to observe you. A <<person1>><<person>> and <<his>> friends seem particularly keen. Your face becomes even more flushed.
	<<ggarousal>><<arousal 1000>>
	<br><br>
	<<endevent>>
	<<cream_walk>>
<<elseif $rng is 5>>
	<<generate1>><<person1>>
	A <<person>> reaches out from a crowd as you pass it by.
	<br><br>

	<<link [[Smack the hand away|Cream Ex Naked Walk Smack]]>><<trauma -6>><<def 1>><<famescrap 10>><</link>><<ltrauma>>
	<br>
	<<link [[Endure|Cream Ex Naked Walk Endure]]>><<arousal 2000>><<trauma 6>><<stress 6>><<sub 1>><</link>><<gtrauma>><<gstress>><<gggarousal>>
	<br>
<<elseif $rng is 4>>
	<<generatey1>><<generatey2>><<generatey3>>
	A gaggle of students emerge from a nearby clothes shop. They stop when they see you, looking bewildered.
	<<garousal>><<arousal 500>><<gstress>><<stress 6>>
	<br><br>
	<<endevent>>

	<<cream_walk>>
<<elseif $rng is 3>>
	<<generate1>><<person1>>
	A <<person>> runs in front of you, a camera in hand.
	<br><br>

	<<if $worn.under_lower.type.includes("naked")>>
		<<link [[Pose|Cream Ex Naked Walk Pose]]>><<arousal 500>><</link>><<exhibitionist5>>
	<<else>>
		<<link [[Pose|Cream Ex Naked Walk Pose]]>><<arousal 500>><</link>><<exhibitionist3>>
	<</if>>
	<br>
	<<if !$worn.face.type.includes("mask") and ($leftarm isnot "bound" or $rightarm isnot "bound")>>
		<<link [[Cover your face|Cream Ex Naked Walk Cover]]>><<arousal 500>><</link>><<garousal>>
		<br>
	<</if>>
	<<link [[Lose them in the crowd|Cream Ex Naked Walk Lose]]>><</link>>
	<br>
<<elseif $rng is 2>>
	<<generate1>><<person1>>
	A <<person>> is so startled by the sight of you that <<he>> trips.
	<br><br>

	<<link [[Help them up|Cream Ex Naked Walk Help]]>><<trauma -6>><</link>><<ltrauma>>
	<br>
	<<cream_walk>>
<<else>>
	<<generate1>><<generate2>><<generate3>><<person1>>
	A <<person>> lunges from the crowd, overwhelmed by lust. <<His>> fingers grasp the air in front of you, as a <<person2>><<person>> and <<person3>><<person>> hold <<person1>><<him>> back.
	<br><br>
	That was close.
	<<garousal>><<arousal 500>><<stress 6>><<gstress>>
	<br><br>
	<<endevent>>
	<<cream_walk>>
<</if>>
<</nobr>><</widget>>

<<widget "cream_audience">><<nobr>>
<<set $rng to random(1, 5)>>
You hear a snippet from the crowd.
<<if ($worn.over_upper.integrity gte 60 and $worn.over_upper.name is "cream") or ($worn.over_lower.integrity gte 60 and $worn.over_lower.name is "cream")>>
	<<switch $rng>>
	<<case 5>>
		"I want to lick <<phim>> all over."
	<<case 4>>
		"Is <<pshe>> advertising a shop?"
	<<case 3>>
		"Cute."
	<<case 2>>
		"I didn't expect a show like this."
	<<default>>
		"Wish I had a spoon."
	<</switch>>
<<elseif ($worn.over_upper.integrity gte 10 and $worn.over_upper.name is "cream") or ($worn.over_lower.integrity gte 10 and $worn.over_lower.name is "cream")>>
	<<switch $rng>>
	<<case 5>>
		"Come quick, you're missing the show!"
	<<case 4>>
		"<<pShe>> looks tasty."
	<<case 3>>
		"I wish I had fruit to dip."
	<<case 2>>
		"<<pShe>> can't have much on under that."
	<<default>>
		"Is <<pshe>> advertising something?"
	<</switch>>
<<else>>
	<<switch $rng>>
	<<case 5>>
		"What a lewd <<girl>>!"
	<<case 4>>
		"Is <<pshe>> wearing anything at all?"
	<<case 3>>
		"What a time to leave my camera at home."
	<<case 2>>
		"Someone call the police."
	<<default>>
		"These perverts are getting bold."
	<</switch>>
<</if>>
<<if $worn.over_lower.integrity lte 0 and $worn.over_lower.name is "cream" or $worn.over_upper.integrity lte 0 and $worn.over_upper.name is "cream">>
	<<if $worn.over_lower.name is "cream">>
		<<overlowerruined>>
	<</if>>
	<<if $worn.over_upper.name is "cream">>
		<<overupperruined>>
	<</if>>
<<elseif $worn.over_lower.integrity lte 10 and $worn.over_lower.name is "cream" or $worn.over_upper.integrity lte 10 and $worn.over_upper.name is "cream">>
	<<if $cream_warning is undefined>>
		<<set $cream_warning to 1>>
		You look down at the cream covering your body. What's left of it. <span class="pink">There's barely any left.</span> If you don't make it soon, you'll be left exposed in the middle of the street!
		<br><br>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "cream_arousal">><<nobr>>
The thrill of walking so exposed, attracting so much attention, with only a thin veneer of cream to cover your modesty is too much. <<orgasmpassage>>

You drop to your knees As the crowd gather closer.
<br>
"Did <<pshe>> just cum?"
<br>
"I can't believe how lewd <<pshe>> is."
<br>
"I think someone should call an ambulance."
<br><br>

You struggle to your feet and continue on before the crowd can encircle you completely.
<br><br>

<<cream_walk>>
<</nobr>><</widget>>

<<widget "cream_finish">><<nobr>>
The buildings on one side of the street give way to the green of the park. Your legs move quicker with your goal so close. You dash off the street as the last of the cream drips away.
<br><br>

Several people follow you, but you soon lose them in the maze of tall bushes and trees. You crouch beside one, your breathing heavy.
<br><br>

<<cream_end>>

<<link [[Next|Park]]>><</link>>
<br>
<</nobr>><</widget>>

<<widget "cream_fail">><<nobr>>
<<if $exposed gte 2>>
	<<fameexhibitionism 20>>
<<else>>
	<<fameexhibitionism 40>>
<</if>>
<<if $leftarm isnot "bound" or $rightarm isnot "bound">>
	You try to clump the remaining cream together, but there's not enough.
<</if>>
<span class="red">You're left exposed in the middle of the street.</span> <<covered>>
<br><br>

The crowd surrounds you, too thick to pass through. More and more people arrive to investigate the commotion.
<br><br>

"Why is that <<girl>> so exposed? Was <<pshe>> attacked?"
<br>
"I think <<pshes>> just a pervert."
<br>
"I can see everything."
<br>
"I bet <<pshe>> loves the attention."
<br>
"I'm telling you, perverts are getting bolder."
<br><br>

<<if $exposed gte 2>>
	<<if random(1, 2) is 2 and $bestialitydisable is "f">>

	A dog emerges from the crowd, running right at you. It pounces, knocking you to the ground and licking you for what little cream remains. More animals appear, drawn by the smell.
	<br><br>
	You writhe as the dogs lick you all over. You could try to stand, but doing so would put you in a mating posture. The crowd watches, not sure how to respond to the strange sight.
	<br><br>

	<<link [[Try to stand anyway|Cream Ex Dog Stand]]>><</link>>
	<br>
	<<link [[Endure|Cream Ex Dog Endure]]>><<arousal 2000>><<stress 6>><<trauma 6>><</link>><<gggarousal>><<gtrauma>><<gstress>>
	<br>

	<<else>>

	<<generate1>><<generate2>><<generate3>><<generate4>>
	You crouch, surrounded by leering eyes, until a <<person1>><<person>> steps forward. A cruel smile contorts <<his>> face. Others follow <<his>> example.
	<br><br>

	<<link [[Next|Cream Ex Naked Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

	<</if>>
<<else>>
	<<generate1>><<person1>>
	You crouch, surrounded by leering eyes, until at last a <<person>> steps forward with a towel.
	<br><br>
	Still vulnerable, but at least covered, you walk to the edge of the crowd. At first you're not sure if they'll let you leave, but then they step aside, giving you an avenue through. The crowd follows you the rest of the way. Your legs move quicker as you reach the park. Some follow you in, but you soon lose them in the maze of tall bushes and trees.
	<br><br>
	<<cream_end>>
	<<towelup>>
	<<link [[Next|Park]]>><<endevent>><</link>>
	<br>
<</if>>

<</nobr>><</widget>>