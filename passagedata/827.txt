<<effects>>

<<if $tending gte random(1, 700)>>
	You grab a <<farm_text dog>> by the collar, and stroke behind <<farm_his dog>> ear, making soothing noises all the while. <span class="green"><<farm_his dog>> panic abates,</span> though <<he>> continues to whimper.
	<br><br>
	
	You calm the other <<farm_text_many dog>> the same way.<<grespect>><<farm_dogs 1>><<ggtending>><<tending 9>><<set $farm_work.dogs_panic to 0>><<gfarm>><<farm_yield 1>>
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	You grab a <<farm_text dog>> by the collar, and stroke behind <<farm_his dog>> ear, making soothing noises all the while. <span class="red">It bites at your hand,</span> and you recoil.<<gtending>><<tending 2>>
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<</if>>