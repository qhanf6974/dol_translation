<<set $outside to 1>><<set $location to "beach">><<effects>>
You approach the teenagers. Several are dancing near a fire on a makeshift stage while others are drinking and chattering.
<br><br>
<<link [[Socialise (0:10)|Beach Party Chat]]>><<pass 10>><<stress -2>><<status 1>><<set $phase to 0>><</link>><<gcool>><<lstress>>
<br>
<<link [[Dance (0:05)|Beach Party Dance]]>><<danceinit>><<set $dancing to 1>><<set $venuemod to 1>><<stress -4>><<tiredness 4>><</link>><<lstress>><<gtiredness>>
<br><br>
<<link [[Leave|Beach]]>><</link>>