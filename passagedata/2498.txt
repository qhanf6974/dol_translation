<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $cool gte 160>>
You sit down to eat. The other students at the table vie for your attention.
<<stress -2>><<trauma -1>><<lstress>><<ltrauma>>
<<elseif $cool lt 40>>
You sit down to eat. The other students quickly vacate the table, not wanting to be seen with you.
<<gtrauma>><<stress 1>><<trauma 1>>
<<else>>
You sit down to eat. No one pays you much attention.
<</if>>
<br><br>

<<link [[Next|Canteen]]>><<endevent>><</link>>
<br>