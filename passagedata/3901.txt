<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<npcset Kylar state "prison">>
<<if $worn.face.type.includes("gag")>>
	<<set $worn.face.type.push("broken")>>
	<<faceruined>>
<</if>>
They arrest Kylar. They untie you too, after you make a bit of noise. They decide against taking you in as "evidence."
<<clothesontowel>>
<br><br>
<<if $schoolday is 1 and $schoolstate isnot "early" and $schoolstate isnot "late">>
You climb the stairs and emerge into the school hallways. Students are being lined up outside and a register taken. They march Kylar through to reach the waiting car, giving the students one last chance to jeer at <<person1>><<himstop>>
<br><br>
<<else>>
You climb the stairs and emerge into the school hallways. They march Kylar across the playground and into a waiting car.
<br><br>
<</if>>
<<He>> waves at you as they drive away.
<br><br>

<<endcombat>>

<<link [[Next|Oxford Street]]>><<set $eventskip to 1>><</link>>
<br>