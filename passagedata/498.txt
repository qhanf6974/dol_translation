<<set $outside to 1>><<set $location to "forest">><<effects>>

<<if $pubtasksetting is "bear">>

You return to the cave, where Landry's black box can be found. You hear something large breathing within.
<br><br>

<<link [[Enter|Forest Bear Box Molestation]]>><<set $molestationstart to 1>><</link>>
<br>
<<link [[Leave|Forest]]>><</link>>
<br>

<<else>>
<<set $pubtasksetting to "bear">>

You come to a cave, the location Landry said the black box would be found. It must be inside.
<br><br>

You've barely taken two steps further when you hear a deep growl. There's something big in there.
<br><br>

<<link [[Enter|Forest Bear Box Molestation]]>><<set $molestationstart to 1>><</link>>
<br>
<<link [[Leave|Forest]]>><</link>>
<br>

<</if>>