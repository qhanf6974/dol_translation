<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You refuse to press the button. "This is for <<person2>><<his>> benefit,<<person1>> and yours," <<he>> says. "Press the button."
<br><br>
<<if $phase is 0>>
	<<link [[Press|Asylum Button Press]]>><</link>>
	<br>
<<else>>
	<<link [[Press|Asylum Button Press]]>><<awareness -1>><<stress 3>><<arousal 600>><<suspicion -1>><</link>><<lsuspicion>><<lawareness>><<gstress>><<garousal>>
	<br>
<</if>>
<<link [[Refuse|Asylum Button Refuse 2]]>><<def 1>><<suspicion 5>><</link>><<ggsuspicion>>
<br>