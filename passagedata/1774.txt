<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>

You take the most expensive looking bottles, while leaving everything else untouched. You close the heavy door behind you, and easily lock it again.
<br>
With a little luck, they will take a while to notice you were ever there.

<<set $blackmoney += 200>>
<br><br>

<<link [[Leave|Danube Street]]>><</link>>
<br>