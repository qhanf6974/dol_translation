<<if ndef _caught>> <<set _caught to 0>> <</if>>
<<if ndef $resting>> <<set $resting to 0>> <</if>>
<<if ndef $cameras_raised>> <<set $cameras_raised to 0>> <</if>>
<<if ndef $still>> <<set $still to 0>> <</if>>
<<if ndef $coffee_sips>> <<set $coffee_sips to 12>> <</if>>

<<if $coffee_sips gte 11>>
	You have a nearly full cup of coffee in front of you.
<<elseif $coffee_sips gte 8>>
	You have a mostly full cup of coffee in front of you.
<<elseif $coffee_sips gte 5>>
	You have about half a cup of coffee left.
<<elseif $coffee_sips gte 3>>
	You have less than half a cup of coffee left.
<<elseif $coffee_sips gt 1>>
	There's not much of your cup of coffee left.
<<elseif $coffee_sips is 1>>
	Your cup is almost empty. Time to sip up and head out.
<<else>>
	Your coffee cup is empty.
<</if>>
<br><br>

<<if $coffee_sips gt 1>>
	<<if $resting>>
		You take a sip of your coffee, savouring the warm caffeiny goodness.
		<<cafecoffeesip>>
	<<elseif $arousal gte 9990>>
		<<orgasmpassage>>
		Still hazy from your orgasm you look up to see the strangers outside grinning at you, a few of them are giggling and gossiping with each other. You've been caught, given away by your own body no less.
		<<if $cameras_raised gte 1>>
			<span class="pink">
				Some of them even got your orgasm on camera.
			</span>
			<<if $uncomfortable.nude is true>>
				You feel utterly humiliated.
				<<trauma 10>><<gtrauma>>
			<<else>>
				Your tummy flutters in delight at having an audience for your public orgasm. It doesn't hurt that you managed to get off purely from exposing yourself either.
				<<stress -5>><<exhibitionism4>>
			<</if>>
			<<fameexhibitionism 50>>
		<</if>>
		<<set _caught to 1>>
	<<else>>
		<<switch $phase>>
			<<case 0>>
				<<if !$still>>
					You bring the coffee up to your mouth to partially obscure your face, and bring your knees ever so slightly apart. Not very far, just far enough to give the tiniest glimpse at your <<undies>> to anyone who happens to be looking.
					You take a sip of your coffee.
					<<cafecoffeesip>>
				<<else>>
					You leave your legs just barely parted, savouring the tease as you sip your coffee.
					<<cafecoffeesip>>
				<</if>>
				<br><br>
				<<if $skulduggery gte 100 + random(0, 50)>>
					A few passers by let their eyes linger on the window a little longer than normal, clearly unsure if they are seeing what they think they are.
				<<else>>
					A few passers by let their eyes linger on the window a little longer than normal, before smirking as they walk away. You got caught way too easily.
					<<trauma $phase>><<gtrauma>>
					<br><br>
					<<set _caught to 1>>
				<</if>>
			<<case 1>>
				<<if !$still>>
					Taking another sip of your coffee you adjust your legs to bring your knees to a slight part, exposing a tantalising amount of your <<undies>> to onlookers without giving all your secrets away.
					<<cafecoffeesip>>
				<<else>>
					Taking another sip of your coffee you adjust your feet a little, making sure to stay at about the same level of exposure. It's fun to tease.
					<<cafecoffeesip>>
				<</if>>
				<br><br>
				<<if $skulduggery gte 200 + random(0, 50)>>
					Passers by make excuses to stop and look in your direction.
				<<else>>
					Some of the passers by make excuses to stop and look in your direction, before smirking as they walk away. You got caught way too easily.
					<<trauma $phase>><<gtrauma>>
					<br><br>
					<<set _caught to 1>>
				<</if>>
			<<case 2>>
				<<if !$still>>
					Exhilarated by your own brazen exhibitionism you shift your feet so as to part your thighs nice and wide, leaving little to the imagination.
					You take another sip of your coffee.
					<<cafecoffeesip>>
				<<else>>
					Savouring the thrill of exposing yourself like this you shift your feet a little, making sure not to cover your <<undies>> even slightly.
					You take another sip of your coffee.
					<<cafecoffeesip>>
				<</if>>
				<br><br>
				<<if $skulduggery gte 300 + random(0, 50)>>
					Some of the passers by gather in small groups at a time, whispering to each other while attempting to stay discreet about their voyeurism.
				<<else>>
					Some of the passers by gather in a small group, whispering to each other before pointing and laughing. You've been caught.
					<<trauma $phase>><<gtrauma>>
					<br><br>
					<<set _caught to 1>>
				<</if>>
			<<case 3>>
				Feeling daring <<seatedflashcrotchunderskirt>> giving the onlookers a <<print ["wonderful", "delightful", "fantastic"].pluck()>> view of your bare <<undies>> while you take another sip of your coffee.
				<<cafecoffeesip>>
				<br><br>
				<<if $skulduggery gte 400 + random(0, 50)>>
					Some of the strangers gawk at what they're seeing, struggling to hide their voyeurism. A few of them jab the others in the rib to try and keep them from spoiling the fun.
				<<else>>
					Some of the strangers gawk at what they're seeing, struggling to hide their voyeurism. A few of them point at you, whisper something to the others before looking back at you and meeting your gaze with a smirk. You've been caught.
					<<trauma $phase>><<gtrauma>>
					<br><br>
					<<set _caught to 1>>
				<</if>>
			<<case 4>>
				You let your legs slowly come to a natural close, taking a moment to enjoy a giggle to yourself at what you've gotten away with. The strangers who were making excuses to spend time staring at the window begin to disperse, thinking the show's over.
				You take another sip of your coffee.
				<<cafecoffeesip>>
				<br><br>
				<<if $cameras_raised gt 0>>
					<<if $cameras_raised gt 1>>
						The strangers who had their phones out pocket them as they go, scowling.
					<<else>>
						The stranger who had their phone out pockets it as they go, scowling.
					<</if>>
				<</if>>
				<<set $cameras_raised to 0>>
				<<set $resting to 1>>
		<</switch>>

		/* up to 3 strangers take photos
		* each stranger gets 2 shots at taking a photo before they give up and move on
		*/

		<<if $cameras_raised gt 0>>
			<br><br>
			/* for loops seem to hate me so we'll have to do it this way i guess */
			<<set _photos_taken to 0>>
			<<if $cameras_raised gte 1>>
				<span class="pink">
				The <<generate1>><<person1>><<person>> raises <<his>> phone and takes a photo.
				</span>
				<<if random (0,2) gte 1>>
					<<He>> brings <<his>> phone to <<his>> face and grins, seemingly happy with the photo <<he>> got of your exposed <<undiesstop>>
					<<set _photos_taken +=1>>
				<<else>>
					<<He>> brings <<his>> phone to <<his>> face and frowns, seemingly unhappy with the photo <<he>> got of your exposed <<undiesstop>>
					Raising it again <<he>> takes another photo of your exposed <<undiesstop>>
					<<if random (0,2) gte 1>>
						<<He>> brings <<his>> phone to <<his>> face and grins, seemingly happy with the photo <<he>> got of your exposed <<undiesstop>>
						<<set _photos_taken +=1>>
					<<else>>
						<<He>> brings <<his>> phone to <<his>> face again and frowns, before pocketing his phone in frustration.
					<</if>>
				<</if>>
				<br><br>
			<</if>>

			<<if $cameras_raised gte 2>>
				<span class="pink">
				The <<generate2>><<person2>><<person>> raises <<his>> phone and takes a photo.
				</span>
				<<if random (0,2) gte 1>>
					<<He>> brings <<his>> phone to <<his>> face and grins, seemingly happy with the photo <<he>> got of your exposed <<undiesstop>>
					<<set _photos_taken +=1>>
				<<else>>
					<<He>> brings <<his>> phone to <<his>> face and frowns, seemingly unhappy with the photo <<he>> got of your exposed <<undiesstop>>
					Raising it again <<he>> takes another photo of your exposed <<undiesstop>>
					<<if random (0,2) gte 1>>
						<<He>> brings <<his>> phone to <<his>> face and grins, seemingly happy with the photo <<he>> got of your exposed <<undiesstop>>
						<<set _photos_taken +=1>>
					<<else>>
						<<He>> brings <<his>> phone to <<his>> face again and frowns, before pocketing his phone in frustration.
					<</if>>
				<</if>>
				<br><br>
			<</if>>

			<<if $cameras_raised gte 3>>
				<span class="pink">
				The <<generate3>><<person3>><<person>> raises <<his>> phone and takes a photo.
				</span>
				<<if random (0,2) gte 1>>
					<<He>> brings <<his>> phone to <<his>> face and grins, seemingly happy with the photo <<he>> got of your exposed <<undiesstop>>
					<<set _photos_taken +=1>>
				<<else>>
					<<He>> brings <<his>> phone to <<his>> face and frowns, seemingly unhappy with the photo <<he>> got of your exposed <<undiesstop>>
					Raising it again <<he>> takes another photo of your exposed <<undiesstop>>
					<<if random (0,2) gte 1>>
						<<He>> brings <<his>> phone to <<his>> face and grins, seemingly happy with the photo <<he>> got of your exposed <<undiesstop>>
						<<set _photos_taken +=1>>
					<<else>>
						<<He>> brings <<his>> phone to <<his>> face again and frowns, before pocketing his phone in frustration.
					<</if>>
				<</if>>
				<br><br>
			<</if>>
			<<set _new_fame to (_photos_taken * $phase * 2)>>
			<<fameexhibitionism _new_fame>>
			<<set $cameras_raised to 0>>
		<</if>>

		<<if !$resting and random(0, 9) + $phase gte 8>>
			<<set $cameras_raised to random(1, 3)>>
			<span class="pink">
			<<switch $cameras_raised>>
				<<case 1>>One
				<<case 2>>Two
				<<case 3>>Three
				<<case 4>>Four
			<</switch>>
			of the onlookers take their phone<<if $cameras_raised gt 1>>s<</if>> out of their pocket<<if $cameras_raised gt 1>>s<</if>>.
			</span>
		<</if>>

		/* handle how aroused we feel about things */
		<<cafecoffeeflasharousal>>
	<</if>>

	/* action choices */
	<<set $still to 0>>

	<br><br>
	<<if $resting>>
		<<link [[Part your legs again (0:01)|Cafe Coffee Flash]]>>
			<<set $phase to 0>>
			<<set $resting to 0>>
			<<pass 1>>
		<</link>>
		<br>
		<<link [[Keep your legs closed (0:01)|Cafe Coffee Flash]]>> <<pass 1>> <</link>>
		<br>
	<</if>>

	<<if !_caught and !$resting>>
		<<if $phase gt 0>>
			<<link [[Tone it down a little (0:01)|Cafe Coffee Flash]]>>
					<<set $phase -=1>>
				<<pass 1>>
			<</link>>
			<br>
		<</if>>
		<<if $phase isnot 4>>
			<<link [[Keep your legs as they are (0:01)|Cafe Coffee Flash]]>>
				<<set $still to 1>>
				<<pass 1>>
			<</link>>
			<br>
		<</if>>

		<<link [[Keep going (0:01)|Cafe Coffee Flash]]>>
			<<if $phase lt 4>>
				<<set $phase +=1>>
			<<else>>
				<<set $phase to 0>>
			<</if>>
			<<pass 1>>
		<</link>>
		<br>
		<<link [[Close your legs (0:01)|Cafe Coffee Flash]]>>
			<<set $phase to 4>>
			<<pass 1>>
		<</link>>
		<br>
	<</if>>
<</if>>
<<link [[Finish your drink|Cafe Coffee Finish]]>><</link>>