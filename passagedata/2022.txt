<<effects>>

The remaining branches return to their original positions and stay there. You wobble for a moment, but manage to keep yourself from falling. <<tearful>> you continue ascending the tree.
<br><br>

You cling to the branch holding your clothes with your hands and knees and crawl across. The branch bends slightly as you reach your goal, but you manage to retrieve your clothing and crawl back to the trunk. You climb down safely.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Garden]]>><</link>>
<br>