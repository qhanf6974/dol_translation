<<if $enemyhealth lte 0>>

They back off for a moment, shocked by your aggression. <<tearful>> you flee to safety.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<<elseif $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

"Don't forget, this is all you're good for. Come on, let's go before someone thinks <<pshes>> with us." <<tearful>> you rise to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<<elseif $alarm is 1 and $rescue is 1>>

Your screaming seems to have impacted them. "Come on, we might catch something from this thing," the <<person1>><<person>> shoves you away, and the group leave you lying there. <<tearful>> you rise to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

<br><br>
<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>

<<elseif $timer lte 0>>

Adult voices approach, and the <<person1>><<person>> shoves you away before two teachers walk into view. <<tearful>> you pick yourself off the ground.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>

<<else>>

The <<person1>><<personcomma>> <<person2>><<person>> and <<person3>><<person>> back off from you, your clothes in their hands. Completely surrounded, you crouch and try to keep your <<lewdness>> concealed.
<br><br>

"Oh my god, you stripped <<phim>> naked!"
<br><br>

"<<pShes>> so embarrassed, look at <<phim>> squirm!"
<br><br>

	<<if $submissive gte 1150>>
"P-please give me my clothes back. It's cold," you say, kneeling and pressing your face against the ground.
<br><br>

"Now that's just pathetic," the <<person1>><<person>> says. "Why would you debase yourself further?" <<He>> drops your clothes on top of you. "Come on, this wretch is making me ill." <<tearful>> you clutch your clothing and find a quiet space to dress.
<br><br>

<br><br>
<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><</link>>

	<<elseif $submissive lte 850>>
You glare at the students. "Give me back my clothes, this instant," you say with surprising confidence given your predicament.
<br><br>

The <<person1>><<person>> laughs "Oh no! We'd better do what <<pshe>> says!" <<He>> throws your clothing at a ceiling light, where it catches and remains suspended. "There you go!"
<br><br>

The students enjoy watching you jump and reach above you, whilst trying to keep yourself covered. You manage to knock the light askew, and your clothes come tumbling on top of you. <<tearful>> you clutch your clothing and find a quiet space to dress.

<br><br>
<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><</link>>

	<<else>>

"Come on," says the <<person1>><<personstop>> "I think that put <<phim>> in <<phim>> place." They leave you, taking your clothes with them. <<tearful>> you look around. You're alone.
<br><br>
<<set $stealtextskip to 1>>
<<stealclothes>>

<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><</link>>
	<</if>>

<</if>>