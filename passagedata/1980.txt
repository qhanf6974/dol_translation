<<effects>>
<<if $submissive gte 1150>>
	"I'm sorry," you say. "I need it."
<<elseif $submissive lte 850>>
	"Get your own," you say.
<<else>>
	"Sorry," you say. "But I can't afford to give you any."
<</if>>
<br><br>
<<He>> nods and walks away.
<br><br>
<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>