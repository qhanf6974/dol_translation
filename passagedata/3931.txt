<<effects>>

You wrap an arm around Kylar's waist. <<His>> body freezes, then melts against you. <<He>> loses <<his>> sense of coordination, and can only offer a token defence as you turn things around and win the game.
<<promiscuity1>>
<br><br>
"Sorry," you say, and plant a kiss on <<his>> cheek. <<He>> doesn't care that <<he>> lost.
<br><br>

<<kylaroptions>>