<<if $molestationstart is 1>>
	<<if $phase is 3>>
		<<set $molestationstart to 0>>
		<<controlloss>>
		<<violence 1>>
		<<neutral 1>>
		<<molested>>
		<<maninit>><<npcstrip>><<set $timer to 20>>
		<<if $delinquency lt 400>>
			<<set $rescue to 1>>
		<</if>>
	<<elseif $phase is 4>>
		<<set $molestationstart to 0>>
		<<controlloss>>
		<<violence 1>>
		<<neutral 1>>
		<<molested>>
		<<maninit>><<hand_gag 0 left>>
		<<if $delinquency lt 400>>
			<<set $rescue to 1>>
		<</if>>
	<</if>>
<</if>>

<<effects>>

<<if $alarm is 1 and $rescue is 0>>
<<set $alarm to 0>>
No one comes to your aid, likely due to your reputation as a delinquent.
<br><br>
<</if>>

<<if $timer gte 5>>
You keep walking back to the classroom, doing your best to ignore the harassment.
<br><br>
<<elseif $timer gte 2>>
You keep walking back to the classroom, doing your best to ignore the harassment. You're almost there.
<br><br>
<<elseif $timer is 1>>
You see the classroom up ahead.
<br><br>
<</if>>

<<effectsman>><<man>>

<<stateman>>
<br><br>
<<actionsman>>

<<if $enemyhealth lte 0>>
	<span id="next"><<link [[Next|Science Dissection Molestation Finish]]>><</link>></span><<nexttext>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<span id="next"><<link [[Next|Science Dissection Molestation Finish]]>><</link>></span><<nexttext>>
<<elseif $rescue is 1 and $alarm is 1>>
	<span id="next"><<link [[Next|Science Dissection Molestation Finish]]>><</link>></span><<nexttext>>
<<elseif $timer is 1>>
	<span id="next"><<link [[Next|Science Dissection Molestation Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link [[Next|Science Dissection Molestation]]>><</link>></span><<nexttext>>
<</if>>