<<set $outside to 0>><<set $location to "police_station">><<effects>>
You sit on the dingy bed and wait.
<br><br>
After an hour, a <<generate1>><<person1>><<person>> in a police uniform arrives beside the cell. "Come on, let's get you processed," <<he>> says, unlocking the cell door. "Turn around."
<br><br>
<<if $worn.neck.collared isnot 1>>
	<<neckwear 1>>
	You comply, and hear <<him>> remove something from <<his>> belt. <<He>> fastens it around your neck, and you hear it click shut. "This collar has a shock function, just to ensure your continued cooperation. We'll remove it once you've served your sentence."
	<br><br>
<<else>>
	You comply. "Oh," <<he>> says. "You're already collared." <<He>> walks right up behind you and examines the collar more closely. "And the shock function appears to work. Everything is in order then. We'll remove it once you've served your sentence."
	<br><br>
<</if>>
<<set $worn.neck.collaredpolice to 1>>
<<He>> leads you up another flight of stairs and into a small room. Three people in suits sit at a table on the far side. In the middle, a <<generate2>><<person2>><<person>> gestures at the chair in front of <<himstop>> "Take a seat," <<he>> says. "And don't tarry." The <<person1>><<person>> follows you in, and closes the door behind you.
<br><br>
Once seated, the <<person2>><<person>> continues. "You didn't think you'd get away with it did you? You've been leaving traces of yourself all over. These are only petty crimes however, so public humiliation is an appropriate punishment. Take <<phim>> to the pillory."
<br><br>
<<link [[Next|Police Pillory Start]]>><</link>>
<br>