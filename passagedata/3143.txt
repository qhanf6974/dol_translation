"Certainly <<if $player.gender_appearance is "m">>sir<<else>>madam<</if>>. We'll have the crate delivered. Fill it as soon as you're ready to sell them. Please bear in mind that we are not allowed to return any items. We will also only take items we can repair and resell."
<br><br>

<<set _value to 0>>
<<for _items range $wardrobe>>
	<<for _i to 0; _i lt _items.length; _i++>>
		<<if _items[_i].shop.length is 0>>
			<<continue>>
		<</if>>
		<<if _items[_i].outfitSecondary isnot undefined>>
			<<continue>>
		<</if>>
		<<set _value += Math.floor(_items[_i].cost * (_items[_i].integrity / _items[_i].integrity_max) / 3)>>
	<</for>>
<</for>>
You think it will earn you £<<print ((_value / 100) + 50).toFixed(2)>> currently after getting the deposit back.

<br><br>
<<if $money gte 5000>>
	<<link [[Confirm the sale and pay the £50 deposit|Tailor Monthly Driver]]>>
		<<set $money -= 5000>>
		<<set $tailorMonthlyService to "discard">>
	<</link>>
<<else>>
	You can't afford the deposit.
	<br>
<</if>>
<br>
<<link [[Back out of the sale|Tailor Shop]]>><<endevent>><</link>>