<<set $outside to 0>><<set $location to "temple">><<effects>>
River asks a <<monk>> to take over command of the kitchen, and leads you into a dusty back room. There's a small table and two chairs.
<br><br>
<<He>> runs you through a series of concepts. There's too much to take in, but here and there you make connections to the competition question. You feel some of the pieces fall into place.
<br><br>
"I hope you paid attention," <<he>> says, standing. "I need to get back to the kitchen. Before the <<monks>> make a mess of things."
<br><br>
<<endevent>>
<<link [[Next|Soup Kitchen]]>><</link>>
<br>