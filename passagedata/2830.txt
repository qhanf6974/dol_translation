<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You refuse to comply, darting away from Leighton and through the classroom doors. You hide in a nearby maintenance cupboard and peek
through a gap, waiting for Leighton to leave. <<He>> does so just as Sirris arrives back. They exchange brief words, then Sirris enters
the class and Leighton heads back to <<his>> office, carrying a bag <<he>> didn't arrive with.
<br><br>

Once Leighton is out of sight you leave your hiding place and walk back to class. The boys talk and laugh with each other. The girls
aren't speaking much, and shift their legs uncomfortably.
<br><br>

<<endevent>>

<<link [[Next|Science Lesson]]>><</link>>
<br>