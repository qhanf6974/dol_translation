<<effects>>

You drop to your knees as instructed. "That's a good <<girl>>," Remy whispers. <<He>> knees beside you, and ties your arms to an iron bar several feet above the ground. <<He>> also ties your collar to a short chain attached to the floor. Your legs remain free, but that's little help with your arms and head in such a position.<<bind>><<headbind>>
<br><br>

<<link [[Next|Livestock Lactate Tubes]]>><</link>>
<br>