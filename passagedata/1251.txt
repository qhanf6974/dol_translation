<<set $location to "forest">><<effects>>
<<pass 60>>

The centaur carries you over fields and hills, until coming to a stop at a crest dotted with trees. You see the forest and town beneath you.
<br><br>
"That's as far as I can go," the centaur says, lifting you off its back. "I'm gonna get a bollicking at the farm, but my family is there." <<He>> places you on the ground.
<br><br>

<<if $worn.face.type.includes("gag")>>
	"Oh," <<he>> says, regarding you. "Here." <<He>> reaches behind your head, <span class="lblue">and unties your $worn.face.name.</span>
	<<set $worn.face.type.push("broken")>>
	<<faceruined>>
	<br><br>
<</if>>

<<link [[Thank|Livestock Escape Thank]]>><</link>>
<br>
<<link [[Kiss|Livestock Escape Kiss]]>><</link>><<deviant1>>
<br>