<<temple_effects>><<effects>>
<<set $temple_garden += 10>>
You encourage the initiates to help tidy the garden. They don't seem motivated, so you get stuck in yourself, pulling weeds and trimming hedges. This works, and together you soon have the garden looking much more presentable.
<br><br>
<<link [[Next|Temple]]>><</link>>
<br>