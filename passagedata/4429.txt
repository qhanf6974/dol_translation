<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewerswood">>
<<set $sewersevent -= 1>>
You are in the old sewers. Decaying logs litter the tunnel.
<br><br>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
		<<link [[Hide in a log|Sewers Wood Hide]]>><</link>>
		<br>
	<</if>>
	<<link [[Tunnel covered in webs (0:05)|Sewers Webs]]>><<pass 5>><</link>>
	<br>
	<<link [[Tunnel filled with litter (0:05)|Sewers Residential]]>><<pass 5>><</link>>
	<br><br>
<</if>>