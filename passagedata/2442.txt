<<set $outside to 0>><<set $location to "police_station">><<effects>>
<<if $submissive gte 1150>>
	You walk up to the desk. "I have something," you say, and put the flash drive on the counter. "T-the head of the local school has been doing awful things to students."
<<elseif $submissive lte 850>>
	You stride up to the desk. "I have evidence of a crime," you say, placing the flash drive on the counter. "The school head has been abusing students."
<<else>>
	You walk up to the desk. "I have evidence of a crime," you say, and put the flash drive on the counter. "The school head has been abusing students."
<</if>>
<br><br>
The <<person>> takes the flash drive. "Alright. I'll add it to the pile," <<he>> says uninterested. "Is there anything else?"
<br><br>
You get the feeling the police aren't going to be helpful. At least you tried to do the right thing.
<<ggcontrol>><<ltrauma>><<lstress>><<trauma -6>><<stress -12>>
<br><br>
<<set $headpolice to 1>><<control 25>>
<<link [[Next|Police Station]]>><</link>>
<br>