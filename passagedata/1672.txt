<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<danceeffects>>
<<danceaudience>>
<<danceactions>>

<<if $danceevent is 0 and $exhibitionism lte 74 and $exposed gte 2>>
There's no way you can continue dancing while so exposed! Face reddening, you flee the scene.
<br><br>
<<elseif $danceevent is 0 and $exhibitionism lte 34 and $exposed gte 1>>
There's no way you can continue dancing while so exposed! Face reddening, you flee the scene.
<br><br>
<</if>>

<<if $danceevent is "finish">>
<<link [[Next|Ocean Breeze]]>><<endevent>><<clotheson>><</link>>
<<elseif $danceevent is 0>>
	<<if $exposed gte 2 and $exhibitionism lte 74>>
	<<link [[Flee|Ocean Breeze Dance Stop]]>><</link>>
	<<elseif $exposed gte 1 and $exhibitionism lte 34>>
	<<link [[Flee|Ocean Breeze Dance Stop]]>><</link>>
	<<else>>
	<<link [[Stop|Ocean Breeze Dance Stop]]>><</link>>
	<</if>>
<</if>>