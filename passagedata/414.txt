<<effects>>

<<if $rng gte 51>>
	You hide behind a postbox. Eden continues bothering passers-by, unaware that you're so close. It looks like <<he>> doesn't intend to move on for a while, until <<he>> starts arguing with a <<generate2>><<person2>><<personstop>> The argument becomes more intense until Eden storms off.
	<br><br>

	<<endevent>>
	<<destinationeventend>>

<<else>>
	You hide behind a postbox. Eden continues bothering passers-by, drawing ever closer. <<He>> stops a moment, and sniffs the air, before continuing.
	<br><br>
	
	Eden waits until <<hes>> right beside the postbox before striking, <span class="red">lunging around it and grasping your arm.</span>
	<br><br>
	"You think this is funny?" <<He>> shouts. "I've been worried half to death!" <<His>> grip is tight. "I'm taking you home, and never letting you leave again. I'll keep you in a cage if I have to."
	<br><br>

	<<if $worn.neck.collared isnot 1>><<neckwear 1>>
	<<He>> fastens a collar around your neck.
	<br><br>
	<</if>>
	<<He>> grabs hold of your leash and pulls. "Come on. I'll punish you when we get home."
	<br><br>

	<<generate2>><<person2>>"You shouldn't treat <<phim>> like that," A <<person>> bravely interjects as Eden drags you by. Eden ignores <<himstop>>
	<br><br>

	The sight of a <<girl>> being dragged along on a leash turns heads. People whisper to each other, but no one intervenes. A <<generatey3>><<person3>><<person>> laughs and pulls out <<his>> phone to record you. Eden ignores <<him>> too.

	Eden calms down a bit once you're in the forest.
	<br><br>
	
	<<endevent>><<npc Eden>><<person1>>
	<<link [[Next|Eden Recaptured]]>><</link>>
	<br>
<</if>>