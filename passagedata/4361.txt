<<effects>><<set $location to "sewers">><<set $outside to 0>>

<<if $submissive gte 1150>>
"T-that's not me," you say. "<<charles>> isn't my name."
<<elseif $submissive lte 850>>
"Don't touch me," you say. "You're crazy."
<<else>>
"That's not my name," you say. "You've got me confused with someone else."
<</if>>
<br><br>

<<He>> grabs your arm and pulls you closer. <<He>> examines your scalp. "Yes, yes, good, looks good." <<he>> babbles, running <<his>> fingers over your head. "My child, you're beginning to upset me. You didn't lose your memory, right?" <<He>> runs <<his>> hands over your body.
<br><br>

<<link [[Allow|Sewers Intro Molestation]]>><<set $molestationstart to 1>><</link>>
<br>
<<link [[Push away|Sewers Intro Fight]]>><<set $fightstart to 1>><</link>>
<br>