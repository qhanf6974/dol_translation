<<effects>>

<<if $mason_count gte 3>>

	You sit beside the pond. Mason opens <<his>> eyes when <<he>> hears you.
	<br><br>

	<<mason_greet>>
	<br><br>

	<<mason_actions>>

<<else>><<set $mason_count to 3>>
	<<swim_check>>
	You sit beside the pond. Mason opens <<his>> eyes when <<he>> hears you.
	<br><br>

	<<if _swim_check is 1>>
		"I'm glad you found me," <<he>> says. "You can get in if you like."
	<<else>>
		"I'm glad you found me," <<he>> says. "You can get in if you like. There's a little hidey hole at the shore of the lake if you need somewhere to keep your clothes."
	<</if>>
	<br><br>

	<<mason_actions>>

<</if>>