<<effects>>
<<if $phase is 0>>
	<<He>> recoils in pain, giving you the chance you need to escape. You push the lid with all your might, creating an opening. <<He>> recovers before you're able to make good on your escape however, and pulls you back down.
	<br><br>
	<<link [[Next|Molestation Residential]]>><<set $phase to 1>><</link>>
	<<set $enemyhealth to $enemyhealthmax>>
<<else>>
	<<He>> recoils in pain again. With the dumpster already ajar, you are able to climb out and escape. <<tearful>> you flee around the corner.
	<br><br><br><br>
	<<clotheson>>
	<<endcombat>>
	<<residentialeventend>>
<</if>>