<<effects>>

You saunter up to the <<personcomma>> and lean over <<his>> desk. <<if $worn.upper.type.includes("naked")>><<His>> eyes are drawn to your bare chest<<else>><<He>> gets a good look down your deliberately dishevelled $worn.upper.name<</if>>. You <<if $skirt is 1>>lift up the side of<<else>>pull down<</if>> your $worn.lower.name just far enough to <<if $worn.under_lower.type.includes("naked")>>hint at your lack of underwear<<else>>reveal a glimpse of your $worn.under_lower.name<</if>>.
<<exhibitionism1>>

<<if $submissive gte 1150>>
"Do you want to keep looking at me?" you ask. "I don't mind."
<<elseif $submissive lte 850>>
"I'll let you stare," you say. "This time."
<<else>>
"It's so hot in here," you say.
<</if>>
<br><br>

<<set $seductiondifficulty to 8000>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>>
<br><br>

<<if $seductionrating gte $seductionrequired>><<set $mathslibraryknown to 1>><<set $mathsinfo += 1>>

<<He>> nods as you put on your show. You read over <<his>> project, keeping your eyes on it even as you sway your <<bottom>> around. You lean in so close you're almost touching <<himstop>>
<br><br>

The <<person>> reaches out to grab you. You glide out from between <<his>> arms. You've memorised everything you'll need. You leave with a playful wave. | <span class="green">+ Mathematical Insight</span>
<br><br>

<<else>>

<<He>> blushes, picks up <<his>> work, and moves to a different seat.
<br><br>

<</if>>

<<link [[Next|School Library]]>><<endevent>><</link>>
<br>