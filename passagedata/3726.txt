<<set $outside to 0>><<set $location to "temple">><<effects>>
<<npc River>><<person1>>
You ask River for help with the maths competition.
<br><br>
<<if $NPCName[$NPCNameList.indexOf("River")].love gte 30>>
	"It wouldn't be fair on the other students," River says, regarding you. "However, I could give you a more general lesson on some advanced concepts. Putting them to use in the context of the competition will be up to you."
	<br><br>
	<<link [[Accept lesson (1:00)|Soup Kitchen Lesson]]>><<set $river_help to 1>><<set $mathsinfo += 3>><<pass 60>><</link>> | <span class="green">+ + Mathematical Insight</span>
	<br>
	<<link [[Not now|Soup Kitchen]]>><<endevent>><</link>>
	<br>
<<else>>
	<<npc River>><<person1>>
	"I won't show favouritism," River says, piercing you with <<his>> blue eyes. <i>Perhaps <<he>> would be more willing to help if <<he>> had a better opinion of you.</i>
	<br><br>
	<<endevent>>
	<<link [[Next|Soup Kitchen]]>><</link>>
<</if>>