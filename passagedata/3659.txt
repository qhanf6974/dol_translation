<<effects>>

<<if $temple_arcade_phase is 0>><<set $temple_arcade_phase += 1>>
	The bell rings again. Another group enters, as eager as the last.
	<br><br>
	<span class="red">The dark-haired $temple_wall_victim screams and bashes the wall.</span> "St-stop. Not again. Please."
	<br><br>
	<span class="lewd">Someone grabs your <<bottomstop>></span>
	<br><br>

	<<link [[Next|Temple Arcade Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<elseif $temple_arcade_phase is 1>><<set $temple_arcade_phase += 1>>
	The bell rings again. Another group enters, as eager as the last.
	<br><br>
	<span class="blue">"St-stay strong,"</span> the freckled $temple_wall_victim says. "We're all getting through this."
	<br><br>
	<span class="lewd">Someone grabs your <<bottomstop>></span>
	<br><br>

	<<link [[Next|Temple Arcade Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<elseif $temple_arcade_phase is 2>><<set $temple_arcade_phase += 1>>
	The bell rings again. Another group enters, as eager as the last.
	<br><br>
	The dark-haired $temple_wall_victim tries to squirm free from the wall. <span class="red">They fail.</span>
	<br><br>
	<span class="lewd">Someone grabs your <<bottomstop>></span>
	<br><br>

	<<link [[Next|Temple Arcade Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<elseif $temple_arcade_phase is 3>><<set $temple_arcade_phase += 1>>
	The bell rings again. Another group enters, as eager as the last.
	<br><br>
	<span class="purple">The freckled $temple_wall_victim chokes back sobs.</span>
	<br><br>
	<span class="lewd">Someone grabs your <<bottomstop>></span>
	<br><br>

	<<link [[Next|Temple Arcade Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<elseif $temple_arcade_phase is 4>><<set $temple_arcade_phase += 1>>
	The bell rings again. Another group enters, as eager as the last.
	<br><br>

	<span class="red">The dark-haired $temple_wall_victim hangs limp.</span> The freckled $temple_wall_victim cradles their face in their hands. <span class="red">Tears fall between their fingers.</span>
	<br><br>
	<span class="lewd">Someone grabs your <<bottomstop>></span>
	<br><br>
	<<link [[Next|Temple Arcade Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	The rings again, but this time it carries a deep, sonorous tone. You hear the door open again, followed by a single pair of footsteps.
	<br><br>

	The tightness around your waist loosens, and you're pulled out of the wall.
	<br><br>

	<<link [[Next|Temple Arcade End]]>><</link>>
	<br>

<</if>>