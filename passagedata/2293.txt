<<effects>>

<<if $submissive gte 1150>>
	"N-no," you say. "I didn't agree to this."
<<elseif $submissive lte 850>>
	"Get the fuck off me," you say. "I didn't consent to this."
<<else>>
	"Get off," you say. "I didn't agree to this."
<</if>>
The <<person1>><<person>> ignores you, and gropes instead.
<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
<br><br>

You kick <<him>> hard in the shin, giving you an opening. You crawl out from beneath the table. The <<person>> soon appears on the other side. <<He>> glares at you before walking away.
<<glewdity>><<set $stall_rejected += 1>>
<br><br>

<<stall_actions>>