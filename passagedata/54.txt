Are you prepared to add all your
<<if $crateContents is "all">>
	clothes
<<else>>
	outfits
<</if>>
to the crate and send them to be sold? Might be best to go shopping shortly after.
<br><br>

<<set _value to 0>>
<<for _items range $wardrobe>>
	<<for _i to 0; _i lt _items.length; _i++>>
		<<if _items[_i].shop.length is 0>>
			<<continue>>
		<</if>>
		<<if _items[_i].outfitSecondary isnot undefined>>
			<<continue>>
		<</if>>
		<<if _items[_i].outfitPrimary is undefined and $crateContents is "outfits">>
			<<continue>>
		<</if>>
		<<set _value += Math.floor(_items[_i].cost * (_items[_i].integrity / _items[_i].integrity_max) / 3)>>
	<</for>>
<</for>>
It will earn you £<<print ((_value + 5000) / 100).toFixed(2)>>.

<br><br>
<<link [[Yes|Wardrobe Sale Crate Result]]>><</link>>
<br>
<<link [[No|$wardrobeReturnLink]]>>
	<<unset $wardrobeReturnLink>>
	<<unset $crateContents>>
<</link>>