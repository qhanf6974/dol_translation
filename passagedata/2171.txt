<<set $outside to 0>><<set $location to "hospital">><<effects>>

<<if $phase is 0>>
<<set $danger to random(1, 10000)>>
	You wait for the number of people in the vicinity to drop. You soon find yourself almost alone. There are still people at the far end of the hall, but they are facing away from you. You won't get a better chance than this. Steeling yourself, you emerge from beneath the bed and run toward the towels.
	<br><br>
	<<if $danger gte (9900 - ($allure))>>
	You've barely left the safety of the bed when doors on both sides of the hall open simultaneously. In an instant, the vacant room becomes packed with bodies. They immediately see you, and react in a diverse manner. Some look offended while others laugh, still others stare at you with depravity in their eyes. There's nowhere to hide; you can do nothing but crouch to preserve what little dignity you can. Not seeing any other option, you continue the original plan. You manage to cover yourself before people start filming the event with their phones. Blushing and light-headed, you flee the scene.
	<<garousal>><<arousal 600>><<gtrauma>><<trauma 6>><<gstress>><<stress 6>>
	<br><br>

	<<link [[Next|Hospital Foyer]]>><<endevent>><<towelup>><</link>>
	<br>
	<<else>>
You make it the cart without incident. Crouching beside it, you help yourself the towels, eyes affixed on a nearby door. You still feel exposed, but at least now you're covered. You should find something more suitable as soon as you can, before you attract unwanted attention.
<br><br>

<<link [[Next|Hospital Foyer]]>><<endevent>><<towelup>><</link>>
<br>
	<</if>>
<<elseif $phase is 1>>
<<set $danger to random(1, 10000)>>
	You slowly inch the bed forward. Slow enough, you hope, that no one will even notice. Peeking from beneath the bed, you see the towels drawing closer.
	<br><br>
	<<if $danger gte (9900 - ($allure / 3))>>
You're almost there when the bed lurches into movement. You cling to the underside as you are pushed further and further away from the towels, through a set of doors, then another, and then down a ramp.
<br><br>

You hear a metallic creaking, then you and the bed are shoved down a steep ramp. A short distance later you plunge into rushing water.
<br><br>

<<link [[Next|Drain Water]]>><<endevent>><<set $bus to "commercialdrain">><</link>>

	<<else>>
When you are within arm's reach, you stretch out and manage to acquire a handful of towels. You awkwardly cover yourself while remaining under the bed. You still feel exposed, but at least now you're covered. You should find something more suitable as soon as you can, before you attract unwanted attention.
<br><br>

<<link [[Next|Hospital Foyer]]>><<endevent>><<towelup>><</link>>
<br>

	<</if>>
<</if>>