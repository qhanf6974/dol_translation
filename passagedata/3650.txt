<<effects>>

<<if $submissive gte 1150>>
"I-it was taken from me," you say. "I'm sorry."
<<elseif $submissive lte 850>>
"No need," you say. "I failed your dumb task."
<<else>>
"I'm sorry," you say. "But I didn't remain chaste."
<</if>>
<br><br>

"Thank you for your honesty," Jordan says, looking at <<his>> feet. "You must be purified. I will summon the specialists. Please cooperate with them." <<He>> produces a small bell from <<his>> robes, and rings.
<br><br>
<<endevent>>
<<link [[Next|Temple Arcade Intro]]>><</link>>