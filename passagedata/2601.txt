<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>><<famerape 10>>
	Satisfied, the <<person>> shoves you to the ground. The rest of the class rushes back to their seats, as footsteps can be heard approaching. You know you'll be in trouble if you're caught lying on the floor. <<tearful>> you struggle into your seat just before Winter returns.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|History Lesson]]>><</link>>
<<elseif $enemyhealth lte 0>>
	The <<person>> recoils in pain. <<He>> glares at you, and <<he>> and <<his>> friends look like they've a mind to punish you, but at that moment footsteps can be heard approaching the class, and they think better of it. <<tearful>> you take your seat. Winter returns, with no knowledge of what just took place.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|History Lesson]]>><</link>>
<<elseif $alarm is 1 and $rescue is 1>>
	<<set $rescued += 1>>
	Footsteps can be heard rapidly approaching the classroom. The <<person>> pushes you away and returns to <<his>> desk, along with the rest of the class. <<tearful>> you take your seat. Winter arrives, looking angry. "I can't leave you alone for five minutes, can I?" Glad that the assault is over and not wanting Winter to know it was you who screamed, you keep your head down as the lesson continues.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|History Lesson]]>><</link>>
<<else>>
	Footsteps can be heard approaching the classroom. The <<person>> pushes you away and returns to <<his>> desk, along with the rest of the class. <<tearful>> you take your seat. Winter arrives, one of the <<persons>> friends in tow, looking diminished. "Don't waste my time again, or you'll never see the end of detention." Glad that the assault is over, you keep your head down as the lesson continues.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|History Lesson]]>><</link>>
<</if>>