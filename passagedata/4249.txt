<<effects>>

You keep your mouth shut. Whitney tugs on your hair, making you wince in pain, but you don't relent. <<He>> rubs your face against <<himcomma>> but you endure. <<He>> soon becomes frustrated and shoves you out from under the desk with <<his>> foot. You land on your back, facing up at a shocked Leighton.
<br><br>

<<endevent>><<npc Leighton>><<person1>>

"Well this is a surprise," the headteacher says. "If you're so keen on detention I can oblige. For now though, get out." <<He>> pushes you from the room.
<br><br>

<<endevent>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>