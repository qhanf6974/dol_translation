<<set $outside to 1>><<set $location to "forest">><<effects>>

You follow the trail and arrive at a grassy knoll. On its top lies a deer carcass, being chewed on by a wild boar. The boar spots the pack and glares down.
<br><br>

<<if $rng gte 51>>
Before the rear wolves even arrive the boar leaps over the carcass and charges down the hill, straight at you!
<br><br>

<<link [[Stand your ground|Wolf Carrion Stand]]>><</link>><<physiquedifficulty 2000 12000>>
<br>
<<link [[Run|Wolf Carrion Run]]>><</link>>
<br>

<<else>>
Before the last wolf even arrives, the boar leaps over the carcass and charges down the hill, straight at the black wolf.
<br><br>

	<<if $wolfpackharmony + $wolfpackferocity + 20 gte $rng>>

	The black wolf stands its ground. The boar veers away and runs into the forest.
	<br><br>

	The black wolf keeps the choice parts for itself. You find some berries growing nearby which serve you better.
	<<stress -12>><<lstress>>
	<br><br>

	The pack lazes around, eating at their leisure. A small dispute breaks out and the black wolf leaves its place by the deer to quell it.
	<br><br>

	<<wolfpackhuntoptions>>

	<<else>>

	The black wolf stands its ground, and the boar's charge knocks it aside. It runs amok among the wolves, who panic and run for the safety of the trees.
	<br><br>

	<i>Higher harmony and ferocity increases the chance of a successful hunt.</i>
	<br><br>

You run with the pack into the forest. There must be easier prey.
<<gathletics>><<athletics 6>><<physique 6>>
<br><br>

<<set $bus to "wolfcaveclearing">>
<<wolfhuntevents>>

	<</if>>
<</if>>