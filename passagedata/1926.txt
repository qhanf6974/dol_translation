<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>

You enter the lounge. Empty bottles and pizza boxes lay strewn about the room. The <<person>> lazes on the sofa and starts watching TV, as if you're not there.
<<pass 30>>
<br><br>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure)>>

You're bending over to pick up a pizza box stuck beneath a chair when the <<person>> grabs you from behind.
<br><br>

<<link [[Next|Domus Lounge Molestation]]>><<set $molestationstart to 1>><</link>>
<br>

<<else>>

<<pass 30>>
An hour passes, and you've cleared the worst of the debris. It seems enough to satisfy <<himstop>> "Good job. Here you go."
<br><br>

You've earned £10.
<<set $money += 1000>>
<br><br>

<<link [[Leave|Domus Street]]>><<endevent>><</link>>
<br>

<</if>>