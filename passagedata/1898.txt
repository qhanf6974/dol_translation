<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "domus">>
You walk up to one of the homes and knock.
<br><br>
<<if $rng gte 51>>
	There's no response.
	<br><br>
	<<link [[Break in|Domus House Lock]]>><</link>>
	<br>
	<<link [[Go to the Next House (0:02)|Domus House Nude]]>><<pass 2>><</link>><<exhibitionist5>>
	<br>
	<<link [[Stop|Domus Street]]>><</link>>
	<br>
<<elseif $rng gte 41>>
	<<generate1>><<person1>>A <<person>> answers the door. <<Hes>> shocked by your nudity. "What on earth?" <<he>> mutters.
	<br><br>
	<<fameexhibitionism 1>>
	<<link [[Ask for clothes|Domus House Beg]]>><<set $phase to 1>><</link>>
	<br>
<<elseif $rng gte 31>>
	<<generate1>><<person1>>A <<person>> answers the door. "Whaddya wa-" <<his>> eyes widen. "Why is there a naked <<girl>> in front of me?"
	<br><br>
	<<fameexhibitionism 1>>
	<<link [[Ask for clothes|Domus House Beg]]>><<set $phase to 2>><</link>>
	<br>
<<elseif $rng gte 21>>
	<<generate1>><<person1>>A <<person>> answers the door. "Oh, you're naked," <<his>> eyes wander across your body. "Is there anything you can d-" <<he>> coughs. "I can do for you?"
	<br><br>
	<<fameexhibitionism 1>>
	<<link [[Ask for clothes|Domus House Beg]]>><<set $phase to 3>><</link>>
	<br>
<<elseif $rng gte 11 and $schoolday is 0>>
	<<generatey1>><<person1>>A <<person>> answers the door. <<He>> looks shocked, then grins. "I know you from school," <<he>> says.
	<br><br>
	<<fameexhibitionism 1>>
	<<link [[Ask for clothes|Domus House Beg]]>><<set $phase to 4>><</link>>
	<br>
<<else>>
	<<generate1>><<person1>>A <<person>> answers the door. <<He>> grins as he sees you. "My my, what do we have here?"
	<br><br>
	<<link [[Ask for clothes|Domus House Beg]]>><<set $phase to 5>><</link>>
	<br>
	<<fameexhibitionism 1>>
<</if>>