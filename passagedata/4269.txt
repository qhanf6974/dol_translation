<<set $dateCount.Total++>><<set $dateCount.Whitney++>>
<<set $outside to 0>><<set $location to "town">><<effects>>
<<pass 20>>

<<He>> pushes you through the school gates, groping your <<bottom>> the whole time without caring who sees. "I have something romantic planned," <<he>> says. "Only the best for my <<bitchstop>>"
<br><br>

<<He>> takes you to Harvest Street, to a seedy pub. "Sit down. I'll get drinks."
<br><br>

<<He>> returns with two colourful looking glasses. "Drink up," <<he>> says.
<br><br>

<<link [[Drink|Bully Romance 2]]>><<set $phase to 0>><<npcincr Whitney dom 1>><</link>><<gdom>>
<br>
<<link [[Don't Drink|Bully Romance 2]]>><<set $phase to 1>><<npcincr Whitney dom -1>><</link>><<ldom>>
<br>