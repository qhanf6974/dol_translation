<<effects>>
<<pass 5>>
You look over shoulder, and see Alex up ahead. There's a <<person2>><<person>> beside <<person1>><<him>>.
<br><br>

Alex spots you first. <<person1>><<His>> face pales. The <<person2>><<person>> beside <<person1>><<him>> has a different reaction. "I didn't know you ran this sort of farm," <<person2>><<he>> says, pulling a phone from <<his>> pocket. <<He>> points the camera at you.
<br><br>

Alex breaks free from <<person1>><<his>> stupour, and looks away. "W-We aren't-"
<br>
<<if $submissive gte 1150>>
	"Animals need special care," you interrupt
<<elseif $submissive lte 850>>
	"We know how to handle animals around here," you interrupt
<<else>>
	"Our animals are given proper attention," you interrupt
<</if>>
as you and the <<farm_text dog>> arrive. "We're willing to go all the way."
<<deviancy5>>
"I can see that," the <<person2>><<person>> says, crouching and moving <<his>> camera right up to the knot. "You've got my business, and then some. I've friends who'll love to hear about this. Can I film until the knot deflates?"
<br><br>

<<link [[Next|Farm Knotted Seen 2]]>><</link>>
<br>