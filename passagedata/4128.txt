<<set $outside to 0>><<set $location to "home">><<effects>>

<<if $robincat isnot 1>><<set $robincat to 1>>

You rest your head on Robin's shoulder and rub. <<He>> looks at you and smiles. "Where did you get this anyway?" <<he>> asks, reaching for your cat ears. <<His>> touch sends a shiver of pleasure across your scalp and down your spine. You purr.
<br><br>

Robin laughs. "Good Kitty!" <<He>> keeps stroking, and wraps <<his>> other arm around your waist. Your eyes close. There's nothing in the world but Robin's touch.
<br><br>

<<robinoptions>>

<<else>>

You rest your head on Robin's shoulder and rub. <<He>> looks at you and smiles. "Who's a good kitty," <<he>> says, reaching up to stroke your ears. <<His>> touch sends a shiver of pleasure across your scalp and down your spine. You purr.
<br><br>

You rest against <<him>> for a while as <<he>> holds you tight and pets you.
<br><br>

<<robinoptions>>

<</if>>