<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $phase is 0>>
You get to work, hoping to get out of here quickly. Twenty minutes pass before Leighton speaks again. "That should do it." <<He>> gestures for you to leave.
<<elseif $phase is 1>>
You start leisurely writing on the board. Leighton looks up at you occasionally, pursing <<his>> lips in annoyance. Eventually, <<he>> looks at <<his>> watch. "That's enough, I don't have all day. Next time put some work into it and we'll both be out of here sooner." <<He>> gestures for you to leave.
<</if>>
<br><br>
<<link [[Next|Hallways]]>><<endevent>><</link>>
<br>