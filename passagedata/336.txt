<<set $outside to 0>><<set $location to "cabin">><<effects>>

You snuggle up to Eden and <<he>> holds you close. Together you watch the fire.
<br><br>

<<if $hour isnot 0>>
<<link [[Keep Cuddling (0:30)|Eden Cuddle]]>><<trauma -3>><<stress -6>><<pass 30>><<npcincr Eden love 1>><</link>><<ltrauma>><<lstress>>
<br>
<</if>>
<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
<br>