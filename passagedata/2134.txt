<<widget "rentdue">><<nobr>>

<<npc Bailey>><<person1>>"Time to pay up," <<He>> says.

<<if $baileydefeatedchain gte 1>>
<<generate2>><<person2>>A rough-looking <<person>> stands with <<person1>><<himstop>> <<person2>><<He>> cracks <<his>> knuckles.<<person1>>
<br><br>
<</if>>

<br><br>

<<if $money gte $rentmoney>>

<<link [[Pay|Rent Pay]]>><</link>>
<br>
<<link [[Refuse|Rent Refuse]]>><<set $submissive -= 1>><</link>>
<br>
<<else>>

	<<if $submissive lte 850>>
	You stare at <<him>> in defiance. "I don't have your money. And wouldn't give it to you if I did."
	<<elseif $submissive gte 1150>>
	You hang your head, unable to pay and afraid of what's to come.
	<<else>>
	"I don't have it," you say, afraid of what's to come.
	<</if>>
<br><br>
"That's okay," <<he>> says, smiling. "I've already made arrangements. I don't know what they intend for you, and frankly I don't care." <<He>> produces a hood and length of rope. "Hold still."
<br><br>

<<link [[Submit|Rent Intro]]>><<endevent>><</link>>
<br>
	<<if $bus is "hospital">>
<<link [[Fight|Rent Hospital Fight]]>><<set $fightstart to 1>><</link>>
<br>
	<<else>>
<<link [[Fight|Rent Fight]]>><<set $fightstart to 1>><</link>>
<br>
	<</if>>

<</if>>

<<set $renttime to 7>><<set $rentday to $weekday>>

<</nobr>><</widget>>

<<widget "rentmod">><<nobr>>
<<if $robinpaid is 1>>
<<set $rentmoney *= 2>>
<</if>>
<</nobr>><</widget>>