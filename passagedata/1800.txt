<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>

You stomp hard on the <<persons>> foot, and <<he>> shouts, tumbling backwards to the ground where <<he>> rolls around in pain. Laughter erupts among the assembled guests.
<br><br>

"Serves the perv right."
<br><br>

The <<person2>><<person>> grips your arm and leads you out the room. "You've ruined my party," <<he>> says. "Get out. And no, you're not seeing a penny. Shoo!"
<br><br>

<<endevent>>
<<link [[Next|Danube Street]]>><</link>>
<br>