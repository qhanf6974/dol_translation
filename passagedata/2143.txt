<<effects>>

<<if $physiquesize lte 6000>>
	Dropping to your hands and knees, you squeeze through the gap. It's a tight fit, but your small frame proves an advantage.<<small_text>>
	<br><br>
	<<set $stripintegrity to 10>><<set $stripobject to "steel wire">><<stripobject>>
	You see light penetrating the ceiling up ahead. After a short journey, you find a manhole leading outside.
	<br><br>

	<<commercialquick>>
<<else>>
	Dropping to your hands and knees, you squeeze through the gap. It goes well at first, you manage to get your stomach through before becoming stuck. You hear a sharp buzz and the sound of grinding metal as you try to pull out, but you're stuck tight. Something pokes your rear!
	<br><br>

	<<link [[Next|Abduction Hospital Hole Wolves]]>><<set $molestationstart to 1>><</link>>
<</if>>