<<widget "cafecoffeeflasharousal">><<nobr>>
You feel
<<if ($worn.under_lower.exposed and $uncomfortable.nude is true) or (!$worn.under_lower.exposed and $uncomfortable.underwear is true)>>
	extremely embarrased letting so many strangers see your <<undies>> in public like this, but despite the feeling of <<print ["embarrassment", "humiliation", "vulnerability", "exposure"].pluck()>> you crave more.
<<else>>
	<<print ["excited", "exhillerated", "turned on", "thrilled", "horny", "lewd"].pluck()>> showing so many strangers your <<undies>> in public like this.
	<<if $assertiveaction is "trauma">> You're so naughty. <<trauma -2>><<ltrauma>> <</if>>
<</if>>

<<switch $phase>>
	<<case 0>> <<arousal 0>>
	<<case 1>> <<arousal 100>>
	<<case 2>> <<arousal 200>>
	<<case 3>> <<arousal 300>>
	<<case 4>> <<arousal 100>>
<</switch>>

<<if $phase isnot 4>>
	<<if $worn.under_lower.exposed>>
		<<exhibitionism4>>
	<<else>>
		<<exhibitionism3>>
	<</if>>
<<else>>
	<br><br>
<</if>>
<</nobr>><</widget>>