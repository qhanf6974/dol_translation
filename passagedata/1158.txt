<<effects>>

You bite down. Hard. <<His>> eyes flash and <<he>> withdraws, but <<he>> doesn't cry out. <<if $wolf gte 2 or $cat gte 2>><span class="red">You can taste <<his>> blood.</span><<ltrauma>><<trauma -6>><</if>>
<br><br>

<<He>> takes hold of your leash. "I'll process it," <<he>> says with relish. "We'll have another docile member of the herd within a week."
<br><br>

<<if $exposed lte 1 or $worn.head.name isnot "naked" or $worn.face.name isnot "naked" or $worn.legs.name isnot "naked" or $worn.feet.name isnot "naked">>
	Hands intrude from behind, grasping your clothes, <span class="red">they tear the fabric from you body.</span>
	<<upperruined>><<lowerruined>><<underupperruined>><<underruined>><<headruined>><<faceruined>><<legsruined>><<feetruined>><<set $genderknown.pushUnique("Remy")>>
	<br><br>
<</if>>

<<link [[Next|Livestock Intro Heavy]]>><</link>>
<br>