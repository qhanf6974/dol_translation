<<effects>>
<<if $athletics gte random(1, 1000)>>
	You deftly <span class="green">leap over the startled cat</span> without slowing down. You continue down an alley between two shops.
	<br><br>
	You round a corner and come to a stop, your whole body shaking.
	<br><br>
	<<link [[Next|Commercial alleyways]]>><</link>>
<<else>>
	You jump a moment too late, <span class="red">and trip over the cat,</span> falling to your knees. The cat meows as it runs away, and seems unharmed.
	<<gpain>><<pain 6>>
	<br><br>
	<<link [[Next|Road Ex Ground]]>><</link>>
	<br>
<</if>>