<<set $outside to 1>><<effects>>
<<if $physique gte random(6000, 16000)>>
	<<if $worn.lower.skirt is 1 and !$worn.under_lower.type.includes("naked")>>
		You hold on to your $worn.under_lower.name as the tentacle thrashes around. <span class="green">The tentacle slips free,</span> and retreats into the manhole.
		<<gstress>><<stress 1>>
		<br><br>
		<<destinationeventend>>
	<<else>>
		You hold on to your $worn.lower.name as the tentacle thrashes around. <span class="green">The tentacle slips free,</span> and retreats into the manhole.
		<<gstress>><<stress 1>>
		<br><br>
		<<destinationeventend>>
	<</if>>
<<else>>
	<<if $worn.lower.skirt is 1 and !$worn.under_lower.type.includes("naked")>>
		You hold on to your $worn.under_lower.name as the tentacle thrashes around. <span class="red">The tentacle tears it from your grip,</span> and retreats into the manhole, still clutching the ruined fabric.
		<<gtrauma>><<gstress>><<stress 6>><<trauma 6>>
		<br><br>
		You're left pantiless in the middle of the street. You press you hand between your legs and hold your $worn.lower.name down, suddenly self-conscious.
		<br><br>
		<<underlowerruined>><<exposure>>
		<<destinationeventend>>
	<<else>>
		You hold on to your $worn.lower.name as the tentacle thrashes around. <span class="red">The tentacle tears it from your grip,</span> and retreats into the manhole, still clutching the ruined fabric.
		<<gtrauma>><<gstress>><<stress 6>><<trauma 6>>
		<br><br>
		You're left bottomless in the middle of the street. <<covered>>
		<br><br>
		<<lowerruined>><<exposure>>
		<<destinationeventend>>
	<</if>>
<</if>>