<<effects>>

<<npc Alex>><<person1>>
You approach Alex's farm. <<Hes>> carrying two heavy buckets across the yard, but smiles and plants them on the ground when <<he>> sees you. "It's good to see you again," <<he>> says.
<br><br>

<<if $submissive gte 1150>>
	"I-I've come about your job offer," you say. "I'd like to help."
<<elseif $submissive lte 850>>
	"I'm here about work," you say. "I'm in."
<<else>>
	"I'm hear about your job offer," you say. "I'd like to help."
<</if>>
<br><br>

Alex beams. "Great! You can start whenever you like, and don't be afraid to ask questions."
<br><br>


<<farm_init>>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>