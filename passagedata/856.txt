<<effects>>

<<if $submissive gte 1150>>
	"N-No," you say. "I knew you'd protect me."
<<elseif $submissive lte 850>>
	"Of course not," you say.
<<else>>
	"I'm braver than that," you say.
<</if>>
<br><br>

"I wasn't either," Alex says, puffing out <<his>> chest. "Come on, let's get back to work."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>