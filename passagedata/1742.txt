<<set $outside to 0>><<set $location to "compound">><<effects>>
<<if $compoundflirt is 1>>
	"You have me all tied up and exposed," you say. "Isn't there something else you'd rather do to me?"<<promiscuity1>>
	<br><br>
	<<He>> blushes, unsure how to respond. "D-don't joke around like that. You're in big trouble." You control your breathing and focus.
	<br><br>
<<elseif $compoundflirt is 2>>
	"Some people would pay good money for the treatment you're giving me," you say. "I should thank you properly."<<promiscuity2>>
	<br><br>
	<<He>> shifts uncomfortably. "Don't think you can sweet talk me. It won't work."
	<br><br>
<<elseif $compoundflirt is 3>>
	"The real torture here," you say. "Is being denied your touch."
	<<promiscuity3>>
	<br><br>
	<<He>> moves <<his>> hand away from the dials and rubs <<his>> temple. "Just stop it will you?"
	<br><br>
<<elseif $compoundflirt is 4>>
	"You're such a tease. I can't wait to see what you want for the main course." You wink.
	<br><br>
	"This is the main course," <<he>> says, blushing. "Where did you learn to talk like this?"
	<br><br>
<<else>>
	"I'm getting too hot here," you say. "Please help me. I'm at your mercy. Your fingers, your tongue, your..."
	<br><br>
	"Stop! Right now." <<He>> seems serious.
	<br><br>
<</if>>
<<compoundoptions>>