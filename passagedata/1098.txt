<<effects>>

You do your best to keep your hands together despite the violent thrusting.
<<deviancy3>>

<<if $handskill gte 400>>
	<<set $livestock_horse to 2>>
	<<set $facesemen += 5>>
	<<set $chestsemen += 5>>
	<<set $hairsemen += 5>>
	<span class="green">You manage it.</span> The enormous cock erupts, covering your face and hair with thick horse semen. It spurts in such quantities that there's nothing you can do to stop it spilling on your <<breastsstop>>
	<br><br>

	You can't see anything, but hear the other horses crowd around. They jostle with each other, hoping for a turn.

	You clear the semen from your eyes. The satisfied horse lies on the grass, eyes shut though still breathing heavily.
	<br><br>

	<<link [[Next|Livestock Field]]>><</link>>
	<br>
<<else>>
	<span class="red">You don't manage it.</span> The force knocks you to the ground, leaving the horse thrusting into empty air. It neighs in frustration.
	<br><br>

	The gate opens, and several farmhands rush in, drawn by the noise and wielding cattle prods. They surround the agitated horse. It rears on its hind legs and kicks at the closest. Another farmhand sneaks up behind it, and sticks a needle in its rear. Its eyes shut, and it collapses in a heap.
	<br><br>

	The farmhands leave the field, shutting the gate behind them. The other horses crowd around their fallen friend, as if to make sure it's alright.

	<<set $handskill += 5>>
	<span class="gold">You feel more confident in your ability to give pleasure with your hands.</span>
	<br><br>

	<<link [[Next|Livestock Field]]>><</link>>
	<br>
<</if>>