<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>

Startled by the collapse, the spiders flee into the darker places of the room. <<tearful>> you struggle to your feet and peel the remaining web from your body. There's nothing stopping you from retrieving the painting.
<br><br>

<<clothesontowel>>

<<person1>>It's heavier than it looks, but you manage to carry it down the ladder. The <<person>> must have heard it creak as <<he>> soon arrives beside you. "That's it. Thank you," <<he>> says as <<he>> takes the painting and hands you £15. <<His>> smile turns sour as <<he>> notices your dust-covered clothing. "Please leave, before you make a mess of the carpet."
<br><br>

<<set $money += 1500>>
<<pass 1 hour>>
<<endcombat>>

<<link [[Next|Danube Street]]>><</link>>
<br>