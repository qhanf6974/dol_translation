<<effects>>

<<if $physique gte random(1, $physiquemax)>>
	You grasp your own leash, and pull it away from Harper. <<He>> stumbles, almost tripping.
	<br><br>

	At Remy's call, two farmhands enter and grasp your arms from behind. You shake one of them off, <span class="green">and punch the other in the nose.</span>
	<br><br>

	Both seem afraid to approach you. <<endevent>><<npc Remy>><<person1>>"What are you waiting for?" Remy asks. "Restrain the beast." <<His>> cronies glance at each other, unwilling to make the first move.
	<br><br>

	"It's alright," <<endevent>><<npc Harper>><<person1>>Harper says, stabilising <<himself>> on the arm of a chair. "The <<if $player.gender_appearance is "m">>bull<<else>>cow<</if>> needs to be cooperative. We can try again another day."
	<br><br>

	<<endevent>><<npc Remy>><<person1>>
	"I'm sorry for this terrible behaviour," Remy says, tugging your leash. "Thank you for understanding."
	<br><br>

	<<link [[Next|Livestock Job Resist End]]>><<endevent>><</link>>
	<br>

<<else>>
	You grasp your own leash, and pull it away from Harper. <<He>> stumbles, almost tripping.
	<br><br>

	At Remy's call, two farmhands enter and grasp your arms from behind. You try to shake them off, <span class="red">but they hold firm.</span> Harper picks up your leash again.
	<br><br>

	The farmhands let go, but remain in the room, ready to intervene.
	<br><br>

	<<link [[Next|Livestock Job Harper 3 Obey]]>><</link>>
	<br>

<</if>>