<<set $outside to 0>><<set $location to "cabin">><<effects>>

<<npc Eden>><<person1>>
<<if $exposed gte 1>>
<<towelup>>
<</if>>
<<if $forestleashed is 1>>
	<<if $NPCName[$NPCNameList.indexOf("Eden")].trust gte 200>>
	<span class="green">Eden unties the leash from the wooden beam.</span> "You've been a good <<girlcomma>> so I'm going to give you a bit more freedom. Don't you dare run out on me though. Stick to the cabin and the clearing just outside."
	<br><br>
	<<set $forestleashed to 0>>
	<<else>>
	You are leashed tightly to a wooden beam, preventing escape.
	<br><br>
	<</if>>
<<else>>
	<<if $NPCName[$NPCNameList.indexOf("Eden")].trust lt 100>>
	<span class="red">Eden ties your leash to a wooden beam.</span> "You've been a bad <<girlcomma>> so I'm going to make sure you don't go anywhere."
	<br><br>
	<<set $forestleashed to 1>>
	<</if>>
<</if>>

<<if $forestleashed isnot 1>>

	<<if $syndromeeden is undefined>><<set $syndromeeden to 1>><<set $NPCName[$NPCNameList.indexOf("Eden")].lust to 0>><<set $edenshrooms to 0>><<set $edengarden to 0>><<set $edenspring to 0>>
	<br><br>
	<span class="red"><i>Eden isn't so bad, <<hes>> just lonely. It must be hard living here in the woods on your own.</i>
	<br>
	You've gained the "Stockholm Syndrome: Eden" trait.</span>
	<br><br>
		<<if $loveInterest.primary is "None">>
			<<set $loveInterest.primary to "Eden">>
			<span class = "gold">Eden is now your love interest! The feeling of danger has passed, and you feel safer here.</span>
			<br><br>
		<<else>>
			<span class = "gold">Eden can now be claimed as your love interest! The feeling of danger has passed, and you feel safer here. <br>You can change your love interest in the "Attitudes" menu.</span>
			<br><br>
		<</if>>
	<</if>>

<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
<br>
<<else>>
<<if $hour lte 6>>
Eden carries you to the bed.
<<if $forestleashed is 1>>
<<He>> wraps your leash around the bars of the headboard, practically pinning you in place.
<</if>>
<<He>> leers at you, a ravenous look in <<his>> eyes. "You're so hot. I'm gonna enjoy this."
<br><br>

<<link [[Next|Cabin Night Rape]]>><<set $molestationstart to 1>><</link>>
<br>

<<elseif $hour lte 8>>

Eden gives you instructions on how to prepare <<his>> breakfast.
<br><br>

<<link [[Prepare the food as instructed|Forest Cabin Food]]>><<npcincr Eden trust 10>><<set $submissive += 1>><<pass 2 hours>><</link>><<gtrust>><<gstockholm>>
<br>
<<link [[Spit in the eggs|Forest Cabin Spit]]>><<npcincr Eden trust -10>><<set $submissive -= 1>><<pass 2 hours>><</link>><<ltrust>><<lstockholm>>
<br>

<<elseif $hour lte 16>>
Eden leads you outside, and goes about <<his>> daily business.

<<if $forestleashed is 1>>
<<He>> keeps you close at all times, tying your leash around a tree when <<he>> needs to use both hands.
<<else>>
<<He>> keeps you close at all times.
<</if>>
<br><br>

<<if $forestleashed is 1>>
<<link [[Weaken the Leash|Forest Cabin Weaken]]>><<set $submissive -= 1>><</link>>
<br>
<</if>>
<<link [[Be good|Forest Cabin Good]]>><<set $submissive += 1>><</link>>
<br>

<<elseif $hour lte 18>>
Eden runs a bath. <<He>> eases <<his>> body into the water, and looks at you expectantly. "What are you waiting for? Strip, get in, and wash me."
<br><br>
<<link [[Wash|Cabin Bath]]>><<pass 2 hours>><<npcincr Eden trust 10>><<set $submissive += 1>><</link>><<gtrust>><<gstockholm>>
<br>
<<link [[Refuse|Cabin Bath Refuse]]>><<pass 2 hours>><<npcincr Eden trust -10>><<set $submissive -= 1>><</link>><<ltrust>><<lstockholm>>
<br>

<<elseif $hour lte 22>>
Eden settles down to read a book, occasionally pausing to stoke the fireplace.
<br><br>

<<link [[Cuddle|Cabin Cuddle]]>><<set $submissive += 1>><<npcincr Eden trust 20>><<npcincr Eden love 1>><<pass 2 hours>><<pass 2 hours>><</link>><<gtrust>><<ggstockholm>>
<br>
<<link [[Catch some sleep|Forest Cabin]]>><<set $submissive -= 1>><<tiredness -12>><<tiredness -12>><<pass 2 hours>><<pass 2 hours>><<endevent>><</link>><<ltiredness>>
<br>
<<else>>
Eden carries you to the bed.
<<if $forestleashed is 1>>
<<He>> wraps your leash around the bars of the headboard, practically pinning you in place.
<</if>>
<<He>> leers at you, a ravenous look in <<his>> eyes. "You're so hot. I'm gonna enjoy this."
<br><br>

<<link [[Next|Cabin Night Rape]]>><<set $molestationstart to 1>><</link>>
<br>
<</if>>
<</if>>