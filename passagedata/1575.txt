<<effects>>
<<generate1>><<person1>>
You approach the van, and peer inside. A rush of footsteps is all the warning you're given. A pair of arms shove you forward.
<br><br>

You tumble onto the van floor. You turn just in time to see a <<person>> slam the doors shut.
<br><br>

You try to open the doors, but they're locked. You look around, but see no other way out. The van rumbles into motion.
<br><br>

<<link [[Next|Street Van Journey]]>><</link>>
<br>