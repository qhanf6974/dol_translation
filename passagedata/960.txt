<<effects>>

"I can take you there," the <<person>> says. "But first,
<<if $worn.upper.set is $worn.lower.set>>
	<<if $worn.upper.gender is "f">>
		give me your dress. Don't worry, I'll give it back."
	<<else>>
		give me your clothes. Don't worry, I'll give them back."
	<</if>>
	<br><br>

	<<Hes>> serious,

	<<if $worn.under_upper.exposed lte 0>>
		<<if $worn.under_lower.exposed lte 0>>
			but at least you're wearing something beneath.
			<br><br>

			<<if $exhibitionism gte 15>>
				<<link [[Give up your clothes (0:15)|Farm Hitchhike Exposed Full]]>><</link>><<exhibitionist2>>
				<br>
			<</if>>
			<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
			<br>
		<<else>>
			and you're not wearing underwear beneath.
			<br><br>

			<<if $exhibitionism gte 35>>
				<<link [[Give up your clothes (0:15)|Farm Hitchhike Exposed Full]]>><</link>><<exhibitionist3>>
				<br>
			<</if>>
			<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
			<br>
		<</if>>
	<<elseif $player.gender is "f" or $player.gender_appearance is "f" or $player.gender is "h">>
		<<if $worn.under_lower.exposed lte 0>>
			and you're not wearing a bra or anything beneath.
			<br><br>

			<<if $exhibitionism gte 15>>
				<<link [[Give up your clothes (0:15)|Farm Hitchhike Exposed Full]]>><</link>><<exhibitionist2>>
				<br>
			<</if>>
			<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
			<br>
		<<else>>
			and you're not wearing anything beneath.
			<br><br>

			<<if $exhibitionism gte 35>>
				<<link [[Give up your clothes (0:15)|Farm Hitchhike Exposed Full]]>><</link>><<exhibitionist3>>
				<br>
			<</if>>
			<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
			<br>
		<</if>>
	<<else>>
		<<if $worn.under_lower.exposed lte 0>>
			and you're not wearing a top beneath.
			<br><br>

			<<if $exhibitionism gte 15>>
				<<link [[Give up your clothes (0:15)|Farm Hitchhike Exposed Full]]>><</link>><<exhibitionist2>>
				<br>
			<</if>>
			<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
			<br>
		<<else>>
		and you're not wearing anything beneath.
		<br><br>

			<<if $exhibitionism gte 35>>
				<<link [[Give up your clothes (0:15)|Farm Hitchhike Exposed Full]]>><</link>><<exhibitionist3>>
				<br>
			<</if>>
			<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
			<br>
		<</if>>
	<</if>>

<<elseif !$worn.upper.type.includes("naked")>>
	give me your shirt. Don't worry, I'll give it back."
	<br><br>

	<<Hes>> serious,

	<<if $worn.under_upper.exposed lte 0>>
		but at least you're wearing something underneath.
		<br><br>

		<<link [[Give up your top (0:15)|Farm Hitchhike Exposed Top]]>><</link>>
		<br>
		<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
		<br>
	<<elseif $player.gender is "f" or $player.gender_appearance is "f" or $player.gender is "h">>
		and you're not wearing anything underneath.
		<br><br>

		<<if $exhibitionism gte 15>>
			<<link [[Give up your top (0:15)|Farm Hitchhike Exposed Top]]>><</link>><<exhibitionist2>>
			<br>
		<</if>>
			<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
			<br>
	<<else>>
		but <<he>> does say <<he>> will return it.
		<br><br>

		<<link [[Give up your top (0:15)|Farm Hitchhike Exposed Top]]>><</link>>
		<br>
		<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
		<br>
	<</if>>
<<else>>
	give me your bottoms. Don't worry, I'll give it back."
	<br><br>

	<<Hes>> serious,

	<<if $worn.under_lower.exposed lte 0>>
		but at least you're wearing something underneath.
		<br><br>

		<<if $exhibitionism gte 15>>
			<<link [[Give up your bottoms (0:15)|Farm Hitchhike Exposed Bottom]]>><</link>><<exhibitionist2>>
			<br>
		<</if>>
		<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
		<br>
	<<else>>
		and you're wearing nothing beneath.
		<br><br>

		<<if $exhibitionism gte 35>>
			<<link [[Give up your bottoms (0:15)|Farm Hitchhike Exposed Bottom]]>><</link>><<exhibitionist3>>
			<br>
		<</if>>
		<<link [[Refuse|Farm Hitchhike Exposed Refuse]]>><</link>>
		<br>
	<</if>>
<</if>>