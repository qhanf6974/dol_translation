<<set $outside to 0>><<set $location to "town">><<effects>>

You lie still on the floor. They open the back of the van and shove the crying Whitney in. The <<person3>><<person>> climbs in after <<person1>> <<himstop>> The <<person2>><<person>> shuts the doors, cutting off the sound of Whitney's cries. <<He>> climbs in the front seat and drives away.
<br><br>
<<npcset Whitney state "dungeon">>
<<endevent>>
<<destinationeventend>>