<<set $outside to 1>><<set $location to "forest">><<effects>>
You run with the pack alongside the river. You come to a soft patch of grass. Some of the wolves lie down for a rest.
<br><br>
<<if $wolfpackleader gte 1>>
	<<link [[Rest (1:00)|Wolf River Rest]]>><<tiredness -12>><<pass 1 hour>><</link>>
	<br>
	<<link [[No time for rest|Wolf River Rouse]]>><</link>>
	<br>
	<<if $rng gte 51>>
		<<link [[Let the black wolf decide|Wolf River Rest]]>><<tiredness -12>><<pass 1 hour>><</link>>
		<br>
	<<else>>
		<<link [[Let the black wolf decide|Wolf River Rouse]]>><</link>>
		<br>
	<</if>>
<<else>>
	<<if $rng gte 51>>
		The black wolf lies down too, and you and the other wolves follow suit.
		<br><br>
		<<link [[Next (1:00)|Wolf River Rest]]>><<tiredness -12>><<pass 1 hour>><</link>>
		<br>
	<<else>>
		The black wolf growls at them, and they stand back up.
		<br><br>
		<<link [[Next|Wolf River Rouse]]>><</link>>
		<br>
	<</if>>
<</if>>