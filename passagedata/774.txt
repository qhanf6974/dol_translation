<<set $outside to 0>><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>><<earnFeat "Alex's Partner">><<npcincr Alex lust -20>>
	<<if $consensual is 1>>
		You lie beside <<him>> for a moment, staring up at the sky. "Couldn't get away with that on my <<if $pronoun is "m">>dad's<<else>>mum's<</if>> farm," <<he>> laughs. <<llust>><<npcincr Alex lust -1>>
		<br><br>
		<<tearful>> you rise to your feet. "I need to get back to work," <<he>> continues. "Give a shout if you need me."
		<br><br>
	<<else>>
		Clarity returns to <<his>> eyes. <<He>> looks away as <<he>> climbs to <<his>> feet. "Sorry if I got carried away," <<he>> says
		<br><br>
		<<tearful>> you return to work. Alex avoids eye contact.
		<br><br>
	<</if>>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	Alex trips over tilled earth, and falls on <<his>> back. <<He>> shakes <<his>> head. "Sorry. I didn't think you were serious about stopping."
	<br><br>
	<<tearful>> you return to work. Alex avoids eye contact.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	Alex climbs to <<his>> feet. "I need to get back to work," <<he>> says. "Give a shout if you need me."
	<br><br>
	<<tearful>> you rise to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<</if>>