<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	"Serves you right," <<he>> says. "Distracting people like that." <<He>> climbs back in <<his>> van, and drives away.
	<br><br>
	<<tearful>> you try to get your bearings.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<<elseif $enemyhealth lte 0>>
	The <<person1>><<person>> crashes backwards into the barrels. <<tearful>> you continue on your journey.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<<else>>
	<<set $rescued += 1>>
	A window opens, far above. A head pokes out. "Everything okay down there?"
	<br><br>
	The <<person1>><<person>> backs away from you. "Everything's fine," <<he>> says, stepping towards <<his>> van. "You didn't see nothing." <<He>> climbs in, and drives away.
	<br><br>
	<<tearful>> you continue your journey.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<</if>>