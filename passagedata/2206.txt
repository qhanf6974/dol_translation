<<set $location to "landfill">><<set $outside to 1>><<effects>>

<<set $seductiondifficulty to 6000>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>>
<br><br>

You pout. "Do you treat all your guests this way?" you coo, walking up to <<himstop>> "It's okay. I bet you're just lonely out here on your own."
<<promiscuity2>>

<<if $seductionrating gte $seductionrequired>>

<span class="green">The <<person>> blushes and looks away.</span> "I-I don't know what you're-" <<he>> manages. <<He>> falls silent when you grasp <<his>> arm and pull <<him>> closer.
<br><br>

<<link [[Next|Trash Sex]]>><<set $sexstart to 1>><</link>>
<br>

<<else>>

<span class="red">The <<person>> holds out an arm to keep you at bay.</span>"You're nuts," <<he>> says, grabbing your arm. <<He>> tries to pull you towards the exit.
<br><br>

<<link [[Struggle|Trash Fight]]>><<set $fightstart to 1>><</link>>
<br>
<<link [[Don't struggle|Trash Rape End]]>><</link>>
<br>

<</if>>