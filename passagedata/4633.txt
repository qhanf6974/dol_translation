<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	The students swagger back to Connudatus Street. You've no doubt they'll talk about this to anyone they can.
	<br><br>
	<<tearful>> you lean against a wall until you catch your breath.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Commercial alleyways]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	You shove the <<person1>><<person>> away from you, and run deeper into the maze of alleys. You hear their footsteps behind you, but you soon manage to lose them.
	<br><br>
	<<tearful>> you lean against a wall until you catch your breath.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Commercial alleyways]]>><</link>>
	<br>
<<else>>
	Your screams are followed by a police siren. "Sh-shit," the <<person1>><<person>> says, backing away from you and turning to run. The other students follow suit.
	<br><br>
	<<tearful>> you lean against the wall. The sirens fade. <<if $awarelevel gte 1>>It's fortunate your assailants were so naive.<<else>>They weren't coming to your rescue after all.<</if>>
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Commercial alleyways]]>><</link>>
	<br>
<</if>>