<<set $outside to 0>><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>><<earnFeat "Alex's Partner">><<npcincr Alex lust -20>>
	<<if $consensual is 1>>
		The pair of you fall back on the grass, and stare at the boughs above.
		<br><br>
		<<He>> looks at <<his>> watch, and sighs. "Better get back to work," <<he>> says. <<He>> climbs to <<his>> feet. <<tearful>> you follow suit. <<He>> kisses your cheek before walking away.
		<br><br>
	<<else>>
		Clarity returns to Alex as <<he>> falls back against the tree. "Sorry," <<he>> says. "I misread the situation."
		<br><br>
		
		<<He>> avoids eye contact as <<he>> climbs to <<his>> feet, and returns to work. <<tearful>> you do likewise.
		<br><br>
	<</if>>
	<<clotheson>>
	<<endcombat>>
	<<link [[Farm Work|Farm Work]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	Alex falls back against the tree. "I'm sorry," <<he>> says. "I misread the situation."
	<br><br>
	<<tearful>> you climb to your feet. "I'd better get back to work," Alex adds, avoiding eye contact. <<He>> walks away.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Farm Work|Farm Work]]>><</link>>
	<br>
<<else>>
	Alex reclines against the tree, and looks at <<his>> watch. "I better get back to work," <<he>> says, rising to <<his>> feet.
	<br><br>
	<<tearful>> you do likewise.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Farm Work|Farm Work]]>><</link>>
	<br>
<</if>>

<<unset $farm_relax_drink>>
<<set $farm_work.alex_relax to 3>>
<<set $farm_work.alex to "still">>