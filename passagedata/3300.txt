<<set $outside to 1>><<effects>>
<<if $athletics gte random(1, 1200)>>
	You shake off their hands and run. You hear a whistle blow behind you. The <<person3>><<person>> tries to stop you, <span class="green">but you jump over <<his>> outstretched leg.</span> You outpace the officers. A taser bolt <span class="green">sails harmlessly by your side</span> and they soon give up.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<<else>>
	You shake off their hands and run. You hear a whistle blow behind you. The <<person3>><<person>> tries to stop you, <span class="green">but you jump over <<his>> outstretched leg.</span> You are outpacing the officers when a taser bolt <span class="red">hits you square in the back.</span>
	<br><br>
	Your muscles violently lock up, sending you slamming to the ground, jerking helplessly. The officers soon tower over you.
	<br>
	"Nice. Is that full power?" the <<person1>><<person>> asks.
	<br>
	The <<person2>><<person>> checks <<his>> taser. "Half."
	<br>
	"Well turn it up," <<person1>><<he>> whispers. "Let's see the little <<if $player.gender_appearance is "m">>shit<<else>>bitch<</if>> squirm!"
	<br>
	"The suspect is still resisting!" The <<person2>><<person>> says loudly.
	<br><br>
	The burning sharply intensifies. Your muscles jerk painfully
	<<if $watersportsdisable is "f">>
		and you almost immediately piss yourself, warm urine
		<<if $worn.genitals.type.includes("chastity")>>
			trickling through your rugged $worn.genitals.name
		<</if>>
		<<if $worn.under_lower.type.includes("naked")>>
			<<if $worn.lower.type.includes("naked")>>
				squirting in all directions.
			<<else>>
				soaking through your $worn.lower.name.
				<<set $lowerwet to 200>><<effects>>
			<</if>>
		<<else>>
			soaking quickly through your $worn.under_lower.name and $worn.lower.name.
			<<set $underlowerwet to 200>><<set $lowerwet to 200>><<effects>>
		<</if>>
		You are
	<<else>>
		and you become
	<</if>>
	aware of the <<person3>><<person>> laughing as <<his>> camera clicks.
	<br><br>
	The burning stops. You feel a clammy breath on your ear.
	<br>
	"Look at you!" the <<person1>><<person>> breathes. "That must be humiliating. Still wanna fight this?"
	<br><br>
	<<link [["N-No. I submit."|Street Police Extreme]]>><<set $phase to 0>><</link>>
	<br>
	<<link [[Run|Street Police KO]]>><<crimeup 300>><</link>><<crime>>
	<br>
<</if>>