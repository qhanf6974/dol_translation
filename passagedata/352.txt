<<effects>>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $eden_shoot is 1>>
	<<set $eden_shoot to 2>>

	Eden takes you outside, lighting an oil lamp before <<he>> does. <<He>> gestures for you to wait beneath the porch, then disappears behind the cabin. <<He>> reappears a few moments later, carrying a target shaped like a deer. <<He>> sets it up on a patch of unused earth a couple of dozen steps away.
	<br><br>

	<<He>> talks you through how to operate the gun before letting you touch it. You think you get the gist. <<He>> passes it to you, and you point it at the target. "Not like that," <<he>> says, sidling close behind you. <<His>> arms wrap around your shoulders, and <<he>> takes your hands in <<his>> own to better demonstrate. "Hold it like this."
	<br><br>

	<<link [[Focus|Eden Shoot First Focus]]>><</link>>
	<br>
	<<link [[Rub your ass against Eden's crotch|Eden Shoot Rub]]>><<npcincr Eden lust 5>><<arousal 600>><</link>><<glust>><<garousal>>
	<br>
<<elseif $danger gte (9900 - $allure) and $eventskip is 0>>
	You practise shooting outside under Eden's instruction. <<He>> takes a hands-on approach. "Not like that," <<he>> says, sidling close behind you. <<His>> arms wrap around your shoulders, and <<he>> takes your hands in <<his>> own to better demonstrate. "Hold it like this."
	<br><br>

	<<link [[Focus|Eden Shoot Focus]]>><</link>>
	<br>
	<<link [[Rub your ass against Eden's crotch|Eden Shoot Rub]]>><<npcincr Eden lust 5>><<arousal 600>><</link>><<glust>><<garousal>>
	<br>
<<else>>
	<<set $stat_shoot += random(20, 40)>>
	You practise shooting outside under Eden's instruction. The deer-shaped target gains some new holes before you're done.
	<br><br>
	<<if $hour isnot 0>>
		<<link [[Keep practising (0:30)|Eden Shoot]]>><<pass 30>><<stress -6>><</link>><<lstress>>
		<br>
	<</if>>
	<<link [[Go inside|Eden Cabin]]>><</link>>
<</if>>