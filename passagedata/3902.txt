<<set $outside to 0>><<set $location to "school">><<effects>>

<<if $worn.face.type.includes("gag")>>
	<<set $worn.face.type.push("broken")>>
	<<faceruined>>
	You mumble through the gag and rock the chair. The <<person3>><<person>> notices you at last. <<He>> rips the gag off your mouth and cuts through your bonds.
	<br><br>
<</if>>
"I did it," you say. "I stole the cake."
<br><br>

The <<person3>><<person>> doesn't look convinced. "Fine," <<he>> sighs. "You two, take <<pher>> in." <<He>> turns to Kylar. "You'd best keep your nose clean."
<br><br>

Kylar watches as you are led away. <<person1>><<He>> weeps harder.
<<lllksuspicion>><<npcincr Kylar rage -50>>
<br><br>
<<endcombat>>
<<link [[Next (0:30)|Kylar Basement Car]]>><<pass 30>><<clothesontowel>><</link>>
<br>