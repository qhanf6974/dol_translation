<<effects>><<set $outside to 1>>

<<npc Remy>><<generate2>><<person2>>
You refuse to leave your cell. The farmhand who opened it, a <<personcomma>> steps inside and reaches for your collar, but you back away.
<br><br>

"Remy!" <<he>> shouts. "We got a stubborn'un."
<br><br>
<<person1>>
<<if $livestock_obey gte 80>>
	"What's gotten into you," Remy says, shaking <<his>> head. "I thought you were a good <<girl>>" <<He>> strokes <<his>> whip with <<his>> thumb as two more farmhands enter the cell. Together they have little trouble pulling you out. They push you with the rest of the herd in their march from the barn. Farmhands flank the group, making sure to drive you in the right direction.
<<elseif $livestock_obey lte 20>>
	"Figures," Remy says. "You can't resist the opportunity to be a nuisance." <<He>> strokes <<his>> whip with <<his>> thumb as two more farmhands enter the cell. Together they have little trouble pulling you out. They push you with the rest of the herd in their march from the barn. Farmhands flank the group, making sure to drive you in the right direction.
<<else>>
	"What's the matter?" Remy asks, shaking <<his>> head. "Or are you just being stubborn for the sake of it?" <<He>> strokes <<his>> whip with <<his>> thumb as two more farmhands enter the cell. Together they have little trouble pulling you out. They push you with the rest of the herd in their march from the barn. Farmhands flank the group, making sure to drive you in the right direction.
<</if>>
<br><br>

<<if $weather is "rain">>
	You emerge outside, into the rain and mud.
<<elseif $weather is "clear">>
	You emerge outside, into sunlight.
<<else>>
	You emerge outside.
<</if>>

Remy hauls walks ahead, where a large <<livestock_horse>> awaits <<himstop>> <<He>> climbs atop it, and spurs it behind the cattle as <<he>> unclips <<his>> whip. A menacing crack hastens the driven cattle.
<br><br>

You follow a windy lane through a small valley, and into a great field. The perimeter is marked by an electric fence. Remy stops by the entrance, sitting on <<his>> steed and observing the cattle as they pass. <<His>> eyes linger on you.
<br><br>
Two farmhands pull the gate shut.
<br><br>

<<hunger 1000>>
<<link [[Next|Livestock Field]]>><<endevent>><</link>>
<br>