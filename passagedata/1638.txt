<<set $outside to 0>><<set $location to "cafe">><<effects>>
<<if $chef_speech is "bailey">>
	<<npc Bailey>><<person1>>
	The party winds down after a few hours. You head to the exit, but Bailey intercepts you.
	<br><br>
	"You're a brave little shit," <<he>> says. <<He>> grasps your shoulders and turns you to face the guests, now drunkenly shuffling out the cafe. "They all want a piece of orphan ass now that you've screamed about it. You've made things difficult."
	<<gstress>><<stress 6>>
	<br><br>
	<<He>> releases you, and walks away.
	<br><br>
	<<endevent>>
	<<link [[Next|Chef Opening End 2]]>><</link>>
	<br>
<<elseif $chef_speech is "sam">>
	<<npc Sam>><<person1>>
	The party winds down after a few hours. You head to the exit, where Sam is seeing off guests. <<He>> beams at you.
	<br><br>
	"No one's ever said such nice things about me," <<he>> confides. "Thank you so much." <<He>> wipes away a tear with the back of <<his>> hand.
	<br><br>
	"I forgot to mention," <<he>> adds. "We're bumping up the price of buns to <span class="gold">£150</span>.
	<<set $bun_value to 15000>>
	It's expensive, but they're worth it. And we have extra staff to pay."
	<br><br>
	<<He>> waves you goodbye, and you exit onto Cliff Street.
	<br><br>
	<<endevent>>
	<<link [[Next|Cliff Street]]>><<set $eventskip to 1>><</link>>
	<br>
<<elseif $chef_speech is "truth">>
	<<npc Sam>><<person1>>
	The party winds down after a few hours. You head to the exit. Sam intercepts you.
	<br><br>
	"I wasn't expecting... that", <<he>> says. "In your speech. I know I said you could say anything, but that didn't cross my mind." <<He>> shakes <<his>> head. "Still, people took the joke well. No damage done. Thank you for everything."
	<br><br>
	"I forgot to mention," <<he>> adds. "We're bumping up the price of buns to <span class="gold">£150</span>.
	<<set $bun_value to 15000>>
	It's expensive, but they're worth it. And we have extra staff to pay."
	<br><br>
	<<He>> waves you goodbye, and you exit onto Cliff Street.
	<br><br>
	<<endevent>>
	<<link [[Next|Cliff Street]]>><<set $eventskip to 1>><</link>>
	<br>
<<else>>
	<<npc Sam>><<person1>>
	The party winds down after a few hours. You head to the exit, where Sam is seeing off guests.
	<br><br>
	"I forgot to mention," <<he>> says. "We're bumping up the price of buns to <span class="gold">£150</span>.
	<<set $bun_value to 15000>>
	It's expensive, but they're worth it. And we have extra staff to pay."
	<br><br>
	<<He>> waves you goodbye, and you exit onto Cliff Street.
	<br><br>
	<<endevent>>
	<<link [[Next|Cliff Street]]>><<set $eventskip to 1>><</link>>
	<br>
<</if>>