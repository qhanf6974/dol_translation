<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
<<if $rng gte 81>>
	You skip rocks across the lake's surface with several others. Someone's skips six times.
	<br><br>
<<elseif $rng gte 61>>
	You help build a sandcastle, until it collapses under its own weight. You have a nice time regardless.
	<br><br>
<<elseif $rng gte 41>>
	You play catch with a Frisbee.
	<<gathletics>><<athletics 3>><<physique 3>>
	<br><br>
<<elseif $rng gte 21>>
	<<if $cool gte 160>>
		You chat with some students you recognise from your classes. A <<generates1>><<person1>><<person>> tries to impress you by entering the forest alone. <<He>> doesn't get far before a strange noise scares <<him>> back.
		<<ltrauma>><<trauma -3>><<endevent>>
		<br><br>
	<<elseif $cool gte 40>>
		You chat with some students you recognise from your classes.
		<br><br>
	<<else>>
		You try to chat with some students you recognise from your classes. You're mostly ignored.
		<<gtrauma>><<trauma 3>>
		<br><br>
	<</if>>
<<else>>
	An impromptu game of tag starts.
	<<if $athletics gte 800>>
		No one can catch you.
		<<gcool>><<status 1>><<gathletics>><<athletics 3>><<physique 3>>
		<br><br>
	<<elseif $athletics gte 400>>
		You give as good as you get.
		<<gathletics>><<athletics 3>><<physique 3>>
		<br><br>
	<<else>>
		Once "it" you struggle to catch anyone.
		<<lcool>><<status -10>><<gathletics>><<athletics 3>><<physique 3>>
		<br><br>
	<</if>>
<</if>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $hour is 20>>
	It's getting late, and the shadows of the forest are lengthening. People are preparing to return to town. No one wants to be left to travel alone.
	<br><br>
	<<if $exposed lte 0>>
		<<link [[Go with them (0:30)|Lake Return Journey]]>><<set $foresthunt to 0>><<pass 30>><</link>>
		<br>
	<</if>>
	<<link [[Stay|Lake Shore]]>><</link>>
	<br>
<<elseif $danger gte (9900 - $allure)>>
	<<if $cool gte 160>>
		A <<generates1>><<person1>><<person>> approaches you. "H-hi," <<he>> says. "Umm, c-can I ask you something?" <<He>> takes a deep breath. "Will you kiss me? They said they'd leave me alone if I could get a kiss from you." <<He>> looks down and blushes. The teenagers <<hes>> referring to watch from a rock not far away. You've seen them bully the <<person>> before.
		<br><br>
		<<link [[Kiss|Lake Kiss]]>><<status -10>><</link>><<promiscuous1>><<lcool>>
		<br>
		<<link [[Refuse|Lake Refuse]]>><</link>>
		<br>
		<<link [[Laugh|Lake Laugh]]>><<status 1>><</link>><<gcool>>
		<br>
		<<else>>
		A <<generatey1>><<person1>><<person>> tackles you behind a rock and gropes you.
		<br><br>
		<<if $promiscuity gte 15>>
			<<link [[Grope back|Lake Hang Sex]]>><<set $sexstart to 1>><</link>><<promiscuous2>>
			<br>
		<</if>>
		<<link [[Shove away|Lake Hang Shove]]>><</link>>
		<br>
	<</if>>
<<else>>
	<<link [[Hang out (0:30)|Lake Hang]]>><<pass 30>><<stress -6>><<status 1>><</link>><<gcool>><<lstress>>
	<br>
	<<link [[Stop|Lake Shore]]>><</link>>
	<br>
<</if>>