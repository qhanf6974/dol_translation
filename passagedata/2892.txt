<<set $outside to 1>><<set $location to "school">><<schooleffects>><<effects>>

<<if $schoolstate is "lunch" or $schoolstate is "morning" or $schoolstate is "afternoon">>
You lean back on the stump. You close your eyes and listen to the murmuring of students in the playground.
<<else>>
You lean back on the stump. You close your eyes and listen to the chirping of birds.
<</if>>
<br><br>

<<if $danger gte (9900 - $allure)>>
<<eventsschoolstump>>
<<else>>
	<<if $schoolstate isnot "late">>
	<<link [[Keep relaxing (0:10)|School Stump]]>><<pass 10>><<stress -1>><<tiredness -1>><</link>><<ltiredness>><<lstress>>
	<br>
	<</if>>
<<link [[Stop|School Rear Playground]]>><</link>>
<br>
<</if>>