<<effects>>
<<if $alarm is 1>>
	Your scream alerts the pair.
	<br><br>
<</if>>
At the sound of their approach, the dog ceases its adventure and bounds away.
<br><br>
<<endcombat>>
<<generatey1>><<generatey2>>Before you can recover, you are discovered by a <<person1>><<person>> and a <<person2>><<personstop>>
<br><br>
<<set $danger to random(1, 10000)>><<set $dangerstreet to 0>>
<<if $danger gte (9900 - ($allure * 3))>>
	<<set $dangerstreet to random(1, 100)>>
	They take advantage of your vulnerability.
	<br><br>

	<<link [[Next|Residential Pair]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	Seeing you in distress, the <<person1>><<person>> and <<person2>><<person>> help you to your feet.
	<br><br>
	<<set $rescued += 1>>
	<<clothesontowel>>
	The <<person1>><<person>> looks concerned. "Are you OK?" <<tearful>> you nod and thank them for their concern, before parting ways.
	<br><br>
	<<residentialeventend>>
<</if>>