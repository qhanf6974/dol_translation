<<set $outside to 1>><<set $location to "park">><<effects>><<set $bus to "park">>

"I have a secret," you purr. The <<person>> frowns.
<<if $worn.upper.type.includes("naked") and $worn.lower.type.includes("naked") and $worn.under_lower.type.includes("naked")>>
"I'm not wearing anything."
<<elseif $worn.upper.type.includes("naked") and $worn.lower.type.includes("naked")>>
"I'm wearing nothing but underwear."
<<elseif $worn.lower.type.includes("naked") and $worn.under_lower.type.includes("naked")>>
"I'm naked from the waist down."
<<else>>
"I'm wearing nothing over my underwear."
<</if>>
<<He>> gasps as you break into a run.
<<exhibitionism2>>

You glance back and see <<him>> leaning across the hedge to get a peek.
<br><br>

<<endevent>>
<<link [[Next|Park]]>><<set $eventskip to 1>><</link>>
<br>