<<set $outside to 1>><<effects>>
You pull away from the tentacle, hoping to shake it off.
<<if $worn.lower.skirt is 1 and !$worn.under_lower.type.includes("naked") and !$worn.genitals.type.includes("chastity")>>
	<<set $worn.under_lower.integrity -= 30>>
	<<if $worn.under_lower.integrity lte 0>>
		It tears your $worn.under_lower.name free as it retreats into the manhole, still clutching the ruined fabric.
		<br><br>
		You're left pantiless in the middle of the street. You press you hand between your legs and hold your $worn.lower.name down, suddenly self-conscious.
		<br><br>
		<<underlowerruined>><<exposure>>
		<<destinationeventend>>
	<<else>>
		It retreats into the manhole, leaving your $worn.under_lower.name torn but intact.
		<br><br>
		<<destinationeventend>>
	<</if>>
<<else>>
	<<set $worn.lower.integrity -= 30>>
	<<if $worn.lower.integrity lte 0>>
		It tears your $worn.lower.name free as it retreats into the manhole, still clutching the ruined fabric.
		<br><br>
		You're left bottomless in the middle of the street. <<covered>>
		<br><br>
		<<lowerruined>><<exposure>>
		<<destinationeventend>>
	<<else>>
		It retreats into the manhole, leaving your $worn.lower.name torn but intact.
		<br><br>
		<<destinationeventend>>
	<</if>>
<</if>>