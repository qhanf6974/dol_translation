<<set $outside to 0>><<set $location to "wolf_cave">><<effects>>

<<if $wolfpacktrust gte 12>>
The wolves look at you as you try to leave, but make no move to stop you. It seems they trust you enough to let you go.
<<lstress>>

<br><br>
<<link [[Next|Forest]]>><<set $stress -= 1000>><<set $eventskip to 1>><<set $forest to 80>><<set $eventskip to 1>><</link>>
<br>
<<elseif $wolfpackfear gte 12>>
The wolves growl at you as you try to leave, but make no move to stop you. It seems they fear you too much to try and stop you.
<<lstress>>

<br><br>
<<link [[Next|Forest]]>><<set $stress -= 1000>><<set $eventskip to 1>><<set $forest to 80>><<set $eventskip to 1>><</link>>
<br>

<<else>>
The wolves immediately stir as you try to leave, and block your path. The pack stares at you, growling a warning.
<<stress 6>><<gstress>>
<br><br>
<<link [[Try to escape anyway|Forest Wolf Cave Rape]]>><<set $molestationstart to 1>><</link>>
<br>
<<link [[Back down|Forest Wolf Cave]]>><</link>>
<br>
<</if>>