<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	The <<person>> walks away, leaving you to slump to the ground.
	<br><br>
	<<tearful>> you struggle to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Cliff Street]]>><<set $eventskip to 1>><</link>>
<<elseif $enemyhealth lte 0>>
	You shove <<him>> against the wall and run out into the street.
	<<set $chef_sus += 10>><<ggsuspicion>>
	<br><br>
	<<tearful>> you look behind you. <<He>> doesn't follow.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Cliff Street]]>><<set $eventskip to 1>><</link>>
<<else>>
	<<set $rescued += 1>>/*Unnecessary if rescue is impossible*/
	"Everything okay down there?" a voice shouts from the end of the alley.
	<br><br>
	"F-It's fine," the <<person>> shouts back, adjusting <<his>> clothes. <<He>> gives you an evil look as <<he>> leaves.
	<<set $chef_sus += 10>><<ggsuspicion>>
	<br><br>
	<<tearful>> you return to the street.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Cliff Street]]>><<set $eventskip to 1>><</link>>
<</if>>