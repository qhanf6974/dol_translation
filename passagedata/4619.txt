<<effects>>

You drop to your knees and crawl beneath it.

<<if $rng gte 51>>

	<span class="red">You're halfway across when the van begins to move.</span> You stay still to avoid the wheels as you are once more exposed to daylight.
	<br><br>

	<<link [[Next|Road Ex Ground]]>><</link>>
	<br>

<<else>>

	You hear the driver's door open above, then two pairs of feet land in front of you. The door shuts, and the driver walks to the back of the van.
	<br><br>

	You shuffle the rest of the way, climb to your feet, and dart down the nearby alley. You round a corner and come to a stop against a wall. Your whole body shakes.
	<br><br>

	<<link [[Next|Commercial alleyways]]>><</link>>
	<br>

<</if>>