<<set $outside to 0>><<set $location to "forest_shop">><<effects>>
"It suits you," you say. "You look like a scary vampire."
<br><br>
"Really?" <<he>> asks, smiling. <<He>> turns back to the mirror and strikes a menacing pose.
<br><br>
<<if $money gte 8000>>
	<<link [[Buy the costume for Robin|Robin Forest Vampire Buy]]>><<npcincr Robin love 3>><<set $money -= 8000>><<set $halloween_robin_costume to "vampire">><</link>><<gglove>>
	<br>
<<else>>
	You don't have enough money to buy the costume. (£80)
	<br><br>
<</if>>
<<link [[Ask Robin to try on the girl's witch costume|Robin Forest Witch]]>><</link>>
<br>
<<link [[Walk home|Robin Forest Shop Home]]>><</link>>
<br>