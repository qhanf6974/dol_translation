<<effects>>

<<npc Alex>><<person1>>
You call out for Alex. <<He>> responds a moment later, though <<hes>> too distant to make out the words.
<br><br>

"What do we have here?" <<he>> says as <<he>> arrives. You nod at the chicken. <<He>> looks up, and laughs. "How'd you get up there? Dumb bird."
<br><br>

You help <<him>> carry a ladder from the stable, and prop it up beside the chicken. Alex climbs, retrieves the rogue, and returns it to the coop.
<br><br>

<<link[[Next|Farm Work]]>><<endevent>><</link>>
<br>