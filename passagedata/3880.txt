<<set $outside to 0>><<set $location to "town">><<effects>>

You lean into <<him>> and sob. <<He>> takes you in <<his>> arms and holds you steady. You don't say anything, but after an hour you're feeling much better.
<<control 10>>
<br><br>
<<if $rng gte 81 and $NPCName[$NPCNameList.indexOf("Doren")].love gte 10>>
	<<He>> pulls away from you. "I have to get ready now." <<He>> pauses a moment. "You can come with me if you like. I'm just going for a jog."
	<br><br>
	<<link [[Go for a jog (0:30)|Doren Jog]]>><<npcincr Doren love 1>><<stress -12>><<trauma -6>><<pass 30>><<athletics 3>><</link>><<gathletics>><<ltrauma>><<lstress>>
	<br>
	<<link [[Leave|Barb Street]]>><<endevent>><</link>>
	<br>
<<else>>
	<<He>> pulls away from you. "I have to get ready now. People are expecting me. It was nice seeing you, you can come back tomorrow if you like."
	<br><br>
	<<endevent>>
	<<link [[Next|Barb Street]]>><</link>>
	<br>
<</if>>