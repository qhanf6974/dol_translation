<<effects>>

<<if $physique gte random(1, $physiquemax)>>
<<famescrap 1>>
You smack the <<person>> hard in the side of the face. <span class="green">You feel something come loose.</span> <<He>> releases you, and staggers away in pain.
<br><br>

	<<if $submissive lte 500>>
		<<link [[Humiliate|Street Bodywriting Humiliate]]>><<def 1>><<famescrap 5>><<trauma -6>><<stress -6>><</link>><<defianttext>><<ltrauma>><<lstress>>
		<br>
	<</if>>
	<<link [[Walk Away|Street Bodywriting Walk]]>><</link>>
	<br>

<<else>>

	You smack the <<person>> in the side of the face, <span class="red">but <<he>> doesn't flinch.</span> <<He>> pulls you into the alley and shoves you against a wall, hidden from the view of the street.
	<br><br>

	<<link [[Next|Street Bodywriting Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

<</if>>