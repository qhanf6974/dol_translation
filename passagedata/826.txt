<<effects>>

<<if $tending gte random(1, 500)>>
	You pat the <<farm_text_many cattle>>, making soothing noises all the while. They remain wary, <span class="green">but their panic abates, and their pacing stops.</span><<grespect>><<farm_cattle 1>><<ggtending>><<tending 9>><<set $farm_work.cattle_panic to 0>><<gfarm>><<farm_yield 1>>
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	You pat the <<farm_text_many cattle>>, making soothing noises all the while. <span class="red">It does nothing. You're not sure they even hear you.</span><<gtending>><<tending 2>>
	<br><br>
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<</if>>