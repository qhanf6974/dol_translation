<<widget "passouttentacleworld">><<nobr>>
	<<passout>>
	[[Everything fades to black...->Passout Tentacle World]]
<</nobr>><</widget>>

<<widget "tentacleworldintro">><<nobr>>

<<set $tentnorth to 0>>
<<set $tenteast to 0>>
<<if $rng gte 51>>
	<<set $tentportalnorth to either(-5, -4, 4, 5)>>
	<<set $tentportaleast to random(-5, 5)>>
<<else>>
	<<set $tentportalnorth to random(-5, 5)>>
	<<set $tentportaleast to either(-5, -4, 4, 5)>>
<</if>>

<</nobr>><</widget>>

<<widget "tentacleworldend">><<nobr>>

<<unset $tentacleadmire>>
<<unset $tentaclewolf>>
<<unset $tentacleangel>>
<<unset $tentaclefallenangel>>
<<unset $tentacleEntrance>>

<</nobr>><</widget>>

<<widget "tentaclewolf">><<nobr>>
<<if $wolfgirl gte 6 and $syndromewolves gte 1>>
	<<if $tentaclewolf is undefined>>
		<<set $tentaclewolf to 0>>
		You hear something familiar in the distance, but you aren't sure what it is. It breaks through the fog and reminds you of the forest.
		<<lstress>><<larousal>><<stress -6>><<arousal -6>>
		<br><br>
	<<elseif $tentaclewolf is 0>>
		<<set $tentaclewolf to 1>>
		You hear it again. It came from <<tentacledirection>>
		<<lstress>><<larousal>><<stress -6>><<arousal -6>>
		<br><br>
	<<elseif $tentaclewolf is 1>>
		<<set $tentaclewolf to 2>>
		The familiar sound is louder now. It sounds like howling. It's coming from <<tentacledirection>>
		<<lstress>><<larousal>><<stress -6>><<arousal -6>>
		<br><br>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "tentacledirection">><<nobr>>

<<if $tentportalnorth gt $tentnorth>>
	<<if $tentportaleast gt $tenteast>>
		the Northeast.
	<<elseif $tentportaleast lt $tenteast>>
		the Northwest.
	<<else>>
		the North.
	<</if>>
<<elseif $tentportalnorth lt $tentnorth>>
	<<if $tentportaleast gt $tenteast>>
		the Southeast.
	<<elseif $tentportaleast lt $tenteast>>
		the Southwest.
	<<else>>
		the South.
	<</if>>
<<else>>
	<<if $tentportaleast gt $tenteast>>
		the East.
	<<elseif $tentportaleast lt $tenteast>>
		the West.
	<<else>>
		nearby.
	<</if>>
<</if>>

<</nobr>><</widget>>