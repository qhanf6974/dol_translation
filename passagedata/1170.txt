<<effects>>

You bite into one of the apples. It's so sweet. Once started, there's no stopping you. You eat the entire basket, careful to not waste a single drop of juice. You lie back on the grass, relieved and satiated.
<br><br>
<<hunger -2000>>
<<link [[Next|Livestock Field]]>><<endevent>><</link>>
<br>