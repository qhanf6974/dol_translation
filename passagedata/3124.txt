You are alone in the women's restroom.
<br><br>
<<towelup>>
You find some towels. They make a poor substitute for actual clothing but it'll keep you covered. You need to find something more solid as soon as you can.
<br><br>

[[Leave through main door|Shopping Centre]]
<br>
<<link [[Leave through escape door|Commercial alleyways]]>><<set $eventskip to 1>><</link>>
<br>