<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "drainexit">>
You are within the storm drain system, close to where it flows out into the sea. Water rushes down the centre, flanked by a thin walkway on each side.
<br><br>
<<if $stress gte 10000>>
	<<passoutdrain>>
<<else>>
	<<set $rng to random(1, 100)>>
	<<if $weather is "rain" and $rng gte 75 and $eventskip is 0>>
		<<eventsdrain>>
	<<elseif $rng gte 92 and $eventskip is 0>>
		<<eventsdrain>>
	<<else>>
		<<if $scienceproject is "ongoing" and $sciencelichendrain is 0>>
			<<link [[Examine Lichen|Drain Lichen]]>><<set $sciencelichendrain to 1>><</link>>
			<br>
		<</if>>
		<<industrialdrain>>
		<br><br>
	<</if>>
<</if>>

<<set $eventskip to 0>>