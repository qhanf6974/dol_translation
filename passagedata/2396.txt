<<set $outside to 1>><<set $location to "park">><<effects>><<set $bus to "park">>
	<<set $rng to random(1, 8)>>
	<<if $rng is 1>>
		<<set _exercise to "do squats">>
	<<elseif $rng is 2>>
		<<set _exercise to "perform jumping jacks">>
	<<elseif $rng is 3>>
		<<set _exercise to "take lunges">>
	<<elseif $rng is 4>>
		<<set _exercise to "do aerobics">>
	<<else>>
		<<set _exercise to "exercise">>
	<</if>>

<<if $daystate is "night">>
	<<if $weather is "rain">>
	You _exercise in the deserted park, stepping in puddles as cold rain pours.
	<<else>>
	You _exercise in the deserted park. The cool night air caresses your skin.
	<</if>>
	<<physique 1>>
	<br><br>
	<<link [[Next|Park]]>><</link>>
	<br>
<<else>>
	<<if $weather is "rain">>
	You _exercise in the nearly-deserted park.
	<<else>>
	You _exercise in the park. It feels good to feel the air and sunshine on your skin.
	<</if>>
	<<physique 1>>

	<<set $rng to random(1, 100)>>
	<<if $rng lte 20>>
		<br><br>
		<<generate1>><<person1>>
		A <<person>> approaches you. "I noticed you were exercising in $worn.feet.name," <<he>> chuckles. "That looks tiring. How about you take a break and I give you a foot massage?" <<he>> asks.
		<br>
		<<if $awareness lte 100>>You're dumbfounded why the <<person>> would offer such a thing, but you appreciate <<his>> kindness.
		<<elseif $awareness lte 300>>The <<person>> seems quite interested in your feet.
		<<else>>The <<person>> has a foot fetish!
		<</if>>
		<br>
		<br>
		<<link [[Accept the foot massage|Park Exercise Heels Massage]]>><<tiredness -5>><<stress -5>><</link>><<ltiredness>><<lstress>>
		<br>
		<<link [[No thanks|Park Exercise Heels No Massage]]>><<control 5>><</link>><<gcontrol>>
	<<else>>
		<br><br>
		<<link [[Next|Park]]>><</link>>
		<br>
	<</if>>
<</if>>