<<effects>>

You have a pleasant time chatting with the <<person>>, until <<he>> leans over. "How about we find somewhere secluded?" <<he>> whispers. "They won't notice we're gone."
<br><br>

<<link [[Accept|Riding School Lesson Accept]]>><</link>><<promiscuous1>>
<br>
<<link [[Refuse|Riding School Lesson Ignore]]>><</link>>
<br>