<<set $outside to 1>><<set $location to "town">><<effects>>
<<flaunting>> you sneak through the orphanage. You move between hiding places, heading towards the front door. You stop and wait for people to pass when you hear footsteps, lest you be caught.
<br><br>

You open the back door, and peek out. You take one step, then another, then close the door behind you. You're outside. <<covered>>
<br><br>

<<link [[Next|Garden Fence Undies Day]]>><<pass 5>><</link>>
<br>