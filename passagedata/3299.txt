<<set $outside to 1>><<effects>>
<<if $athletics gte random(1, 1200)>>
	You shake off their hands and run. You hear a whistle blow behind you. The <<person3>><<person>> tries to stop you, <span class="green">but you jump over <<his>> outstretched leg.</span> You outpace the officers, and they soon give up.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<<else>>
	You shake off their hands and run. You hear a whistle blow behind you. The <<person3>><<person>> puts out <<his>> leg, <span class="red">and you trip.</span>
	<br><br>
	<<link [[Next|Street Police Full]]>><<set $phase to 0>><</link>>
	<br>
<</if>>