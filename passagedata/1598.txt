<<set $outside to 0>><<set $location to "cafe">><<effects>>

You release <<his>> arm. <<He>> gives you a grateful look before disappearing out the back door.
<br><br>

You pay at the cash register on <<his>> behalf before returning to work.
<br><br>

<<endevent>>

<<chefwork>>