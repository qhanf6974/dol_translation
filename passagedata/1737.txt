<<set $outside to 0>><<set $location to "compound">><<effects>>

You take the crystal. It's warm to the touch.
<<if $trash_key isnot 1>>
<<set $trash_key to 1>>You grab the key as well. It's not as heavy as it looks. <i>It must fit a large lock somewhere.</i>
<</if>>
<br><br>
Finding your way out is much easier than getting here.
<br><br>
<<set $compoundsmoke to 1>>
<<set $blackmoney += 50>>
<<set $compoundalarm += 1>>
<<crimeup 50>>
<<link [[Next|Elk Compound]]>><</link>>
<br>