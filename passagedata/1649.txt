<<effects>>
<<pass 60>>
You ignore the <<personcomma>> instead staring out the window. You catch a glimpse of the forest as the car crests a hill. "A <<girl>> of few words," <<he>> says. "That'll suit just fine where you're going."
<br><br>
After what feels like an hour, you turn down a thin lane and pull to a stop outside a barn.
<br><br>
<<link [[Next|Chef Blackmail Livestock 2]]>><</link>>
<br>