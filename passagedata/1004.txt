<<effects>>

<<if $physique gte 16000>>
	You pour feed into a couple of buckets, and traipse across the field. The buckets are heavy, <span class="green">but no trouble for someone of your strength.</span><<set $farm_work.pigs_fed += 1>><<physique 2>>
	<br><br>
<<else>>
	You pour feed into a couple of buckets, and traipse across the field. Your arms ache by the time you arrive at the sty.<<physique 2>>
	<br><br>
<</if>>



You empty the buckets in the troughs. 

<<if $farm_work.pigs_fed gte 6>>
	The <<farm_text_many pig>> eat the feed with gusto, but by the end<span class="green"> lie on their sides, content.</span>
<<else>>
	<span class="blue">The <<farm_text_many pig>> devour the feed. Seems they're still hungry.</span>
<</if>>
<br><br>

<<set $danger to random(1, 10000)>>
<<if $danger gte (9900 - $allure) and $worn.lower.name isnot "naked">>
	<<if $farm.beasts.pigs lte -20 and $bestialitydisable is "f" and $rng gte 51>>
		<<if $farm_work.pig.monster is true>>
			Something catches your hair as you turn away from the trough. One of the <<farm_text pig>>s has reached over the fence. "You're not finished here," <<farm_he pig>> says. <<farm_He pig>> tries to pull you inside.			
		<<else>>
			Something catches your hair as you turn away from the trough. One of the <<farm_text pig>>s has reached over the fence. It tries to pull you inside. 
		<</if>>
		<br><br>
		
		<<link [[Scold|Farm Pigs Feed Extreme Scold]]>><</link>><<tendingdifficulty 1 1100>>
		<br>
		<<link [[Call for help|Farm Pigs Feed Extreme Help]]>><<npcincr Alex dom 1>><<farm_pigs -1>><</link>><<gdom>><<lrespect>>
		<br>
	<<else>>
		<<if $farm_work.pig.monster is true>>
			Something catches your $worn.lower.name as you turn away from the trough. One of the <<farm_text pig>>s has stuck its head through the fence, and grasps your clothing in its mouth.
		<<else>>
			Something catches your $worn.lower.name as you turn away from the trough. One of the <<farm_text pig>>s has stuck its snout through the fence, and grasps your clothing in its mouth.
		<</if>>
		<br><br>
		
		<<link [[Scold|Farm Pigs Feed Scold]]>><</link>><<tendingdifficulty 1 600>>
		<br>
		<<link [[Call for help|Farm Pigs Feed Help]]>><<npcincr Alex dom 1>><<farm_pigs -1>><</link>><<gdom>><<lrespect>>
		<br>
	<</if>>
<<else>>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>

<</if>>