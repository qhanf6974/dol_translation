<<effects>>

<<if $worn.under_lower.type.includes("naked")>>
	<<if $worn.lower.skirt is 1>>
		You lift your skirt at the thigh, high enough to show your waistline, confirming that you're not wearing underwear beneath. You cover yourself quickly.<<exhibitionism1>>

		"Nice," Whitney says, prompting River to turn and glare.
		<br><br>
		<<unset $whitneypantiesmaths>>
	<<else>>
		You pull your $worn.lower.name down a little, confirming that you're not wearing underwear beneath. You cover yourself quickly.<<exhibitionism1>>

		"Nice," Whitney says, prompting River to turn and glare.
		<br><br>
		<<unset $whitneypantiesmaths>>
	<</if>>
<<elseif $whitneypantiescheck is 1>>
	<<if $worn.lower.skirt is 1>>
		You lift your skirt at the thigh, high enough to show your waistline, showing your $worn.under_lower.name beneath. You cover yourself quickly.<<exhibitionism1>>

		Whitney glares at you, but doesn't say anything. This won't be the end of it.
		<br><br>

		<<set $whitneypantiesmaths to "seen">>
	<<else>>
		You pull your $worn.lower.name down a little, showing your $worn.under_lower.name beneath. You cover yourself quickly.<<exhibitionism1>>

		Whitney glares at you, but doesn't say anything. This won't be the end of it.
		<br><br>

		<<set $whitneypantiesmaths to "seen">>
	<</if>>
<<else>>
	<<if $worn.lower.skirt is 1>>
		You lift your skirt at the thigh, high enough to show your $worn.under_lower.name. You cover yourself quickly.<<exhibitionism1>>

		"Nice," Whitney says, prompting River to turn and glare.
		<br><br>

	<<else>>
		You pull your $worn.lower.name down a little, low enough to show your $worn.under_lower.name. You cover yourself quickly.<<exhibitionism1>>

		"Nice," Whitney says, prompting River to turn and glare.
		<br><br>

	<</if>>
<</if>>

<<link [[Next|Maths Lesson]]>><<set $eventskip to 1>><</link>>
<br>