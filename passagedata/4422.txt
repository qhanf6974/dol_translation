<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewersindustrial">>
<<set $sewersevent -= 1>>
You are in the old sewers. A canal runs past, disappearing into the dark. Much of the ceiling and walls have collapsed, almost damming the water. The current looks violent.
<br><br>
<<if $sewerschased is 1>>
	There's a ladder leading up to the town's drain system, but it's retracted and out of reach.
	<br><br>
<<else>>
	There's a ladder leading up to the town's drain system.
	<br><br>
<</if>>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
	<</if>>
	<<link [[Algae-coated tunnel (0:05)|Sewers Algae]]>><<pass 5>><</link>>
	<br>
	<<link [[Crumbling tunnel (0:05)|Sewers Rubble]]>><<pass 5>><</link>>
	<br>
	<<link [[Swim against the current (0:05)|Sewers Industrial Swim]]>><<pass 5>><<tiredness 1>><</link>><<swimmingdifficulty 1 800>><<gtiredness>>
	<br>
	<<link [[Swim with the current (0:01)|Sewers Commercial]]>><<pass 1>><<water>><</link>>
	<br><br>
	<<if $sewerschased isnot 1>>
		<<link [[Climb out of the sewers|Industrial Drain]]>><<sewersend>><</link>>
		<br>
	<</if>>
<</if>>
<<set $eventskip to 0>>