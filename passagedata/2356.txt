<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>"This one might be younger than me," Winter laughs. You assume <<hes>> joking. "I'm not surprised you found it in the countryside. I hear of some impressive collections."
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>
<br>