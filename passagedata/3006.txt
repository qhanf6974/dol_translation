<b>JANET:</b>
<br>
"Oh Raul, mine own Raul!<span class="black"> (Oh Raul, my Raul!)</span>
<br>
Curs'd our fate!<span class="black"> (Cursed our fate!)</span>
<br>
I loveth thee with all mine own heart.<span class="black"> (I love you with all my heart.)</span>
<br>
But we shalt not beest togeth'r.<span class="black"> (But we shall not be together.)</span>
<br>
Mine own fath'r that gent shall nev'r grant us his blessing."<span class="black"> (My father he will never grant us his blessing.)</span>
<br><br>
<b>RAUL:</b>
<br>
"Mine own belov'd Janet<span class="black"> (My beloved Janet)</span>
<br>
We doth not needeth his p'rmission any m're.<span class="black"> (We do not need his permission any more.)</span>
<br>
Moon shall beest our augurer.<span class="black"> (Moon will be our priest.)</span>
<br>
And the stars shall singeth us a wedding song tonight!"<span class="black"> (And the Stars will sing us a wedding song tonight!)</span>
<br><br>
<b>JANET:</b>
<br>
"Nay! What art thee declaring!<span class="black"> (No! What are you saying!)</span>
<br>
Passeth me mine own coat.<span class="black"> (Pass me my coat.)</span>
<br>
I needeth to seeth that gent anon.<span class="black"> (I need to see him now.)</span>
<br>
I'll wend anon and begeth that gent to heareth me."<span class="black"> (I'll go now and beg him to hear me.)</span>
<br><br>
<i>She gets up.</i>
<br><br>
<<link [[Continueth|ScarletBook7nay_page2]]>><<arousal 1200>><</link>>