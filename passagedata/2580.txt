<<set $outside to 0>><<set $location to "town">><<effects>>
Doren drives to Barb street and pulls up outside the police station. "I know this is hard for you, but you need to tell them what you told me." You leave the car and enter the station.
<br><br>
<<generate3>><<person3>>A <<person>> sits at the desk, wearing an officer's uniform. <<He>> doesn't glance up as you enter. Doren knocks on the surface, hard enough that a pencil rolls onto the floor. <<person1>>"We've got a crime to report," <<he>> says. "Maybe several."
<br><br>
The officer looks up. "There's a queue," <<person3>><<he>> says.
<br><br>
Doren looks around the empty room. "Doesn't look like a queue to me."
<br><br>
"A backlog then," the officer replies. "We can't help you for three years."
<br><br>
"Three years!?" Doren slams <<person1>><<his>> fist into the desk, knocking over a mug this time. "This <<girl>> was ra..." <<he>> catches <<himselfstop>> "<<pShe>> was attacked. More than once. You're telling me you can't do anything?"
<br><br>
"We can. In three years," <<person3>><<he>> types something into <<his>> computer. "We already have you on record. We'll contact you when we're ready to take your report. Have a nice day."
<br><br>
<<link [[Next|Doren Intro]]>><</link>>
<br>