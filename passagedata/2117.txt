<<effects>>
<<pass 2>>
You stay still, and listen to the argument at the door. It becomes more heated, until the stranger threatens to call the police. The <<person2>><<person>> slams the door in response. <<He>> returns to the living room.
<br><br>
The pair blame each other for the mishap, and spend a few moments arguing before deciding they can't risk keeping you here. They shove you out the back door, and then a gate, leading to a path running behind the garden.
<br><br>
"You'll pay for this," the <<person1>><<person>> says as you stumble. "Good luck getting home with your arms tied." <<He>> slams the gate shut.
<br><br>
<<tearful>> you follow the path, and emerge near Harvest Street.
<br><br>
<<endevent>>
<<link [[Next|Harvest Street]]>><<set $eventskip to 1>><</link>>
<br>