<<effects>><<set $outside to 0>>

<<generatey1>><<generatey2>><<person1>>You wake up lying on your back, naked in the dunes. You hear a voice, "You're awake." You hasten to your feet and see the speaker, a <<personstop>> A <<person2>><<person>> stands next to <<person1>><<himstop>>
<br><br>

<<if $loweroff is 0 and $upperoff is 0 and $underloweroff is 0 and $underupperoff is 0>>
"You must be a huge slut, to be wandering around like that. Though I do feel bad about leaving you in such a state. There are some nasty people around."
<br><br>

<<person2>>The <<person>> interjects, "I know! We'll lend you some towels," <<he>> produces some thin cables from <<his>> coat pocket "if you let us dress you first."
<br><br>

<<person1>> The <<person>> seems taken with the idea, "It's your choice. If you don't want to play along, we'll leave you to be raped. You'd probably get off from it anyway."
<br><br>

<<else>>

You see your <<if $upperoff isnot 0>>$upperoff <<elseif $loweroff isnot 0>>$loweroff <<elseif $underupperoff isnot 0>>$underupperoff <<else>>$underloweroff <</if>>in <<his>> hand. <<He>> twirls it playfully, "Finders keepers. Though I do feel bad about leaving you in such a state. There are some nasty people around."
<br><br>

<<person2>>The <<person>> interjects, "I know! We'll give the clothes back," <<he>> produces some thin cables from <<his>> coat pocket "if you let us dress you first."
<br><br>

<<person1>> The <<person>> seems taken with the idea, "It's your choice. If you don't want to play along, we'll be keeping your clothes to do with as we please." <<He>> slowly starts tearing the fabric.
<br><br>

<</if>>

<<link [[Accept|Beach Abduction Bound]]>><</link>>
<br>
<<link [[Refuse|Beach Abduction Molestation]]>><<set $molestationstart to 1>><</link>>
<br>