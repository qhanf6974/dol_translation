<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>
<<He>> hands you a feather duster and sends you inside with a slap on your rear. "No slacking."
<br><br>
<<pass 5>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure)>>
	<<generate2>><<generate3>>You enter the living room, only to find it occupied by a <<person2>><<person>> and <<person3>><<personstop>>
	<<garousal>><<gtrauma>><<gstress>><<arousal 600>><<trauma 6>><<stress 3>>
	<br><br>
	"Don't mind them," <<he>> says. "They won't touch you."
	<br><br>
	<<if $leftarm is "bound" and $rightarm is "bound">>
		You nod in agreement and begin your work, made more difficult by the guest's loud comments. Not only that, but your bindings make it hard to reach specific places without bending over to show your <<genitals>> to the guests lazing on the chair.
		<br><br>
		"Don't mind us," the <<person3>><<person>> says as you bend over to dust the lower half of a cabinet. "Though, can you bend your knees a little less?"
		<br><br>
	<<else>>
		You nod and get to work, crouching when you can and covering your <<genitals>> with your free hand.
		"<<pShes>> shy," the <<person3>><<person>> says as you cover your rear while reaching up to brush a cabinet.
	<</if>>
	<br><br>
	You finish cleaning the living room, and leave the gaze of the guests and report to the <<person>> for payment. "Good job," <<He>> says, still leering at you. "Here's some stuff I found in a closet. Thanks for entertaining my guests."
	<br><br>
	<<pass 5>>
	<<link [[Get changed and leave (0:01)|Domus Street]]>><<spareclothesdomus>><<endevent>><<pass 1>><</link>>
	<br>
<<else>>
	Once the living room is cleaned, you report to the <<person>> for payment. "Good job," <<He>> says, still leering at you. "Here's some stuff I found in a closet."
	<br><br>
	<<pass 5>>
	<<link [[Get changed and leave (0:01)|Domus Street]]>><<spareclothesdomus>><<endevent>><<pass 1>><</link>>
	<br>
<</if>>