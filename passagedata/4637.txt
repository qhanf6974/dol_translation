<<effects>>
You're left in the middle of the street, <span class="lewd">on your knees with your <<bottom>> stuck in the air.</span> Worse, the noise attracted attention.
<<if $exposed gte 2>>
	<<fameexhibitionism 40>>
<<else>>
	<<fameexhibitionism 20>>
<</if>>
<br>
<<set $rng to random(1, 3)>>
<<if $rng is 3>>
	"What's that <<girl>> doing?"
<<elseif $rng is 2>>
	"I need to visit the market more often."
<<else>>
	"Nice."
<</if>>
<br>
<<set $rng to random(1, 3)>>
<<if $rng is 3>>
	"That's no way to dress in public."
<<elseif $rng is 2>>
	"That's a nice sight."
<<else>>
	"What a slut."
<</if>>
<br>
<<set $rng to random(1, 3)>>
<<if $rng is 3>>
	"I don't know what this town's coming to."
<<elseif $rng is 2>>
	"Someone call the police."
<<else>>
	"Is this for a film? Is there a camera crew around?"
<</if>>
<br><br>
You push yourself to your feet, but your legs are shaking so hard you fall once more.
<br><br>
Trying to ignore the numerous eyes running all over, you run the rest of the way, taking shelter around a corner down an alley.
<br><br>
You lean against a wall, your whole body shaking. So many people saw you.
<br><br>
<<link [[Next|Commercial alleyways]]>><</link>>
<br>